package Graphs.AdjacencyMatrix;

public class Solution {

    static class Graph {
        private boolean adjMatrix[][];
        int V;

        Graph(int _V) {
            this.V = _V;
            this.adjMatrix = new boolean[V][V];
        }

        private void addEdge(int i , int j) {
            if (isValid(i, j)) {
                adjMatrix[i][j] = true;
                adjMatrix[j][i] = true;
            }
        }

        private void removeEdge(int i, int j) {
            if (isValid(i, j)) {
                adjMatrix[i][j] = false;
                adjMatrix[j][i] = false;
            }
        }

        private boolean isEdge(int i, int j) {
            if (isValid(i, j)) {
                return adjMatrix[i][j];
            }
            return false;
        }

        private boolean isValid(int i, int j) {
            if (i >=0 && i < V && j >= 0 && j < V) {
                return true;
            }
            return false;
        }

        private void printGraph() {
            for (int i = 0 ; i < V; i++) {
                System.out.println(" Adjacency Matrix representation of Vertex : " + i);
                for (int j = 0; j < V; j++) {
                    if (adjMatrix[i][j]) {
                        System.out.println(" --> " + j);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(5);

        graph.addEdge( 0, 1);
        graph.addEdge( 0, 4);
        graph.addEdge( 1, 2);
        graph.addEdge( 1, 3);
        graph.addEdge( 1, 4);
        graph.addEdge( 2, 3);
        graph.addEdge( 3, 4);


        graph.printGraph();
    }
}
