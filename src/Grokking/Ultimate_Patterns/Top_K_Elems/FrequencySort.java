package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.*;

/**
 * Given a string, sort it based on the decreasing frequency of its characters.
 *
 * Input: "Programming"
 * Output: "rrggmmPiano"
 * Explanation: 'r', 'g', and 'm' appeared twice, so they need to appear before any other character.
 *
 * Input: "abcbab"
 * Output: "bbbaac"
 * Explanation: 'b' appeared three times, 'a' appeared twice, and 'c' appeared only once.
 *
 */
public class FrequencySort {

    public static String sortCharacterByFrequency(String str) {
        char[] arr = str.toCharArray();
        Map<Character, Integer> frequencyMap = new HashMap<>();

        for (int i = 0; i < arr.length; i++) {
            frequencyMap.put(arr[i], frequencyMap.getOrDefault(arr[i], 0) + 1);
        }

        Queue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>((o1, o2) -> o2.getValue() - o1.getValue());
        StringBuilder result = new StringBuilder();

        maxHeap.addAll(frequencyMap.entrySet());

        while (!maxHeap.isEmpty()) {
            Map.Entry<Character, Integer> entry = maxHeap.poll();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < entry.getValue(); i++) {
                stringBuilder.append(entry.getKey());
            }
            result.append(stringBuilder);
        }

        return result.toString();
    }

    public static void main(String[] args) {
        String result = FrequencySort.sortCharacterByFrequency("Programming");
        System.out.println("Here is the given string after sorting characters by frequency: " + result);

        result = FrequencySort.sortCharacterByFrequency("abcbab");
        System.out.println("Here is the given string after sorting characters by frequency: " + result);
    }
}
