package hackerrank.Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

public class Solution {


    static class Player {
        String name;
        int totalSum;

        public Player(String name, int totalSum) {
            this.name = name;
            this.totalSum = totalSum;
        }
    }

    public static void main(String[] args) {

        List<Player> players = new ArrayList<>();

        List<String> scores = new ArrayList<>();

        int k = 3;

        scores.add("Sachin,20,40,60,100,110,50");
        scores.add("Virat,10,21,23,48,24");
        scores.add("Dhoni,10,119,27,90,69,200");

        for (String score : scores) {
            String[] str = score.split(",");

            if (str.length >= k) {
                String name = str[0];
                int sum = 0;

                for (int i = 1; i < str.length; i++) {
                    sum += Integer.parseInt(str[i]);
                }

                Player player = new Player(name, sum);
                players.add(player);
            }
        }

        Collections.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                return p2.totalSum - p1.totalSum;
            }
        });

        List<Player> topScorers = new ArrayList<>();

        for (int i = 0; i < players.size(); i++) {
            if (i < 2) {
                topScorers.add(players.get(i));
            } else {
                break;
            }
        }

        Collections.sort(topScorers, new Comparator<Player>() {
            @Override
            public int compare(Player player, Player t1) {
                return player.name.compareTo(t1.name);
            }
        });

        List<String> result = new ArrayList<>();

        for (int i = 0; i < topScorers.size(); i++) {
            result.add(topScorers.get(i).name);
        }

        result.forEach(res -> System.out.println(res));
    }
}
