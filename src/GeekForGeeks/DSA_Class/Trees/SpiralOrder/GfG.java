package GeekForGeeks.DSA_Class.Trees.SpiralOrder;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.Stack;

public class GfG {

    void printSpiral(Node node)
    {
        // Your code here
        if (null == node) {
            return;
        }

        Stack<Node> stack1 = new Stack<>();
        Stack<Node> stack2 = new Stack<>();

        stack1.push(node);

        while (!stack1.isEmpty() || !stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                Node pop = stack1.pop();
                System.out.print(pop.data + " ");
                if (null != pop.right) {
                    stack2.push(pop.right);
                }
                if (null != pop.left) {
                    stack2.push(pop.left);
                }
            }

            while (!stack2.isEmpty()) {
                Node pop = stack2.pop();
                System.out.println(pop.data + " ");
                if (null != pop.left) {
                    stack1.push(pop.left);
                }
                if (null != pop.right) {
                    stack1.push(pop.right);
                }

            }
        }
    }
}
