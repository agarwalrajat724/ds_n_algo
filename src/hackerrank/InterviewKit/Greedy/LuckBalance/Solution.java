package hackerrank.InterviewKit.Greedy.LuckBalance;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    static class Node {
        int luck;
        int imp;

        public Node(int luck, int imp) {
            this.luck = luck;
            this.imp = imp;
        }
    }

    // Complete the luckBalance function below.
    static int luckBalance(int k, int[][] contests) {

        List<Integer> list = new ArrayList<>();

        int rem = 0;

        for (int i = 0; i < contests.length; i++) {
            if (contests[i][1] == 1) {
                list.add(contests[i][0]);
            } else {
                rem += contests[i][0];
            }
        }

        Collections.sort(list, Collections.reverseOrder());

        for (int i = 0; i < list.size(); i++) {
            if (i < k) {
                rem += list.get(i);
            } else {
                rem -= list.get(i);
            }
        }

        return rem;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        int[][] contests = new int[n][2];

        for (int i = 0; i < n; i++) {
            String[] contestsRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 2; j++) {
                int contestsItem = Integer.parseInt(contestsRowItems[j]);
                contests[i][j] = contestsItem;
            }
        }

        int result = luckBalance(k, contests);

        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
