package LeetCode.Easy.Others.HammingDistance;

public class Solution {

    public int hammingDistance(int x, int y) {
        int XOR = x ^ y;

        int noOfSetBits = 0;

        while (XOR > 0) {
            noOfSetBits += XOR & 1;
            XOR = XOR >> 1;
        }

        return noOfSetBits;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.hammingDistance(1, 4));
    }
}
