package Graphs.DepthFirstTraversal.recursive;

import Graphs.Graph;

import java.util.Iterator;

public class Solution {

    private boolean visited[];

    private void DFS(int n, Graph graph) {
        visited = new boolean[n];

        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                DFSUtils(i, visited, graph);
            }
        }
    }

    private void DFSUtils(int vertex, boolean[] visited, Graph graph) {

        visited[vertex] = true;
        System.out.println(vertex + " --> ");

        Iterator<Integer> integerIterator = graph.adjListArray[vertex].listIterator();

        while (integerIterator.hasNext()) {
            int v = integerIterator.next();
            if (!visited[v]) {
                DFSUtils(v, visited, graph);
            }
        }


    }

    public static void main(String[] args) {
        int n = 4;
        Graph graph = new Graph(n);

        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);

        graph.printGraph();

        Solution solution = new Solution();

        solution.DFS(n, graph);

    }
}
