package LeetCode.Medium.BinaryTree.KthSmallestElem;

public class OrderStatistics {

    static TreeNode root = null;

    public TreeNode insert_node(TreeNode root, int val) {
        if (null == root) {
            root = new TreeNode(val);
            return root;
        }

        if (val < root.val) {
            root.left = insert_node(root.left, val);
        } else if (val > root.val){
            root.right = insert_node(root.right, val);
        }
        return root;
    }


    public static void main(String[] args) {
        int[] arr = new int[]{20, 8, 22, 4, 12, 10, 14};
        OrderStatistics orderStatistics = new OrderStatistics();

    }
}
