package LeetCode.Easy.Sample;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *  451. Sort Characters By Frequency
 *
 * Given a string, sort it in decreasing order based on the frequency of characters.
 *
 * Example 1:
 *
 * Input:
 * "tree"
 *
 * Output:
 * "eert"
 *
 * Explanation:
 * 'e' appears twice while 'r' and 't' both appear once.
 * So 'e' must appear before both 'r' and 't'. Therefore "eetr" is also a valid answer.
 * Example 2:
 *
 * Input:
 * "cccaaa"
 *
 * Output:
 * "cccaaa"
 *
 * Explanation:
 * Both 'c' and 'a' appear three times, so "aaaccc" is also a valid answer.
 * Note that "cacaca" is incorrect, as the same characters must be together.
 * Example 3:
 *
 * Input:
 * "Aabb"
 *
 * Output:
 * "bbAa"
 *
 * Explanation:
 * "bbaA" is also a valid answer, but "Aabb" is incorrect.
 * Note that 'A' and 'a' are treated as two different characters.
 */
public class SortCharByFreq {

    class Node {
        char ch;
        int freq;

        public Node(char ch, int freq) {
            this.ch = ch;
            this.freq = freq;
        }
    }

    public String frequencySort(String s) {

        Queue<Map.Entry<Character, Integer>> queue = new PriorityQueue<>((node1, node2) ->
                node2.getValue() - node1.getValue());

        Map<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }

        queue.addAll(map.entrySet());

        StringBuilder stringBuilder = new StringBuilder();

        while (!queue.isEmpty()) {
            Map.Entry node = queue.poll();
            int n = (int) node.getValue();
            while (n > 0) {
                stringBuilder.append(node.getKey());
                n--;
            }
        }

        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        SortCharByFreq sortCharByFreq = new SortCharByFreq();
        System.out.println(sortCharByFreq.frequencySort("tree"));
    }
}
