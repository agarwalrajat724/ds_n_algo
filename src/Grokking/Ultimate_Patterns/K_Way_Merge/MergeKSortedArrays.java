package Grokking.Ultimate_Patterns.K_Way_Merge;


import java.util.*;

class Node {
    int arrayIndex;
    int elemIndex;

    public Node(int arrayIndex, int elemIndex) {
        this.arrayIndex = arrayIndex;
        this.elemIndex = elemIndex;
    }
}

public class MergeKSortedArrays {


    private static int[] mergeKSortedArrays(List<Integer[]> lists) {
        Queue<Node> minHeap = new PriorityQueue<>(lists.size(), (node1, node2) ->
                lists.get(node1.arrayIndex)[node1.elemIndex] - lists.get(node2.arrayIndex)[node2.elemIndex]);

//        minHeap = new PriorityQueue<>(k, Comparator.comparingInt(value -> lists.get(value.arrayIndex)[value.elemIndex]));
        int resultSize = 0;
        for (int i = 0; i < lists.size(); i++) {
            if (null != lists.get(i)) {
                minHeap.offer(new Node(i, 0));
                resultSize += lists.get(i).length;
            }
        }

        int result[] = new int[resultSize];
        int index = 0;

        while (!minHeap.isEmpty()) {
            Node node = minHeap.poll();
            result[index++] = lists.get(node.arrayIndex)[node.elemIndex];

            int elemIndex = node.elemIndex;
            elemIndex++;

            if (lists.get(node.arrayIndex).length > elemIndex) {
                minHeap.offer(new Node(node.arrayIndex, elemIndex));
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Integer[] l1 = new Integer[] { 2, 6, 8 , 9};
        Integer[] l2 = new Integer[] { 3, 6, 7 , 10, 10};
        Integer[] l3 = new Integer[] { 1, 3, 4 , 9, 9, 9, 9, 20};
        List<Integer[]> lists = new ArrayList<Integer[]>();
        lists.add(l1);
        lists.add(l2);
        lists.add(l3);
        int result[] = MergeKSortedArrays.mergeKSortedArrays(lists);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + "  ");
        }
        System.out.print("Kth smallest number is: " + result);
    }
}
