package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int distFromOrigin() {
        // ignoring sqrt
        return (x * x) + (y * y);
    }
}

/**
 * Given an array of points in the a 2D2D plane, find ‘K’ closest points to the origin.
 *
 * Input: points = [[1,2],[1,3]], K = 1
 * Output: [[1,2]]
 * Explanation: The Euclidean distance between (1, 2) and the origin is sqrt(5).
 * The Euclidean distance between (1, 3) and the origin is sqrt(10).
 * Since sqrt(5) < sqrt(10), therefore (1, 2) is closer to the origin.
 *
 * Input: point = [[1, 3], [3, 4], [2, -1]], K = 2
 * Output: [[1, 3], [2, -1]]
 */
public class KClosestPointsToOrigin {

    public static List<Point> findClosestPoints(Point[] points, int k) {
        ArrayList<Point> result = new ArrayList<>();

        Queue<Point> maxHeap = new PriorityQueue<>(k, (o1, o2) -> o2.distFromOrigin() - o1.distFromOrigin());

        for (int i = 0; i < k; i++) {
            maxHeap.offer(points[i]);
        }

        for (int i = k; i < points.length; i++) {
            if (maxHeap.peek().distFromOrigin() > points[i].distFromOrigin()) {
                maxHeap.poll();
                maxHeap.offer(points[i]);
            }
        }
        result = new ArrayList<>(maxHeap);
        return result;
    }

    public static void main(String[] args) {
        Point[] points = new Point[] { new Point(1, 3), new Point(3, 4), new Point(2, -1) };
        List<Point> result = KClosestPointsToOrigin.findClosestPoints(points, 2);
        System.out.print("Here are the k points closest the origin: ");
        for (Point p : result)
            System.out.print("[" + p.x + " , " + p.y + "] ");
    }
}
