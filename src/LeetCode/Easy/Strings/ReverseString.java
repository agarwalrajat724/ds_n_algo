package LeetCode.Easy.Strings;

public class ReverseString {


    public String reverseStringRecursive(String s) {

        if (s.length() < 2) {
            return s;
        }

        return reverseStringRecursive(s.substring(1,s.length() )) + s.charAt(0);
    }

    public String reverseString(String s) {

        if (null == s || s.length() < 1) {
            return s;
        }

        int i = 0;
        int j = s.length() - 1;

        char array[] = s.toCharArray();

        while (i < j) {
            char temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            i++;
            j--;
        }

        StringBuffer stringBuffer = new StringBuffer();
        for (char ch : array) {
            stringBuffer.append(ch);
        }

        return stringBuffer.toString();

    }


    public static void main(String[] args) {
        ReverseString reverseString = new ReverseString();

        System.out.println(reverseString.reverseString("hello"));
    }
}
