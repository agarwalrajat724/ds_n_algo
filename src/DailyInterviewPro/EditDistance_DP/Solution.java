package DailyInterviewPro.EditDistance_DP;

/**
 * Given two strings, determine the edit distance between them.
 * The edit distance is defined as the minimum number of edits (insertion,
 * deletion, or substitution) needed to change one string to the other.
 *
 * For example, "biting" and "sitting" have an edit distance of 2
 * (substitute b for s, and insert a t).
 */
public class Solution {

    public int distance(String s1, String s2) {
        return editDistance(s1, s2, s1.length(), s2.length());
    }

    public int distanceDP(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        int dp[][] = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {

                // If s1 is empty , insert all chars of s2 ie n
                if (i == 0) {
                    dp[i][j] = j;
                }
                // If s2 is empty, remove all characters of s2
                else if (j == 0) {
                    dp[i][j] = i;
                }

                else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    dp[i][j] = dp[i-1][j-1];
                } else {
                    dp[i][j] = 1 + min(dp[i][j-1], dp[i-1][j], dp[i-1][j-1]);
                }
            }
        }
        return dp[m][n];
    }

    /**
     * Here all the modifications will be made wrt to s1
     * @param s1
     * @param s2
     * @param m
     * @param n
     * @return
     */
    private int editDistance(String s1, String s2, int m, int n) {
        // If s1 is empty, then insert all chars of s2 or n
        if (m == 0) {
            return n;
        }

        // If s2 is empty, then remove all chars of s1 or m
        if (n == 0) {
            return m;
        }

        // If Last characters of 2 strings are same, get count for remaining characters
        if (s1.charAt(m - 1) == s2.charAt(n - 1)) {
            return editDistance(s1, s2, m - 1, n - 1);
        }

        // case 1)-> Insert at m
        // case 2)-> Remove char at m
        // case 3)-> Replace char at m with char at n
        return 1 + min(editDistance(s1, s2, m, n - 1),
                       editDistance(s1, s2, m - 1, n),
                       editDistance(s1, s2, m - 1, n - 1));
    }

    private int min(int x, int y, int z) {
        if (x <= y && x <= z) {
            return x;
        }
        if (y <= x && y <= z) {
            return y;
        }
        return z;
    }

    public static void main(String[] args) {

        Solution solution = new Solution();
        System.out.println(solution.distance("biting", "sitting"));
        System.out.println(solution.distanceDP("biting", "sitting"));
    }
}
