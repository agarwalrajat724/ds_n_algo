package LeetCode.Medium.BinaryTree.InorderTraversal;

public class TreeNode {
	
	int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }

}
