package hackerrank.ProblemSolving.Greedy.GridChallange;

import hackerrank.FileLocation;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the gridChallenge function below.
    static String gridChallenge(String[] grid) {

        int n = grid.length;

        char[][] arr = new char[n][grid[0].length()];

        for (int i = 0; i < n; i++) {
            char[] str = grid[i].toCharArray();
            Arrays.sort(str);
            arr[i] = str;
        }

        // i represents the column
        // j represents the row
        for (int i = 0; i < grid[0].length(); i++) {
            for (int j = 1; j < n; j++) {
                if (arr[j][i] < arr[j - 1][i]) {
                    return "NO";
                }
            }
        }

        return "YES";
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FileLocation.FILE_LOC));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            String[] grid = new String[n];

            for (int i = 0; i < n; i++) {
                String gridItem = scanner.nextLine();
                grid[i] = gridItem;
            }

            String result = gridChallenge(grid);

            System.out.println(result);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}