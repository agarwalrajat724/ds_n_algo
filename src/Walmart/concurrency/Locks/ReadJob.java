package Walmart.concurrency.Locks;

public class ReadJob implements Runnable {

    private final TestResource testResource;

    public ReadJob(TestResource testResource) {
        this.testResource = testResource;
    }

    @Override
    public void run() {
        System.out.println("Read Job");
        testResource.readRecord();
    }
}
