package LeetCode.Easy.Arrays.IntersectionArraysII;

import java.util.*;

/**
 * 350. Intersection of Two Arrays II
 *
 * Given two arrays, write a function to compute their intersection.
 *
 * Example 1:
 *
 * Input: nums1 = [1,2,2,1], nums2 = [2,2]
 * Output: [2,2]
 * Example 2:
 *
 * Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * Output: [4,9]
 * Note:
 *
 * Each element in the result should appear as many times as it shows in both arrays.
 * The result can be in any order.
 * Follow up:
 *
 * What if the given array is already sorted? How would you optimize your algorithm?
 * What if nums1's size is small compared to nums2's size? Which algorithm is better?
 * What if elements of nums2 are stored on disk, and the memory is limited such that you cannot
 * load all elements into the memory at once?
 */
public class Solution {

    public int[] intersect(int[] nums1, int[] nums2) {

        Map<Integer, Integer> map = null;
        List<Integer> result = null;

        if (null != nums1 && nums1.length > 0 && null != nums2 && nums2.length > 0) {
            map = new HashMap<>();
            result = new ArrayList<>();

            if (nums1.length < nums2.length) {
                int[] temp = nums1;
                nums1 = nums2;
                nums2 = temp;
            }

            for (Integer integer : nums1) {
                map.put(integer, map.getOrDefault(integer, 0) + 1);
            }

            for (Integer integer : nums2) {
                if (map.containsKey(integer)) {
                    int count = map.get(integer);
                    if (count > 1) {
                        map.put(integer, count - 1);
                    } else {
                        map.remove(integer);
                    }

                    result.add(integer);
                }
            }
        }

        if (null == result || result.isEmpty()) {
            return new int[]{};
        }
        int res[] = new int[result.size()];
        int i = 0;
        for (Integer integer : result) {
            res[i] = integer;
            i++;
        }
        return res;
    }


    public int[] intersectBetter(int[] nums1, int[] nums2) {

        if (null == nums1 || nums1.length == 0 || null == nums2 || nums2.length == 0) {
            return new int[]{};
        }

        List<Integer> result = new ArrayList<>();

        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int p1 = 0, p2 = 0;

        int l1 = nums1.length;
        int l2 = nums2.length;

        while (p1 < l1 && p2 < l2) {

            if (nums1[p1] < nums2[p2]) {
                p1++;
            } else if (nums1[p1] > nums2[p2]) {
                p2++;
            } else {
                result.add(nums1[p1]);
                p1++;
                p2++;
            }
        }

        int[] res = new int[result.size()];

        int i = 0;


        for (Integer integer : result) {
            res[i++] = integer;
        }
        return res;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        Arrays.stream(solution.intersect(new int[]{1,2}, new int[]{1,1})).forEach(i -> System.out.println(i));

        Arrays.stream(solution.intersectBetter(new int[]{1,2}, new int[]{1,1})).forEach(i -> System.out.println(i));

//        Arrays.stream(solution.intersect(new int[]{1,2,2,1}, new int[]{2,2})).forEach(i -> System.out.println(i));
//
//        System.out.println("sjds");
//
//        Arrays.stream(solution.intersect(new int[]{4,9,5}, new int[]{9,4,9,8,4})).forEach(i -> System.out.println(i));
//
//        System.out.println("dkjskdjsk");
//        Arrays.stream(solution.intersect(new int[]{1,2,2,1}, new int[]{2,2})).forEach(i -> System.out.println(i));
//
//        System.out.println("sjds");
//
//        Arrays.stream(solution.intersect(new int[]{4,9,5}, new int[]{9,4,9,8,4})).forEach(i -> System.out.println(i));
    }
}
