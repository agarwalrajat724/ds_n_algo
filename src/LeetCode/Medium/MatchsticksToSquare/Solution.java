package LeetCode.Medium.MatchsticksToSquare;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 473. Matchsticks to Square
 * <p>
 * Remember the story of Little Match Girl? By now, you know exactly what matchsticks the little match girl
 * has, please find out a way you can make one square by using up all those matchsticks.
 * You should not break any stick, but you can link them up, and each matchstick must be
 * used exactly one time.
 * <p>
 * Your input will be several matchsticks the girl has, represented with their stick length.
 * Your output will either be true or false, to represent whether you could make one square
 * using all the matchsticks the little match girl has.
 * <p>
 * Example 1:
 * Input: [1,1,2,2,2]
 * Output: true
 * <p>
 * Explanation: You can form a square with length 2, one side of the square came two sticks with length 1.
 * Example 2:
 * Input: [3,3,3,3,4]
 * Output: false
 * <p>
 * Explanation: You cannot find a way to form a square with all the matchsticks.
 * Note:
 * The length sum of the given matchsticks is in the range of 0 to 10^9.
 * The length of the given matchstick array will not exceed 15.
 */
public class Solution {

    private List<Integer> nums;

    private int possibleSum;

    private int sum[];

    Solution() {
        sum = new int[4];
        for (int i = 0; i < 4; i++) {
            sum[i] = 0;
        }
    }

    /**
     * Depth First Search based GFG
     *
     * @param nums
     * @return
     */
    public boolean makesquare(int[] nums) {

        if (null == nums || nums.length < 3) {
            return false;
        }
        int n = nums.length;
        int perimeter = 0;
        for (int i = 0; i < n; i++) {
            perimeter += nums[i];
        }
        this.possibleSum = perimeter / 4;

        if (this.possibleSum * 4 != perimeter) {
            return false;
        }

        this.nums = Arrays.stream(nums).boxed().collect(Collectors.toList());
        Collections.sort(this.nums, Collections.reverseOrder());

        return dfs(0);
    }

    private boolean dfs(int index) {

        if (index == nums.size()) {
            return (sum[0] == sum[1] && sum[1] == sum[2] && sum[2] == sum[3]);
        }
        int elem = nums.get(index);
        for (int i = 0; i < 4; i++) {
            if ((sum[i] + elem) <= this.possibleSum) {
                sum[i] += elem;

                if (dfs(index + 1)) {
                    return true;
                }

                sum[i] -= elem;
            }
        }
        return false;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        //System.out.println(solution.makesquare(new int[]{1, 1, 2, 2, 2}));
        //System.out.println(solution.makesquare(new int[]{3, 3, 3, 3, 4}));
        System.out.println(solution.makesquare(new int[]{}));
    }
}
