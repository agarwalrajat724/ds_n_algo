package LeetCode.Easy.Others.MissingNumber;

/**
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one
 * that is missing from the array.
 *
 * Example 1:
 *
 * Input: [3,0,1]
 * Output: 2
 * Example 2:
 *
 * Input: [9,6,4,2,3,5,7,0,1]
 * Output: 8
 * Note:
 * Your algorithm should run in linear runtime complexity. Could you implement it using
 * only constant extra space complexity?
 */
public class Solution {

    public int missingNumber(int[] nums) {
        if (null == nums) {
            return -1;
        }

        int XOR_1 = nums[0];
        for (int i = 1; i < nums.length; i++) {
            XOR_1 = XOR_1 ^ nums[i];
        }

        int limit = nums.length;
        int XOR_2 = 0;
        for (int i = 1; i <= nums.length; i++) {
            XOR_2 = XOR_2 ^ i;
        }

        return XOR_1 ^ XOR_2;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.missingNumber(new int[]{3,0,1}));
        System.out.println(solution.missingNumber(new int[]{9,6,4,2,3,5,7,0,1}));
    }
}
