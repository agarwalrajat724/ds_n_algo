package LeetCode.Easy.BinaryTree.ValidateBST;

/**
 * Validate Binary Search Tree
 *   Go to Discuss
 * Given a binary tree, determine if it is a valid binary search tree (BST).
 *
 * Assume a BST is defined as follows:
 *
 * The left subtree of a node contains only nodes with keys less than the node's key.
 * The right subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 *
 *
 * Example 1:
 *
 *     2
 *    / \
 *   1   3
 *
 * Input: [2,1,3]
 * Output: true
 * Example 2:
 *
 *     5
 *    / \
 *   1   4
 *      / \
 *     3   6
 *
 * Input: [5,1,4,null,null,3,6]
 * Output: false
 * Explanation: The root node's value is 5 but its right child's value is 4.
 */
public class Solution {

    private TreeNode prev = null;

    public boolean isValidBST(TreeNode root) {
        if (null == root) {
            return true;
        }
        return isValidBSTUtil(root, null, null);
    }

    private boolean isValidBSTUtil(TreeNode root, TreeNode left, TreeNode right) {
        if (null == root) {
            return true;
        }
        if (null != left && left.val >= root.val) {
            return false;
        }
        if (null != right && right.val <= root.val) {
            return false;
        }

        return isValidBSTUtil(root.left, left, root) && isValidBSTUtil(root.right, root, right);
    }

    public boolean isValidBSTUsingInorder(TreeNode root) {
        if (null != root) {
            if (!isValidBSTUsingInorder(root.left)) {
                return false;
            }

            if (null != prev && prev.val >= root.val) {
                return false;
            }
            prev = root;

            if (!isValidBSTUsingInorder(root.right)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(1);
        root.right = new TreeNode(3);
        System.out.println(solution.isValidBST(root));
        System.out.println(solution.isValidBSTUsingInorder(root));

        TreeNode root2 = new TreeNode(5);
        root2.left = new TreeNode(1);
        root2.right = new TreeNode(4);
        root2.right.left = new TreeNode(3);
        root2.right.right = new TreeNode(6);
        System.out.println(solution.isValidBST(root2));
        System.out.println(solution.isValidBSTUsingInorder(root2));
    }
}
