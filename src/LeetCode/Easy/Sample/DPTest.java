package LeetCode.Easy.Sample;

public class DPTest {


     int[] arr;

    public int dp(int n) {
        if (n == 0 || n == 1) {
            return 2;
        }

        arr = new int[n + 1];

        return dpUtil(n);
    }

    public int dpUtil(int n) {
        if (n == 0 || n == 1) {
            return 2;
        }

        if (arr[n] != 0) {
            return arr[n];
        }

        for (int i = 1; i <= n; i++) {
            arr[i] += 2 * dpUtil(i) * dpUtil(i - 1);
        }

        return arr[n];
    }

    public static void main(String[] args) {
        DPTest dpTest = new DPTest();

        System.out.println(dpTest.dp(2));
    }
}
