package Walmart.FractionInString;

/**
 * Given two integers representing the Numerator and Denominator of a fraction, return
 * the fraction in string format. If the fractional part is repeating, enclose the
 * repeating part in parentheses.
 *
 * Input: Numerator = 1, Denominator = 2
 * Output: "0.5"
 * 1/2 = 0.5 with no repeating part.
 *
 * Input: Numerator = 50, Denominator = 22
 * Output: "2.(27)"
 * 50/22 = 2.27272727... Since fractional part (27)
 * is repeating, it is enclosed in parentheses.
 *
 */
public class Solution {

    public static void main(String[] args) {

        int num = 1, denom = 2;

        int sign = (num < 0) ^ (denom < 0) ? -1 : 1;

        num = Math.abs(num);
        denom = Math.abs(denom);

        String res = "";

        int initial = num/denom;

        if (sign == -1) {
            res += "-" + String.valueOf(initial);
        }



    }
}
