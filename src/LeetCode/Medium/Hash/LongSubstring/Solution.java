package LeetCode.Medium.Hash.LongSubstring;

import java.util.Arrays;
import java.util.Map;

/**
 * 3. Longest Substring Without Repeating Characters
 */
public class Solution {


    public int lengthOfLongestSubstring(String s) {

        int curr_len = 1;
        int max_len = 1;

        int visited[] = new int[26];

        Arrays.fill(visited, -1);

        char[] arr = s.toCharArray();

        visited[arr[0] - 'a'] = 0;

        for (int i = 1; i < arr.length; i++) {

            int index = arr[i] - 'a';

            int prev_index = visited[index];

            if (index == -1 || (i - curr_len) > prev_index) {
                curr_len++;
            } else {
                if (curr_len > max_len) {
                    max_len = curr_len;
                }

                curr_len = i - prev_index;
            }

            visited[index] = i;
        }

        if (curr_len > max_len) {
            max_len = curr_len;
        }

        return max_len;

    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.lengthOfLongestSubstring("abcabcbb"));
    }
}
