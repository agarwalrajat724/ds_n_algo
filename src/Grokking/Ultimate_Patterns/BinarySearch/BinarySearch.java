package Grokking.Ultimate_Patterns.BinarySearch;

/**
 * Given a sorted array of numbers, find if a given number ‘key’ is present in the array.
 * Though we know that the array is sorted, we don’t know if it’s sorted in ascending or
 * descending order. You should assume that the array can have duplicates.
 * <p>
 * Write a function to return the index of the ‘key’ if it is present in the array, otherwise return -1.
 * <p>
 * Input: [4, 6, 10], key = 10
 * Output: 2
 * <p>
 * Input: [1, 2, 3, 4, 5, 6, 7], key = 5
 * Output: 4
 * <p>
 * Input: [10, 6, 4], key = 10
 * Output: 0
 * <p>
 * Input: [10, 6, 4], key = 4
 * Output: 2
 */
public class BinarySearch {

    public static int search(int[] arr, int key) {
        int low = 0;
        int high = arr.length - 1;

        boolean isAscending = arr[low] < arr[high];

        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (arr[mid] == key) {
                return mid;
            } else {
                if (isAscending) {
                    if (arr[mid] > key) {
                        high = mid - 1;
                    } else {
                        low = mid + 1;
                    }
                } else {
                    if (arr[mid] > key) {
                        low = mid + 1;
                    } else {
                        high = mid - 1;
                    }
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(BinarySearch.search(new int[]{4, 6, 10}, 10));
        System.out.println(BinarySearch.search(new int[]{1, 2, 3, 4, 5, 6, 7}, 5));
        System.out.println(BinarySearch.search(new int[]{10, 6, 4}, 10));
        System.out.println(BinarySearch.search(new int[]{10, 6, 4}, 4));
    }
}
