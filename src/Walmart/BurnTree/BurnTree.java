package Walmart.BurnTree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Node {
    int data;
    Node left, right;

    public Node(int data) {
        this.data = data;
    }
}
public class BurnTree {

    public static void main(String[] args) {
        Node root = new Node(12);
        root.left = new Node(13);
        root.right = new Node(10);

        root.right.left = new Node(14);
        root.right.right = new Node(15);

        root.right.left.left = new Node(21);
        root.right.left.right = new Node(22);

        root.right.right.left = new Node(23);
        root.right.right.right = new Node(24);

        burnTree(root, 14);
    }

    private static void burnTree(Node root, int targetNode) {
        Queue<Node> queue = new LinkedList<>();
        List<List<Integer>> result = new ArrayList<>();
        burnTreeUtil(root, targetNode, queue, result);

        while (!queue.isEmpty()) {
            Node pop = queue.poll();

            System.out.println(pop.data);

        }
    }

    private static int burnTreeUtil(Node root, int targetNode, Queue<Node> queue, List<List<Integer>> result) {
        if (null == root) {
            return 0;
        }
        if (root.data == targetNode) {
            List<Integer> curr = new ArrayList<>();
            curr.add(root.data);

            System.out.println(root.data);

            if (null != root.left) {
                queue.offer(root.left);
            }

            if (null != root.right) {
                queue.offer(root.right);
            }
            result.add(curr);
            return 1;
        }

        int a = burnTreeUtil(root.left, targetNode, queue, result);

        if (a == 1) {
            List<Integer> res = new ArrayList<>();
            while (!queue.isEmpty()) {
                Node pop = queue.poll();
                res.add(pop.data);

                System.out.println(pop.data + " , ");

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }

            if (null != root.right) {
                queue.offer(root.right);
            }

            System.out.println(root.data);

            return 1;
        }

        int b = burnTreeUtil(root.right, targetNode, queue, result);

        if (b == 1) {

            while (!queue.isEmpty()) {
                Node pop = queue.poll();

                System.out.println(pop.data + " , ");

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }

            if (null != root.left) {
                queue.offer(root.left);
            }

            System.out.println(root.data + " , ");
            return 1;
        }
        return 0;
    }
}
