package LeetCode.Easy.Arrays.MergeSortedArray;

import java.util.Arrays;

public class Solution {

    public void merge(int[] nums1, int m, int[] nums2, int n) {

        int k = 0;
        int L[] = new int[m];

        for (int i = 0; i < m; i++) {
            L[i] = nums1[i];
        }

        int l1 = 0, r1 = 0;

        while (l1 < m && r1 < n) {
            if (L[l1] < nums2[r1]) {
                nums1[k] = L[l1];
                l1++;
            } else {
                nums1[k] = nums2[r1];
                r1++;
            }
            k++;
        }

        while (l1 < m) {
            nums1[k] = L[l1];
            l1++;
            k++;
        }

        while (r1 < n) {
            nums1[k] = nums2[r1];
            r1++;
            k++;
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] nums1 = new int[]{1,2,3,0,0,0};

        solution.merge(nums1,3, new int[]{2,5,6}, 3);


        Arrays.stream(nums1).forEach(i -> System.out.println(i));
    }
}
