package GeekForGeeks.DSA_Class.Trees.MaxWidth;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.LinkedList;
import java.util.Queue;

public class GfG {

    // /* Function to get the maximum width of a binary tree*/
    int getMaxWidth(Node root)
    {
        // Your code here
        if (null == root) {
            return 0;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);

        int width = Integer.MIN_VALUE;

        while (!queue.isEmpty()) {
            int size = queue.size();
            width = Math.max(size, width);
            while (size > 0) {
                Node pop = queue.poll();

                if (null != pop.right) {
                    queue.offer(pop.right);
                }

                if (null != pop.left) {
                    queue.offer(pop.left);
                }
                size--;
            }
        }
        return width;
    }


}
