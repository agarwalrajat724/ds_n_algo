package Grokking.Ultimate_Patterns.K_Way_Merge;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

class Pair {
    int a;
    int b;

    public Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }
}

public class LargestPairs {

    public static List<int[]> findKLargestPairs(int[] nums1, int[] nums2, int k) {
        List<int[]> result = new ArrayList<>();

        Queue<Pair> minHeap = new PriorityQueue<>(k, (pair1, pair2) -> (pair1.a + pair1.b) - (pair2.a + pair2.b));

        for (int i = 0; i < nums1.length ; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if (minHeap.size() < k) {
                    minHeap.offer(new Pair(nums1[i], nums2[j]));
                } else {
                    if ((nums1[i] + nums2[j]) < (minHeap.peek().a + minHeap.peek().b)) {
                        break;
                    } else {
                        minHeap.poll();
                        minHeap.offer(new Pair(nums1[i], nums2[j]));
                    }
                }
            }
        }
        while (!minHeap.isEmpty()) {
            Pair poll = minHeap.poll();
            result.add(new int[]{poll.a, poll.b});
        }
        return result;
    }

    public static void main(String[] args) {
        int[] l1 = new int[] { 9, 8, 2 };
        int[] l2 = new int[] { 6, 3, 1 };
        List<int[]> result = LargestPairs.findKLargestPairs(l1, l2, 3);
        System.out.print("Pairs with largest sum are: ");
        for (int[] pair : result)
            System.out.print("[" + pair[0] + ", " + pair[1] + "] ");
    }
}
