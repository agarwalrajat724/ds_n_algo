package GeekForGeeks.DSA_Class.Trees.Preorder;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.Stack;

public class GfG {

    void preorder(Node root)
    {
        // Your code goes here
        if(null == root) {
            return;
        }

        Stack<Node> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node node = stack.pop();
            System.out.print(node.data + " ");
            if (null != node.right) {
                stack.push(node.right);
            }

            if (null != node.left) {
                stack.push(node.left);
            }
        }
    }
}
