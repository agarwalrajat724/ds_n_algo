package Grokking.Ultimate_Patterns.DFS;

import java.util.ArrayList;
import java.util.List;

class MaxSum {
    int sum;
    int currentSum;
    public MaxSum(int sum) {
        this.sum = sum;
    }
}

public class RootToLeafMaxSum {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(5);
        MaxSum maxSum = new MaxSum(Integer.MIN_VALUE);
        List<Integer> result = RootToLeafMaxSum.findPathWithMaxSum(root, maxSum);
        System.out.println("Tree paths with sum " + maxSum.sum + ": " + result);
    }

    private static List<Integer> findPathWithMaxSum(TreeNode root, MaxSum maxSum) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> maxSumPath = new ArrayList<>();
        List<Integer> currentPath = new ArrayList<>();
        int currentSum = 0;
        findPathWithMaxSumUtil(root, maxSum, currentSum, maxSumPath, currentPath);
        return maxSumPath;
    }

    private static void findPathWithMaxSumUtil(TreeNode current, MaxSum maxSum, int currentSum, List<Integer> sumPath,
                                               List<Integer> currentPath) {
        if (null == current) {
            return;
        }
        currentSum += current.val;
        currentPath.add(current.val);
        if (null == current.left && null == current.right) {
            if (currentSum > maxSum.sum) {
                maxSum.sum = currentSum;
                sumPath.clear();
                for (int i : currentPath) {
                    sumPath.add(i);
                }
            }
        } else {
            findPathWithMaxSumUtil(current.left, maxSum, currentSum, sumPath, currentPath);
            findPathWithMaxSumUtil(current.right, maxSum, currentSum, sumPath, currentPath);
        }

        currentSum -= current.val;
        currentPath.remove(currentPath.size() - 1);
    }
}
