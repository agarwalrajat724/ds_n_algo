package LeetCode.Array.ExamRoom;

import java.util.Arrays;

/**
 * 855. Exam Room
 * Medium
 *
 * 352
 *
 * 163
 *
 * Favorite
 *
 * Share
 * In an exam room, there are N seats in a single row, numbered 0, 1, 2, ..., N-1.
 *
 * When a student enters the room, they must sit in the seat that maximizes the distance to the closest person.
 * If there are multiple such seats, they sit in the seat with the lowest number.  (Also, if no one is in the room,
 * then the student sits at seat number 0.)
 *
 * Return a class ExamRoom(int N) that exposes two functions: ExamRoom.seat() returning an int representing what
 * seat the student sat in, and ExamRoom.leave(int p) representing that the student in seat number p now leaves
 * the room.  It is guaranteed that any calls to ExamRoom.leave(p) have a student sitting in seat p.
 *
 *
 *
 * Example 1:
 *
 * Input: ["ExamRoom","seat","seat","seat","seat","leave","seat"], [[10],[],[],[],[],[4],[]]
 * Output: [null,0,9,4,2,null,5]
 * Explanation:
 * ExamRoom(10) -> null
 * seat() -> 0, no one is in the room, then the student sits at seat number 0.
 * seat() -> 9, the student sits at the last seat number 9.
 * seat() -> 4, the student sits at the last seat number 4.
 * seat() -> 2, the student sits at the last seat number 2.
 * leave(4) -> null
 * seat() -> 5, the student sits at the last seat number 5.
 * ​​​​​​​
 *
 * Note:
 *
 * 1 <= N <= 10^9
 * ExamRoom.seat() and ExamRoom.leave() will be called at most 10^4 times across all test cases.
 * Calls to ExamRoom.leave(p) are guaranteed to have a student currently sitting in seat number p.
 */
public class ExamRoom {

    int[] seats = null;
    int noOfSeatsFilled = 0;

    public ExamRoom(int N) {
        seats = new int[10];
        Arrays.fill(seats, 0);
    }

    public int seat() {
        if (noOfSeatsFilled == 0) {
            noOfSeatsFilled++;
            seats[0] = 1;
            return 0;
        }
        int maxDistance = Integer.MIN_VALUE;
        int minIndex = Integer.MAX_VALUE;
        int N = seats.length;
        int K = 0;

        for (int i = 0; i < N; i++) {
            if (seats[i] == 1) {
                K = 0;
            } else {
                K++;
                int dist = (K+1)/2;
                if (dist >= maxDistance) {
                    if (dist == maxDistance) {
                        minIndex = Math.min(minIndex, i);
                    } else {
                        minIndex = i;
                    }
                    maxDistance = dist;
                }
            }
        }

        for (int i = 0; i < N; i++) {
            if (seats[i] == 1 && i >= maxDistance) {
                if (i == maxDistance) {
                    minIndex = Math.min(minIndex, i);
                } else {
                    minIndex = i;
                }
                maxDistance = i;
                break;
            }
        }

        for (int i = N - 1; i >= 0; i--) {
            if (seats[i] == 1 && i >= maxDistance) {
                if (i == maxDistance) {
                    minIndex = Math.min(minIndex, i);
                } else {
                    minIndex = i;
                }
                maxDistance = i;
                break;
            }
        }
        seats[minIndex] = 1;
        noOfSeatsFilled++;
        return minIndex;
    }

    public void leave(int p) {
        seats[p] = 0;
        noOfSeatsFilled--;
    }

    public static void main(String[] args) {
        ExamRoom examRoom = new ExamRoom(10);
        System.out.println(examRoom.seat());
        System.out.println(examRoom.seat());
        System.out.println(examRoom.seat());
        System.out.println(examRoom.seat());
        examRoom.leave(4);
        System.out.println(examRoom.seat());
    }
}
