package GeekForGeeks.DSA_Class.Arrays.InsertAtEnd;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        while (testCases-- > 0) {
            int size = scanner.nextInt();
            int arr[] = new int[size];

            for (int i = 0; i < size - 1; i++) {
                arr[i] = scanner.nextInt();
            }

            int element = scanner.nextInt();

            Gfg gfg = new Gfg();
            gfg.insertAtEnd(arr, size, element);

            for (int i : arr) {
                System.out.println(i);
            }
        }
    }
}
