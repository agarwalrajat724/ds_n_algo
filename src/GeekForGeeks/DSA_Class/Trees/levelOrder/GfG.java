package GeekForGeeks.DSA_Class.Trees.levelOrder;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GfG {

    void levelOrder(Node node)
    {
        // Your code here
        if (null == node) {
            return;
        }

        Queue<Node> queue = new LinkedList<>();

        queue.offer(node);

        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                Node pop = queue.poll();
                System.out.print(pop.data + " ");

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }
            System.out.print("$" + " ");
        }
    }

    public static void main(String[] args) {
        Node root = new Node(1);

        root.left = new Node(2);
        root.right = new Node(3);

        root.left.left = new Node(4);
        root.left.right = new Node(5);

        root.left.left.right = new Node(8);

        root.right.left = new Node(6);
        root.right.right = new Node(7);

        GfG levelOrder = new GfG();
        levelOrder.levelOrder(root);
    }
}
