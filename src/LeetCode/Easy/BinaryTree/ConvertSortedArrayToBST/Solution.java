package LeetCode.Easy.BinaryTree.ConvertSortedArrayToBST;

public class Solution {


    public TreeNode sortedArrayToBST(int[] nums) {

        if (null == nums) {
            return null;
        }

        if (nums.length == 1) {
            return new TreeNode(nums[0]);
        }

        return sortedArrayToBST(nums, 0, nums.length - 1, nums.length);
    }

    private TreeNode sortedArrayToBST(int[] nums, int start, int end, int length) {
        if (start > end) {
            return null;
        }

        int mid = start + (end - start)/2;

        TreeNode node = new TreeNode(nums[mid]);

        node.left = sortedArrayToBST(nums, start, mid - 1, length);
        node.right = sortedArrayToBST(nums, mid + 1, end, length);
        return node;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        TreeNode res = solution.sortedArrayToBST(new int[]{-10,-3,0,5,9});
        System.out.println(res);
    }
}
