package Walmart.Design.First;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

class DoublyNode {

}

class Node {
    int data;
    int minHeapIndex;
    int maxHeapIndex;
    Node prev, next;
}

class HeapNode {
    int size;
    int capacity;
    Node node;
}

public class Solution {

    private Queue<HeapNode> minHeap = new PriorityQueue<>((o1, o2) -> o1.node.data - o2.node.data);
    private Queue<HeapNode> maxHeap = new PriorityQueue<>((node1, node2) -> node2.node.data - node1.node.data);
    private List<Node> list = new LinkedList<>();
}
