package GeekForGeeks.MustDo.Trees.LCABST;

public class Solution {


    static class Node {
        int data;
        Node left;
        Node right;

        public Node(int data) {
            this.data = data;
        }
    }

    private static Node insert(Node root, int data) {
        if (null == root) {
            return new Node(data);
        } else {
            Node cur = null;

            if (data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    private static Node lca(Node root, int l, int r) {
        if (l <= root.data && root.data <= r) {
            return root;
        }

        if (root.data < l) {
            return lca(root.right, l, r);
        }

        if (root.data > r) {
            return lca(root.left, l, r);
        }

        return null;
    }
}
