package GeekForGeeks.DSA_Class.Trees.ChildrenSumParent;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.LinkedList;
import java.util.Queue;

public class GfG {

    public static int isSumProperty(Node node) {
        // add your code here
        if (null == node) {
            return 1;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);

        while (!queue.isEmpty()) {
            Node root = queue.poll();
            int sum = root.data;
            if (null != root.left) {
                sum = sum - root.left.data;
                queue.offer(root.left);
            }

            if (null != root.right) {
                sum = sum - root.right.data;
                queue.offer(root.right);
            }
            if (sum != 0 && (null != root.left || null != root.right)) {
                return 0;
            }
        }
        return 1;
    }


    public static void main(String[] args) {
        Node root = new Node(10);

        root.left = new Node(8);
        root.right = new Node(2);

        root.left.left = new Node(3);
        root.left.right = new Node(5);

        root.right.left = new Node(2);

        System.out.println(isSumProperty(root));
    }
}
