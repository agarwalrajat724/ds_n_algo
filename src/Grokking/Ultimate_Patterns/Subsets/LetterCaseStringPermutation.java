package Grokking.Ultimate_Patterns.Subsets;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * String Permutations by changing case (medium)
 *
 * Given a string, find all of its permutations preserving the character sequence but changing case.
 *
 * Input: "ad52"
 * Output: "ad52", "Ad52", "aD52", "AD52"
 *
 * Input: "ab7c"
 * Output: "ab7c", "Ab7c", "aB7c", "AB7c", "ab7C", "Ab7C", "aB7C", "AB7C"
 *
 */
public class LetterCaseStringPermutation {

    public static List<String> findLetterCaseStringPermutations(String str) {
        List<String> permutations = new ArrayList<>();

        permutations.add(str);

        char[] input = str.toCharArray();

        for (int i = 0; i < input.length; i++) {
            if (Character.isDigit(input[i])) {
                if (Character.isUpperCase(input[i])) {
//                    input[i] =
                }
            }
        }


        return permutations;
    }

//    private static void findLetterCaseStringPermutationsRecursive(String str, int index, String currentString, List<String> permutations) {
//        if (index == str.length()) {
//            permutations.add(currentString);
//        } else {
//            for (int i = 0; i <= currentString.length(); i++) {
//                String newString = currentString;
//                newString.
//            }
//        }
//    }

    public static void main(String[] args) {
        List<String> result = LetterCaseStringPermutation.findLetterCaseStringPermutations("ad52");
        System.out.println(" String permutations are: " + result);

        result = LetterCaseStringPermutation.findLetterCaseStringPermutations("ab7c");
        System.out.println(" String permutations are: " + result);
    }
}
