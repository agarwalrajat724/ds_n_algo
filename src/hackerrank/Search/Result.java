package hackerrank.Search;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {



        /*
         * Complete the 'findTopScorers' function below.
         *
         * The function is expected to return a STRING_ARRAY.
         * The function accepts following parameters:
         *  1. INTEGER totalNoPlayers
         *  2. INTEGER totalMatches
         *  3. List<String> scores
         *  4. INTEGER n Minium no of matches for which a player should have been a top scorer
         */

        static class Player {
            String name;
            List<Integer> scores;


            public Player(String name, List<Integer> scores) {
                this.name = name;
                this.scores = scores;

            }

            public Player() {
            }
        }

        public static List<String> findTopScorers(int totalNoPlayers, int totalMatches, List<String> scores, int n) {

            if(null == scores || scores.isEmpty()) {
                return new ArrayList();
            }

            List<Player> players = new ArrayList<>();

            for (int i = 0; i < totalNoPlayers; i++) {
                String[] arr = scores.get(i).split(",");
                List<Integer> scoreList = new ArrayList<>();
                for (int j = 1; j < arr.length; j++) {
                    int elem = Integer.parseInt(arr[j]);
                    scoreList.add(elem);
                }
                Collections.sort(scoreList, Collections.reverseOrder());
                Player player = new Player(arr[0], scoreList);
                players.add(player);
            }

            Collections.sort(players, new Comparator<Player>() {
                @Override
                public int compare(Player o1, Player o2) {
                    int sum1 = 0, sum2 = 0;

                    if (o1.scores.size() >= n) {
                        for (int i = 0; i < o1.scores.size(); i++) {
                            if (i < n) {
                                sum1 += o1.scores.get(i);
                            } else {
                                break;
                            }
                        }
                    }

                    if (o2.scores.size() >= n) {
                        for (int i = 0; i < o2.scores.size(); i++) {
                            if (i < n) {
                                sum2 += o2.scores.get(i);
                            } else {
                                break;
                            }
                        }
                    }

                    if (sum1 < sum2) {
                        return 1;
                    } else if (sum1 > sum2) {
                        return -1;
                    } else {
                        return o1.name.compareTo(o2.name);
                    }
                }
            });

            List<String> result = new ArrayList<>();

            List<Player> resultList = new ArrayList<>();

            for (int i = 0; i < players.size(); i++) {
                if (i < 2) {
                    resultList.add(players.get(i));
                } else {
                    break;
                }
            }

            Collections.sort(resultList, new Comparator<Player>() {
                @Override
                public int compare(Player o1, Player o2) {
                    return o1.name.compareTo(o2.name);
                }
            });

            for (int i = 0; i < resultList.size(); i++) {
                result.add(resultList.get(i).name);
            }

            return result;
        }






    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/rajatagarwal/Learning/IdeaProjects/ds_n_algo/src/hackerrank/FileWriter.txt"));

        int totalNoPlayers = Integer.parseInt(bufferedReader.readLine().trim());

        int totalMatches = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> scores = IntStream.range(0, totalNoPlayers).mapToObj(i -> {
            try {
                return bufferedReader.readLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
                .collect(toList());

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<String> result = Result.findTopScorers(totalNoPlayers, totalMatches, scores, n);

        result.forEach(res -> System.out.println(res));

        bufferedWriter.write(
                result.stream()
                        .collect(joining("\n"))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }

}

