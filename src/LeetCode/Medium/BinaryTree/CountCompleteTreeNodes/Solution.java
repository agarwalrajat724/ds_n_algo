package LeetCode.Medium.BinaryTree.CountCompleteTreeNodes;

import LeetCode.Medium.BinaryTree.TreeNode;

/**
 * 222. Count Complete Tree Nodes
 * <p>
 * Given a complete binary tree, count the number of nodes.
 * <p>
 * Note:
 * <p>
 * Definition of a complete binary tree from Wikipedia:
 * In a complete binary tree every level, except possibly the last, is completely filled, and all nodes in the last level
 * are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.
 * <p>
 * Example:
 * <p>
 * Input:
 * 1
 * / \
 * 2   3
 * / \  /
 * 4  5 6
 * <p>
 * Output: 6
 */
public class Solution {

    public int countNodes(TreeNode root) {

        if (null == root) {
            return 0;
        }

        int lHeight = countLeftNodes(root);
        int rHeight = countRightNodes(root);

        if (lHeight == rHeight) {
            return (2 << (lHeight - 1)) -1;
        }

        return countLeftNodes(root.left) + countRightNodes(root.right) + 1;

    }

    private int countRightNodes(TreeNode treeNode) {
        if (null == treeNode) {
            return 0;
        }

        int height = 1;
        while (treeNode.right != null) {
            treeNode = treeNode.right;
            height++;
        }

        return height;
    }

    private int countLeftNodes(TreeNode treeNode) {
        if (treeNode == null) {
            return 0;
        }

        int height = 1;
        while (treeNode.left != null) {
            treeNode = treeNode.left;
            height++;
        }
        return height;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);

        root.right.left = new TreeNode(6);


        System.out.println(solution.countNodes(root));
    }

}
