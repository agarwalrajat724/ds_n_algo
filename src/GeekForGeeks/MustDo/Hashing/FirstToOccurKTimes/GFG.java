package GeekForGeeks.MustDo.Hashing.FirstToOccurKTimes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class GFG {


    static class Node {
        int elem;
        int freq;

        public Node(int elem, int freq) {
            this.elem = elem;
            this.freq = freq;
        }

        public int getElem() {
            return elem;
        }

        public void setElem(int elem) {
            this.elem = elem;
        }

        public int getFreq() {
            return freq;
        }

        public void setFreq(int freq) {
            this.freq = freq;
        }
    }


    public static int firstToOccurKTimes(int[] arr, int k) {

        Map<Integer, Integer> map = new LinkedHashMap<>();

        for (int i = 0; i < arr.length; i++) {
            map.put(arr[i], map.getOrDefault(arr[i], 0) + 1);
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == k) {
                return entry.getKey();
            }
        }
        return -1;
    }


    public static void main(String[] args) {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int T = Integer.parseInt(bufferedReader.readLine());
            for (int i = 0; i < T; i++) {
                StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int n = Integer.parseInt(stringTokenizer.nextToken());
                int k = Integer.parseInt(stringTokenizer.nextToken());

                int arr[] = new int[n];

                stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int j = 0;
                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }
                System.out.println(GFG.firstToOccurKTimes(arr, k));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
