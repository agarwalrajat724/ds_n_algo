package CollectionDesign.ThreadException;

import java.util.Arrays;

class MyExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread thread, Throwable t) {
        System.err.println("Uncaught exception is detected! " + t
                + " st: " + Arrays.toString(t.getStackTrace()));
        // ... Handle the exception
    }
}
