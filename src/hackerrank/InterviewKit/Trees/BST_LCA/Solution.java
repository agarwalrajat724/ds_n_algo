package hackerrank.InterviewKit.Trees.BST_LCA;

import java.util.*;

/*

8
8 4 9 1 2 3 6 5
1 2
 */

class Node {

    int data;
    Node left;
    Node right;

    public Node(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }
}

public class Solution {


    public static Node lca(Node root, int v1, int v2) {
        // Write your code here.

        if (null != root) {

            if (root.data > v1 && root.data > v2) {
                return lca(root.left, v1, v2);
            }

            if (root.data < v1 && root.data < v2) {
                return lca(root.right, v1, v2);
            }
        }

        return root;
    }


    public static Node lcaIterative(Node root, int v1, int v2) {
//        if (null == root) {
//            return null;
//        }

        while (null != root) {
            if (root.data > v1 && root.data > v2) {
                root = root.left;
            } else if (root.data < v1 && root.data < v2) {
                root = root.right;
            } else {
                break;
            }
        }
        return root;
    }


    public static Node insert(Node root, int data) {
        if (null == root) {
            return new Node(data);
        } else {

            Node curr;

            if (data <= root.data) {
                curr = insert(root.left, data);
                root.left = curr;
            } else {
                curr = insert(root.right, data);
                root.right = curr;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Node root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
        int v1 = scan.nextInt();
        int v2 = scan.nextInt();
        scan.close();
        Node ans = lcaIterative(root,v1,v2);
        System.out.println(ans.data);
    }


}
