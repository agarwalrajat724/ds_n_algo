package GeekForGeeks.DSA_Class.Arrays.InsertAtIndex;

public class Gfg {

    void insertAtIndex(int arr[],int sizeOfArray,int index,int element)
    {
        //Your code here, Geeks!

        for (int i = sizeOfArray - 1; i > index - 1 ; i --) {
            arr[i] = arr[i - 1];
        }
        arr[index] = element;
    }
}
