package Grokking.Ultimate_Patterns.K_Way_Merge;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class SmallestRange {

    public static int[] findSmallestRange(List<Integer[]> lists) {
        Queue<Node> minHeap = new PriorityQueue<>(lists.size(), (node1, node2) ->
                lists.get(node1.arrayIndex)[node1.elemIndex] - lists.get(node2.arrayIndex)[node2.elemIndex]);

        int rangeStart = 0, rangeEnd = Integer.MAX_VALUE, currMax = Integer.MIN_VALUE;

        for (int i = 0; i < lists.size(); i++) {
            if (null != lists.get(i)) {
                minHeap.offer(new Node(i, 0));
                currMax = Math.max(currMax, lists.get(i)[0]);
            }
        }

        while (minHeap.size() == lists.size()) {

            Node node = minHeap.poll();
            int arrayIndex = node.arrayIndex;
            int elemIndex = node.elemIndex;
            if ((rangeEnd - rangeStart) > (currMax - lists.get(arrayIndex)[elemIndex])) {
                rangeStart = lists.get(arrayIndex)[elemIndex];
                rangeEnd = currMax;
            }

            elemIndex++;

            if (lists.get(arrayIndex).length > elemIndex) {
                minHeap.offer(new Node(arrayIndex, elemIndex));
                currMax = Math.max(currMax, lists.get(arrayIndex)[elemIndex]);
            }
        }

        return new int[]{rangeStart, rangeEnd};
    }

    public static void main(String[] args) {
        Integer[] l1 = new Integer[] { 1, 5, 8 };
        Integer[] l2 = new Integer[] { 4, 12 };
        Integer[] l3 = new Integer[] { 7, 8, 10 };
        List<Integer[]> lists = new ArrayList<Integer[]>();
        lists.add(l1);
        lists.add(l2);
        lists.add(l3);
        int[] result = SmallestRange.findSmallestRange(lists);
        System.out.print("Smallest range is: [" + result[0] + ", " + result[1] + "]");
    }
}
