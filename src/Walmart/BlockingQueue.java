package Walmart;

import java.io.Serializable;
import java.rmi.Remote;
import java.util.*;
import java.util.function.Function;

public class BlockingQueue<T> {

    private Queue<T> queue = new LinkedList<>();

    private int limit = 10;

    public synchronized void put(T t) {
        while (queue.size() == limit) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (queue.isEmpty()) {
            notifyAll();
        }

        queue.offer(t);
    }


    public synchronized T take() {
        if (queue.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (queue.size() == limit) {
            notifyAll();
        }

        return queue.poll();
    }

    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue = new BlockingQueue<>();
        Thread producer = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                blockingQueue.put(i);
            }
        });

        Thread consumer = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(blockingQueue.take());
            }
        });

        producer.start();
        consumer.start();
        Function function = (o) -> {
            return 0;
        };
        Object object = function;
    }
}
