package GeekForGeeks.DSA_Class.Recursion;

import java.util.Scanner;

public class GFG {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        GFG gfg = new GFG();
        int T = scanner.nextInt();

        for (int i = 0; i < T; i++) {
            int n = scanner.nextInt();
            int k = scanner.nextInt();

            System.out.println(gfg.gameOfDeath(n, k));
        }
    }

    private int gameOfDeath(int n, int k) {

        if (n == 1) {
            return 1;
        }
        return ((gameOfDeath(n - 1, k) + (k - 1)) % n) + 1;
    }
}
