package LeetCode.Easy.Strings.FirstUniqChar;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {


    static class Node {
        int count;
        int index;

        public Node(int index) {
            this.count = 1;
            this.index = index;
        }

        public void incCount() {
            this.count++;
        }
    }


    public int firstUniqChar(String s) {
        if (null == s) {
            return 0;
        }

        if (s.length() == 1) {
            return 0;
        }
        int res = Integer.MAX_VALUE;
        Map<Character, Node> map = new HashMap<>();
        char arr[] = s.toCharArray();

        for (int i = 0;  i < arr.length; i++) {
            if (map.containsKey(arr[i])) {
                map.get(arr[i]).incCount();
            } else {
                map.put(arr[i], new Node(i));
            }
        }
        boolean isPres = Boolean.FALSE;
        for (Map.Entry<Character, Node> entry : map.entrySet()) {
            if (entry.getValue().count == 1 && res > entry.getValue().index) {
                isPres = Boolean.TRUE;
                res = entry.getValue().index;
            }
        }

        if (isPres) {
            return res;
        }
        return -1;
    }

    public int firstUniqCharBetter(String s) {

        if (null == s) {
            return 0;
        }

        if (s.length() == 1) {
            return 0;
        }

        int hash[] = new int[26];

        Arrays.fill(hash, -1);

        char arr[] = s.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            int index = arr[i] - 'a';
            if (hash[index] == -1) {
                hash[index] = i;
            } else {
                hash[index] = -2;
            }
        }

        int res = Integer.MAX_VALUE;
        boolean isPres = Boolean.FALSE;
        for (int i = 0; i < hash.length; i++) {
            if (hash[i] >= 0) {
                isPres = Boolean.TRUE;
                res = Math.min(res, hash[i]);
            }
        }
        if (isPres) {
            return res;
        } else {
            return -1;
        }

    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.firstUniqChar("leetcode"));

        System.out.println(solution.firstUniqCharBetter("leetcode"));
    }
}
