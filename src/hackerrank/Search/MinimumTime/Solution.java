package hackerrank.Search.MinimumTime;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the minTime function below.
    static long minTime(long[] machines, long goal) {
        int n = machines.length;

        long maxVal = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            maxVal = Math.max(maxVal, machines[i]);
        }

        return binarySearch(machines, n, goal, maxVal * goal);
    }

    private static long binarySearch(long[] machines, int n, long goal, long high) {


        long low = 0l;

        while (low < high) {

            long middle = low + (high - low)/2;

            long items = findItems(machines, n, middle);

            if (items < goal) {
                low = middle + 1;
            } else {
                high = middle;
            }
        }

        return high;
    }

    private static long findItems(long[] machines, int n, long middle) {
        long ans = 0l;

        for (int i = 0; i < n; i++) {
            ans += middle/machines[i];
        }

        return ans;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
                "/Users/rajatagarwal/Learning/IdeaProjects/ds_n_algo/src/hackerrank/FileWriter.txt"));

        String[] nGoal = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nGoal[0]);

        long goal = Long.parseLong(nGoal[1]);

        long[] machines = new long[n];

        String[] machinesItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            long machinesItem = Long.parseLong(machinesItems[i]);
            machines[i] = machinesItem;
        }

        long ans = minTime(machines, goal);

        System.out.println(ans);

        bufferedWriter.write(String.valueOf(ans));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}