package hackerrank;

public class Test {


    static int count(int[] arr) {

        if (null == arr) {
            return 0;
        }

        if (arr.length < 2) {
            return 1;
        }

        int[] max = new int[arr.length];


        max[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            max[i] = Math.max(max[i - 1], arr[i - 1]);
        }

        int count = 1;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max[i]) {
                count++;
            }
        }


        return count;

    }


    public static void main(String[] args) {
        char c = 65;
        char a = 'a';
        System.out.println(a - 97);
        System.out.println(c - 'A');


        int arr[] = new int[]{7, 5, 8, 4, 8};

        System.out.println(count(arr));


//        if (null == arr) {
//            return 0;
//        }
//
//        if (arr.length < 2) {
//            return 1;
//        }
//
//        int[] max = new int[arr.length];
//
//        int maxVal = Integer.MIN_VALUE;
//
//        max[0] = arr[0];
//        for (int i = 1; i < arr.length; i++) {
//            max[i] = Math.max(maxVal, arr[i - 1]);
//        }
//
//        int count = 1;
//
//        for (int i = 1; i < arr.length;i++) {
//            if (arr[i] > max[i]) {
//                count++;
//            }
//        }
//
//
//
//        return count;
    }
}
