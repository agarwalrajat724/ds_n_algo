package GeekForGeeks.MustDo.Trees.CheckBST;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Solution {

    static class Node {
        int data;
        Node left, right;

        public Node(int data) {
            this.data = data;
        }
    }

    private static boolean checkBST(Node root) {

        if (null == root) {
            return true;
        }

        return checkBSTUtil(root, null, null);
    }

    private static boolean checkBSTUtil(Node root, Node left, Node right) {

        if (null == root) {
            return true;
        }

        if (null != left && left.data > root.data) {
            return false;
        }

        if (null != right && right.data < root.data) {
            return false;
        }

        return checkBSTUtil(root.left, left, root) && checkBSTUtil(root.right, root, right);
    }

    private static boolean isBST(Node root) {

        if (null == root) {
            return true;
        }

        Node cur = root;
        Node prev = null;
        List<Integer> res = new ArrayList<>();
        Stack<Node> stack = new Stack<>();

        while (null != cur || !stack.isEmpty()) {

            while (null != cur) {
                stack.push(cur);
                cur = cur.left;
            }

            if (null == cur) {
                Node pop = stack.pop();
                if (null != prev && prev.data > pop.data) {
                    return false;
                }
                res.add(pop.data);
                prev = pop;
                cur = pop.right;
            }
        }

        res.forEach(integer -> System.out.println(integer));
        return true;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.left.left.right = new Node(8);

        root.right.left = new Node(6);
        root.right.right = new Node(7);

        //System.out.println(KthSmallestElem.checkBST(root));

        Node root2 = new Node(4);
        root2.left = new Node(2);
        root2.right = new Node(5);
        root2.left.left = new Node(1);
        root2.left.right = new Node(3);

        System.out.println(Solution.isBST(root));

        System.out.println(Solution.isBST(root2));
    }
}
