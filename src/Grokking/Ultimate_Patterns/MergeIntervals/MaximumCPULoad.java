package Grokking.Ultimate_Patterns.MergeIntervals;

import java.util.*;

public class MaximumCPULoad {

    public static int findMaxCPULoad(List<Job> jobs) {
        if (null == jobs || jobs.isEmpty()) {
            return -1;
        }

        Collections.sort(jobs, (a, b) -> Integer.compare(a.start, b.start));
        int maxLoad = jobs.get(0).cpuLoad;

        for (int i = 1; i < jobs.size(); i++) {

            int index = i - 1;

            if (index >=0 && jobs.get(i).start <= jobs.get(index).end) {
                int cpuLoad = jobs.get(i).cpuLoad;
                while (index >= 0 && jobs.get(i).start <= jobs.get(index).end) {
                    cpuLoad += jobs.get(index).cpuLoad;
                    index--;
                }
                maxLoad = Math.max(maxLoad, cpuLoad);
            }

            maxLoad = Math.max(maxLoad, jobs.get(i).cpuLoad);
        }

        return maxLoad;
    }

    public static int findMaxCPULoadUtil(List<Job> jobs) {
        if (null == jobs || jobs.isEmpty()) {
            return -1;
        }
        Collections.sort(jobs, (a, b) -> Integer.compare(a.start, b.start));
        Queue<Job> minHeap = new PriorityQueue<Job>(jobs.size(), (a, b) -> Integer.compare(a.end, b.end));

        int maxCPULoad = Integer.MIN_VALUE;
        int currentCPULoad = 0;

        for (Job job : jobs) {

            while (!minHeap.isEmpty() && job.start > minHeap.peek().end) {
                currentCPULoad -= minHeap.poll().cpuLoad;
            }

            minHeap.offer(job);
            currentCPULoad += job.cpuLoad;
            maxCPULoad = Math.max(maxCPULoad, currentCPULoad);
        }

        return maxCPULoad;
    }

    public static void main(String[] args) {
        List<Job> input = new ArrayList<Job>(Arrays.asList(new Job(1, 4, 3),
                new Job(2, 5, 4), new Job(7, 9, 6)));
        System.out.println("Maximum CPU load at any time: " + MaximumCPULoad.findMaxCPULoad(input));
        System.out.println("Maximum CPU load at any time: " + MaximumCPULoad.findMaxCPULoadUtil(input));

        input = new ArrayList<Job>(Arrays.asList(new Job(6, 7, 10),
                new Job(2, 4, 11), new Job(8, 12, 15)));
        System.out.println("Maximum CPU load at any time: " + MaximumCPULoad.findMaxCPULoad(input));
        System.out.println("Maximum CPU load at any time: " + MaximumCPULoad.findMaxCPULoadUtil(input));

        input = new ArrayList<Job>(Arrays.asList(new Job(1, 4, 2),
                new Job(2, 4, 1), new Job(3, 6, 5)));
        System.out.println("Maximum CPU load at any time: " + MaximumCPULoad.findMaxCPULoad(input));
        System.out.println("Maximum CPU load at any time: " + MaximumCPULoad.findMaxCPULoadUtil(input));
    }
}
