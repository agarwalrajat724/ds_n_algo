package GeekForGeeks.DSA_Class.Queues.Max_SubArray_size_k;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class GfG {


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputTestCase = bufferedReader.readLine();
        StringTokenizer stringTokenizer = new StringTokenizer(inputTestCase);

        int T = Integer.parseInt(stringTokenizer.nextToken());

        for (int i = 0; i < T; i++) {
            String input = bufferedReader.readLine();
            stringTokenizer = new StringTokenizer(input, " ");
            int n = Integer.parseInt(stringTokenizer.nextToken());
            int k = Integer.parseInt(stringTokenizer.nextToken());
            int[] arr = new int[n];
            stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
            int j = 0;
            while (stringTokenizer.hasMoreTokens()) {
                arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
            }
            maximumSlidingWindow(arr, k);
        }
    }

    private static void maximumSlidingWindow(int[] arr, int k) {
        Deque<Integer> deque = new LinkedList<>();

        for (int i = 0; i < k; i++) {
            while (!deque.isEmpty() && arr[deque.peekLast()] <= arr[i]) {
                deque.removeLast();
            }
            deque.offerLast(i);
        }
        System.out.print(arr[deque.peekFirst()] + " ");
        for (int i = k; i < arr.length; i++) {

            // remove index which are out of window
            while (!deque.isEmpty() && deque.peekFirst() <= (i-k)) {
                deque.pollFirst();
            }

            while (!deque.isEmpty() && arr[deque.peekLast()] <= arr[i]) {
                deque.pollLast();
            }
            deque.offerLast(i);
            System.out.print(arr[deque.peekFirst()] + " ");
        }
        System.out.println();
    }
}
