package Grokking.Ultimate_Patterns.BinarySearch;

/**
 * Given an array of numbers sorted in ascending order, find the floor of a given number ‘key’.
 * The floor of the ‘key’ will be the biggest element in the given array smaller than or equal to the ‘key’
 *
 * Write a function to return the index of the floor of the ‘key’. If there isn’t a floor, return -1.
 *
 * Input: [4, 6, 10], key = 6
 * Output: 1
 * Explanation: The biggest number smaller than or equal to '6' is '6' having index '1'.
 *
 * Input: [1, 3, 8, 10, 15], key = 12
 * Output: 3
 * Explanation: The biggest number smaller than or equal to '12' is '10' having index '3'.
 *
 * Input: [4, 6, 10], key = 17
 * Output: 2
 * Explanation: The biggest number smaller than or equal to '17' is '10' having index '2'.
 *
 * Input: [4, 6, 10], key = -1
 * Output: -1
 * Explanation: There is no number smaller than or equal to '-1' in the given array.
 */
public class FloorOfANumber {

    public static void main(String[] args) {
        System.out.println(FloorOfANumber.searchFloorOfANumber(new int[] { 4, 6, 10 }, 6));
        System.out.println(FloorOfANumber.searchFloorOfANumber(new int[] { 1, 3, 8, 10, 15 }, 12));
        System.out.println(FloorOfANumber.searchFloorOfANumber(new int[] { 4, 6, 10 }, 17));
        System.out.println(FloorOfANumber.searchFloorOfANumber(new int[] { 4, 6, 10 }, -1));
    }

    private static int searchFloorOfANumber(int[] arr, int key) {
        if (key < arr[0]) {
            return -1;
        }

        int low = 0, high = arr.length - 1;

        while (low <= high) {
            int mid = low + (high - low)/2;

            if (arr[mid] == key) {
                return mid;
            } else if (arr[mid] < key) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        // since the loop is running until 'start <= end', so at the end of the while loop, 'start == end+1'
        // we are not able to find the element in the given array, so the next smaller number will be arr[end]
        return high;
    }
}
