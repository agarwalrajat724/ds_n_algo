package LeetCode.Medium.BinaryTree.ZigZagLevelOrder;

import LeetCode.Medium.BinaryTree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 103. Binary Tree Zigzag Level Order Traversal
 * <p>
 * Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).
 * <p>
 * For example:
 * Given binary tree [3,9,20,null,null,15,7],
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * return its zigzag level order traversal as:
 * [
 * [3],
 * [20,9],
 * [15,7]
 * ]
 */
public class Solution {

    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> result = new ArrayList<>();
        Stack<TreeNode> s1 = new Stack<>();
        Stack<TreeNode> s2 = new Stack<>();

        s1.push(root);

        while (!s1.isEmpty() || !s2.isEmpty()) {
            List<Integer> items = new ArrayList<>();
            if (!s1.isEmpty()) {
                int size = s1.size();

                for (int i = 0; i < size; i++) {
                    TreeNode pop = s1.pop();
                    items.add(pop.val);
                    if (pop.left != null) {
                        s2.push(pop.left);
                    }

                    if (pop.right != null) {
                        s2.push(pop.right);
                    }
                }
            } else {

                if (!s2.isEmpty()) {
                    int size = s2.size();

                    for (int i = 0; i < size; i++) {
                        TreeNode pop = s2.pop();
                        items.add(pop.val);
                        if (pop.right != null) {
                            s1.push(pop.right);
                        }

                        if (pop.left != null) {
                            s1.push(pop.left);
                        }
                    }
                }
            }

            if (!items.isEmpty()) {
                result.add(items);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);

        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);

        List<List<Integer>> result = solution.zigzagLevelOrder(root);

        int i = 0;
        for (List<Integer> list : result) {
            System.out.println("Index : " + i);
            list.forEach(integer -> System.out.println(integer));
            i++;
        }
     }


}
