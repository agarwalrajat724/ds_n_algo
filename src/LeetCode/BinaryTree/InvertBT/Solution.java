package LeetCode.BinaryTree.InvertBT;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 226. Invert Binary Tree
 *
 * Invert a binary tree.
 *
 * Example:
 *
 * Input:
 *
 *      4
 *    /   \
 *   2     7
 *  / \   / \
 * 1   3 6   9
 * Output:
 *
 *      4
 *    /   \
 *   7     2
 *  / \   / \
 * 9   6 3   1
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public TreeNode invertTree(TreeNode root) {

        if (null == root) {
            return null;
        } else if (null == root.left && null == root.right) {
            return root;
        }

        TreeNode current = root;

        TreeNode left = current.left;
        current.left = current.right;
        current.right = left;

        invertTree(root.left);
        invertTree(root.right);

        return root;
    }

    public TreeNode invertTreeRecursive(TreeNode root) {
        if (null == root) {
            return null;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            TreeNode pop = queue.poll();

            if (null == pop.left && null == pop.right) {
                continue;
            }

            TreeNode left = pop.left;
            pop.left = pop.right;
            pop.right = left;

            if (null != pop.left) {
                queue.offer(pop.left);
            }

            if (null != pop.right) {
                queue.offer(pop.right);
            }
        }

        return root;
    }

//    public TreeNode invertTreeUtil(TreeNode root) {
//
//        if (null == root) {
//            return null;
//        } else if (null == root.left && null == root.right) {
//            return root;
//        }
//
//        TreeNode current = root;
//
//        TreeNode left = current.left;
//        current.left = current.right;
//        current.right = left;
//
//        return root;
//    }

    public void inOrder(TreeNode root) {
        if (null != root) {
            inOrder(root.left);
            System.out.println(root.val);
            inOrder(root.right);
        }
    }

    public static void main(String[] args) {

        TreeNode root = new TreeNode(4);
        root.left = new TreeNode(2);
        root.right = new TreeNode(7);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(9);

        Solution solution = new Solution();

        TreeNode result = solution.invertTreeRecursive(root);

        solution.inOrder(result);

        System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT");

//        result = solution.invertTreeRecursive(root);
//
//        solution.inOrder(root);
    }
}
