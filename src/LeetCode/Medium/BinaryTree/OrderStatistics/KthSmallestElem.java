package LeetCode.Medium.BinaryTree.OrderStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

public class KthSmallestElem {


    public static void main(String[] args) {
        int[] arr = new int[]{20, 8, 22, 4, 12, 10, 14};
        Node root = null;
        for (int i = 0; i < arr.length; i++) {
            root = insert_node(root, new Node(arr[i], 0));
        }
        int kthSmallestElem = binarySearchForKthSmallestElem(root, 1);
        System.out.println(kthSmallestElem);
        kthSmallestElem = KThSmallestItem(root, 1);
        System.out.println(kthSmallestElem);
        System.out.println("Inorder Traversal");
        inorderTraversal(root).stream().forEach(integer -> System.out.println(integer));
    }

    private static List<Integer> inorderTraversal(Node root) {

        List<Integer> result = new ArrayList<>();
        Stack<Node> stack = new Stack<>();

        Node current = root;

        while (!stack.isEmpty() || null != current) {
            while (null != current) {
                stack.push(current);
                current = current.left;
            }

            if (null == current && !stack.isEmpty()) {
                Node pop = stack.pop();
                result.add(pop.val);
                current = pop.right;
            }
        }
        return result;
    }


    private static int KThSmallestItem(Node root, int k) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        return KThSmallestItem(root, atomicInteger, k);
    }

    private static int KThSmallestItem(Node root, AtomicInteger atomicInteger, int k) {
        if (null == root) {
            return Integer.MAX_VALUE;
        }
        int left = KThSmallestItem(root.left, atomicInteger, k);
        if (left != Integer.MAX_VALUE) {
            return left;
        }
        if (atomicInteger.incrementAndGet() == k) {
            return root.val;
        }
        return KThSmallestItem(root.right, atomicInteger, k);
    }

    private static int binarySearchForKthSmallestElem(Node root, int k) {

        if (null != root) {
            Node current = root;
            while (null != current) {
                if (current.lCount + 1 == k) {
                    return current.val;
                } else if (k > current.lCount) {
                    k = k - (current.lCount + 1);
                    current = current.right;
                } else {
                    current = current.left;
                }
            }
        }
        return -1;
    }

    private static Node insert_node(Node root, Node newNode) {

        Node pTraverse = root;
        Node currentParent = root;

        while (null != pTraverse) {
            currentParent = pTraverse;

            if (newNode.val < pTraverse.val) {
                pTraverse.lCount++;
                pTraverse = pTraverse.left;
            } else {
                pTraverse = pTraverse.right;
            }
        }

        if (null == root) {
            root = newNode;
        } else if (newNode.val < currentParent.val) {
            currentParent.left = newNode;
        } else {
            currentParent.right = newNode;
        }
        return root;
    }


}
