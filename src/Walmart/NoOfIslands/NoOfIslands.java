package Walmart.NoOfIslands;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class IslandNode {
    int row, col;

    public IslandNode(int row, int col) {
        this.row = row;
        this.col = col;
    }
}

public class NoOfIslands {

    private int[] rows = {-1, -1, -1, 0, 0, 1, 1, 1};
    private int[] cols = {-1, 0, 1, -1, 1, -1, 0, 1};


    public static void main(String[] args) {
        NoOfIslands noOfIslands = new NoOfIslands();

        int[][] arr = new int[][]{
                {1, 1, 0, 0, 0},
                {0, 1, 0, 0, 1},
                {1, 0, 0, 1, 1},
                {0, 0, 0, 0, 0},
                {1, 0, 1, 0, 1}
        };

        System.out.println(noOfIslands.countByBFS(arr));
        System.out.println(noOfIslands.countByDFS(arr));
        System.out.println(noOfIslands.countByDFSIterative(arr));
    }

    private int countByDFSIterative(int[][] arr) {
        int mRows = arr.length;
        int mCols = arr[0].length;
        int res = 0;
        boolean[][] visited = new boolean[mRows][mCols];
        for (int i = 0; i < mRows; i++) {
            for (int j = 0; j < mCols; j++) {
                if (arr[i][j] == 1 && !visited[i][j]) {
                    res++;
                    dfsIterative(arr, visited, i, j);
                }
            }
        }
        return res;
    }

    private void dfsIterative(int[][] arr, boolean[][] visited, int row, int col) {
        int mRows = arr.length;
        int nCols = arr[0].length;

        Stack<IslandNode> stack = new Stack<>();
        stack.push(new IslandNode(row, col));

        visited[row][col] = true;

        while (!stack.isEmpty()) {
            IslandNode node = stack.pop();

            int pRow = node.row;
            int pCol = node.col;

            for (int k = 0; k < 8; k++) {
                int dRow = pRow + rows[k];
                int dCol = pCol + cols[k];

                if (dRow >= 0 && dRow < mRows && dCol >= 0 && dCol < nCols && arr[dRow][dCol] == 1
                        && !visited[dRow][dCol]) {
                    visited[dRow][dCol] = true;
                    stack.push(new IslandNode(dRow, dCol));
                }
            }
        }
    }

    private int countByDFS(int[][] arr) {
        int mRows = arr.length;
        int mCols = arr[0].length;
        int res = 0;
        boolean[][] visited = new boolean[mRows][mCols];
        for (int i = 0; i < mRows; i++) {
            for (int j = 0; j < mCols; j++) {
                if (arr[i][j] == 1 && !visited[i][j]) {
                    res++;
                    dfs(arr, visited, i, j);
                }
            }
        }
        return res;
    }

    private void dfs(int[][] arr, boolean[][] visited, int row, int col) {
        visited[row][col] = true;
        int mRows = arr.length;
        int nCols = arr[0].length;
        for (int k = 0; k < 8; k++) {
            int dRow = row + rows[k];
            int dCol = col + cols[k];
            if (dRow >= 0 && dRow < mRows && dCol >= 0 && dCol < nCols && arr[dRow][dCol] == 1 && !visited[dRow][dCol]) {
                dfs(arr, visited, dRow, dCol);
            }
        }
    }

    private int countByBFS(int[][] arr) {
        int mRows = arr.length;
        int mCols = arr[0].length;
        int res = 0;
        boolean[][] visited = new boolean[mRows][mCols];
        for (int i = 0; i < mRows; i++) {
            for (int j = 0; j < mCols; j++) {
                if (arr[i][j] == 1 && !visited[i][j]) {
                    res++;
                    bfs(arr, visited, i, j);
                }
            }
        }
        return res;
    }

    private void bfs(int[][] arr, boolean[][] visited, int row, int col) {
        int mRows = arr.length;
        int nCols = arr[0].length;
        Queue<IslandNode> queue = new LinkedList<>();
        queue.offer(new IslandNode(row, col));

        while (!queue.isEmpty()) {
            IslandNode node = queue.poll();
            int pRow = node.row;
            int pCol = node.col;

            for (int k = 0; k < 8; k++) {
                int dRow = pRow + rows[k];
                int dCol = pCol + cols[k];

                if (dRow >= 0 && dRow < mRows && dCol >= 0 && dCol < nCols && !visited[dRow][dCol] && arr[dRow][dCol] == 1) {
                    queue.offer(new IslandNode(dRow, dCol));
                    visited[dRow][dCol] = true;
                }
            }

        }
    }
}
