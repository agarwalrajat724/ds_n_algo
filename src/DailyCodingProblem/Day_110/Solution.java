package DailyCodingProblem.Day_110;

import java.util.ArrayList;
import java.util.List;

/**
 * This problem was asked by Facebook.
 *
 * Given a binary tree, return all paths from the root to leaves.
 *
 * For example, given the tree:
 *
 *    1
 *   / \
 *  2   3
 *     / \
 *    4   5
 * Return [[1, 2], [1, 3, 4], [1, 3, 5]].
 */

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }
};
public class Solution {

    public static List<List<Integer>> allPathsFromRootToLeaves(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> current = new ArrayList<>();
        allPathsFromRootToLeavesDFS(root, current, result);
        return result;
    }

    private static void allPathsFromRootToLeavesDFS(TreeNode root, List<Integer> current, List<List<Integer>> result) {
        if (null == root) {
            return;
        }
        current.add(root.val);

        if (null == root.left && null == root.right) {
            result.add(new ArrayList<>(current));
        } else {
            allPathsFromRootToLeavesDFS(root.left, current, result);
            allPathsFromRootToLeavesDFS(root.right, current, result);
        }

        current.remove(current.size() - 1);
    }

    public static void main(String[] args) {

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.right.left = new TreeNode(4);
        root.right.right = new TreeNode(5);

        allPathsFromRootToLeaves(root).stream().forEach(res -> System.out.println(res));
     }
}
