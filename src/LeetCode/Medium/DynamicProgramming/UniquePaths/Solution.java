package LeetCode.Medium.DynamicProgramming.UniquePaths;

public class Solution {

    public int uniquePaths(int m, int n) {

        if (m == 0 || n == 0) {
            return 0;
        }

        if (m == 1 || n == 1) {
            return 1;
        }

        int[][] count = new int[m][n];

        // Count of Paths to reach any cell in 1st col
        for (int i = 0; i < m; i++) {
            count[i][0] = 1;
        }

        // Count of Paths to reach any cell in first row
        for (int j = 0; j < n; j++) {
            count[0][j] = 1;
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                count[i][j] = count[i - 1][j] + count[i][j - 1];
                // count[i-1][j-1] if diagonal moves are allowed
            }
        }

        return count[m-1][n-1];
    }
}
