package LeetCode.Easy.BitManipulation.ConvertDecimalToBinary;

public class Solution {

    private String getBinaryRepresentation(int number) {
        String res = "";

        while (number > 0) {
            int a = number % 2;

            res += a;

            number/=2;
        }

        return res;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.getBinaryRepresentation(1));
    }
}
