package LeetCode.BinaryTree.TwoSumIV;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 *
 * 653. Two Sum IV - Input is a BST
 *
 * Given a Binary Search Tree and a target number, return true if there exist two elements
 * in the BST such that their sum is equal to the given target.
 *
 * Example 1:
 * Input:
 *     5
 *    / \
 *   3   6
 *  / \   \
 * 2   4   7
 *
 * Target = 9
 *
 * Output: True
 * Example 2:
 * Input:
 *     5
 *    / \
 *   3   6
 *  / \   \
 * 2   4   7
 *
 * Target = 28
 *
 * Output: False
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public boolean findTarget(TreeNode root, int k) {

        if (null == root) {
            return false;
        }

        Set<Integer> set = new HashSet<>();

        return (findTargetUtil(root, k, set));
    }

    private boolean findTargetUtil(TreeNode root, int k, Set<Integer> set) {

        if (null == root) {
            return false;
        }

        if (set.contains(k - root.val)) {
            return true;
        }

        set.add(root.val);

        return (findTargetUtil(root.left, k, set) || findTargetUtil(root.right, k, set));

    }

    private boolean findTargetLevelOrder(TreeNode root, int k) {
        if (null == root) {
            return false;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        Set<Integer> set = new HashSet<>();

        while (!queue.isEmpty()) {
            TreeNode pop = queue.poll();

            if (set.contains(k - pop.val)) {
                return true;
            }

            set.add(pop.val);

            if (null != pop.left) {
                queue.offer(pop.left);
            }

            if (null != pop.right) {
                queue.offer(pop.right);
            }
        }

        return false;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(3);
        root.right = new TreeNode(6);

        root.left.left = new TreeNode(2);
        root.left.right = new TreeNode(4);

        root.right.right = new TreeNode(7);

        System.out.println(solution.findTarget(root, 9));

        System.out.println(solution.findTargetLevelOrder(root, 9));
    }
}
