package LeetCode.Medium.BinaryTree.ConstructFromPreOrderNInOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Construct Binary Tree from Preorder and Inorder Traversal
 *   Go to Discuss
 * Given preorder and inorder traversal of a tree, construct the binary tree.
 *
 * Note:
 * You may assume that duplicates do not exist in the tree.
 *
 * For example, given
 *
 * preorder = [3,9,20,15,7]
 * inorder = [9,3,15,20,7]
 * Return the following binary tree:
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 */
public class Solution {

    static class PreorderIndex {
        int preIndex = 0;
    }

    Map<Integer, Integer> map = new HashMap<>();

    public TreeNode buildTree(int[] preorder, int[] inorder) {

        for (int i = 0; i < inorder.length; i++) {
            map.put(inorder[i], i);
        }
        PreorderIndex preorderIndex = new PreorderIndex();
        return buildTreeUtil(preorder, inorder, 0, inorder.length - 1, inorder.length, preorder.length,
                preorderIndex);
    }

    private TreeNode buildTreeUtil(int[] preorder, int[] inorder, int start, int high, int inOrderLength,
                                   int preOrderLength, PreorderIndex preorderIndex) {
        if (start > high) {
            return null;
        }

        TreeNode node = new TreeNode(preorder[preorderIndex.preIndex++]);

        if (start == high) {
            return node;
        }
        int index = map.get(node.val);
        node.left = buildTreeUtil(preorder, inorder, start, index - 1, inOrderLength, preOrderLength, preorderIndex);
        node.right = buildTreeUtil(preorder, inorder, index + 1, high, inOrderLength, preOrderLength, preorderIndex);
        return node;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        TreeNode node = solution.buildTree(new int[]{3,9,20,15,7}, new int[]{9,3,15,20,7});
//                solution.buildTree(new int[]{-1}, new int[]{-1});

        System.out.println(node);
    }

}
