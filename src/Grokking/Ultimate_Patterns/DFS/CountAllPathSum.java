package Grokking.Ultimate_Patterns.DFS;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a binary tree and a number ‘S’, find all paths in the tree such that the sum of
 * all the node values of each path equals ‘S’. Please note that the paths can start or
 * end at any node but all paths must follow direction from parent to child (top to bottom).
 *
 *
 */
class Count {
    int count = 0;
};

public class CountAllPathSum {

    public static int countPaths(TreeNode root, int S) {
        Count count = new Count();
        List<Integer> curr = new ArrayList<>();
        countPaths(root, S, count, curr);
        return count.count;
    }

    private static void countPaths(TreeNode root, int S, Count count, List<Integer> curr) {
        if (null == root) {
            return;
        }

        curr.add(root.val);

        if (null == root.left && null == root.right) {
            int sum = 0;
            for (int i = curr.size() - 1; i >= 0; i--) {
                sum += curr.get(i);
                if (sum == S) {
                    count.count++;
                }
            }
        } else {
            countPaths(root.left, S, count, curr);
            countPaths(root.right, S, count, curr);
        }
        curr.remove(curr.size() - 1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        System.out.println("Tree has path: " + CountAllPathSum.countPaths(root, 11));
    }
}
