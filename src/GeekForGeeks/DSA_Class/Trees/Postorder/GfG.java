package GeekForGeeks.DSA_Class.Trees.Postorder;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.Stack;

public class GfG {

    void postOrder(Node root) {

        if (null == root) {
            return;
        }

        Stack<Node> stack = new Stack<>();
        Node curr = root;

        while (null != curr || !stack.isEmpty()) {
            while (null != curr) {
                if (null != curr.right) {
                    stack.push(curr.right);
                }
                stack.push(curr);

                curr = curr.left;
            }

            if (null == curr) {
                Node pop = stack.pop();

                if (!stack.isEmpty() && pop.right == stack.peek()) {
                    curr = stack.pop();
                    stack.push(pop);
                } else {
                    System.out.print(pop.data + " ");
                }
            }
        }
    }

    public static void main(String[] args) {
        GfG postOrder = new GfG();
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);

        root.left.left = new Node(4);
        root.left.right = new Node(5);

        root.right.left = new Node(6);
        root.right.right = new Node(7);

        postOrder.postOrder(root);
    }
}
