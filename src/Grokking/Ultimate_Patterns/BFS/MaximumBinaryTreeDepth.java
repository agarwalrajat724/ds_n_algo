package Grokking.Ultimate_Patterns.BFS;

import java.util.LinkedList;
import java.util.Queue;

public class MaximumBinaryTreeDepth {


    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        System.out.println("Tree Maximum Depth: " + MaximumBinaryTreeDepth.findDepth(root));
        root.left.left = new TreeNode(9);
        root.right.left.left = new TreeNode(11);
        System.out.println("Tree Maximum Depth: " + MaximumBinaryTreeDepth.findDepth(root));
    }

    private static int findDepth(TreeNode root) {
        if (null == root) {
            return 0;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int maxDepth = 0;

        while (!queue.isEmpty()) {
            maxDepth++;
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }
        }
        return maxDepth;
    }
}
