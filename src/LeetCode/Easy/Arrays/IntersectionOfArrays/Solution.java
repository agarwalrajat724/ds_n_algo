package LeetCode.Easy.Arrays.IntersectionOfArrays;

import java.util.*;
import java.util.stream.Collectors;

/**
 *  349. Intersection Of Two Arrays
 *
 * Given two arrays, write a function to compute their intersection.
 *
 * Example 1:
 *
 * Input: nums1 = [1,2,2,1], nums2 = [2,2]
 * Output: [2]
 * Example 2:
 *
 * Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * Output: [9,4]
 * Note:
 *
 * Each element in the result must be unique.
 * The result can be in any order.
 */
public class Solution {

    public int[] intersection(int[] nums1, int[] nums2) {
        List<Integer> result = new ArrayList<>();

        if (null != nums1 && nums1.length > 0 && null != nums2 && nums2.length > 0) {
            Set<Integer> set1 = new HashSet<>(Arrays.stream(nums1).boxed().collect(Collectors.toList()));
            Set<Integer> set2 = new HashSet<>(Arrays.stream(nums2).boxed().collect(Collectors.toList()));

            for (Integer integer : set2) {
                if (set1.contains(integer)) {
                    result.add(integer);
                }
            }
        }
        int res[] = new int[result.size()];

        for (int i = 0; i < result.size(); i++) {
            res[i] = result.get(i);
        }

        return res;
    }

    public int[] intersectionBS(int[] nums1, int[] nums2) {

        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < nums1.length; i++) {
            if (i == 0 || (i > 0 && nums1[i] != nums1[i - 1])) {
                if (Arrays.binarySearch(nums2, nums1[i]) > -1) {
                    list.add(nums1[i]);
                }
            }
        }

        int res[] = new int[list.size()];
        int i = 0;
        for (Integer integer : list) {
            res[i++] = integer;
        }

        return res;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        Arrays.stream(solution.intersection(new int[]{1,2,2,1}, new int[]{2,2})).forEach(i -> System.out.println(i));

        System.out.println("sjds");

        Arrays.stream(solution.intersection(new int[]{4,9,5}, new int[]{9,4,9,8,4})).forEach(i -> System.out.println(i));

        System.out.println("dkjskdjsk");
        Arrays.stream(solution.intersectionBS(new int[]{1,2,2,1}, new int[]{2,2})).forEach(i -> System.out.println(i));

        System.out.println("sjds");

        Arrays.stream(solution.intersectionBS(new int[]{4,9,5}, new int[]{9,4,9,8,4})).forEach(i -> System.out.println(i));
    }
}
