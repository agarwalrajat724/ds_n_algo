package Walmart.ShortestDistanceBetweenNodes;

public class ShortestDistanceInBST {

    private static Node insert(Node root, int key) {

        if (null == root) {
            root = new Node(key);
        } else if (root.val < key) {
            root.right = insert(root.right, key);
        } else if (root.val >= key) {
            root.left = insert(root.left, key);
        }
        return root;
    }

    private static int findDistanceWrapper(Node root, int a, int b) {
        if (a > b) {
            int temp = a;
            a = b;
            b = temp;
        }

        return findDistanceBetween(root, a, b);
    }

    private static int findDistanceBetween(Node root, int a, int b) {
        if (null == root) {
            return 0;
        }
        if (root.val > a && root.val > b) {
            return findDistanceBetween(root.left , a, b);
        } else if (root.val < a  && root.val < b) {
            return findDistanceBetween(root.right, a, b);
        } else if (root.val >= a && root.val <= b) {
            return findDistanceFromRoot(root, a) + findDistanceFromRoot(root, b);
        }
        return 0;
    }

    private static int findDistanceFromRoot(Node root, int x) {
        if (null == root || root.val == x) {
            return 0;
        }

        if (root.val > x) {
            return 1 + findDistanceFromRoot(root.left, x);
        }
        return 1 + findDistanceFromRoot(root.right, x);
    }

    public static void main(String[] args) {
        Node root = null;
        root = insert(root, 20);
        insert(root, 10);
        insert(root, 5);
        insert(root, 15);
        insert(root, 30);
        insert(root, 25);
        insert(root, 35);
        System.out.println(findDistanceWrapper(root, 5, 35));
    }
}
