package GeekForGeeks.DSA_Class.Stacks.RemoveConsecutiveDuplicates_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class GFG {


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        GFG solution = new GFG();
        int T = Integer.parseInt(bufferedReader.readLine());
        for (int i = 0; i < T; i++) {
            String str = bufferedReader.readLine();
            String result = solution.removeDuplicates(str);
            System.out.println(result);
        }
    }

    private String removeDuplicates(String str) {
        if (null == str || str.trim().length() == 0 || str.length() < 2) {
            return str;
        }

        char[] arr = str.toCharArray();
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < arr.length; i++) {
            if (!stack.isEmpty() && stack.peek().equals(Character.valueOf(arr[i]))) {
                stack.pop();
                continue;
            }
            stack.push(Character.valueOf(arr[i]));
        }
        if (stack.isEmpty()) {
            return "$";
        }
        char[] res = new char[stack.size()];
        int j = stack.size() - 1;
        while (!stack.isEmpty()) {
            res[j--] = stack.pop();
        }
        return new String(res);
    }
}
