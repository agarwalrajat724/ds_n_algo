package GeekForGeeks.MustDo.Arrays.LeftSmallNRightGreater;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class GFG {

    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer stringTokenizer = null;
        try {
            int T = Integer.parseInt(bufferedReader.readLine());

            for (int i = 0; i < T; i++) {
                int n = Integer.parseInt(bufferedReader.readLine());
                int[] arr = new int[n];

                stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int j = 0;

                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }

                System.out.println(GFG.partitionIndex(arr));
            }


        } catch (Exception ex) {

        }
    }

    /**
     * 3
     * 4
     * 4 2 5 7
     * 3
     * 11 9 12
     * 6
     * 4 3 2 7 8 9
     *
     * @param arr
     * @return
     */
    private static int partitionIndex(int[] arr) {

        int[] leftMax = new int[arr.length];
        leftMax[0] = Integer.MIN_VALUE;
        int rightMin = Integer.MAX_VALUE;

        for (int i = 1; i < arr.length; i++) {
            leftMax[i] = Math.max(leftMax[i - 1], arr[i - 1]);
        }

        for (int i = arr.length - 2; i >= 0; i--) {
            if (leftMax[i] < arr[i] && arr[i] < rightMin) {
                return i;
            }

            rightMin = Math.min(rightMin, arr[i]);
        }

        return -1;
    }
}
