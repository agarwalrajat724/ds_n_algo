package LeetCode.DynamicProgramming.Rohan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Solution {



    public void smart(int[] nums, int n) {
        if (null == nums) {
            System.out.println("0 0");
        }

        if (nums.length < 2) {
            System.out.println(nums[0] + " " + nums[0]);
        }

        if (nums.length == 2) {
            int max = Math.max(nums[0], nums[1]);
            System.out.println(max + " " + max);
        }

        int a = 0;
        int b = n - 1;

//        while (a < b) {
//            if (b - a > 2) {
//                if (nums[a+1] > nums[a] && nums[]) {
//
//                }
//            }
//        }

//        int dp[] = new int[nums.length];
//
//        dp[0] = nums[0];
//        dp[1] = Math.max(nums[0], nums[1]);
//
//        for (int i = 2; i < nums.length; i++) {
//            dp[i] = Math.max(nums[i] + dp[i-2], dp[i - 1]);
//        }
//
//        int res1 = dp[n-1];

        int i = 0;
        int j = n - 1;
        int res2 = 0;
        while (i < j) {
            int max = Math.max(nums[i], nums[j]);
            res2 += max;
            i++;
            j--;
        }

//        System.out.println(res1 + " " + res2);
    }


//    public void smart(int[] nums, int n) {
//        if (null == nums) {
//            System.out.println("0 0");
//        }
//
//        if (nums.length < 2) {
//            System.out.println(nums[0] + " " + nums[0]);
//        }
//
//        if (nums.length == 2) {
//            int max = Math.max(nums[0], nums[1]);
//            System.out.println(max + " " + max);
//        }
//
//        int dp[] = new int[nums.length];
//
//        dp[0] = nums[0];
//        dp[1] = Math.max(nums[0], nums[1]);
//
//        for (int i = 2; i < nums.length; i++) {
//            dp[i] = Math.max(nums[i] + dp[i-2], dp[i - 1]);
//        }
//
//        int res1 = dp[n-1];
//
//        int i = 0;
//        int j = n - 1;
//        int res2 = 0;
//        while (i < j) {
//            int max = Math.max(nums[i], nums[j]);
//            res2 += max;
//            i++;
//            j--;
//        }
//
//        System.out.println(res1 + " " + res2);
//    }


    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(bufferedReader.readLine().trim());
//        StringTokenizer stringTokenizer = null;
        for (int i = 0; i < T; i++) {
            int n = Integer.parseInt(bufferedReader.readLine().trim());
//            Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
//                    .map(Integer::parseInt)
//                    .collect();
            String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

            int[] arr = new int[n];

            for (int j = 0; j < n; j++) {
                arr[j] = Integer.parseInt(nr[j]);
            }

//            stringTokenizer = new StringTokenizer(bufferedReader.readLine());
//            int j = 0;
//            while (stringTokenizer.hasMoreTokens()) {
//                arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
//            }
            solution.smart(arr, n);
        }
    }
}
