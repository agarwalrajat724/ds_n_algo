package GeekForGeeks.MustDo.Hashing.LongestConsecutiveSunsequence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class GFG {


    private static int longestConsecutive(int[] arr) {

        Set<Integer> set = Arrays.stream(arr).boxed().collect(Collectors.toSet());

        int ans = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {

            if (!set.contains(arr[i] - 1)) {
                int j = arr[i];

                while (set.contains(j)) {
                    j++;
                }

                int res = j - arr[i];

                ans = Math.max(res, ans);
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int T = Integer.parseInt(bufferedReader.readLine());

            for (int i = 0; i < T; i++) {
                int n = Integer.parseInt(bufferedReader.readLine());

                int arr[] = new int[n];

                StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");

                int j = 0;

                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }

                System.out.println(GFG.longestConsecutive(arr));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
