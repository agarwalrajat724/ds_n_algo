package LeetCode.Easy.Arrays.FirstBadVersion;

public class Solution extends VersionControl {


    public int firstBadVersion(int n) {
        if (n < 2) {
            if (isBadVersion(n)) {
                return n;
            }
        }

        int low = 1;
        int high = n;

        while (low <= high) {
            int mid = low + (high - low)/2;

            if (isBadVersion(mid)) {
                if (mid - 1 >= 0 && isBadVersion(mid - 1)) {
                    high = mid - 1;
                } else {
                    return mid;
                }
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.firstBadVersion(5));
    }
}
