package LeetCode.BackTracking.Permutations_II;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();

        permuteUniqueUtil(nums, 0, nums.length - 1, result);

        return result;
    }

    private void permuteUniqueUtil(int[] nums, int low, int high, List<List<Integer>> result) {
        if (low == high) {
            result.add(Arrays.stream(nums).boxed().collect(Collectors.toList()));
        } else {
            Set<Integer> set = new HashSet<>();
            for (int i = low; i <= high; i++) {
                if (set.add(nums[i])) {
                    swap(nums, low, i);
                    permuteUniqueUtil(nums, low + 1, high, result);
                    swap(nums, low, i);
                }
            }
        }
    }


    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.permuteUnique(new int[]{1,1,2}).forEach(integers -> integers.forEach(integer -> System.out.println(integer)));
    }
}
