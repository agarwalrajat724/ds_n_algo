package Walmart.TreeSerialization;

import java.util.Stack;

class Node {
    int data;
    Node left, right;

    public Node(int data) {
        this.data = data;
    }
}
public class TreeSerialization {

    private static int MARKER = -1;

    private static int INDEX = 0;

    public static void main(String[] args) {
        TreeSerialization treeSerialization = new TreeSerialization();
        Node root1 = new Node(12);
        root1.left = new Node(13);
        StringBuilder stringBuilder = new StringBuilder();
        treeSerialization.serialize(root1, stringBuilder);
        System.out.println(stringBuilder.toString());
        int[] treeArray = treeArray(stringBuilder);
        Node res = treeSerialization.deserialize(treeArray);
        inOrder(res);


        Node root2 = new Node(20);
        root2.left = new Node(8);
        root2.right = new Node(22);
        INDEX = 0;
        System.out.println();
        StringBuilder stringBuilder1 = new StringBuilder();
        treeSerialization.serialize(root2, stringBuilder1);
        inOrder(treeSerialization.deserialize(treeArray(stringBuilder1)));

        Node root3 = new Node(20);
        root3.left = new Node(8);
        root3.left.left = new Node(4);
        root3.left.right = new Node(12);
        root3.left.right.left = new Node(10);
        root3.left.right.right = new Node(14);
        INDEX = 0;
        System.out.println();
        StringBuilder stringBuilder2 = new StringBuilder();
        treeSerialization.serialize(root3, stringBuilder2);
        inOrder(treeSerialization.deserialize(treeArray(stringBuilder2)));

        System.out.println();
        Node bstRoot = treeSerialization.BSTFromPreorderTraversal(new int[]{10, 5, 1, 7, 40, 50});
        inOrder(bstRoot);
    }

    private static void inOrder(Node root) {
        if (null != root) {
            inOrder(root.left);
            System.out.print(root.data + " ");
            inOrder(root.right);
        }
    }

    private static int[] treeArray(StringBuilder stringBuilder) {
        String[] tree = stringBuilder.toString().split(" ");
        int[] treeArray = new int[tree.length];
        for (int i = 0; i < tree.length; i++) {
            treeArray[i] = Integer.parseInt(tree[i]);
        }
        return treeArray;
    }

    private void serialize(Node root1, StringBuilder stringBuilder) {
        if (null == root1) {
            stringBuilder.append("-1 ");
            return;
        }
        stringBuilder.append(root1.data + " ");
        serialize(root1.left, stringBuilder);
        serialize(root1.right, stringBuilder);
    }

    private Node deserialize(int[] arr) {
        if (arr[INDEX] == -1 && INDEX < arr.length) {
            return null;
        }
        Node root = new Node(arr[INDEX]);
        INDEX++;
        root.left = deserialize(arr);
        INDEX++;
        root.right = deserialize(arr);
        return root;
    }

    private Node BSTFromPreorderTraversal(int[] pre) {
        Node root = new Node(pre[0]);
        Stack<Node> stack = new Stack<>();
        stack.push(root);

        for (int i = 1; i < pre.length; i++) {
            Node temp = null;
            while (!stack.isEmpty() && pre[i] > stack.peek().data) {
                temp = stack.pop();
            }

            if (null != temp) {
                temp.right = new Node(pre[i]);
                stack.push(temp.right);
            } else {
                temp = stack.peek();
                temp.left = new Node(pre[i]);
                stack.push(temp.left);
            }
        }

        return root;
    }
}
