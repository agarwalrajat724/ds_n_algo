package hackerrank.InterviewKit.HashMap.FrequencyQuery;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * https://www.hackerrank.com/challenges/frequency-queries/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=dictionaries-hashmaps
 */
public class Solution {
    // Complete the freqQuery function below.
    static List<Integer> freqQuery(List<List<Integer>> queries) {
        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> result = new ArrayList<>();
        Map<Integer, Set<Integer>> freq = new HashMap<>();
        for (List<Integer> list : queries) {
            int op = list.get(0);
            int key = list.get(1);

            if (op == 1) {
                int prev_count = map.getOrDefault(key, 0);

                if (prev_count != 0) {
                    freq.get(prev_count).remove(key);
                }

                int curr_count = prev_count + 1;
                map.put(key, curr_count);

                if (!freq.containsKey(curr_count)) {
                    freq.put(curr_count, new HashSet<>());
                }

                freq.get(curr_count).add(key);

            } else if (op == 2) {

                Integer prev_count = map.get(key);

                if (null != prev_count) {

                    freq.get(prev_count).remove(key);

                    int curr_count = prev_count - 1;
                    if (prev_count < 2) {
                        map.remove(key);
                    } else {
                        map.put(key, curr_count);
                    }

                    if (!freq.containsKey(curr_count)) {
                        freq.put(curr_count, new HashSet<>());
                    }
                    freq.get(curr_count).add(key);
                }

            } else if (op == 3) {
                if (freq.containsKey(key) && !freq.get(key).isEmpty()) {
                    result.add(1);
                } else {
                    result.add(0);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> queries = new ArrayList<>();

        IntStream.range(0, q).forEach(i -> {
            try {
                queries.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        List<Integer> ans = freqQuery(queries);


        ans.forEach(integer -> System.out.println(integer));

        bufferedWriter.write(
                ans.stream()
                        .map(Object::toString)
                        .collect(joining("\n"))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
