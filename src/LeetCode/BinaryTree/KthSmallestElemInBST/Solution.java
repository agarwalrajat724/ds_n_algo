package LeetCode.BinaryTree.KthSmallestElemInBST;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }



    public TreeNode insert(TreeNode root, int data) {
        if (null == root) {
            return new TreeNode(data);
        } else {
            TreeNode cur = null;

            if (data <= root.val) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }

            return root;
        }
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        int[] arr = new int[]{3,1,4,2};
        TreeNode root = null;

        for (int i = 0; i < arr.length; i++) {
            root = solution.insert(root, arr[i]);
        }

        //int kthSmallestElem = solution.kthSmallestElem(root, 1);
    }

//    public int kthSmallest(TreeNode root, int k) {
//        return kthSmallestUtil(root, 1, k);
//    }

//    private int kthSmallestUtil(TreeNode root, int i, int k) {
//
//
//    }
}
