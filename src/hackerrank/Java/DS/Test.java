package hackerrank.Java.DS;

import java.util.Scanner;
import java.util.Stack;

public class Test {


    private static void checkParanthases(char[] arr) {

        Stack<String> stack = new Stack<>();
        for (int i = 0; i < arr.length; i++) {

            String input = String.valueOf(arr[i]);
            if ("{".equals(input) || "(".equals(input) || "[".equals(input)) {
                stack.push(input);
            } else if (!stack.isEmpty()) {
                if ("}".equals(input) && stack.peek().equals("{")) {
                    stack.pop();
                } else if (")".equals(input) && stack.peek().equals("(")) {
                    stack.pop();
                } else if ("]".equals(input) && stack.peek().equals("[")) {
                    stack.pop();
                } else {
                    System.out.println("false");
                    return;
                }
            } else {
                System.out.println("false");
                return;
            }
        }


        if (!stack.isEmpty()) {
            System.out.println("false");
        } else {
            System.out.println("true");
        }


    }

    public static void main(String[] argh) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            String input = sc.next();
            //Complete the code
            if (null == input || input.trim().equals("")) {
                System.out.println("true");
            } else {
                Test.checkParanthases(input.toCharArray());
            }

        }

    }
}