package Grokking.Ultimate_Patterns.DFS;

import java.util.*;


/**
 * Given a binary tree where each node can only have a digit (0-9) value,
 * each root-to-leaf path will represent a number. Find the total sum of
 * all the numbers represented by all paths.
 */
class SumOfPathNumbers {

    public static int findSumOfPathNumbers(TreeNode root) {
        // TODO: Write your code here
        if (null == root) {
            return -1;
        }
        List<Integer> currentPath = new ArrayList<>();
        return findSumOfPathNumbersUtil(root, 0);
    }

    private static int findSumOfPathNumbersUtil(TreeNode current, int totalPathSum) {
        if (null == current) {
            return 0;
        }

        totalPathSum = 10 * totalPathSum + current.val;

        if (null == current.left && null == current.right) {
            return totalPathSum;
        }
        return findSumOfPathNumbersUtil(current.left, totalPathSum) + findSumOfPathNumbersUtil(current.right, totalPathSum);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(0);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(1);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(5);
        System.out.println("Total Sum of Path Numbers: " + SumOfPathNumbers.findSumOfPathNumbers(root));
    }
}