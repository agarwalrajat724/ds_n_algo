package Grokking.Ultimate_Patterns.SlidingWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Words Concatenation (hard)
 * Given a string and a list of words, find all the starting indices of substrings in
 * the given string that are a concatenation of all the given words exactly once without
 * any overlapping of words. It is given that all words are of the same length.
 *
 * Input: String="catfoxcat", Words=["cat", "fox"]
 * Output: [0, 3]
 * Explanation: The two substring containing both the words are "catfox" & "foxcat".
 *
 * Input: String="catcatfoxfox", Words=["cat", "fox"]
 * Output: [3]
 * Explanation: The only substring containing both the words is "catfox".
 *
 */
public class WordConcatenation {

    public static List<Integer> findWordConcatenation(String str, String[] words) {
        List<Integer> resultIndices = new ArrayList<Integer>();

        Map<String, Integer> wordFreqMap = new HashMap<>();

        for (String word : words) {
            wordFreqMap.put(word, wordFreqMap.getOrDefault(word, 0) + 1);
        }

        int wordCount = words.length, wordLength = words[0].length();

        for (int i = 0; i <= (str.length() - (wordCount * wordLength)); i++) {
            Map<String, Integer> wordSeen = new HashMap<>();

            for (int j = 0; j < wordCount; j++) {
                int nextWordIndex = i + j * wordLength;

                String word = str.substring(nextWordIndex, nextWordIndex + wordLength);

                if (!wordFreqMap.containsKey(word)) {
                    break;
                }

                wordSeen.put(word, wordSeen.getOrDefault(word, 0) + 1);

                if (wordSeen.get(word) > wordFreqMap.getOrDefault(word, 0)) {
                    break;
                }

                if ((j + 1) == wordCount) {
                    resultIndices.add(i);
                }
            }
        }

        return resultIndices;
    }

    public static void main(String[] args) {
        List<Integer> result = WordConcatenation.findWordConcatenation("catfoxcat", new String[] { "cat", "fox" });
        System.out.println(result);
        result = WordConcatenation.findWordConcatenation("catcatfoxfox", new String[] { "cat", "fox" });
        System.out.println(result);
    }
}
