package LeetCode.Medium.TwoPointers.PermutationInString;

import java.util.Arrays;

public class Solution {

    public boolean checkInclusion(String s1, String s2) {
        if (s1.length() == 0 || s2.length() == 0) {
            return false;
        }

        if (s1.length() > s2.length()) {
            return false;
        }

        int hash[] = calculateHash(s1);

        int i = 0;
        int j = s2.length() - 1;

        int s2Len = s2.length();

        while (i <= j) {

            if (((i + (s1.length() -1)) < (s2Len - 1)) &&
                    Arrays.equals(hash, calculateHash(s2.substring(i, i + s1.length())))) {
                return true;
            }

            if (((j - (s1.length() - 1)) >= 0) &&
                    Arrays.equals(hash, calculateHash(s2.substring((j - (s1.length() - 1)), j + 1)))) {
                return true;
            }

            i++;
            j--;
        }

        return false;
    }

    private int[] calculateHash(String str) {
        int hashArray[] = new int[26];

        for (int i = 0; i < str.length() ; i++) {
            hashArray[str.charAt(i) - 'a']++;
        }

        return hashArray;
    }

    public static void main(String[] args) {


        Solution solution = new Solution();

        System.out.println(solution.checkInclusion("plasma", "altruism"));

        System.out.println(solution.checkInclusion("a", "a"));
    }
}
