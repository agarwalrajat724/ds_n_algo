package Grokking.Ultimate_Patterns.DFS;

import java.util.ArrayList;
import java.util.List;

public class AllRootToLeafPaths {


    private static List<List<Integer>> findAllPaths(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<List<Integer>> allPaths = new ArrayList<>();
        List<Integer> currentPath = new ArrayList<>();

        findAllPathsRecursive(root, allPaths, currentPath);
        return allPaths;
    }

    private static void findAllPathsRecursive(TreeNode current, List<List<Integer>> allPaths, List<Integer> currentPath) {
        if (null == current) {
            return;
        }

        currentPath.add(current.val);

        if (null == current.left && null == current.right) {
            allPaths.add(new ArrayList<>(currentPath));
        } else {
            findAllPathsRecursive(current.left, allPaths, currentPath);
            findAllPathsRecursive(current.right, allPaths, currentPath);
        }

        currentPath.remove(currentPath.size() - 1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        List<List<Integer>> result = AllRootToLeafPaths.findAllPaths(root);
        System.out.println("Tree paths : " + result);
    }
}
