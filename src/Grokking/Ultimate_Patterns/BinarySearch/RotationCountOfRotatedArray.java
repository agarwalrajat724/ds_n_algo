package Grokking.Ultimate_Patterns.BinarySearch;

/**
 * Given an array of numbers which is sorted in ascending order and is rotated ‘k’ times around a pivot, find ‘k’.
 *
 * You can assume that the array does not have any duplicates.
 *
 * Input: [10, 15, 1, 3, 8]
 * Output: 2
 * Explanation: The array has been rotated 2 times.
 *
 * Input: [4, 5, 7, 9, 10, -1, 2]
 * Output: 5
 * Explanation: The array has been rotated 5 times.
 *
 * Input: [1, 3, 8, 10]
 * Output: 0
 * Explanation: The array has been not been rotated.
 */
public class RotationCountOfRotatedArray {
    public static int countRotations(int[] arr) {
        int pivot = findPivot(arr);
        return pivot;
    }

    /**
     * In this problem, actually, we are asked to find the index of the minimum element.
     * The number of times the minimum element is moved to the right will be equal to the
     * number of rotations. An interesting fact about the minimum element is that it is the
     * only element in the given array which is smaller than its previous element. Since the
     * array is sorted in ascending order, all other elements are bigger than their previous element.
     * @param arr
     * @return
     */
    private static int findPivot(int[] arr) {
        int low = 0, high = arr.length - 1;
        while (low <= high) {
            int mid = low + (high - low)/2;

            if ((mid + 1) < arr.length && arr[mid] > arr[mid + 1]) {
                return mid + 1;
            }

            if ((mid - 1) >= 0 && arr[mid - 1] > arr[mid]) {
                return mid;
            }

            if (arr[low] < arr[mid]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }

        // The Array has not been Rotated
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(RotationCountOfRotatedArray.countRotations(new int[] { 10, 15, 1, 3, 8 }));
        System.out.println(RotationCountOfRotatedArray.countRotations(new int[] { 4, 5, 7, 9, 10, -1, 2 }));
        System.out.println(RotationCountOfRotatedArray.countRotations(new int[] { 1, 3, 8, 10 }));
    }
}
