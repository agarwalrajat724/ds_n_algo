package DailyInterviewPro.TrappingRainWater;

/**
 * You have a landscape, in which puddles can form. You are given an array of non-negative integers
 * representing the elevation at each location. Return the amount of water that would accumulate if it rains.
 *
 * For example: [0,1,0,2,1,0,1,3,2,1,2,1] should return 6 because 6 units of water can get trapped here.
 *        X
 *    X███XX█X
 *  X█XX█XXXXXX
 * # [0,1,0,2,1,0,1,3,2,1,2,1]
 * Here's your starting pont:
 *
 * def capacity(arr):
 *   # Fill this in.
 *
 * print capacity([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1])
 * # 6
 */
public class TrappingRainWater {

    private int capacityBetter(int[] arr) {

        int low = 0, high = arr.length - 1, n = arr.length - 1;
        int res = 0, left_max = 0, right_max = 0;

        while (low <= high) {
            if (arr[low] < arr[high]) {
                if (arr[low] > left_max) {
                    left_max = arr[low];
                } else {
                    res += (left_max - arr[low]);
                }
                low++;
            } else {
                if (arr[high] > right_max) {
                    right_max = arr[high];
                } else {
                    res += (right_max - arr[high]);
                }
                high--;
            }
        }
        return res;
    }


    private int capacity(int[] arr) {
        int n = arr.length;
        int[] left = new int[n];
        int[] right = new int[n];

        left[0] = arr[0];
        for (int i = 1; i < n; i++) {
            left[i] = Math.max(left[i - 1], arr[i]);
        }

        right[n - 1] = arr[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            right[i] = Math.max(right[i + 1], arr[i]);
        }

        int res = 0;

        for (int i = 0; i < n; i++) {
            res += Math.min(left[i], right[i]) - arr[i];
        }

        return res;
    }

    public static void main(String[] args) {
        TrappingRainWater trappingRainWater = new TrappingRainWater();
        System.out.println(trappingRainWater.capacityBetter(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
        System.out.println(trappingRainWater.capacity(new int[]{1, 3, 5, 4, 1}));
    }
}
