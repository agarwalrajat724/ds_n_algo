package hackerrank.Java.Dequeue;

import java.util.*;

public class Test {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque<Integer> deque = new ArrayDeque();
        Map<Integer, Integer> map = new HashMap<>();
        int n = in.nextInt();
        int m = in.nextInt();
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            int num = in.nextInt();
            if (i >= m) {
                int removed = deque.removeFirst();
                if (map.get(removed) == 1) {
                    map.remove(removed);
                } else {
                    map.merge(removed, -1, Integer::sum);
                }
            }
            map.merge(num, 1, Integer::sum);

            deque.addLast(num);

            max = Math.max(max, map.size());

            if (max == m) {
                break;
            }
        }
        System.out.println(max);
    }
}
