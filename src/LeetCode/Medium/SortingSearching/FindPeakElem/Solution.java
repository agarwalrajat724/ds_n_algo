package LeetCode.Medium.SortingSearching.FindPeakElem;

/**
 * Find Peak Element
 *   Go to Discuss
 * A peak element is an element that is greater than its neighbors.
 *
 * Given an input array nums, where nums[i] ≠ nums[i+1], find a peak element and return its index.
 *
 * The array may contain multiple peaks, in that case return the index to any one of the peaks is fine.
 *
 * You may imagine that nums[-1] = nums[n] = -∞.
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1]
 * Output: 2
 * Explanation: 3 is a peak element and your function should return the index number 2.
 * Example 2:
 *
 * Input: nums = [1,2,1,3,5,6,4]
 * Output: 1 or 5
 * Explanation: Your function can return either index number 1 where the peak element is 2,
 *              or index number 5 where the peak element is 6.
 * Note:
 *
 * Your solution should be in logarithmic complexity
 */
public class Solution {

    public int findPeakElement(int[] nums) {
        if (null == nums) {
            return -1;
        }

        if (nums.length < 2) {
            return 0;
        }

        return findPeakElementUtil(nums, 0, nums.length - 1, nums.length);
    }

    private int findPeakElementUtil(int[] nums, int low, int high, int length) {
        int mid = low + (high - low)/2;

        if ((mid == 0 || nums[mid - 1] <= nums[mid]) && ((mid == length - 1) || nums[mid+1] <= nums[mid])) {
            return mid;
        } else if (mid > 0 && nums[mid - 1] > nums[mid]) {
            return findPeakElementUtil(nums, low, mid - 1, length);
        }
        return findPeakElementUtil(nums, mid + 1, high, length);
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] nums1 = new int[]{1,2,3,1};
        System.out.println(solution.findPeakElement(nums1));

        int[] nums2 = new int[]{1,2,1,3,5,6,4};
        System.out.println(solution.findPeakElement(nums2));
    }
}
