package Walmart.concurrency.Locks;

import java.util.ArrayList;
import java.util.List;

public class LockTest {


    public static void main(String[] args) {
        TestResource testResource = new TestResource();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            threads.add(new Thread(new DisplayJob(testResource), "Display Thread : " + i));
        }

        for (int i = 5; i < 10; i++) {
            threads.add(new Thread(new ReadJob(testResource), "Read Thread : " + i));
        }

        threads.stream().forEach(thread -> thread.start());
    }
}
