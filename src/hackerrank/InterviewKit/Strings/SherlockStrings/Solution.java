package hackerrank.InterviewKit.Strings.SherlockStrings;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {


    // Complete the isValid function below.
    static String isValid(String s) {

        Map<Character, Integer> map = new HashMap<>();

        char[] arr = s.toCharArray();

        for (char ch : arr) {
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }

        Set<Integer> set = new HashSet<>();


        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            set.add(entry.getValue());
        }

        if (set.size() > 2) {
            return "NO";
        } else if (set.size() == 1) {
            return "YES";
        } else {

            int f1 = 0, f2 = 0;
            int f1Freq = 0, f2Freq = 0;
            int i = 0;
            for (int val : set) {
                if (i == 0) {
                    f1 = val;
                } else {
                    f2 = val;
                }
                i++;
            }

            for (int val : map.values()) {
                if (val == f1) {
                    f1Freq++;
                }
                if (val == f2) {
                    f2Freq++;
                }
            }


            if ((f1 == 1 && f1Freq == 1) || (f2 == 1 && f2Freq == 1)) {
                return "YES";
            } else if ((Math.abs(f1 - f2) == 1) && (f1Freq == 1 || f2Freq == 1)) {
                return "YES";
            } else {
                return "NO";
            }
        }

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        String s = scanner.nextLine();

        String result = isValid(s);

        System.out.println(result);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
