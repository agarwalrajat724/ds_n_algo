package Grokking.Ultimate_Patterns.MergeIntervals;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * 1)->
 *
 * Merge Overlapping Sub Intervals
 *
 * Applicable only when Intervals are sorted by ascending order of Start Time
 *
 */

public class MergeIntervalsForSortedByAsc {

    public static List<Interval> merge(List<Interval> intervals) {
        List<Interval> mergedIntervals = new LinkedList<Interval>();
        // TODO: Write your code here

        if (null == intervals || intervals.isEmpty()) {
            return new ArrayList<>();
        }

        Stack<Interval> stack = new Stack<>();
        stack.push(intervals.get(0));

        for (int i = 1; i < intervals.size(); i++) {
            Interval currentInterval = intervals.get(i);
            Interval peek = stack.peek();

            if (peek.end < currentInterval.start) {
                stack.push(currentInterval);
            } else if (peek.end < currentInterval.end) {
                peek.start = Math.min(peek.start, currentInterval.start);
                peek.end = Math.max(peek.end, currentInterval.end);
            }
        }

        while (!stack.isEmpty()) {
            mergedIntervals.add(0, stack.pop());
        }

        return mergedIntervals;
    }

    public static void main(String[] args) {
        List<Interval> input = new ArrayList<Interval>();
        input.add(new Interval(1, 4));
        input.add(new Interval(2, 5));
        input.add(new Interval(7, 9));
        System.out.print("Merged intervals: ");
        for (Interval interval : MergeIntervalsForSortedByAsc.merge(input))
            System.out.print("[" + interval.start + "," + interval.end + "] ");
        System.out.println();

//        input = new ArrayList<Interval>();
//        input.add(new Interval(6, 7));
//        input.add(new Interval(2, 4));
//        input.add(new Interval(5, 9));
//        System.out.print("Merged intervals: ");
//        for (Interval interval : MergeIntervalsForSortedByAsc.merge(input))
//            System.out.print("[" + interval.start + "," + interval.end + "] ");
//        System.out.println();
//
//        input = new ArrayList<Interval>();
//        input.add(new Interval(1, 4));
//        input.add(new Interval(2, 6));
//        input.add(new Interval(3, 5));
//        System.out.print("Merged intervals: ");
//        for (Interval interval : MergeIntervalsForSortedByAsc.merge(input))
//            System.out.print("[" + interval.start + "," + interval.end + "] ");
//        System.out.println();
    }
}
