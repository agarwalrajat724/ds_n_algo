package CollectionDesign.Testing.practice;

public class Class2 extends Class1 {

    static
    {
        --i;
    }

    {
        i--;
    }

    public static void main(String[] args) {
        System.out.println(new Class2().i);
    }
}
