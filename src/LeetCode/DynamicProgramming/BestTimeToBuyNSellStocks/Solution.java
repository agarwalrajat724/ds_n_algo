package LeetCode.DynamicProgramming.BestTimeToBuyNSellStocks;

public class Solution {


    public int maxProfit(int[] prices) {

        if (null == prices || prices.length < 2) {
            return 0;
        }

        int maxProfit = Integer.MIN_VALUE;

        int minPrice = prices[0];

        for (int i = 1; i < prices.length; i++) {
            minPrice = Math.min(minPrice, prices[i]);

            maxProfit = Math.max(maxProfit, prices[i] - minPrice);
        }

        return maxProfit;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.maxProfit(new int[]{7,1,5,3,6,4}));
    }
}
