package Walmart.ReverseVowels;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Solution {

    public String reverseVowels(String s) {

        char[] arr = s.toCharArray();

        int i = 0, j = arr.length - 1;
        Set<Character> set = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));
        while (i < j) {
            if (set.contains(arr[i]) && set.contains(arr[j])) {
                char temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            } else {
                if (!set.contains(arr[i])) {
                    i++;
                }
                if (!set.contains(arr[j])) {
                    j--;
                }
            }
        }
        return new String(arr);
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.reverseVowels("hello"));
        System.out.println(solution.reverseVowels("leetcode"));

    }
}
