package Machine_Coding.SnakeNLadder.entities;

public class Player {

    private String id;
    private int name;

    public Player(String id, int name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }
}
