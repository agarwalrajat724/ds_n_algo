package Machine_Coding.SnakeNLadder.services;

import Machine_Coding.SnakeNLadder.entities.Move;
import Machine_Coding.SnakeNLadder.entities.Player;
import Machine_Coding.SnakeNLadder.entities.SnakeAndLadderBoard;

import java.util.*;
import java.util.stream.Collectors;

public class SnakeAndLadderService {


    private int numberOfPlayers;

    private SnakeAndLadderBoard snakeAndLadderBoard;
    private Map<Integer, Integer> snakeMoves;
    private Map<Integer, Integer> ladderMoves;
    private Queue<Player> players;

    // OPTIONAL
    private int noOfDices;
    private boolean shouldGameContinueTillLastPlayer;
    private boolean shouldAllowMultipleDiceRollsOnSix;



    private static int DEFAULT_BOARD_SIZE = 100;
    private static int DEFAULT_NO_OF_DICE = 1;

    public SnakeAndLadderService(int numberOfPlayers, SnakeAndLadderBoard snakeAndLadderBoard, int noOfDices, List<Player> players,
                                 boolean shouldGameContinueTillLastPlayer, boolean shouldAllowMultipleDiceRollsOnSix) {
        this.numberOfPlayers = numberOfPlayers;
        this.snakeAndLadderBoard = snakeAndLadderBoard;
        this.noOfDices = noOfDices;
        this.shouldGameContinueTillLastPlayer = shouldGameContinueTillLastPlayer;
        this.shouldAllowMultipleDiceRollsOnSix = shouldAllowMultipleDiceRollsOnSix;
        this.snakeMoves = snakeAndLadderBoard.getSnakes().stream().collect(Collectors.toMap(Move::getStart, Move::getEnd));
        this.ladderMoves = snakeAndLadderBoard.getLadders().stream().collect(Collectors.toMap(Move::getStart, Move::getEnd));
        this.players = new LinkedList<>();
        Map<String, Integer> playerPosition = new HashMap<>();
        for (Player player : players) {
            this.players.add(player);
            playerPosition.put(player.getId(), 0);
        }
        snakeAndLadderBoard.setPlayerPieces(playerPosition);
    }

    public void start() {
        while (!players.isEmpty()) {
            int totalDiceValue = DiceService.roll(1);
            Player currentPlayer = players.poll();

            movePlayer(currentPlayer, totalDiceValue);
        }
    }

    private void movePlayer(Player currentPlayer, int totalDiceValue) {
        Map<String, Integer> playerPositions = snakeAndLadderBoard.getPlayerPieces();
        int currentPos = playerPositions.get(currentPlayer.getId());

        int nextPos = 0;

        if ((currentPos + totalDiceValue) > DEFAULT_BOARD_SIZE) {
            nextPos = currentPos;
        } else {
            nextPos = currentPos + totalDiceValue;
        }
    }
}
