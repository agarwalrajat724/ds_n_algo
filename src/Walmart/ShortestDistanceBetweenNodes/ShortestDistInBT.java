package Walmart.ShortestDistanceBetweenNodes;

public class ShortestDistInBT {


    public static void main(String[] args) {

        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);

        root.left.left = new Node(4);
        root.left.right = new Node(5);

        root.right.left = new Node(6);
        root.right.right = new Node(7);

        root.right.left.right = new Node(8);

        System.out.println(findDistance(root, 4, 5));
        System.out.println(findDistance(root, 4, 6));
        System.out.println(findDistance(root, 3, 4));
        System.out.println(findDistance(root, 2, 4));
        System.out.println(findDistance(root, 8, 5));
    }

    private static int findDistance(Node root, int n1, int n2) {
        Node LCA = LCA(root, n1, n2);

        int d1 = findLevel(LCA, n1, 0);
        int d2 = findLevel(LCA, n2, 0);

        return d1 + d2;
    }

    private static int findLevel(Node lca, int x, int level) {
        if (null == lca) {
            return -1;
        }

        if (lca.val == x) {
            return level;
        }

        int left = findLevel(lca.left, x, level + 1);
        if (left == -1) {
            return findLevel(lca.right, x, level + 1);
        }
        return left;
    }

    private static Node LCA(Node root, int n1, int n2) {
        if (null == root) {
            return root;
        }

        if (root.val == n1 || root.val == n2) {
            return root;
        }

        Node left = LCA(root.left, n1, n2);
        Node right = LCA(root.right, n1, n2);

        if (null != left && null != right) {
            return root;
        }

        if (null != left) {
            return LCA(root.left, n1, n2);
        } else {
            return LCA(root.right, n1, n2);
        }
    }
}
