package Grokking.Ultimate_Patterns.MergeIntervals;

import java.util.*;

/**
 * Given a list of intervals representing the start and end time of ‘N’ meetings,
 * find the minimum number of rooms required to hold all the meetings.
 *
 * Example 1:
 *
 * Meetings: [[1,4], [2,5], [7,9]]
 * Output: 2
 * Explanation: Since [1,4] and [2,5] overlap, we need two rooms to hold these two meetings. [7,9] can
 * occur in any of the two rooms later.
 * Example 2:
 *
 * Meetings: [[6,7], [2,4], [8,12]]
 * Output: 1
 * Explanation: None of the meetings overlap, therefore we only need one room to hold all meetings.
 * Example 3:
 *
 * Meetings: [[1,4], [2,3], [3,6]]
 * Output:2
 * Explanation: Since [1,4] overlaps with the other two meetings [2,3] and [3,6], we need two rooms to
 * hold all the meetings.
 * ​
 * Example 4:
 *
 * Meetings: [[4,5], [2,3], [2,4], [3,5]]
 * Output: 2
 * Explanation: We will need one room for [2,3] and [3,5], and another room for [2,4] and [4,5].
 * ​
 *
 * Problem 1: Given a list of intervals, find the point where the maximum number of intervals overlap.
 *
 * Problem 2: Given a list of intervals representing the arrival and departure times of trains to a
 * train station, our goal is to find the minimum number of platforms required for the train station
 * so that no train has to wait.
 */
class MinimumMeetingRooms {

    public static int findMinimumMeetingRooms(List<Meeting> meetings) {
        if (null == meetings || meetings.isEmpty()) {
            return -1;
        }

        Collections.sort(meetings, (a, b) -> Integer.compare(a.end, b.end));
        Queue<Meeting> minHeap = new PriorityQueue<>(meetings.size(), (a, b) -> Integer.compare(a.end, b.end));
        int minNoOfMeetings = Integer.MIN_VALUE;
        for (Meeting meeting : meetings) {

            while (!minHeap.isEmpty() && minHeap.peek().end <= meeting.start) {
                minHeap.poll();
            }
            minHeap.offer(meeting);
            minNoOfMeetings = Math.max(minNoOfMeetings, minHeap.size());
        }

        return minNoOfMeetings;
    }


    /**
     * Keep track of the ending time of all the meetings currently happening
     *
     * 1)-> Sort all Meetings in ASC order of start time
     * 2)-> Min Heap to store all the active meetings. This Min Heap will also be used to find the
     *      active meeting with the smallest end time
     * 3)-> Before Scheduling meeting m1 remove all meetings from heap with end time less than the start of m1
     * 4)-> Now add m1 to Heap
     * 5)-> The Heap will always have all the overlapping meetings, so we need rooms for all of them.
     * 6)-> Keep a counter to remember the maximum size of the heap at any time which will be the
     *      Minimum number of rooms needed.
     * @param args
     */

    public static void main(String[] args) {
        List<Meeting> input = new ArrayList<Meeting>() {
            {
                add(new Meeting(4, 5));
                add(new Meeting(2, 3));
                add(new Meeting(2, 4));
                add(new Meeting(3, 5));
            }
        };
        int result = MinimumMeetingRooms.findMinimumMeetingRooms(input);
        System.out.println("Minimum meeting rooms required: " + result);

        input = new ArrayList<Meeting>() {
            {
                add(new Meeting(1, 4));
                add(new Meeting(2, 5));
                add(new Meeting(7, 9));
            }
        };
        result = MinimumMeetingRooms.findMinimumMeetingRooms(input);
        System.out.println("Minimum meeting rooms required: " + result);

        input = new ArrayList<Meeting>() {
            {
                add(new Meeting(6, 7));
                add(new Meeting(2, 4));
                add(new Meeting(8, 12));
            }
        };
        result = MinimumMeetingRooms.findMinimumMeetingRooms(input);
        System.out.println("Minimum meeting rooms required: " + result);

        input = new ArrayList<Meeting>() {
            {
                add(new Meeting(1, 4));
                add(new Meeting(2, 3));
                add(new Meeting(3, 6));
            }
        };
        result = MinimumMeetingRooms.findMinimumMeetingRooms(input);
        System.out.println("Minimum meeting rooms required: " + result);

        input = new ArrayList<Meeting>() {
            {
                add(new Meeting(4, 5));
                add(new Meeting(2, 3));
                add(new Meeting(2, 4));
                add(new Meeting(3, 5));
            }
        };
        result = MinimumMeetingRooms.findMinimumMeetingRooms(input);
        System.out.println("Minimum meeting rooms required: " + result);
    }
}

