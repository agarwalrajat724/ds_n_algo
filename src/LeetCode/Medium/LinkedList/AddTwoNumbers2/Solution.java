package LeetCode.Medium.LinkedList.AddTwoNumbers2;

import java.util.List;
import java.util.Stack;

/**
 * 445. Add Two Numbers II
 * <p>
 * You are given two non-empty linked lists representing two non-negative integers. The most
 * significant digit comes first and each of their nodes contain a single digit.
 * Add the two numbers and return it as a linked list.
 * <p>
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 * <p>
 * Follow up:
 * What if you cannot modify the input lists? In other words, reversing the lists is not allowed.
 * <p>
 * Example:
 * <p>
 * Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 8 -> 0 -> 7
 */
public class Solution {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int _val) {
            this.val = _val;
        }
    }


    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();
        ListNode result = new ListNode(0);
        ListNode p = result;

        Stack<Integer> res = new Stack<>();
        while (l1 != null) {
            s1.push(l1.val);
            l1 = l1.next;
        }

        while (l2 != null) {
            s2.push(l2.val);
            l2 = l2.next;
        }
        int carry = 0;
        while (!s1.isEmpty() || !s2.isEmpty()) {

            if (!s1.isEmpty()) {
                carry += s1.pop();
            }

            if (!s2.isEmpty()) {
                carry += s2.pop();
            }

            res.push(carry % 10);
            carry /= 10;
        }

        if (carry > 0) {
            res.push(carry);
        }

        while (!res.isEmpty()) {
            p.next = new ListNode(res.pop());
            p = p.next;
        }

        return result.next;

    }

    public static void main(String[] args) {

        ListNode listNode1 = new ListNode(7);
        listNode1.next = new ListNode(2);
        listNode1.next.next = new ListNode(4);
        listNode1.next.next.next = new ListNode(3);

        ListNode listNode2 = new ListNode(5);
        listNode2.next = new ListNode(6);
        listNode2.next.next = new ListNode(4);

        Solution solution = new Solution();

        ListNode result = solution.addTwoNumbers(listNode1, listNode2);

        while (result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }
}
