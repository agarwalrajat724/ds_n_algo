package LeetCode.Medium.SortingSearching.SortColors;

/**
 * Given an array with n objects colored red, white or blue, sort them in-place so that objects
 * of the same color are adjacent, with the colors in the order red, white and blue.
 *
 * Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.
 *
 * Note: You are not suppose to use the library's sort function for this problem.
 *
 * Example:
 *
 * Input: [2,0,2,1,1,0]
 * Output: [0,0,1,1,2,2]
 * Follow up:
 *
 * A rather straight forward solution is a two-pass algorithm using counting sort.
 * First, iterate the array counting number of 0's, 1's, and 2's, then overwrite array with total
 * number of 0's, then 1's and followed by 2's.
 * Could you come up with a one-pass algorithm using only constant space?
 */
public class Solution {

    public void sortColors(int[] nums) {
        if (null == nums || nums.length < 2) {
            return;
        }
        int lo = 0, mid = 0, hi = nums.length - 1;
        while (mid <= hi) {
            switch (nums[mid]) {
                case 0:
                    if (nums[lo] != nums[mid]) {
                        int temp = nums[lo];
                        nums[lo] = nums[mid];
                        nums[mid] = temp;
                    }
                    lo++;
                    mid++;
                    break;
                case 1:
                    mid++;
                    break;
                case 2:
                    if (nums[mid] != nums[hi]) {
                        int temp = nums[mid];
                        nums[mid] = nums[hi];
                        nums[hi] = temp;
                    }
                    hi--;
                    break;
            }
        }
    }
}
