package GeekForGeeks.MustDo.Hashing.CheckFreqREqual;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class GFG {



    public static void main (String[] args) {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int T = Integer.parseInt(bufferedReader.readLine());
            for (int i = 0; i < T; i++) {
                String s = bufferedReader.readLine();

                System.out.println(GFG.checkFreq(s));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static int checkFreq(String s) {

        int[] hash = new int[26];
        char arr[] = s.toCharArray();

        for (char c : arr) {
            hash[c - 'a']++;
        }

        boolean isSame = isSame(hash);

        if (isSame) {
            return 1;
        } else {
            for (char c = 'a'; c <= 'z'; c++) {
                int index = c - 'a';
                if (hash[index] > 0) {
                    hash[index]--;

                    if (isSame(hash)) {
                        return 1;
                    }

                    hash[index]++;
                }
            }
        }

        return 0;
    }

    private static boolean isSame(int[] hash) {

        int same = Integer.MIN_VALUE;

        for (int i = 0; i < hash.length; i++) {
            if (hash[i] > 0) {
                same = hash[i];
                break;
            }
        }

        for (int i = 0; i < hash.length; i++) {
            if (hash[i] > 0 && same != hash[i]) {
                return false;
            }
        }

        return true;
    }
}
