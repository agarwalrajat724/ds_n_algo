package LeetCode;

public class Test {

    public String reverseStringRecursive(String s) {

        if (s.length() < 2) {
            return s;
        }

        return s.substring(s.length()-1, s.length()) + reverseString(s.substring(0,s.length()-1));
    }

    public String reverseString(String s) {

        if (s.length() < 2) {
            return s;
        }

        StringBuffer stringBuffer = new StringBuffer(s);
        return stringBuffer.reverse().toString();
    }

    /**
     * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
     *
     * Note:
     *
     * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
     *
     * Example 1:
     *
     * Input: [2,2,1]
     * Output: 1
     * Example 2:
     *
     * Input: [4,1,2,1,2]
     * Output: 4
     * @param nums
     * @return
     */
    public int singleNumber(int[] nums) {
        if (nums.length < 2) {
            return nums[0];
        }
        int result = nums[0];
        for (int i = 1 ; i < nums.length ; i++) {
            result = result ^ nums[i];
        }

        return result;
    }

    public static void main(String[] args) {
        Test test = new Test();
        System.out.println(test.reverseStringRecursive("Hello"));
        System.out.println(test.reverseString("Hello"));
        System.out.println(test.singleNumber(new int[]{4,1,2,1,2}));
    }
}
