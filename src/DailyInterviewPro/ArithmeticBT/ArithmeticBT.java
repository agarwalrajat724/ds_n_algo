package DailyInterviewPro.ArithmeticBT;

class Node {
    Character data;
    Node left, right;

    public Node(Character data) {
        this.data = data;
    }
}
public class ArithmeticBT {


    public static void main(String[] args) {
        Node node = new Node('*');
        node.left = new Node('+');
        node.right = new Node('+');

        node.left.left = new Node('3');
        node.left.right = new Node('2');

        node.right.left = new Node('4');
        node.right.right = new Node('5');

        int res = evaluate(node);

        System.out.println(res);
    }

    private static int evaluate(Node root) {
        if (null != root) {
            if (null == root.left && null == root.right) {
                return Integer.valueOf(root.data.toString());
            }

            int left = evaluate(root.left);
            int right = evaluate(root.right);

            int res = 0;
            switch (root.data) {
                case '*' : res = left * right;
                            break;
                case '+' : res = left + right;
                            break;
                default: break;
            }
            return res;
        }
        return 0;
    }
}
