package LeetCode.Medium.LinkedList.AddTwoNumbers;

import java.util.ArrayList;
import java.util.List;

/**
 * 2. Add Two Numbers
 * <p>
 * You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse
 * order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
 * <p>
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 * <p>
 * Example:
 * <p>
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * Explanation: 342 + 465 = 807.
 */
public class Solution {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int _val) {
            this.val = _val;
        }
    }


    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        int carry = 0;
        ListNode p1 = l1, p2 = l2;
        ListNode res = new ListNode(0);
        ListNode p3 = res;

        while (null != p1 || null != p2) {
            if (null != p1) {
                carry += p1.val;
                p1 = p1.next;
            }

            if (null != p2) {
                carry += p2.val;
                p2 = p2.next;
            }

            p3.next = new ListNode(carry % 10);
            p3 = p3.next;

            carry = carry / 10;
        }

        if (carry > 0) {
            p3.next = new ListNode(carry);
        }

        return res.next;
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        ListNode node1 = new ListNode(2);
        node1.next = new ListNode(4);
        node1.next.next = new ListNode(3);

        ListNode node2 = new ListNode(5);
        node2.next = new ListNode(6);
        node2.next.next = new ListNode(4);

        ListNode result = solution.addTwoNumbers(node1, node2);

        while (result != null) {
            System.out.println(result.val);
            result = result.next;
        }
    }
}
