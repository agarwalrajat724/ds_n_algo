package LeetCode.BackTracking.Combinations;

import java.util.ArrayList;
import java.util.List;

public class Solution {


    public List<List<Integer>> combine(int n, int k) {

        if (n <= 0 || k <=0 || n < k) {
            return new ArrayList<>();
        }

        List<List<Integer>> result = new ArrayList<>();
        List<Integer> item = new ArrayList<>();
        dfs(n, k, 1, result, item);

        return result;
    }

    private void dfs(int n, int k, int start, List<List<Integer>> result, List<Integer> item) {
        if (item.size() == k) {
            result.add(new ArrayList<>(item));
        } else {
            for (int i = start; i <= n; i++) {
                item.add(i);
                dfs(n, k, i + 1, result, item);
                item.remove(item.size() - 1);
            }
        }
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.combine(4,2).forEach(integers -> {
            System.out.println("List : ");
            integers.forEach(integer -> System.out.println(integer));
        });
    }
}
