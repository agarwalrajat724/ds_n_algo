package LeetCode.Easy.Sample;

/**
 *  203. Remove Linked List Elements
 *
 * Remove all elements from a linked list of integers that have value val.
 *
 * Example:
 *
 * Input:  1->2->6->3->4->5->6, val = 6
 * Output: 1->2->3->4->5
 */
public class RemoveLL {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int _val) {
            this.val = _val;
        }
    }

    public ListNode removeElements(ListNode head, int val) {
        ListNode dummy = new ListNode(Integer.MIN_VALUE);
        dummy.next = head;

        ListNode pre = dummy;

        while (pre.next !=  null) {
            if (pre.next.val == val) {
                pre.next = pre.next.next;
            } else {
                pre = pre.next;
            }
        }
        return dummy.next;
    }

    public ListNode removeElementsOther(ListNode head, int val) {
        while (null != head && head.val == val) {
            head = head.next;
        }

        ListNode curr = head;

        while (null != curr && null != curr.next) {
            if (curr.next.val == val) {
                curr.next = curr.next.next;
            } else {
                curr = curr.next;
            }
        }
        return head;
    }

    public static void main(String[] args) {
        RemoveLL removeLL = new RemoveLL();

        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        head.next.next.next.next.next = new ListNode(6);

        removeLL.removeElements(head, 6);
    }
}
