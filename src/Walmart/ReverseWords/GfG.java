package Walmart.ReverseWords;

import java.util.Arrays;
import java.util.regex.Pattern;

public class GfG {

    public static void main(String[] args) {
        String s = "i.like.this.program.very.much";

//        String[] arr = s.split("\\.");

        String[] arr = s.split(Pattern.quote("."));

        int i = 0, j = arr.length - 1;

        while (i < j) {
            String temp = arr[j];
            arr[j] = arr[i];
            arr[i] = temp;
            i++;
            j--;
        }

        System.out.println(arr);

    }
}
