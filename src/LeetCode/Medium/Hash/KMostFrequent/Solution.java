package LeetCode.Medium.Hash.KMostFrequent;

import java.util.*;

/**
 * Given a non-empty array of integers, return the k most frequent elements.
 *
 * For example,
 * Given [1,1,1,2,2,3] and k = 2, return [1,2].
 *
 * Note:
 * You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
 * Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
 */
public class Solution {

    static class FreqNumb {
        int number;
        int frequency;

        public FreqNumb(int number, int frequency) {
            this.number = number;
            this.frequency = frequency;
        }
    }

    public List<Integer> topKFrequent(int[] nums, int k) {

        if (nums.length < 2) {
            if (k > 0) {
                return new ArrayList<>(Arrays.asList(nums[0]));
            } else {
                return new ArrayList<>();
            }
        }

        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> result = new ArrayList<>();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                int value = map.get(nums[i]);
                map.put(nums[i], ++value);
            } else {
                map.put(nums[i], 1);
            }
        }

        PriorityQueue<FreqNumb> priorityQueue = new PriorityQueue<>(map.size(), (freqNumb1, freqNumb2) -> {
            if (freqNumb1.frequency < freqNumb2.frequency) {
                return 1;
            } else if (freqNumb1.frequency > freqNumb2.frequency){
                return -1;
            } else {
                return 0;
            }
        });

        for (Map.Entry<Integer,Integer> entry : map.entrySet()) {
            priorityQueue.add(new FreqNumb(entry.getKey(), entry.getValue()));
        }

        while (!priorityQueue.isEmpty() && k > 0) {
            FreqNumb freqNumb = priorityQueue.poll();
            result.add(freqNumb.number);
            k--;
        }


        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.topKFrequent(new int[]{1,1,1,2,2,3}, 2).forEach(integer -> System.out.println(integer));
    }
}
