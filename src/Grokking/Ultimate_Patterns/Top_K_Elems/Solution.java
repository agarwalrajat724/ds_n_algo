package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution {
    // DO NOT MODIFY THE LIST
    public int repeatedNumber(final List<Integer> a) {

        Set<Integer> set = new HashSet<>();

        for (int no : a) {
            if (set.contains(no)) {
                return no;
            }
            set.add(no);
        }
        return -1;
    }
}
