package Grokking.Ultimate_Patterns.MergeIntervals;

public class Interval {

    int start;
    int end;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Interval)) return false;

        Interval interval = (Interval) o;

        if (getStart() != interval.getStart()) return false;
        return getEnd() == interval.getEnd();
    }

    @Override
    public int hashCode() {
        int result = getStart();
        result = 31 * result + getEnd();
        return result;
    }

    @Override
    public String toString() {
        return "Interval{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    public Interval() {
    }
}
