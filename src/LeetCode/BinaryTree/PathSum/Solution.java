package LeetCode.BinaryTree.PathSum;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    static class Node {
        TreeNode treeNode;
        int sum;

        public Node(TreeNode treeNode, int sum) {
            this.treeNode = treeNode;
            this.sum = sum;
        }
    }

    public boolean hasPathSum(TreeNode root, int sum) {

        if (null == root) {
            return false;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(new Node(root, root.val));

        while (!queue.isEmpty()) {
            Node node = queue.poll();
            int total = node.sum;
            TreeNode pop = node.treeNode;

            if (null == pop.left && null == pop.right && total == sum) {
                return true;
            }

            if (null != pop.left) {
                int leftSum = total + pop.left.val;
                queue.offer(new Node(pop.left, leftSum));
            }

            if (null != pop.right) {
                int rightSum = pop.right.val + total;
                queue.offer(new Node(pop.right, rightSum));
            }

        }

        return false;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(4);
        root.right = new TreeNode(8);

        root.left.left = new TreeNode(11);

        root.left.left.left = new TreeNode(7);
        root.left.left.right = new TreeNode(2);

        root.right.left = new TreeNode(13);
        root.right.right = new TreeNode(4);
        root.right.right.right = new TreeNode(1);

        System.out.println(solution.hasPathSum(root, 22));
    }
}
