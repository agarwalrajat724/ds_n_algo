package DailyCodingProblem.First_Easy.PrintAllPairsWithGivenSum;


/**
 * Given an array of integers, and a number ‘sum’, print all pairs in the array whose sum is equal to ‘sum’.
 *
 * Examples :
 * Input  :  arr[] = {1, 5, 7, -1, 5},
 *           sum = 6
 * Output : (1, 5) (7, -1) (1, 5)
 *
 * Input  :  arr[] = {2, 5, 17, -1},
 *           sum = 7
 * Output :  (2, 5)
 *
 * Method 1: Hashing
 * Method 2 : First Sort ,than two pointer
 */
public class PrintAllPairsWIthSum {
}
