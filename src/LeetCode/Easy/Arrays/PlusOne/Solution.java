package LeetCode.Easy.Arrays.PlusOne;

import java.util.Arrays;

public class Solution {

    public int[] plusOne(int[] digits) {
        int[] res = null;

        int length = digits.length - 1;

        int carry = (digits[length] + 1)/10;
        digits[length] = (digits[length] + 1) % 10;

        for (int i = length - 1; i >= 0; i--) {
            int val = digits[i] + carry;
            digits[i] = val % 10;
            carry = val/10;
        }

        if (carry > 0) {
            res = new int[digits.length + 1];

            res[0] = carry;

            for (int i = 0; i < digits.length; i++) {
                res[i + 1] = digits[i];
            }
        }

        if (null != res) {
            return res;
        }

        return digits;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        Arrays.stream(solution.plusOne(new int[]{1, 2, 3})).boxed().forEach(integer -> System.out.println(integer));
        System.out.println("hsjashaj");
        System.out.println(10/10);
        System.out.println(10%10);

    }
}
