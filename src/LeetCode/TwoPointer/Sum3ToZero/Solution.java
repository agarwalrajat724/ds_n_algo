package LeetCode.TwoPointer.Sum3ToZero;

import java.util.*;

public class Solution {


    static class CustomObj {
        int a;
        int b;
        int c;

        public CustomObj(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            CustomObj customObj = (CustomObj) object;
            return this.a == customObj.a && this.b == customObj.b && this.c == customObj.c;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.a, this.b, this.c);
        }

    }



    public List<List<Integer>> threeSum(int[] nums) {

        if (null == nums || nums.length < 3) {
            return new ArrayList<>();
        }

        List<List<Integer>> list = new ArrayList<>();
        Map<CustomObj, List<Integer>> map = new HashMap<>();

        Arrays.sort(nums);
        int n = nums.length - 1;
        for (int i = 0; i < n - 1; i++) {
            int l = i + 1;
            int r = n;

            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];


                if (sum == 0) {

                    map.putIfAbsent(new CustomObj(nums[i], nums[l], nums[r]),
                            Arrays.asList(nums[i], nums[l], nums[r]));
                    l++;
                    r--;
                } else if (sum < 0) {
                    l++;
                } else {
                    r--;
                }
            }
        }

        return new ArrayList<>(map.values());
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.threeSum(new int[]{-1, 0, 1, 2, -1, -4}).forEach(integers -> {
            System.out.println("skjdsgdkhsgdhs");
            integers.forEach(integer -> System.out.println(integer));
        });
    }
}
