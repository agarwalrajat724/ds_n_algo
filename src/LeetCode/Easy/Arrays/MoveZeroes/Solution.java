package LeetCode.Easy.Arrays.MoveZeroes;

import java.util.Arrays;

/**
 * 283. Move Zeroes
 * Given an array nums, write a function to move all 0's to the end of it while maintaining
 * the relative order of the non-zero elements.
 *
 * Example:
 *
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 * Note:
 *
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 */
public class Solution {

    public void moveZeroes(int[] nums) {

        for (int lastNonZero = 0, curr = 0; curr < nums.length; curr++) {
            if (nums[curr] != 0) {
                int temp = nums[lastNonZero];
                nums[lastNonZero] = nums[curr];
                nums[curr] = temp;
                lastNonZero++;
            }
        }

        Arrays.stream(nums).forEach(i -> System.out.println(i));
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.moveZeroes(new int[]{0,1,0,3,12});
    }
}
