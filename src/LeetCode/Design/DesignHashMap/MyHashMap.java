package LeetCode.Design.DesignHashMap;

import java.util.*;

public class MyHashMap {

    private static int SIZE = 10000;

    static class Entry<K, V> implements Map.Entry<K, V> {

        private K key;

        private V value;

        private Entry<K, V> next;

        public Entry(K key, V value, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V v) {
            V oldValue = this.getValue();
            this.value = v;

            return oldValue;
        }
    }

    private List<Entry> list = null;
    /** Initialize your data structure here. */
    public MyHashMap() {
        list = new ArrayList<>(10000);
    }

    /** value will always be non-negative. */
    public void put(int key, int value) {
        int index = hasCode(key);

        Entry<Integer, Integer> entry = list.get(index);

        if (null != entry) {
            entry = new Entry<>(key, value, null);
        } else {
            Entry<Integer, Integer> entry1 = entry;

            while (entry1.next != null) {
                if (entry1.key == key) {
                    entry1.setValue(value);
                    break;
                }
                entry1 = entry1.next;
            }
            entry1.next = new Entry<>(key, value, null);
        }
    }

    /** Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key */
    public int get(int key) {
        int index = hasCode(key);

        Entry<Integer, Integer> entry = list.get(index);

        if (null == entry) {
            return -1;
        }

        Entry<Integer, Integer> entry1 = entry;

        while (entry1 != null) {
            if (entry1.key == key) {
                return entry1.getValue();
            }
            entry1 = entry1.next;
        }

        return -1;
    }

    /** Removes the mapping of the specified value key if this map contains a mapping for the key */
    public void remove(int key) {

        int index = hasCode(key);

        Entry<Integer, Integer> entry = list.get(index);

        if (null == entry) {
            return;
        }

        Entry<Integer, Integer> curr = entry;
        Entry<Integer, Integer> prev = null;

//        while (curr.next != null) {
//
//        }

    }

    public int hasCode(int key) {
        return key % SIZE;
    }

    public static void main(String[] args) {
         // Your MyHashMap object will be instantiated and called as such:
        MyHashMap hashMap = new MyHashMap();
        hashMap.put(1, 1);
        hashMap.put(2, 2);
        hashMap.get(1);            // returns 1
        hashMap.get(3);            // returns -1 (not found)
        hashMap.put(2, 1);          // update the existing value
        hashMap.get(2);            // returns 1
        hashMap.remove(2);          // remove the mapping for 2
        hashMap.get(2);            // returns -1 (not found)
    }
}
