package GeekForGeeks.MustDo.Trees.ConnectNodes;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {

    static class Node {
        int data;
        Node left;
        Node right;
        Node nextRight;

        public Node(int data) {
            this.data = data;
        }
    }

    private static void connect(Node root) {

        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        Node prev = null;
        while (!queue.isEmpty()) {
            prev = queue.poll();

            int count = queue.size();

            if (null != prev.left) {
                queue.offer(prev.left);
            }

            if (null != prev.right) {
                queue.offer(prev.right);
            }

            while (count > 0) {
                Node pop = queue.poll();

                prev.nextRight = pop;

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }

                prev = pop;
                count--;
            }
        }

    }

    public static void main(String[] args) {
        Node root = new Node(10);
        root.left = new Node(3);
        root.right = new Node(5);
        root.left.left = new Node(4);
        root.left.right = new Node(1);
        root.right.right = new Node(2);

        Solution.connect(root);

        Node current = root;

    }
}
