package Grokking.Ultimate_Patterns.SlidingWindow;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string with lowercase letters only, if you are allowed to replace no more than ‘k’
 * letters with any letter, find the length of the longest substring having the same letters
 * after replacement.
 * <p>
 * Example 1:
 * <p>
 * Input: String="aabccbb", k=2
 * Output: 5
 * Explanation: Replace the two 'c' with 'b' to have a longest repeating substring "bbbbb".
 * Example 2:
 * <p>
 * Input: String="abbcb", k=1
 * Output: 4
 * Explanation: Replace the 'c' with 'b' to have a longest repeating substring "bbbb".
 * Example 3:
 * <p>
 * Input: String="abccde", k=1
 * Output: 3
 * Explanation: Replace the 'b' or 'd' with 'c' to have the longest repeating substring "ccc".
 */
public class CharacterReplacement {

    public static int findLength(String str, int k) {
        // TODO: Write your code here
        if (null == str || str.isEmpty() || str.trim().isEmpty()) {
            return -1;
        }
        int windowStart = 0;
        int maxRepeatLetterCount = Integer.MIN_VALUE;
        int maxLength = Integer.MIN_VALUE;
        char[] arr = str.toCharArray();
        Map<Character, Integer> freqMap = new HashMap<>();

        // try to increase the range windowStart to windowEnd
        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            Character rightChar = Character.valueOf(arr[windowEnd]);
            freqMap.put(rightChar, freqMap.getOrDefault(rightChar, 0) + 1);
            maxRepeatLetterCount = Math.max(maxRepeatLetterCount, freqMap.get(rightChar));

            // current window size is from windowStart to windowEnd, overall we have a letter which is
            // repeating 'maxRepeatLetterCount' times, this means we can have a window which has one letter
            // repeating 'maxRepeatLetterCount' times and the remaining letters we should replace.
            // if the remaining letters are more than 'k', it is the time to shrink the window as we
            // are not allowed to replace more than 'k' letters
            if ((windowEnd - windowStart + 1 - maxRepeatLetterCount) > k) {
                Character leftChar = Character.valueOf(arr[windowStart]);
                freqMap.put(leftChar, freqMap.get(leftChar) - 1);
                windowStart++;
            }
            maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
        }
        return maxLength;
    }

    public static void main(String[] args) {
        System.out.println(CharacterReplacement.findLength("aabccbb", 2));
        System.out.println(CharacterReplacement.findLength("abbcb", 1));
        System.out.println(CharacterReplacement.findLength("abccde", 1));
    }

}
