package LeetCode.Easy.Hash.ValidAnagrams;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.Arrays;

/**
 *  242. Valid Anagram
 *
 * Given two strings s and t , write a function to determine if t is an anagram of s.
 *
 * Example 1:
 *
 * Input: s = "anagram", t = "nagaram"
 * Output: true
 * Example 2:
 *
 * Input: s = "rat", t = "car"
 * Output: false
 * Note:
 * You may assume the string contains only lowercase alphabets.
 *
 * Follow up:
 * What if the inputs contain unicode characters? How would you adapt your solution to such case?
 */
public class Solution {


    public boolean isAnagram(String s, String t) {

        if (s.length() != t.length()) {
            return false;
        }

        char[] c1 = s.toCharArray();
        Arrays.sort(c1);

        char[] c2 = t.toCharArray();
        Arrays.sort(c2);


        return Arrays.equals(c1, c2);
    }


    /**
     *
     * O(n) time complexity
     * O(1) space complexity
     *
     * @param s1
     * @param s2
     * @return
     */
    public boolean isValidAnagram(String s1, String s2) {

        if (s1.length() != s2.length()) {
            return false;
        }

        int array[] = new int[26];

        for (int i = 0; i < s1.length(); i++) {
            array[s1.charAt(i) - 'a']++;
        }

        for (int i = 0; i < s2.length(); i++) {
            array[s2.charAt(i) - 'a']--;

            if (array[s2.charAt(i) - 'a'] < 0) {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isValidAnagram("anagram", "nagaram"));
    }
}
