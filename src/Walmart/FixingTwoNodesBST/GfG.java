package Walmart.FixingTwoNodesBST;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GfG {

    public Node correctBST(Node root)
    {
        // Your code here
        if (null == root || (null == root.left && null == root.right)) {
            return root;
        }

        List<Integer> inorder = new ArrayList<>();
        inorder(root, inorder);
        List<Integer> sortedInorder = new ArrayList<>(inorder);
        Collections.sort(sortedInorder);
        if (inorder.equals(sortedInorder)) {
            return root;
        }

        int i = 0;
        while (i < inorder.size()) {
            if (!inorder.get(i).equals(sortedInorder.get(i))) {
                fixBSTNodes(root, inorder.get(i), sortedInorder.get(i));
                return root;
            }
            i++;
        }
        return root;
    }

    private void fixBSTNodes(Node root, Integer val1, Integer val2) {
        if (null != root) {
            fixBSTNodes(root.left, val1, val2);

            if (val1.equals(Integer.valueOf(root.data))) {
                root.data = val2;
            } else if (val2.equals(Integer.valueOf(root.data))) {
                root.data = val1;
            }

            fixBSTNodes(root.right, val1, val2);
        }
    }

    private void inorder(Node root, List<Integer> inorder) {
        if (null != root) {
            inorder(root.left, inorder);
            inorder.add(root.data);
            inorder(root.right, inorder);
        }
    }


    public static void main(String[] args) {
        GfG sol = new GfG();
        Node root1 = new Node(8);
        root1.left = new Node(3);
        root1.right = new Node(10);

        root1.left.left = new Node(1);
        root1.left.right = new Node(6);
        root1.left.right.right = new Node(7);

        sol.correctBST(root1);

        Node root2 = new Node(10);
        root2.left = new Node(5);
        root2.right = new Node(8);
        root2.left.left = new Node(2);
        root2.left.right = new Node(20);

        sol.correctBST(root2);

        Node root3 = new Node(9);
        root3.left = new Node(8);
        root3.right = new Node(15);
        root3.left.left = new Node(7);
        root3.left.left.left = new Node(5);
        root3.left.left.left.left = new Node(3);
        root3.left.left.left.right = new Node(12);
        root3.right.left = new Node(10);
        root3.right.left.right = new Node(13);
        root3.right.left.right.left = new Node(6);

        sol.correctBST(root3);
    }
}
