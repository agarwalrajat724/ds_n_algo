package LeetCode.Medium.Array.ProductArrayExcept;

/**
 * Given an array nums of n integers where n > 1,  return an array output such that output[i]
 * is equal to the product of all the elements of nums except nums[i].
 * <p>
 * Example:
 * <p>
 * Input:  [1,2,3,4]
 * Output: [24,12,8,6]
 * Note: Please solve it without division and in O(n).
 * <p>
 * Follow up:
 * Could you solve it with constant space complexity? (The output array does not count
 * as extra space for the purpose of space complexity analysis.)
 */
public class Solution {

    public int[] productExceptSelf(int[] nums) {

        int[] left = new int[nums.length];
        int[] right = new int[nums.length];
        left[0] = 1;
        right[nums.length -1] = 1;
        int prodLeft = 1;
        for (int i = 1 ; i < nums.length; i++) {
            prodLeft = prodLeft * nums[i - 1];
            left[i] = prodLeft;
        }

        int prodRight = 1;
        for (int i = nums.length - 2; i>=0;i--) {
            prodRight = prodRight * nums[i + 1];
            right[i] = prodRight;
        }

        for (int i = 0; i < nums.length; i++) {
            nums[i] = left[i] * right[i];
        }

        return nums;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] res = solution.productExceptSelf(new int[]{1,2,3,4});

        for (int i = 0; i < res.length ; i++) {
            System.out.println(res[i]);
        }
    }
}
