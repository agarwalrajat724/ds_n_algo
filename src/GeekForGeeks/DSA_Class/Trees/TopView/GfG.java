package GeekForGeeks.DSA_Class.Trees.TopView;

import GeekForGeeks.DSA_Class.Trees.TreeNode;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class GfG {

    static class Node {
        TreeNode treeNode;
        int dist;

        public Node(TreeNode treeNode, int dist) {
            this.treeNode = treeNode;
            this.dist = dist;
        }
    }

    public void printTopView(TreeNode root) {

        if (null == root) {
            return;
        }

        Node node = new Node(root, 0);
        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);
        Map<Integer, Integer> map = new LinkedHashMap<>();

        while (!queue.isEmpty()) {
            Node pop = queue.poll();
            int dist = pop.dist;
            if (!map.containsKey(dist)) {
                map.put(dist, pop.treeNode.key);
            }
            if (null != pop.treeNode.left) {
                Node left = new Node(pop.treeNode.left, dist - 1);
                queue.offer(left);
            }

            if (null != pop.treeNode.right) {
                Node right = new Node(pop.treeNode.right, dist + 1);
                queue.offer(right);
            }
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            System.out.print(entry.getValue() + " ");
        }
    }

    public static void main(String[] args) {
        GfG topView = new GfG();

        TreeNode treeNode = new TreeNode(1);

        treeNode.left = new TreeNode(2);
        treeNode.right = new TreeNode(3);

        treeNode.left.left = new TreeNode(4);
        treeNode.left.right = new TreeNode(5);

        treeNode.right.left = new TreeNode(6);
        treeNode.right.right = new TreeNode(7);

        topView.printTopView(treeNode);
    }
}
