package LeetCode.Medium.BinaryTree.SmallestSubTreeDeepestNode;

import java.util.HashMap;
import java.util.Map;

/**
 * 865. Smallest Subtree with all the Deepest Nodes
 *
 * Given a binary tree rooted at root, the depth of each node is the shortest distance to the root.
 *
 * A node is deepest if it has the largest depth possible among any node in the entire tree.
 *
 * The subtree of a node is that node, plus the set of all descendants of that node.
 *
 * Return the node with the largest depth such that it contains all the deepest nodes in its subtree.
 *
 *
 *
 * Example 1:
 *
 * Input: [3,5,1,6,2,0,8,null,null,7,4]
 * Output: [2,7,4]
 * Explanation:
 *
 *
 *
 * We return the node with value 2, colored in yellow in the diagram.
 * The nodes colored in blue are the deepest nodes of the tree.
 * The input "[3, 5, 1, 6, 2, 0, 8, null, null, 7, 4]" is a serialization of the given tree.
 * The output "[2, 7, 4]" is a serialization of the subtree rooted at the node with value 2.
 * Both the input and output have TreeNode type.
 *
 *
 * Note:
 *
 * The number of nodes in the tree will be between 1 and 500.
 * The values of each node are unique.
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public TreeNode subtreeWithAllDeepest(TreeNode root) {

        if (null == root) {
            return null;
        }

        Map<TreeNode, Integer> map = new HashMap<>();

        return subtreeWithAllDeepestUtil(root, map, 1);

    }

    //

    private TreeNode subtreeWithAllDeepestUtil(TreeNode root, Map<TreeNode, Integer> map, int depth) {
        if (null == root.left && null == root.right) {
            map.put(root, depth);
            return root;
        }

        if (null == root.left) {
            return subtreeWithAllDeepestUtil(root.right, map, depth + 1);
        }

        if (null == root.right) {
            return subtreeWithAllDeepestUtil(root.left, map, depth + 1);
        }

        TreeNode leftResult = subtreeWithAllDeepestUtil(root.right, map, depth + 1);
        TreeNode rightResult = subtreeWithAllDeepestUtil(root.left, map, depth + 1);

        if (map.get(leftResult) == map.get(rightResult)) {
            map.put(root, depth);
            return leftResult;
        }

        return map.get(leftResult) > map.get(rightResult) ? leftResult : rightResult;
    }


    public static void main(String[] args) {


        Solution solution = new Solution();

        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(5);
        root.right = new TreeNode(1);

        root.left.left = new TreeNode(6);
        root.left.right = new TreeNode(2);
        root.left.right.left = new TreeNode(7);
        root.left.right.right = new TreeNode(4);

        root.right.left = new TreeNode(0);
        root.right.right = new TreeNode(8);

        TreeNode result = solution.subtreeWithAllDeepest(root);

        System.out.println(null != result ? result.val : -1);

    }
}
