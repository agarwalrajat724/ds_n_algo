package LeetCode.Medium.BinaryTree.LargestValInRow;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 515. Find Largest Value in Each Tree Row
 *
 * You need to find the largest value in each row of a binary tree.
 *
 * Example:
 * Input:
 *
 *           1
 *          / \
 *         3   2
 *        / \   \
 *       5   3   9
 *
 * Output: [1, 3, 9]
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public List<Integer> largestValues(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        List<Integer> result = new ArrayList<>();

        while (!queue.isEmpty()) {

            int n = queue.size();
            int maxValue = Integer.MIN_VALUE;
            for (int i = 0; i < n; i++) {
                TreeNode pop = queue.poll();
                maxValue = Math.max(maxValue, pop.val);

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }

            result.add(maxValue);
        }
        return result;
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(3);
        root.right = new TreeNode(2);
        root.left.left = new TreeNode(5);
        root.left.right = new TreeNode(3);
        root.right.right = new TreeNode(9);

        solution.largestValues(root).forEach(integer -> System.out.println(integer));
    }
}
