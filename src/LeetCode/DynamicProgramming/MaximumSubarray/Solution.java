package LeetCode.DynamicProgramming.MaximumSubarray;

/**
 * Input: [-2,1,-3,4,-1,2,1,-5,4],
 * Output: 6
 * Explanation: [4,-1,2,1] has the largest sum = 6
 */
public class Solution {


    public int maxSubArray(int[] nums) {

        if (null == nums) {
            return 0;
        }
        if (nums.length < 2) {
            return nums[0];
        }

        int maxSoFar = nums[0];
        int currMax = nums[0];

        for (int i = 1; i < nums.length; i++) {

            currMax = Math.max(nums[i], nums[i] + currMax);
            maxSoFar = Math.max(maxSoFar, currMax);
        }

        return maxSoFar;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
    }
}
