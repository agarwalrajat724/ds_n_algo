package LeetCode.Easy.Strings.ValidAnagram;

public class Solution {


    public boolean isAnagram(String s, String t) {
        int hash[] = new int[26];

        char[] s1 = s.toCharArray();

        for (int i = 0; i < s1.length; i++) {
            int index = s1[i] - 'a';

            hash[index]++;
        }

        char s2[] = t.toCharArray();

        for (int i = 0; i < t.length(); i++) {
            int index = s2[i] - 'a';

            hash[index]--;

            if (hash[index] < 0) {
                return false;
            }
        }

        for (int i = 0; i < hash.length; i++) {
            if (hash[i] != 0) {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isAnagram("anagram", "nagaram"));
    }
}
