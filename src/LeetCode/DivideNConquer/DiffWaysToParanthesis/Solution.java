package LeetCode.DivideNConquer.DiffWaysToParanthesis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * 241. Different Ways to Add Parentheses
 *
 * Given a string of numbers and operators, return all possible results from
 * computing all the different possible ways to group numbers and operators.
 * The valid operators are +, - and *.
 *
 * Example 1:
 *
 * Input: "2-1-1"
 * Output: [0, 2]
 * Explanation:
 * ((2-1)-1) = 0
 * (2-(1-1)) = 2
 * Example 2:
 *
 * Input: "2*3-4*5"
 * Output: [-34, -14, -10, -10, 10]
 * Explanation:
 * (2*(3-(4*5))) = -34
 * ((2*3)-(4*5)) = -14
 * ((2*(3-4))*5) = -10
 * (2*((3-4)*5)) = -10
 * (((2*3)-4)*5) = 10
 */
public class Solution {

    public List<Integer> diffWaysToCompute(String input) {

        if (null == input || input.trim().length() == 0) {
            return new ArrayList<>();
        }

        Map<String, List<Integer>> map = new HashMap<>();

        return diffWaysToComputeUtil(input, map);
    }

    private List<Integer> diffWaysToComputeUtil(String input, Map<String, List<Integer>> map) {

        if(map.containsKey(input)) {
            return map.get(input);
        }

        List<Integer> result = new ArrayList<>();


        for(int i = 0; i < input.length(); i++) {
            if (isCharacter(input.charAt(i))) {
                List<Integer> leftRes = diffWaysToComputeUtil(input.substring(0, i), map);
                List<Integer> rightRes = diffWaysToComputeUtil(input.substring(i + 1, input.length()), map);

                for (int j = 0; j < leftRes.size(); j++) {
                    for (int k = 0; k < rightRes.size(); k++) {
                        if (input.charAt(i) == '+') {
                            result.add(leftRes.get(j) + rightRes.get(k));
                        } else if (input.charAt(i) == '-') {
                            result.add(leftRes.get(j) - rightRes.get(k));
                        } else if (input.charAt(i) == '*') {
                            result.add(leftRes.get(j) * rightRes.get(k));
                        }
                    }
                }
            }
        }

        if (result.size() == 0) {
            result.add(Integer.valueOf(input));
        }

        map.put(input, result);

        return result;
    }

    private boolean isCharacter(char c) {

        if (c == '+' || c == '-' || c == '*') {
            return true;
        }

        return false;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.diffWaysToCompute("2-1-1").forEach(integer -> System.out.println(integer));
    }
}
