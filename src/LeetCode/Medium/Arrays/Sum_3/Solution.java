package LeetCode.Medium.Arrays.Sum_3;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0?
 * Find all unique triplets in the array which gives the sum of zero.
 *
 * Note:
 *
 * The solution set must not contain duplicate triplets.
 *
 * Example:
 *
 * Given array nums = [-1, 0, 1, 2, -1, -4],
 *
 * A solution set is:
 * [
 *   [-1, 0, 1],
 *   [-1, -1, 2]
 * ]
 */
public class Solution {

    static class CustomObject {
        int a;
        int b;
        int c;

        public CustomObject(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CustomObject)) return false;
            CustomObject that = (CustomObject) o;
            return a == that.a &&
                    b == that.b &&
                    c == that.c;
        }

        @Override
        public int hashCode() {

            return Objects.hash(a, b, c);
        }
    }

    public List<List<Integer>> threeSum(int[] nums) {
        if (null == nums || nums.length < 2) {
            return new ArrayList<>();
        }
        Set<CustomObject> set = new HashSet<>();
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int l = i + 1;
            int h = nums.length - 1;
            int v1 = nums[i];
            while (l < h) {
                int sum = v1 + nums[l] + nums[h];
                if (sum < 0) {
                    l++;
                } else if (sum > 0) {
                    h--;
                } else {
                    set.add(new CustomObject(v1, nums[l], nums[h]));
                    l++;
                    h--;
                }
            }
        }
        return set.stream().map(customObject -> Arrays.asList(customObject.a, customObject.b, customObject.c)).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] arr = new int[]{-1, 0, 1, 2, -1, -4};
        List<List<Integer>> res = solution.threeSum(arr);
        System.out.println(res);
    }
}
