package Grokking.Ultimate_Patterns.TopologicalSort;

import java.util.*;

/**
 * There is a dictionary containing words from an alien language for which we don’t know
 * the ordering of the characters. Write a method to find the correct order of characters
 * in the alien language.
 *
 * Input: Words: ["ba", "bc", "ac", "cab"]
 * Output: bac
 * Explanation: Given that the words are sorted lexicographically by the rules of the alien language, so
 * from the given words we can conclude the following ordering among its characters:
 *
 * 1. From "ba" and "bc", we can conclude that 'a' comes before 'c'.
 * 2. From "bc" and "ac", we can conclude that 'b' comes before 'a'
 *
 * From the above two points we can conclude that the correct character order is: "bac"
 *
 *Input: Words: ["cab", "aaa", "aab"]
 * Output: cab
 * Explanation: From the given words we can conclude the following ordering among its characters:
 *
 * 1. From "cab" and "aaa", we can conclude that 'c' comes before 'a'.
 * 2. From "aaa" and "aab", we can conclude that 'a' comes before 'b'
 *
 * From the above two points, we can conclude that the correct character order is: "cab"
 *
 * Input: Words: ["ywx", "xww", "xz", "zyy", "zwz"]
 * Output: yxwz
 * Explanation: From the given words we can conclude the following ordering among its characters:
 *
 * 1. From "ywx" and "xww", we can conclude that 'y' comes before 'x'.
 * 2. From "xww" and "xz", we can conclude that 'w' comes before 'z'
 * 3. From "xz" and "zyy", we can conclude that 'x' comes before 'z'
 * 2. From "zyy" and "zwz", we can conclude that 'y' comes before 'w'
 *
 * From the above two points we can conclude that the correct character order is: "yxwz"
 */
public class AlienDictionary {

    public static String findOrder(String[] words) {
        if (null == words || words.length == 0) {
            return "";
        }

        Map<Character, List<Character>> graph = new HashMap<>();
        Map<Character, Integer> indegree = new HashMap<>();
        Queue<Character> sources = new LinkedList<>();

        for (String word : words) {
            for (char letter : word.toCharArray()) {
                graph.putIfAbsent(letter, new ArrayList<>());
                indegree.putIfAbsent(letter, 0);
            }
        }

        for (int i = 0; i < words.length - 1; i++) {
            char[] w1 = words[i].toCharArray();
            char[] w2 = words[i + 1].toCharArray();

            for (int j = 0; j < Math.min(w1.length, w2.length); j++) {
                char parent = w1[j];
                char child = w2[j];

                if (parent != child) {
                    graph.get(parent).add(child);
                    indegree.put(child, indegree.get(child) + 1);
                    // only the first different character between the two words will help us find the order
                    break;
                }
            }
        }

        for (Map.Entry<Character, Integer> entry : indegree.entrySet()) {
            if (entry.getValue() == 0) {
                sources.offer(entry.getKey());
            }
        }
        StringBuilder result = new StringBuilder();
        while (!sources.isEmpty()) {
            char source = sources.poll();
            result.append(source);

            List<Character> children = graph.get(source);
            for (char child : children) {
                if (indegree.containsKey(child)) {
                    indegree.put(child, indegree.get(child) - 1);
                    if (indegree.get(child) == 0) {
                        sources.offer(child);
                    }
                }
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        String result = AlienDictionary.findOrder(new String[] { "ba", "bc", "ac", "cab" });
        System.out.println("Character order: " + result);

        result = AlienDictionary.findOrder(new String[] { "cab", "aaa", "aab" });
        System.out.println("Character order: " + result);

        result = AlienDictionary.findOrder(new String[] { "ywx", "xww", "xz", "zyy", "zwz" });
        System.out.println("Character order: " + result);
    }
}
