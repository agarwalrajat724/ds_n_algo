package Grokking.Ultimate_Patterns.Two_Pointers;


/**
 * Comparing Strings containing Backspaces (medium)
 *
 * Given two strings containing backspaces (identified by the character ‘#’), check if the two strings are equal.
 *
 * Input: str1="xy#z", str2="xzz#"
 * Output: true
 * Explanation: After applying backspaces the strings become "xz" and "xz" respectively.
 *
 * Input: str1="xy#z", str2="xyz#"
 * Output: false
 * Explanation: After applying backspaces the strings become "xz" and "xy" respectively.
 *
 * Input: str1="xp#", str2="xyz##"
 * Output: true
 * Explanation: After applying backspaces the strings become "x" and "x" respectively.
 * In "xyz##", the first '#' removes the character 'z' and the second '#' removes the character 'y'.
 *
 * Input: str1="xywrrmp", str2="xywrrmu#p"
 * Output: true
 * Explanation: After applying backspaces the strings become "xywrrmp" and "xywrrmp" respectively.
 *
 */
public class BackspaceCompare {

    public static boolean compare(String str1, String str2) {

        int index1 = str1.length() - 1, index2 = str2.length() - 1;

        while (index1>= 0 && index2 >= 0) {

            index1 = getNextValidCharIndex(str1, index1);
            index2 = getNextValidCharIndex(str2, index2);

            if (index1 < 0 && index2 < 0) {
                return true;
            }

            if (index1 < 0 || index2 < 0) {
                return false;
            }

            if (str1.charAt(index1) != str2.charAt(index2)) {
                return false;
            }

            index1--;
            index2--;

        }

        return true;
    }

    private static int getNextValidCharIndex(String str, int index) {
        int backSpaceCount = 0;
        while (index >= 0) {
            if (str.charAt(index) == '#') {
                backSpaceCount++;
            } else if (backSpaceCount > 0) { // A Non Backspace Char Found
                backSpaceCount--;
            } else {
                break;
            }
            index--;
        }
        return index;
    }

    public static void main(String[] args) {
        System.out.println(BackspaceCompare.compare("xy#z", "xzz#"));
        System.out.println(BackspaceCompare.compare("xy#z", "xyz#"));
        System.out.println(BackspaceCompare.compare("xp#", "xyz##"));
        System.out.println(BackspaceCompare.compare("xywrrmp", "xywrrmu#p"));
    }
}
