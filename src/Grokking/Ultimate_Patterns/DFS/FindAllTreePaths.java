package Grokking.Ultimate_Patterns.DFS;

import java.util.*;


/**
 * Given a binary tree and a number ‘S’, find all paths from root-to-leaf such
 * that the sum of all the node values of each path equals ‘S’.
 */
class FindAllTreePaths {
    public static List<List<Integer>> findPaths(TreeNode root, int sum) {
        List<List<Integer>> allPaths = new ArrayList<>();
        List<Integer> currentPath = new ArrayList<>();
        findPathsRecursive(root, allPaths, currentPath, sum);
        return allPaths;
    }

    private static void findPathsRecursive(TreeNode root, List<List<Integer>> allPaths, List<Integer> currentPath, int sum) {

        if (null == root) {
            return;
        }

        currentPath.add(root.val);

        if (root.val == sum && null == root.left && null == root.right) {
            allPaths.add(new ArrayList<>(currentPath));
        } else {
            findPathsRecursive(root.left, allPaths, currentPath, sum - root.val);
            findPathsRecursive(root.right, allPaths, currentPath, sum - root.val);
        }

        currentPath.remove(currentPath.size() - 1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        int sum = 23;
        List<List<Integer>> result = FindAllTreePaths.findPaths(root, sum);
        System.out.println("Tree paths with sum " + sum + ": " + result);
    }
}
