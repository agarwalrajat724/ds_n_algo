package LeetCode.Hard.Design.LRUCache;

import java.util.*;

public class LRUCache {

    class LinkedNode {
        int key;
        int value;

        LinkedNode pre;
        LinkedNode post;

        public LinkedNode(int key, int value) {
            this.key = key;
            this.value = value;
        }

        public LinkedNode() {}
    }

    private LinkedNode head;
    private LinkedNode tail;

    private Map<Integer, LinkedNode> map;
    private int count;
    private int capacity;


    public LRUCache(int capacity) {

        this.capacity = capacity;
        this.count = 0;
        this.map = new HashMap<>();

        head = new LinkedNode();
        head.pre = null;

        tail = new LinkedNode();
        tail.post = null;

        head.post = tail;
        tail.pre = head;
    }

    public int get(int key) {

        LinkedNode node = map.get(key);

        if (null == node) {
            return -1;
        }

        moveToHead(node);

        return node.value;
    }

    public void put(int key, int value) {
        LinkedNode node = map.get(key);

        if (null != node) {

            node.value = value;
            moveToHead(node);

        } else {
            LinkedNode linkedNode = new LinkedNode(key, value);

            this.map.put(key, linkedNode);

            addNodeToHead(linkedNode);

            ++count;

            if (count > capacity) {
                LinkedNode tail = popTail();
                this.map.remove(tail.key);
                --count;
            }
        }
    }

    private void moveToHead(LinkedNode node) {

        removeNode(node);
        addNodeToHead(node);
    }

    private LinkedNode popTail() {

        LinkedNode toRemove = tail.pre;

        removeNode(toRemove);

        return toRemove;
    }

    private void removeNode(LinkedNode toRemove) {

        toRemove.pre.post = toRemove.post;

        toRemove.post.pre = toRemove.pre;
    }

    private void addNodeToHead(LinkedNode linkedNode) {

        linkedNode.pre = head;

        linkedNode.post = head.post;
        head.post.pre = linkedNode;

        head.post = linkedNode;
    }


    public static void main(String[] args) {
        LRUCache cache = new LRUCache( 2 /* capacity */ );

        cache.put(1, 1);
        cache.put(2, 2);
        System.out.println(cache.get(1));       // returns 1
        cache.put(3, 3);    // evicts key 2
        System.out.println(cache.get(2));       // returns -1 (not found)
        cache.put(4, 4);    // evicts key 1
        System.out.println(cache.get(1));       // returns -1 (not found)
        System.out.println(cache.get(3));       // returns 3
        System.out.println(cache.get(4));
    }
}
