package Grokking.Ultimate_Patterns.SlidingWindow;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Permutation in a String (hard)
 * Given a string and a pattern, find out if the string contains any permutation of the pattern.
 *
 * Permutation is defined as the re-arranging of the characters of the string. For example, “abc” has the following six permutations:
 *
 * abc
 * acb
 * bac
 * bca
 * cab
 * cba
 * If a string has ‘n’ distinct characters it will have n!n! permutations.
 *
 * Example 1:
 *
 * Input: String="oidbcaf", Pattern="abc"
 * Output: true
 * Explanation: The string contains "bca" which is a permutation of the given pattern.
 * Example 2:
 *
 * Input: String="odicf", Pattern="dc"
 * Output: false
 * Explanation: No permutation of the pattern is present in the given string as a substring.
 * Example 3:
 *
 * Input: String="bcdxabcdy", Pattern="bcdyabcdx"
 * Output: true
 * Explanation: Both the string and the pattern are a permutation of each other.
 * Example 4:
 *
 * Input: String="aaacb", Pattern="abc"
 * Output: true
 * Explanation: The string contains "acb" which is a permutation of the given pattern.
 */
public class StringPermutation {

    public static boolean findPermutationBetter(String str , String pattern) {
        char[] input = str.toCharArray();
        char[] patt = pattern.toCharArray();
        Map<Character, Integer> characterFrequencymap = new HashMap<>();
        for (int i = 0; i < patt.length; i++) {
            characterFrequencymap.put(patt[i], characterFrequencymap.getOrDefault(patt[i], 0) + 1);
        }

        int matchedCount = 0, windowStart = 0;

        for (int windowEnd = 0; windowEnd < input.length; windowEnd++) {

            char rightChar = input[windowEnd];

            // STEP : 1
            if (characterFrequencymap.containsKey(rightChar)) {
                characterFrequencymap.put(rightChar, characterFrequencymap.get(rightChar) - 1);

                if (characterFrequencymap.get(rightChar) == 0) {
                    matchedCount++;
                }
            }

            if (matchedCount == characterFrequencymap.size()) {
                return true;
            }

            if (windowEnd >= (patt.length - 1)) {
                char leftChar = input[windowStart];
                if (characterFrequencymap.containsKey(leftChar)) {

                    if (characterFrequencymap.get(leftChar) == 0) {
                        matchedCount--;
                    }

                    characterFrequencymap.put(leftChar, characterFrequencymap.get(leftChar) + 1);
                }
                windowStart++;
            }
        }
        return false;
    }

    public static boolean findPermutation(String str, String pattern) {
        if (null == str || null == pattern) {
            return false;
        }
        char[] patternArr = pattern.toCharArray();
        int[] patternMatch = new int[26];
        for (int i = 0; i < patternArr.length; i++) {
            patternMatch[patternArr[i] - 'a']++;
        }
        char[] arr = str.toCharArray();
        int[] match = new int[26];
        int windowStart = 0;
        for (int i = 0; i < arr.length; i++) {
            match[arr[i] - 'a']++;
            if (i >= (pattern.length() - 1)) {
                if (Arrays.equals(patternMatch, match)) {
                    return true;
                }
                match[arr[windowStart] - 'a']--;
                windowStart++;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println("Permutation exist: " + StringPermutation.findPermutationBetter("oidbcaf", "abc"));
        System.out.println("Permutation exist: " + StringPermutation.findPermutationBetter("odicf", "dc"));
        System.out.println("Permutation exist: " + StringPermutation.findPermutationBetter("bcdxabcdy", "bcdyabcdx"));
        System.out.println("Permutation exist: " + StringPermutation.findPermutationBetter("aaacb", "abc"));
    }
}
