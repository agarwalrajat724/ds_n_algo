package LeetCode.Hard.Design.AddNSearchWord;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * 211. Add and Search Word - Data structure design
 *
 *
 */
public class WordDictionary {


    private Set<String> set;

    /** Initialize your data structure here. */
    public WordDictionary() {
        set = new HashSet<>();
    }

    /** Adds a word into the data structure. */
    public void addWord(String word) {
        set.add(word);
    }

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {

        if (null == word || "".equals(word) || "".equals(word.trim())) {
            return false;
        }

        if (word.contains(".")) {
            Pattern pattern = Pattern.compile(word);

            return set.stream().anyMatch(i -> pattern.matcher(word).matches());
        }

        return set.contains(word);
    }


    public static void main(String[] args) {
        WordDictionary wordDictionary = new WordDictionary();

        wordDictionary.addWord("bad");
        wordDictionary.addWord("dad");
        wordDictionary.addWord("mad");
        System.out.println(wordDictionary.search("pad")); // -> false
        System.out.println(wordDictionary.search("bad")); // -> true
        System.out.println(wordDictionary.search(".ad")); // -> true
        System.out.println(wordDictionary.search("b..")); // -> true
    }
}
