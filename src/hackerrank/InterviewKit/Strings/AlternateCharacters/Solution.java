package hackerrank.InterviewKit.Strings.AlternateCharacters;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;



public class Solution {


    // Complete the alternatingCharacters function below.
    static int alternatingCharacters(String s) {

        if (null == s || s.length() < 2) {
            return 0;
        }

        char ch = s.charAt(0);

        int count = 0;
        char[] arr = s.toCharArray();

        for (int currIndex = 1; currIndex < arr.length; currIndex++) {

            if (arr[currIndex] != ch) {
                ch = arr[currIndex];
            } else {
                count++;
            }
        }

        return count;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int qItr = 0; qItr < q; qItr++) {
            String s = scanner.nextLine();

            int result = alternatingCharacters(s);

            System.out.println(result);

            bufferedWriter.write(String.valueOf(result));
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
