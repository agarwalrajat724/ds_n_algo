package LeetCode.Trie;

import java.util.HashMap;
import java.util.Map;

public class Trie {

    private class TrieNode {
        Map<Character, TrieNode> children;
        boolean eof;

        public TrieNode() {
            this.children = new HashMap<>();
            this.eof = Boolean.FALSE;
        }
    }

    private final TrieNode root;

    public Trie() {
        this.root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode current = root;
        for (int i = 0 ; i < word.length() ; i++) {

            char ch = word.charAt(i);

            TrieNode node = current.children.get(ch);

            if (null == node) {
                node = new TrieNode();
                current.children.put(ch, node);
            }

            current = node;
        }

        current.eof = Boolean.TRUE;
    }

    public void insertRecursive(String word) {

    }

    public static void main(String[] args) {
        Trie trie = new Trie();

        trie.insert("abc");
    }


}
