package LeetCode.BinaryTree.MaxPathSum;

public class MaxPathSum {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    class Result {
        int maxTotal;

        public Result() {
            this.maxTotal = 0;
        }
    }


    public int maxPathSum(TreeNode root) {

        if (null == root) {
            return 0;
        }

        if (null == root.left && null == root.right) {
            return root.val;
        }

        Result result = new Result();

        maxPathSumUtil(root, result);

        return result.maxTotal;
    }

    private int maxPathSumUtil(TreeNode root, Result result) {
        if (null == root) {
            return 0;
        }

        int lMax = maxPathSumUtil(root.left, result);
        int rMax = maxPathSumUtil(root.right, result);

        int max_single = Math.max(Math.max(lMax, rMax) + root.val, root.val);

        int max_total = Math.max(max_single, lMax + rMax + root.val);

        result.maxTotal = Math.max(result.maxTotal, max_total);

        return max_single;
    }


    public static void main(String[] args) {
        MaxPathSum maxPathSum = new MaxPathSum();

        TreeNode root = new TreeNode(-10);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);

        System.out.println(maxPathSum.maxPathSum(root));
    }
}
