package LeetCode.Hard.Design.BSTIterator;

import java.util.Stack;

public class BSTIterator {


    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    Stack<TreeNode> stack = new Stack<>();

    public BSTIterator(TreeNode root) {
        TreeNode node = root;
        while (null != node) {
            stack.push(node);
            node = node.left;
        }
    }

    /** @return whether we have a next smallest number */
    public boolean hasNext() {

        return !stack.isEmpty();
    }

    /** @return the next smallest number */
    public int next() {

        TreeNode next = stack.pop();

        push(next.right);

        return next.val;
    }

    private void push(TreeNode next) {

        while (next != null) {
            stack.push(next);
            next = next.left;
        }
    }

    public static void main(String[] args) {
//        TreeNode root = new TreeNode(10);
//
//        root.left = new TreeNode(1);
//        root.left.right = new TreeNode(6);
//
//        root.right = new TreeNode(11);
//        root.right.right = new TreeNode(12);

        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(1);

        BSTIterator i = new BSTIterator(root);

        while (i.hasNext()) {
            System.out.println(i.next());
        }
    }
}
