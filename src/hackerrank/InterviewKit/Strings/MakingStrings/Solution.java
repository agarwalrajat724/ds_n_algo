package hackerrank.InterviewKit.Strings.MakingStrings;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {


    // Complete the makeAnagram function below.
    static int makeAnagram(String a, String b) {

        int[] hash = new int[26];
        int result = 0;

        char[] arr = a.toCharArray();

        for (char ch : arr) {
            hash[ch - 'a']--;
        }

        char[] arr2 = b.toCharArray();

        for (char ch : arr2) {

            hash[ch - 'a']++;
        }

        for (int i = 0; i < hash.length; i++) {
            int val = Math.abs(hash[i]);

            result += val;
        }

        return result;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        String a = scanner.nextLine();

        String b = scanner.nextLine();

        int res = makeAnagram(a, b);

        System.out.println(res);

        bufferedWriter.write(String.valueOf(res));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
