package Walmart.KthLargestInHeap;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.SortedSet;

public class GfG {

    public static void main(String[] args) {
        int k = 1;
        int nums[] = new int[]{3, 4};

        Queue<Integer> queue = new PriorityQueue<>();

        for (int i = 0; i < nums.length; i++) {
            if (queue.size() < k) {
                queue.offer(nums[i]);
            } else if (queue.peek() < nums[i]) {
                queue.poll();
                queue.offer(nums[i]);
            }
            if (queue.size() < k) {
                System.out.println(-1);
            } else {
                System.out.println(queue.peek());
            }
        }
    }
}
