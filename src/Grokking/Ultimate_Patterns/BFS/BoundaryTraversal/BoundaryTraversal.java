package Grokking.Ultimate_Patterns.BFS.BoundaryTraversal;


import java.util.*;
import java.util.stream.Collectors;

class Node {
    int data;
    Node left;
    Node right;

    public Node(int data) {
        this.data = data;
    }
};

/**
 * Iterative
 */
public class BoundaryTraversal {


    private List<Integer> boundaryTravesalRecursive(Node root) {
        List<Integer> result = new ArrayList<>();
        if (null != root) {
            result.add(root.data);
            printLeftNodes(root.left, result);

            printLeaves(root.left, result);
            printLeaves(root.right, result);

            printRightNodes(root.right, result);
        }
        return result;
    }

    private void printRightNodes(Node node, List<Integer> result) {
        if (null != node) {
            if (null != node.right) {
                printRightNodes(node.right, result);
                result.add(node.data);
            } else if (null != node.left) {
                printRightNodes(node.left, result);
                result.add(node.data);
            }
        }
    }

    private void printLeaves(Node node, List<Integer> result) {
        if (null != node) {
            printLeaves(node.left, result);
            if (null == node.left && null == node.right) {
                result.add(node.data);
            }
            printLeaves(node.right, result);
        }
    }

    private void printLeftNodes(Node node, List<Integer> result) {
        if (null != node) {
            if (null != node.left) {
                result.add(node.data);
                printLeftNodes(node.left, result);
            } else if (null != node.right) {
                result.add(node.data);
                printLeftNodes(node.right, result);
            }
        }
    }

    private List<Integer> boundaryTraversal(Node root) {
        if (null == root) {
            return new ArrayList<>();
        }
        if (null == root.left && null == root.right) {
            return Arrays.asList(root.data);
        }

        List<Node> result = new LinkedList<>();
        result.add(root);

        if (null != root.left) {
            Node leftTraversal = root.left;
            while (null != leftTraversal.left) {
                result.add(leftTraversal);
                leftTraversal = leftTraversal.left;
            }
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            Node currentNode = queue.poll();
            if (null == currentNode.left && null == currentNode.right) {
                result.add(currentNode);
            }
            if (null != currentNode.left) {
                queue.offer(currentNode.left);
            }

            if (null != currentNode.right) {
                queue.offer(currentNode.right);
            }
        }

        if (null != root.right) {
            Node rightTraversal = root.right;
            int index = result.size();
            while (null != rightTraversal.right) {
                result.add(index, rightTraversal);
                rightTraversal = rightTraversal.right;
            }
        }

        System.out.println(result);

        return result.stream().map(node -> node.data).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        BoundaryTraversal boundaryTraversal = new BoundaryTraversal();

        Node root = new Node(20);
        root.left = new Node(8);
        root.right = new Node(22);

        root.left.left = new Node(4);
        root.left.right = new Node(12);

        root.left.right.left = new Node(10);
        root.left.right.right = new Node(14);

        root.right.right = new Node(25);

        List<Integer> result = boundaryTraversal.boundaryTraversal(root);
        System.out.println(result);

        List<Integer> res = boundaryTraversal.boundaryTravesalRecursive(root);
        System.out.println(res);
    }


}
