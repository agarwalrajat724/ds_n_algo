package LeetCode.Hard.String.MinWindowSubString;


/**
 *
 * 76. Minimum Window Substring
 *
 * Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
 *
 * Example:
 *
 * Input: S = "ADOBECODEBANC", T = "ABC"
 * Output: "BANC"
 * Note:
 *
 * If there is no such window in S that covers all characters in T, return the empty string "".
 * If there is such window, you are guaranteed that there will always be only one unique minimum window in S.
 */
public class Solution {

    public String minWindow(String s, String t) {

        String pattern = t;

        if (s.length() < t.length()) {
            return "";
        }

        int hash_pattern[] = new int[26];
        for (int i = 0; i < pattern.length(); i++) {
            hash_pattern[pattern.charAt(i) - 'A']++;
        }

        int start = 0;

        int len1 = pattern.length();
        int len2 = s.length();

        int hash_str[] = new int[26];

        int count = 0;

        int min_length = Integer.MAX_VALUE;

        int start_index = Integer.MIN_VALUE;

        for (int i = 0; i < len2; i++) {
            int pos = s.charAt(i) - 'A';
            hash_str[pos]++;

            if (hash_str[pos] != 0 && hash_str[pos] <= hash_pattern[pos]) {
                count++;
            }

            if (count == len1) {
                while ((hash_str[start] > hash_pattern[start]) || hash_pattern[start] == 0) {

                    if (hash_str[start] > hash_pattern[start]) {
                        hash_str[start]--;
                    }

                    start++;
                }

                int window_length = i - start + 1;

                if (window_length < min_length) {
                    min_length = window_length;
                    start_index = start;
                }
            }
        }

        if (start_index < 0) {
            return "";
        }

        return s.substring(start_index, start_index + min_length);
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.minWindow("ADOBECODEBANC", "ABC"));
    }
}
