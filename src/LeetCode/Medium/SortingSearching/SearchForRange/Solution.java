package LeetCode.Medium.SortingSearching.SearchForRange;

import java.util.Arrays;

/**
 * Given an array of integers nums sorted in ascending order, find the starting and ending
 * position of a given target value.
 *
 * Your algorithm's runtime complexity must be in the order of O(log n).
 *
 * If the target is not found in the array, return [-1, -1].
 *
 * Example 1:
 *
 * Input: nums = [5,7,7,8,8,10], target = 8
 * Output: [3,4]
 * Example 2:
 *
 * Input: nums = [5,7,7,8,8,10], target = 6
 * Output: [-1,-1]
 */
public class Solution {

    public int[] searchRange(int[] nums, int target) {
        if (nums == null) {
            return null;
        }

        if (nums[nums.length - 1] < target || target < nums[0]) {
            return new int[]{-1, -1};
        }

        int first = first(nums, 0, nums.length - 1, target, nums.length);
        int last = last(nums, 0, nums.length - 1, target, nums.length);
        return new int[]{first, last};
    }

    private int last(int[] nums, int low, int high, int target, int n) {
        if (low > high) {
            return -1;
        }
        int mid = low + (high - low)/2;

        if ((mid == n - 1 || nums[mid + 1] > target) && nums[mid] == target) {
            return mid;
        } else if (target < nums[mid]) {
            return last(nums, low, mid - 1, target, n);
        } else {
            return last(nums, mid + 1, high, target, n);
        }
    }

    private int first(int[] nums, int low, int high, int target, int n) {
        if (low > high) {
            return -1;
        }

        int mid = low + (high - low)/2;

        if ((mid == 0 || nums[mid - 1] < target) && (nums[mid] == target)) {
            return mid;
        } else if (target > nums[mid]) {
            return first(nums, mid + 1, high, target, n);
        } else {
            return first(nums, low, mid - 1, target, n);
        }
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        Arrays.stream(solution.searchRange(new int[]{5,7,7,8,8,10}, 6))
                .forEach(value -> System.out.println(value));
    }
}
