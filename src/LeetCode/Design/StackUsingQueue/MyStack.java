package LeetCode.Design.StackUsingQueue;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class MyStack {


    Queue<Integer> queue1 = null;
    Queue<Integer> queue2 = null;


    /**
     * Initialize your data structure here.
     */
    public MyStack() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    /**
     * Push element x onto stack.
     */
    public void push(int x) {

        queue1.offer(x);
    }

    /**
     * Removes the element on top of the stack and returns that element.
     */
    public int pop() {
        if (queue1.isEmpty()) {
            System.out.println("ERROR ..................");
            return Integer.MIN_VALUE;
        }
        return mainLogic(Boolean.FALSE);
    }

    private int mainLogic(boolean isForTop) {
        int size = queue1.size();
        while (size > 1) {
            queue2.offer(queue1.poll());
            size--;
        }

        int res = queue1.poll();

        if (isForTop) {
            queue2.offer(res);
        }

        Queue<Integer> temp = queue1;

        queue1 = queue2;
        queue2 = temp;

        return res;
    }

    /**
     * Get the top element.
     */
    public int top() {
        if (queue1.isEmpty()) {
            System.out.println("ERROR .............");
        }

        return mainLogic(Boolean.TRUE);
    }

    /**
     * Returns whether the stack is empty.
     */
    public boolean empty() {
        return queue1.isEmpty();
    }


    public static void main(String[] args) {

        Map<Map.Entry<Integer, Integer>, Integer> map = new HashMap<>();

        MyStack obj = new MyStack();
        obj.push(1);
        obj.push(2);
        int top = obj.top();
        System.out.println(top);
        int pop = obj.pop();
        System.out.println(pop);
        System.out.println(obj.empty());
    }
}
