package DailyCodingProblem.SpriralMatrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

public class MatrixInSpiralForm {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int row = scanner.nextInt();
        int col = scanner.nextInt();

        int arr[][] = new int[row][col];

        for (int i = 0; i < row; i++) {
            int j = 0;
            StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
            while (stringTokenizer.hasMoreTokens()) {
                arr[i][j] = Integer.parseInt(stringTokenizer.nextToken());
                j++;
            }
        }

        printInSpiralForm(arr);

        System.out.println(arr);
    }

    private static void printInSpiralForm(int[][] arr) {
        int startRowIndex = 0;
        int endRowIndex = arr.length;
        int startColIndex = 0;
        int endColIndex = arr[0].length;

        while (startRowIndex < endRowIndex && startColIndex < endColIndex) {

            for (int i = startColIndex; i < endColIndex; i++) {
                System.out.println(arr[startRowIndex][i]);
            }
            startRowIndex++;
            for (int i = startRowIndex; i < endRowIndex; i++) {
                System.out.println(arr[i][endColIndex - 1]);
            }
            endColIndex--;
            if (startRowIndex < endRowIndex) {
                for (int i = endColIndex - 1; i >= startColIndex; i--) {
                    System.out.println(arr[endRowIndex - 1][i]);
                }
                endRowIndex--;
            }
            if (startColIndex < endColIndex) {
                for (int i = endRowIndex - 1; i >= startRowIndex; i--) {
                    System.out.println(arr[i][startColIndex]);
                }
                startColIndex++;
            }
        }
    }
}
