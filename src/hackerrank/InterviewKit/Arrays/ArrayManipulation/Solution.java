package hackerrank.InterviewKit.Arrays.ArrayManipulation;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the arrayManipulation function below.
    static long arrayManipulation1(int n, int[][] queries) {

        long[] arr = new long[n + 1];

        Set<Long> set = new TreeSet<>(Comparator.reverseOrder());

        for (int i = 0; i < queries.length; i++) {
            int low = queries[i][0];
            int high = queries[i][1];
            int k = queries[i][2];

            for (int j = low; j <= high; j++) {
                arr[j] += (long) k;
                set.add(arr[j]);
            }
        }

//        Arrays.sort(arr);
//
//        return arr[arr.length - 1];

        return ((TreeSet<Long>) set).first();
    }


    static long arrayManipulation(int n, int[][] queries) {
        long[] arr = new long[n + 1];

        Long max = Long.MIN_VALUE;

        for (int i = 0; i < queries.length; i++) {
            int low = queries[i][0];
            int high = queries[i][1];
            int k = queries[i][2];

            arr[low - 1] += k;
            arr[high] -= k;
        }

        long sum = arr[0];

        for (int i = 1; i < arr.length; i++) {
            sum += arr[i];
            max = Math.max(sum, max);
        }

        return max;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        String[] nm = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nm[0]);

        int m = Integer.parseInt(nm[1]);

        int[][] queries = new int[m][3];

        for (int i = 0; i < m; i++) {
            String[] queriesRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 3; j++) {
                int queriesItem = Integer.parseInt(queriesRowItems[j]);
                queries[i][j] = queriesItem;
            }
        }

        long result = arrayManipulation(n, queries);

        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
