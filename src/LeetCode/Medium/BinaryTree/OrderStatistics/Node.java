package LeetCode.Medium.BinaryTree.OrderStatistics;

public class Node {

    int val;
    int lCount;
    Node left;
    Node right;

    public Node(int val, int lCount) {
        this.val = val;
        this.lCount = lCount;
        this.left = null;
        this.right = null;
    }
}
