package Walmart.CourseSchedule;

import Walmart.RemoveNthNodeFromEnd.ListNode;

import java.util.*;

public class Solution {

    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        Map<Integer, Integer> indegree = new HashMap<>();
        Queue<Integer> sources = new LinkedList<>();
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < numCourses; i++) {
            graph.put(i, new ArrayList<>());
            indegree.put(i, 0);
        }

        for (int i = 0; i < prerequisites.length; i++) {
            int parent = prerequisites[i][1];
            int child = prerequisites[i][0];

            graph.get(parent).add(child);
            indegree.put(child, indegree.get(child) + 1);
        }

        for (Map.Entry<Integer, Integer> entry : indegree.entrySet()) {
            if (entry.getValue() == 0) {
                sources.offer(entry.getKey());
            }
        }

        while (!sources.isEmpty()) {
            Integer source = sources.poll();
            res.add(source);
            List<Integer> children = graph.get(source);
            for (int child : children) {
                indegree.put(child, indegree.get(child) - 1);
                if (indegree.get(child) == 0) {
                    sources.offer(child);
                }
            }
        }

        return res.size() == numCourses;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.canFinish(2, new int[][]{{1,0}}));
    }
}
