package hackerrank.Greedy.GreedyFlorist;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.stream.IntStream;

public class Solution {


    static class Node {
        int total;
        int price;

        public Node(int total, int price) {
            this.total = total;
            this.price = price;
        }
    }

    // Complete the getMinimumCost function below.
    static int getMinimumCost(int k, int[] c) {


//        int[] reverse = IntStream.of(c).boxed().sorted(Comparator.reverseOrder())
//                .mapToInt(i -> i).toArray();

        Arrays.sort(c);

        int result = 0;
        int customer = 0;
        int prevCount = 0;

        for (int i = c.length - 1; i >= 0; i--) {

            result += (c[i] * (prevCount + 1));

            customer++;

            if (customer == k) {
                customer = 0;
                prevCount++;
            }
        }

        return result;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/rajatagarwal/Learning/IdeaProjects/ds_n_algo/src/hackerrank/FileWriter.txt"));

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        int[] c = new int[n];

        String[] cItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int cItem = Integer.parseInt(cItems[i]);
            c[i] = cItem;
        }

        int minimumCost = getMinimumCost(k, c);


        System.out.println(minimumCost);

        bufferedWriter.write(String.valueOf(minimumCost));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}