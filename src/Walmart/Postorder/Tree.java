package Walmart.Postorder;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

class Node {
    int data;
    Node left, right;
    Node(int item)    {
        data = item;
        left = right = null;
    }
}

public class Tree {

    void postOrder(Node root) {
        if (null == root) {
            return;
        }

        List<Integer> result = new LinkedList<>();

        Stack<Node> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node pop = stack.pop();
            result.add(0, pop.data);

            if (null != pop.left) {
                stack.push(pop.left);
            }

            if (null != pop.right) {
                stack.push(pop.right);
            }
        }

        while (!result.isEmpty()) {
            System.out.print(((LinkedList<Integer>) result).removeFirst() + " ");
        }
    }

    public static void main(String[] args) {
        Tree tree = new Tree();

        Node root = new Node(1);
        root.left = new Node(2);
        root.left.left = new Node(3);
        root.left.right = new Node(4);
        root.left.left.left = new Node(5);
        root.left.left.right = new Node(6);
        root.left.left.right.left = new Node(7);
        root.left.left.right.left.left = new Node(8);

        tree.postOrder(root);
    }
}
