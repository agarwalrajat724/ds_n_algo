package GeekForGeeks.DSA_Class.Stacks.DeleteMid;

import java.util.Stack;

public class Geeks {


    //Complete this function
    public Stack<Integer> deleteMid(Stack<Integer> s, int sizeOfStack, int current) {
        //Your code here
        if (null == s || s.isEmpty() || current == sizeOfStack) {
            return s;
        }

        int pop = s.pop();

        deleteMid(s, sizeOfStack, current + 1);

        if (current != (sizeOfStack / 2)) {
            s.push(pop);
        }
        return s;
    }
}
