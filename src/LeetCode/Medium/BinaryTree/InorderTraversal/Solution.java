package LeetCode.Medium.BinaryTree.InorderTraversal;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


public class Solution {

    public List<Integer> inorderTraversal1(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        TreeNode current = root;
        current = pushToStack(stack, current);

        while (current == null && !stack.isEmpty()) {
            TreeNode pop = stack.pop();
            result.add(pop.val);
            current = pop.right;

            current = pushToStack(stack, current);
        }

        return result;
    }

    private TreeNode pushToStack(Stack<TreeNode> stack, TreeNode current) {

        while (null != current) {
            stack.push(current);
            current = current.left;
        }

        return current;
    }

    public List<Integer> inorderTraversal(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }

        TreeNode cur = root;
        Stack<TreeNode> stack = new Stack<>();
        List<Integer> res = new ArrayList<>();

        while (null != cur || !stack.isEmpty()) {
            while (null != cur) {
                stack.push(cur);
                cur = cur.left;
            }

            if (!stack.isEmpty()) {
                TreeNode pop = stack.pop();
                res.add(pop.val);
                cur = pop.right;
            }
        }

        return res;
    }

    public static void main(String[] args) {

        TreeNode treeNode = new TreeNode(1);

        treeNode.right = new TreeNode(2);
        treeNode.right.left = new TreeNode(3);

        Solution solution = new Solution();
        solution.inorderTraversal(treeNode).forEach(integer -> System.out.println(integer));
    }
}
