//package Grokking.Ultimate_Patterns.Fast_N_Slow;
//
//public class PalindromicLinkedList {
//
//    public static boolean isPalindrome(ListNode head) {
//        ListNode slow = head, fast = head;
//
//        while (null != fast && null != fast.next) {
//            fast = fast.next.next;
//            slow = slow.next;
//        }
//
//        ListNode curr = slow, prev = slow, succ = null;
//
//        while (null != curr) {
//            curr = curr.next;
//        }
//    }
//
//    public static void main(String[] args) {
//        ListNode head = new ListNode(2);
//        head.next = new ListNode(4);
//        head.next.next = new ListNode(6);
//        head.next.next.next = new ListNode(4);
//        head.next.next.next.next = new ListNode(2);
//        System.out.println("Is palindrome: " + PalindromicLinkedList.isPalindrome(head));
//
//        head.next.next.next.next.next = new ListNode(2);
//        System.out.println("Is palindrome: " + PalindromicLinkedList.isPalindrome(head));
//    }
//}
