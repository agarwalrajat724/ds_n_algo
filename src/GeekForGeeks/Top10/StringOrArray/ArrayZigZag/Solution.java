package GeekForGeeks.Top10.StringOrArray.ArrayZigZag;

import java.util.Arrays;

public class Solution {


    public static int[] zigzag(int[] arr) {
        boolean flag = Boolean.TRUE;
        for (int i = 0; i < arr.length - 2; i++) {
            if (flag) {
                if (arr[i] > arr[i + 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            } else {
                if (arr[i] < arr[i + 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }

            flag = !flag;
        }
        return arr;
    }


    public static void main(String[] args) {
        Arrays.stream(Solution.zigzag(new int[]{4, 3, 7, 8, 6, 2, 1})).forEach(i -> System.out.println(i));
    }
}
