package hackerrank.InterviewKit.Arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;


/**
 *
 *
 * 2
 * 5
 * 22 1 34 22 16
 * 22
 * 7
 * 3 5 3 5 5 11 5
 * 5
 */

public class TestClass {


    private void rearrange(int[] arr, int elem) {

        int no = 0;
        int[] auxArr = new int[arr.length];

        for (int i = 0; i < arr.length;i++) {
            if (arr[i] != elem) {

                auxArr[no++] = arr[i];
            }
        }

        int rem = arr.length - no;

        for (int i = 0; i < rem; i++) {
            arr[i] = -1;
        }
        int j = 0;
        for (int i = rem; i < arr.length;i++) {
            arr[i] = auxArr[j++];
        }
    }


    public static void main(String[] args) {

        TestClass solution = new TestClass();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(System.in);
        try {
            int T = Integer.parseInt(br.readLine());

            for (int k = 0; k < T; k++) {

             int n = Integer.parseInt(br.readLine().trim());

               // int n = scanner.nextInt();



                int arr[] = new int[n];
                StringTokenizer stringTokenizer = new StringTokenizer(br.readLine());
                for (int i = 0; i < n; i++) {
                    arr[i] = Integer.parseInt(stringTokenizer.nextToken());
                }

                int X = Integer.parseInt(br.readLine());


                solution.rearrange(arr, X);
                Arrays.stream(arr).boxed().forEach(integer -> System.out.print(integer + " "));
                System.out.println();

            }

//            String name = br.readLine();                // Reading input from STDIN
//            System.out.println("Hi, " + name + ".");    // Writing output to STDOUT
//
//            //Scanner
//            Scanner s = new Scanner(System.in);
//            //String name = s.nextLine();                 // Reading input from STDIN


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

