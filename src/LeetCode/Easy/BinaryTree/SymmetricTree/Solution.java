package LeetCode.Easy.BinaryTree.SymmetricTree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Symmetric Tree
 *   Go to Discuss
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 *
 * For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
 *
 *     1
 *    / \
 *   2   2
 *  / \ / \
 * 3  4 4  3
 *
 *
 * But the following [1,2,2,null,3,null,3] is not:
 *
 *     1
 *    / \
 *   2   2
 *    \   \
 *    3    3
 *
 *
 * Note:
 * Bonus points if you could solve it both recursively and iteratively
 */
public class Solution {

    public boolean isSymmetricIterative(TreeNode root) {
        if (null == root) {
            return true;
        }
        if (null == root.left && null == root.right) {
            return true;
        }
        if (null == root.left || null == root.right) {
            return false;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root.left);
        queue.offer(root.right);

        while (!queue.isEmpty()) {
            TreeNode left = queue.poll();
            TreeNode right = queue.poll();

            if (null == left && null == right) {
                continue;
            }

            if ((null != left && null == right) || (null == left && null != right)) {
                return false;
            }

            if (left.val != right.val) {
                return false;
            }

            queue.offer(left.left);
            queue.offer(right.right);
            queue.offer(left.right);
            queue.offer(right.left);
        }

        return true;
    }

    public boolean isSymmetric(TreeNode root) {
        if (null == root) {
            return true;
        }
        if (null == root.left && null == root.right) {
            return true;
        }
        if (null == root.left || null == root.right) {
            return false;
        }
        return isSymmetricUtil(root, root);
    }

    private boolean isSymmetricUtil(TreeNode left, TreeNode right) {
        if (null == left && null == right) {
            return true;
        }
        if (null == left || null == right) {
            return false;
        }

        if (left.val != right.val) {
            return false;
        }

        return isSymmetricUtil(left.left, right.right) && isSymmetricUtil(left.right, right.left);
    }
}
