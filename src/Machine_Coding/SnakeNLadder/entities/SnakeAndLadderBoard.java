package Machine_Coding.SnakeNLadder.entities;

import java.util.List;
import java.util.Map;

public class SnakeAndLadderBoard {

    private int size;
    private List<Move> snakes;
    private List<Move> ladders;
    private Map<String, Integer> playerPieces;

    public SnakeAndLadderBoard(int size, List<Move> snakes, List<Move> ladders, Map<String, Integer> playerPieces) {
        this.size = size;
        this.snakes = snakes;
        this.ladders = ladders;
        this.playerPieces = playerPieces;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Move> getSnakes() {
        return snakes;
    }

    public void setSnakes(List<Move> snakes) {
        this.snakes = snakes;
    }

    public List<Move> getLadders() {
        return ladders;
    }

    public void setLadders(List<Move> ladders) {
        this.ladders = ladders;
    }

    public Map<String, Integer> getPlayerPieces() {
        return playerPieces;
    }

    public void setPlayerPieces(Map<String, Integer> playerPieces) {
        this.playerPieces = playerPieces;
    }
}
