package LeetCode.Medium.BinaryTree.OrderStatistics;

import java.util.concurrent.atomic.AtomicInteger;

public class kthLargestElem {

    static class Node {
        int val;
        int rCount;
        Node left;
        Node right;

        public Node(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{20, 8, 22, 4, 12, 10, 14};
        Node root = null;
        for (int i = 0; i < arr.length; i++) {
            root = insert_node(root, new Node(arr[i]));
        }

        System.out.println(binarySearchKthLargestElem(root, 3));
        System.out.println(KthLargestElement(root, 3));
    }

    private static int KthLargestElement(Node root, int k) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        return KthLargestElement(root, atomicInteger, k);
    }

    private static int KthLargestElement(Node root, AtomicInteger atomicInteger, int k) {
        if (null == root) {
            return Integer.MIN_VALUE;
        }
        int right = KthLargestElement(root.right, atomicInteger, k);

        if (right != Integer.MIN_VALUE) {
            return right;
        }
        if (atomicInteger.incrementAndGet() == k) {
            return root.val;
        }
        return KthLargestElement(root.left, atomicInteger, k);
    }

    private static Node insert_node(Node root, Node new_node) {
        if (null == root) {
            root = new_node;
            return root;
        }

        Node traverse = root;
        Node current = root;

        while (null != traverse) {
            current = traverse;
            if (new_node.val < traverse.val) {
                traverse = traverse.left;
            } else {
                traverse.rCount++;
                traverse = traverse.right;
            }
        }

        if (new_node.val < current.val) {
            current.left = new_node;
        } else {
            current.right = new_node;
        }

        return root;
    }

    private static int binarySearchKthLargestElem(Node root, int k) {

        if (null != root) {
            Node current = root;

            while (null != current) {
                if (k == (current.rCount + 1)) {
                    return current.val;
                } else if (k > current.rCount) {
                    k = k - (current.rCount + 1);
                    current = current.left;
                } else {
                    current = current.right;
                }
            }
        }

        return -1;
    }
}
