package CollectionDesign.Map;

import java.util.*;
import java.util.stream.Stream;

public class Test {


    public static void main(String[] args) {
        Map<Integer, Integer> map = new HashMap<>();
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            map.put(i, random.nextInt(100));
        }

        map.forEach((k, v) -> {
            System.out.println("Key : " + k + " Value : " + v);
        });


        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(map.size());
        list.addAll(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> e1, Map.Entry<Integer, Integer> e2) {
                return e1.getValue().compareTo(e2.getValue());
            }
        });


        Map<Integer, Integer> sortMap = new LinkedHashMap<>();

        list.forEach(entry -> {
            sortMap.put(entry.getKey(), entry.getValue());
        });

        System.out.println("##################################");

        sortMap.forEach((k,v) -> {
            System.out.println("Key : " + k + " Value : " + v);
        });


        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

        Map<Integer, Integer> sortMap1 = new LinkedHashMap<>();

        Stream<Map.Entry<Integer, Integer>> entryStream = map.entrySet().stream();


        entryStream.sorted(Map.Entry.comparingByKey()).forEachOrdered(i -> {
            sortMap1.put(i.getKey(), i.getValue());
        });

        entryStream.sorted(Map.Entry.comparingByValue()).forEachOrdered(i -> {
            sortMap.put(i.getKey(), i.getValue());
        });


        entryStream.sorted(Map.Entry.comparingByKey()).forEachOrdered(integerIntegerEntry -> {
            sortMap1.put(integerIntegerEntry.getKey(), integerIntegerEntry.getValue());
        });


        sortMap1.forEach((k, v) -> {
            System.out.println("Key : " + k + " Value : " + v);
        } );

        Map<Integer, Integer> sortMap2 = new LinkedHashMap<>();

        Stream<Map.Entry<Integer, Integer>> stream = map.entrySet().stream();

        stream.sorted(Map.Entry.comparingByValue()).forEachOrdered(c -> sortMap2.put(c.getKey(), c.getValue()));

        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

        sortMap2.forEach((k, v) -> {
            System.out.println("Key : " + k + " Value : " + v);
        } );

    }
}
