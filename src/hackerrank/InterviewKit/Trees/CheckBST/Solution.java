package hackerrank.InterviewKit.Trees.CheckBST;


class Node {
    int data;
    Node left;
    Node right;

    public Node(int data) {
        this.data = data;
    }
}

class PrevNode {
    Node prev;

    public PrevNode() {
        this.prev = null;
    }
}
public class Solution {


    boolean checkBST(Node root) {
        if (null == root) {
            return true;
        }

        return checkBSTUtil(root, null, null);
    }

    private boolean checkBSTUtil(Node root, Node left, Node right) {

        if (null == root) {
            return true;
        }

        if (null != left && left.data >= root.data) {
            return false;
        }

        if (null != right && right.data <= root.data) {
            return false;
        }

        return checkBSTUtil(root.left, left, root) && checkBSTUtil(root.right, root, right);
    }

    public boolean checkBSTInorder(Node root) {
        if (null == root) {
            return true;
        }

        Node prev = null;
        PrevNode prevNode = new PrevNode();
        return checkBSTInorderUtil(root, prevNode);
    }

    private boolean checkBSTInorderUtil(Node root, PrevNode prevNode) {

        if (null != root) {

            if (!checkBSTInorderUtil(root.left, prevNode)) {
                return false;
            }

            if (null != prevNode.prev && prevNode.prev.data > root.data) {
                return false;
            }

            prevNode.prev = root;

            return checkBSTInorderUtil(root.right, prevNode);
        }

        return true;
    }


    public static Node insert(Node root, int data) {

        if (null == root) {
            return new Node(data);
        } else {
            Node curr;

            if (data <= root.data) {
                curr = insert(root.left, data);
                root.left = curr;
            } else {
                curr = insert(root.right, data);
                root.right = curr;
            }
            return root;
        }
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        int arr[] = new int[]{8,4,9,1,2,3,6,5};
        Node root = null;
        for (int i = 0; i < arr.length; i++) {
            root = insert(root, arr[i]);
        }


        System.out.println(solution.checkBST(root));


        System.out.println(solution.checkBSTInorder(root));
    }
}
