package Grokking.Ultimate_Patterns.BinarySearch;

/**
 * Given an array of numbers sorted in ascending order, find the element in the array
 * that has the minimum difference with the given ‘key’.
 *
 * Input: [4, 6, 10], key = 7
 * Output: 6
 * Explanation: The difference between the key '7' and '6' is minimum than any other number in the array
 *
 * Input: [4, 6, 10], key = 4
 * Output: 4
 *
 * Input: [1, 3, 8, 10, 15], key = 12
 * Output: 10
 *
 * Input: [4, 6, 10], key = 17
 * Output: 10
 *
 */
public class MinimumDifference {


    public static int searchMinDiffElementBetter(int[] arr, int key) {

        if (key < arr[0]) {
            return arr[0];
        } else if (key > arr[arr.length - 1]) {
            return arr[arr.length - 1];
        }

        int low = 0, high = arr.length - 1;

        while (low <= high) {
            int mid = low + (high - low)/2;

            if (key < arr[mid]) {
                high = mid - 1;
            } else if (key > arr[mid]) {
                low = mid + 1;
            } else { // ELSE Condition very Important if need to access arr low or high by the index
                return arr[mid];
            }
        }

        if (Math.abs(arr[low] - key) < Math.abs(arr[high] - key)) {
            return arr[low];
        }
        return arr[high];
    }

    public static int searchMinDiffElement(int[] arr, int key) {
        int low = 0, high = arr.length - 1, minDiff = Integer.MAX_VALUE, minDiffElem = -1;

        while (low <= high) {
            int mid = low + (high - low)/2;
            if (Math.abs(arr[mid] - key) < minDiff) {
                minDiff = Math.abs(arr[mid] - key);
                minDiffElem = arr[mid];
            }
            if (arr[mid] == key) {
                return arr[mid];
            } else if (arr[mid] > key) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        return minDiffElem;
    }

    public static void main(String[] args) {
        System.out.println(MinimumDifference.searchMinDiffElementBetter(new int[] { 4, 6, 10 }, 7));
        System.out.println(MinimumDifference.searchMinDiffElementBetter(new int[] { 4, 6, 10 }, 4));
        System.out.println(MinimumDifference.searchMinDiffElementBetter(new int[] { 1, 3, 8, 10, 15 }, 12));
        System.out.println(MinimumDifference.searchMinDiffElementBetter(new int[] { 4, 6, 10 }, 17));
    }
}
