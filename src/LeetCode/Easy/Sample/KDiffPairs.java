package LeetCode.Easy.Sample;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 532. K-diff Pairs in an Array
 *
 * Input: [3, 1, 4, 1, 5], k = 2
 * Output: 2
 *
 * 1 1 3 4 5
 * Explanation: There are two 2-diff pairs in the array, (1, 3) and (3, 5).
 * Although we have two 1s in the input, we should only return the number of unique pairs.
 */
public class KDiffPairs {

    public int findPairs(int[] nums, int k) {

        Set<Integer> set = new HashSet<>();
        Arrays.sort(nums);
        int count = 0;
        for (int i = 0; i < nums.length; i++) {

//            while ((i + 1) < nums.length && nums[i] == nums[i+1]) {
//                i++;
//            }

            if (set.contains(nums[i] - k) || set.contains(nums[i] + k)) {
                // System.out.println("(" + nums[i] + "," + (nums[i] - k) + ")");
                count++;
            }

//            if (set.contains(nums[i] + k)) {
//                // System.out.println("(" + nums[i] + "," + (nums[i] + k) + ")");
//                count++;
//            }

            set.add(nums[i]);
        }

        return count;
    }

    public static void main(String[] args) {
        KDiffPairs kDiffPairs = new KDiffPairs();

        System.out.println(kDiffPairs.findPairs(new int[]{1, 2,3,4,5}, -1));
    }
}
