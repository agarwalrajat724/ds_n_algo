package hackerrank.InterviewKit.Arrays.NewYearChaos;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the minimumBribes function below.
    static void minimumBribes(int[] q) {
        int totalBribeCount = 0;
        int n = q.length;
        Map<Integer, Integer> map = new HashMap<>();
        boolean hasAllSorted = Boolean.FALSE;
        boolean tooChaotic = Boolean.FALSE;
        while (!hasAllSorted) {
            boolean hasMoreSwaps = Boolean.FALSE;
            for (int i = 0; i < n - 1; i++) {
                if (q[i] > q[i + 1]) {
                    hasMoreSwaps = Boolean.TRUE;
                    int temp = q[i];
                    q[i] = q[i + 1];
                    q[i + 1] = temp;
                    if (!map.containsKey(temp)) {
                        map.put(temp, 1);
                    } else {
                        if (map.get(temp) == 2) {
                            tooChaotic = Boolean.TRUE;
                            break;
                        }
                        map.put(temp, map.get(temp) + 1);
                    }
                    totalBribeCount++;
                }
            }
            if (tooChaotic) {
                break;
            }
            if (!hasMoreSwaps) {
                hasAllSorted = Boolean.TRUE;
            }
        }
        if (tooChaotic) {
            System.out.println("Too chaotic");
        } else {
            System.out.println(totalBribeCount);
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] q = new int[n];

            String[] qItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int qItem = Integer.parseInt(qItems[i]);
                q[i] = qItem;
            }

            minimumBribes(q);
        }

        scanner.close();
    }
}
