package LeetCode.BackTracking.Permutations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {


    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        permuteUtil(nums, 0, nums.length - 1, result);
        return result;
    }

    private void permuteUtil(int[] nums, int low, int high, List<List<Integer>> result) {
        if (low == high) {
            result.add(Arrays.stream(nums).boxed().collect(Collectors.toList()));
        } else {
            for (int i = low; i <= high; i++) {
                swap(nums, low, i);
                permuteUtil(nums, low + 1, high, result);
                swap(nums, low, i);
            }
        }
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.permute(new int[]{1,2,3}).forEach(integers -> {
            System.out.println("skdgsgdhj");
            integers.forEach(integer -> System.out.println(integer));
        });
    }
}
