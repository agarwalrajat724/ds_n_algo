package Grokking.Ultimate_Patterns.SlidingWindow;


import java.util.HashMap;
import java.util.Map;

/**
 * 6)
 * Given an array containing 0s and 1s, if you are allowed to replace no more than
 * ‘k’ 0s with 1s, find the length of the longest subarray having all 1s.
 *
 * Example 1:
 *
 * Input: Array=[0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1], k=2
 * Output: 6
 * Explanation: Replace the '0' at index 5 and 8 to have the longest subarray of 1s having length 6.
 * Example 2:
 *
 * Input: Array=[0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1], k=3
 * Output: 9
 * Explanation: Replace the '0' at index 6, 9, and 10 to have the longest subarray of 1s having length 9.
 *
 */
public class ReplacingOnes {

    public static int findLength(int[] arr, int k) {
        // TODO: Write your code here
        int windowStart = 0;
        int maxRepeatingNoOf_1_s = 0;
        int maxLength = Integer.MIN_VALUE;
        Map<Integer, Integer> numberFrequencyCount = new HashMap<>();

        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            int rightNumber = arr[windowEnd];
            numberFrequencyCount.put(rightNumber, numberFrequencyCount.getOrDefault(rightNumber, 0) + 1);

            if (numberFrequencyCount.containsKey(1)) {
                maxRepeatingNoOf_1_s = Math.max(maxRepeatingNoOf_1_s, numberFrequencyCount.get(1));
            }

            if ((windowEnd - windowStart + 1 - maxRepeatingNoOf_1_s) > k) {
                int leftNumber = arr[windowStart];
                if (numberFrequencyCount.containsKey(leftNumber)) {
                    numberFrequencyCount.put(leftNumber, numberFrequencyCount.get(leftNumber) - 1);
                    if (numberFrequencyCount.get(leftNumber) == 0) {
                        numberFrequencyCount.remove(leftNumber);
                    }
                }
                windowStart++;
            }

            maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
        }
        return maxLength;
    }

    public static int findLengthBetter(int[] arr, int k) {
        int windowStart = 0;
        int maxLength = Integer.MIN_VALUE;
        int maxOnesCount = 0;

        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            if (arr[windowEnd] == 1) {
                maxOnesCount++;
            }

            if ((windowEnd - windowStart + 1 - maxOnesCount) > k) {
                if (arr[windowStart] == 1) {
                    maxOnesCount--;
                }
                windowStart++;
            }
            maxLength = Math.max(maxLength, windowEnd - windowStart + 1);
        }
        return maxLength;
    }

    public static void main(String[] args) {
        System.out.println(ReplacingOnes.findLength(new int[] { 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1 }, 2));
        System.out.println(ReplacingOnes.findLength(new int[] { 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1 }, 3));

        System.out.println(ReplacingOnes.findLengthBetter(new int[] { 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1 }, 2));
        System.out.println(ReplacingOnes.findLengthBetter(new int[] { 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1 }, 3));
    }
}
