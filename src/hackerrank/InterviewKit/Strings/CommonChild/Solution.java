package hackerrank.InterviewKit.Strings.CommonChild;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {


    // Complete the commonChild function below.
    static int commonChild(String s1, String s2) {

        char[] X = s1.toCharArray();
        char[] Y = s2.toCharArray();

        return lcs(X, Y, X.length, Y.length);
    }

    private static int lcs(char[] X, char[] Y, int M, int N) {

        int L[][] = new int[M + 1][N + 1];

        for (int i = 1; i <= M; i++) {
            for (int j = 1; j <= N; j++) {
//                if (i == 0 || j == 0) {
//                    L[i][j] = 0;
//                } else
                    if (X[i - 1] == Y[j - 1]) {
                    L[i][j] = L[i - 1][j - 1] + 1;
                } else {
                    L[i][j] = Math.max(L[i-1][j], L[i][j-1]);
                }
            }
        }

        return L[M][N];
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        String s1 = scanner.nextLine();

        String s2 = scanner.nextLine();

        int result = commonChild(s1, s2);

        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
