package Grokking.Ultimate_Patterns.TwoHeaps;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Given an array of numbers and a number ‘k’, find the
 * median of all the ‘k’ sized sub-arrays (or windows) of the array.
 */
public class SlidingWindowMedian {

    public double[] findSlidingWindowMedian(int[] nums, int k) {
        double[] result = new double[nums.length - k + 1];
        // TODO: Write your code here

        Queue<Integer> maxHeap = new PriorityQueue<>((a, b) -> (b - a));
        Queue<Integer> minHeap = new PriorityQueue<>((a, b) -> (a - b));
        int j = 0;
        for (int i = 0; i < nums.length; i++) {


            if (maxHeap.isEmpty() || maxHeap.peek() <= nums[i]) {
                maxHeap.offer(nums[i]);
            } else {
                minHeap.offer(nums[i]);
            }

            if (maxHeap.size() > minHeap.size() + 1) {
                minHeap.offer(maxHeap.poll());
            } else if (minHeap.size() > maxHeap.size()) {
                maxHeap.offer(minHeap.poll());
            }

            if ((i - k + 1) >= 0) {

                if (maxHeap.size() == minHeap.size()) {
                    result[j] = (maxHeap.peek() + minHeap.peek())/2.0;
                } else {
                    result[j] = maxHeap.peek();
                }

                int elementToBeRemoved = nums[i- k + 1];

                if (elementToBeRemoved <= maxHeap.peek()) {
                    maxHeap.remove(elementToBeRemoved);
                } else {
                    minHeap.remove(elementToBeRemoved);
                }
                j++;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        SlidingWindowMedian slidingWindowMedian = new SlidingWindowMedian();
        double[] result = slidingWindowMedian.findSlidingWindowMedian(new int[] { 1, 2, -1, 3, 5 }, 2);
        System.out.print("Sliding window medians are: ");
        for (double num : result)
            System.out.print(num + " ");
        System.out.println();

        slidingWindowMedian = new SlidingWindowMedian();
        result = slidingWindowMedian.findSlidingWindowMedian(new int[] { 1, 2, -1, 3, 5 }, 3);
        System.out.print("Sliding window medians are: ");
        for (double num : result)
            System.out.print(num + " ");
    }
}

/**
 * The time complexity of our algorithm is O(N*K)O(N∗K) where ‘N’ is the total number of elements in the
 * input array and ‘K’ is the size of the sliding window. This is due to the fact that we are going through
 * all the ‘N’ numbers and, while doing so, we are doing two things:
 *
 * Inserting/removing numbers from heaps of size ‘K’. This will take O(logK)O(logK)
 * Removing the element going out of the sliding window. This will take O(K)O(K) as we will be searching this
 * element in an array of size ‘K’ (i.e., a heap).
 * Space complexity
 * The space complexity will be O(K)O(K) because, at any time, we will be storing all the numbers within the sliding window.
 */
