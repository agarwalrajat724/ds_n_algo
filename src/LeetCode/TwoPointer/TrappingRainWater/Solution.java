package LeetCode.TwoPointer.TrappingRainWater;

public class Solution {

    public int trap(int[] height) {
        if (null == height || height.length < 2) {
            return 0;
        }
        int n = height.length;
        int left[] = new int[n];
        int right[] = new int[n];
        left[0] = height[0];
        for (int i = 1; i < n; i++) {
            left[i] = Math.max(left[i - 1], height[i]);
        }
        right[n - 1] = height[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            right[i] = Math.max(right[i + 1], height[i]);
        }
        int trappedWater = 0;
        for (int i = 0; i < n; i++) {
            trappedWater += Math.min(left[i], right[i]) - height[i];
        }
        return trappedWater;
    }

    public int trapBetterApproach(int[] height) {
        int low = 0;
        int high = height.length - 1;

        int left_max = 0, right_max = 0;
        int result = 0;

        while (low <= high) {

            if (height[low] < height[high]) {
                if (height[low] > left_max) {
                    left_max = height[low];
                } else {
                    result += left_max - height[low];
                }
                low++;
            } else {
                if (height[high] > right_max) {
                    right_max = height[high];
                } else {
                    result += right_max - height[high];
                }

                high--;
            }
        }

        return result;
    }


    public static void main(String[] args) {
        System.out.println(new Solution().trapBetterApproach(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
    }
}
