package LeetCode.Medium.BinaryTree.BinaryTreeLevelOrderTraversal;

import LeetCode.Medium.BinaryTree.TreeNode;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * 102. Binary Tree Level Order Traversal
 * <p>
 * Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
 * <p>
 * For example:
 * Given binary tree [3,9,20,null,null,15,7],
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * return its level order traversal as:
 * [
 * [3],
 * [9,20],
 * [15,7]
 * ]
 */
public class Solution {

    public List<List<Integer>> levelOrderDFS(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        DFS(root, 1, result);
        return result;
    }

    public List<List<Integer>> levelOrderIIDFS(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        DFS(root, 1, result);
        Collections.reverse(result);
        return result;
    }

    private void DFS(TreeNode root, int depth, List<List<Integer>> result) {

        if (null == root) {
            return;
        }

        if (result.size() < depth) {
            result.add(new ArrayList<>());
        }

        result.get(depth - 1).add(root.val);

        if (null != root.left) {
            DFS(root.left, depth + 1, result);
        }

        if (null != root.right) {
            DFS(root.right, depth + 1, result);
        }
    }

    public List<List<Integer>> levelOrder(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> results = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> items = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();
                items.add(pop.val);

                if (pop.left != null) {
                    queue.offer(pop.left);
                }

                if (pop.right != null) {
                    queue.offer(pop.right);
                }
            }

            if (!items.isEmpty()) {
                results.add(items);
            }
        }

        return results;
    }

    public List<Double> averageOfLevels(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> results = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> items = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();
                items.add(pop.val);

                if (pop.left != null) {
                    queue.offer(pop.left);
                }

                if (pop.right != null) {
                    queue.offer(pop.right);
                }
            }

            if (!items.isEmpty()) {
                results.add(items);
            }
        }

        List<Double> ans = new ArrayList<>();
        if (!results.isEmpty()) {
            ans.add(results.get(0).get(0).doubleValue());
            for (int i = 1; i < results.size(); i++) {
                Double sum = results.get(i).stream().collect(Collectors.averagingInt(Integer::intValue));
                ans.add(sum);
            }
        }

        return ans;
    }

    public static void main(String[] args) {

        Solution solution = new Solution();
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);

        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);

        List<List<Integer>> result = solution.levelOrder(root);

        List<Double> avg = solution.averageOfLevels(root);

        System.out.println(avg);

        result.forEach(integers -> integers.forEach(integer -> System.out.println(integer)));
    }
}
