package LeetCode.BackTracking.LetterCasePermutation;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public List<String> letterCasePermutation(String S) {

        if (null == S || S.length() < 1) {
            return new ArrayList<>();
        }

        List<String> result = new ArrayList<>();
        letterCasePermutation(0, S, new char[S.length()], result);
        return result;
    }

    private void letterCasePermutation(int d, String S, char[] cur, List<String> ans) {

        if (d == S.length()) {
            ans.add(new String(cur));
            return;
        }

        if (!Character.isDigit(S.charAt(d))) {
            char c = Character.toLowerCase(S.charAt(d));
            cur[d] = c;
            letterCasePermutation(d + 1, S, cur, ans);
            cur[d] = Character.toUpperCase(S.charAt(d));
            letterCasePermutation(d + 1, S, cur, ans);
        } else {
            cur[d] = S.charAt(d);
            letterCasePermutation(d + 1, S, cur, ans);
        }
    }


    public static void main(String[] args) {


        Solution solution = new Solution();

        solution.letterCasePermutation("a1b2").forEach(s -> System.out.println(s));

    }
}
