package Grokking.Ultimate_Patterns.Subsets;

public class MaximumSubstring {

    public static String maxSubstring(String s) {
        char[] input = s.toCharArray();
        Character previousChar = null;
        StringBuilder longestSubstring = new StringBuilder();
        StringBuilder currentString = new StringBuilder();

        for (int i = 0; i < input.length ; i++) {
            if (null != previousChar && previousChar.compareTo(input[i]) >= 0) {
                currentString.append(input[i]);
                if (longestSubstring.length() <= currentString.length()) {
                    longestSubstring = new StringBuilder(currentString);
                }
            } else {
                currentString = new StringBuilder();
                currentString.append(input[i]);
            }
            previousChar = input[i];
        }

//        if (currentString.length() > longestSubstring.length()) {
//            return currentString.toString();
//        }

        return longestSubstring.toString();
    }

    public static void main(String[] args) {
        System.out.println(maxSubstring("ba"));
    }
}
