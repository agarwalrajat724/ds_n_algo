package Walmart.ShortestDistanceBetweenNodes;

public class Node {
    int val;
    Node left, right;

    public Node(int val) {
        this.val = val;
    }
}
