package LeetCode.BinaryTree.IncreasingOrderSearchTree;

import java.util.Stack;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int data) {
            this.val = data;
        }
    }

    public TreeNode increasingBST(TreeNode root) {
        if (null == root) {
            return root;
        }

        TreeNode result = null;
        TreeNode res = null;
        TreeNode current = root;

        Stack<TreeNode> stack = new Stack<>();
        current = pushToStack(current, stack);

        while (null == current && !stack.isEmpty()) {
            TreeNode pop = stack.peek();
            stack.pop();
            if (null == result) {
                result = new TreeNode(pop.val);
                res = result;
                while (result.right != null) {
                    result = result.right;
                }
            } else {
                while (null != result.right) {
                    result = result.right;
                }
                result.right = new TreeNode(pop.val);
            }
            current = pop.right;

            current = pushToStack(current, stack);
        }

        return res;
    }

    private TreeNode pushToStack(TreeNode current, Stack<TreeNode> stack) {
        while (null != current) {
            stack.push(current);
            current = current.left;
        }
        return current;
    }


    TreeNode curr;

    public TreeNode increasingBSTBetter(TreeNode root) {
        TreeNode ans = new TreeNode(0);
        curr = ans;

        inorder(root);

        return ans.right;
    }

    private void inorder(TreeNode root) {
        if (null != root) {
            inorder(root.left);

            root.left = null;
            curr.right = root;
            curr = root;
            inorder(root.right);

        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
        root.left.left = new TreeNode(2);
        root.left.left.left = new TreeNode(1);
        root.right = new TreeNode(6);
        root.right.right = new TreeNode(8);
        root.right.right.left = new TreeNode(7);
        root.right.right.right = new TreeNode(9);

        TreeNode result = solution.increasingBSTBetter(root);

        while (result != null) {
            System.out.println(result.val);
            result = result.right;
        }
    }
}
