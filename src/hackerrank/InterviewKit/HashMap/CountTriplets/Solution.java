package hackerrank.InterviewKit.HashMap.CountTriplets;


import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * https://www.hackerrank.com/challenges/count-triplets-1/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=dictionaries-hashmaps
 */
public class Solution {


    // Complete the countTriplets function below.
    static long countTriplets(List<Long> arr, long r) {
        long ans = 0l;

        Map<Long, Long> left = new HashMap<>(), right = new HashMap<>();

        for (Long elem : arr) {
            right.put(elem, right.getOrDefault(elem, 0l) + 1);
        }

        for (Long elem : arr) {
            long c1 = 0l;
            long c2 = 0l;

            if ((elem % r) == 0) {
                c1 = left.getOrDefault(elem / r, 0l);
            }
            if (right.containsKey(elem)) {
                long val = right.get(elem);
                if (val < 2) {
                    right.remove(elem);
                } else {
                    right.put(elem, val - 1);
                }
            }
            if (right.containsKey(elem * r)) {
                c2 = right.getOrDefault(elem * r, 0l);
            }
            ans += (c1 * c2);
            left.put(elem, left.getOrDefault(elem, 0l) + 1);
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(nr[0]);

        long r = Long.parseLong(nr[1]);

        List<Long> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Long::parseLong)
                .collect(toList());

        long ans = countTriplets(arr, r);

        System.out.println(ans);

        bufferedWriter.write(String.valueOf(ans));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
