package LeetCode.Easy.BinaryTree.NAryLevelOrderTraversal;

import java.lang.reflect.Array;
import java.util.*;

/**
 *429. N-ary Tree Level Order Traversal
 *
 * Given an n-ary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
 *
 * For example, given a 3-ary tree:
 *      1
 *
 *  3   2   4
 *
 *
 *  5   6
 *
 * We should return its level order traversal:
 *
 *
 *
 *
 *
 * [
 *      [1],
 *      [3,2,4],
 *      [5,6]
 * ]
 *
 *
 * Note:
 *
 * The depth of the tree is at most 1000.
 * The total number of nodes is at most 5000.
 */
public class Solution {

    static class Node {

        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val, List<Node> _children) {
            this.val = _val;
            this.children = _children;
        }
    }

    public List<List<Integer>> levelOrderDFS(Node root) {
        List<List<Integer>> result = new ArrayList<>();
        DFS(root, 1, result);
        return result;
    }

    private void DFS(Node root, int depth, List<List<Integer>> result) {

        if (null == root) {
            return;
        }

        if (result.size() < depth) {
            result.add(new ArrayList<>());
        }

        result.get(depth - 1).add(root.val);

        if (null != root.children && !root.children.isEmpty()) {
            for (Node node : root.children) {
                DFS(node, depth + 1, result);
            }
        }
    }

    public List<List<Integer>> levelOrder(Node root) {

        if (null == root) {
            return new ArrayList<>();
        }

        List<List<Integer>> result = new ArrayList<>();

        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> items = new ArrayList<>();

            for (int i = 0; i < size; i++) {
                Node pop = queue.poll();
                items.add(pop.val);
                List<Node> children = pop.children;
                if (null != children && !children.isEmpty()) {
                    for (Node node : children) {
                        queue.offer(node);
                    }
                }
            }

            if (!items.isEmpty()) {
                result.add(items);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        List<Node> chilLeft = new ArrayList<>();
        chilLeft.add(new Node(5, null));
        chilLeft.add(new Node(6, null));
        Node node = new Node(3, chilLeft);

        List<Node> allChild = new ArrayList<>();
        allChild.add(node);
        allChild.add(new Node(2, null));
        allChild.add(new Node(4, null));

        Node root = new Node(1, allChild);

//        List<List<Integer>> result = solution.levelOrder(root);
//
//        result.forEach(integers -> integers.forEach(integer -> System.out.println(integer)));


        System.out.println("DFS Approach");

        List<List<Integer>> result = solution.levelOrderDFS(root);

        result.forEach(integers -> integers.forEach(integer -> System.out.println(integer)));
    }
}
