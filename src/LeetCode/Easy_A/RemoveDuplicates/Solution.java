package LeetCode.Easy_A.RemoveDuplicates;

public class Solution {

    public int removeDuplicates(int[] nums) {
        if(null == nums) {
            return 0;
        }
        if (nums.length < 2) {
            return 1;
        }

        int i = 1, j = 0;

        while (i < nums.length) {
            if (nums[i] == nums[j]) {
                i++;
                j++;
            } else {
                int temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
                i++;
            }
        }
        return j + 1;
     }
}
