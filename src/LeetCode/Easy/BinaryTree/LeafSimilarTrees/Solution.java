package LeetCode.Easy.BinaryTree.LeafSimilarTrees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * 872. Leaf-Similar Trees
 *
 * Consider all the leaves of a binary tree.  From left to right order, the values of those leaves form a leaf value sequence.
 *
 *
 *
 * For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).
 *
 * Two binary trees are considered leaf-similar if their leaf value sequence is the same.
 *
 * Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.
 *
 *
 *
 * Note:
 *
 * Both of the given trees will have between 1 and 100 nodes.
 */
public class Solution {


    static class TreeNode {
        int val;

        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }


    public boolean leafSimilar(TreeNode root1, TreeNode root2) {

        if (null == root1 && null == root2) {
            return true;
        } else if (null == root1 && null != root2) {
            return false;
        } else if (null != root1 && null == root2) {
            return false;
        } else {

            //List<Integer> leaf1 = getLeaftNodes(root1);

            List<Integer> leaf1 = new ArrayList<>();
            getLeafNodesRecursive(root1, leaf1);

            List<Integer> leaf2 = new ArrayList<>();
            getLeafNodesRecursive(root2, leaf2);


            if (leaf1.size() == leaf2.size()) {

                return leaf1.equals(leaf2);
            } else {
                return false;
            }
        }

    }

    private void getLeafNodesRecursive(TreeNode root, List<Integer> result) {

        if (null == root) {
            return;
        }

        if (null == root.left && null == root.right) {
            result.add(root.val);
            return;
        }

        getLeafNodesRecursive(root.left, result);

        getLeafNodesRecursive(root.right, result);

    }

    private List<Integer> getLeaftNodes(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();

        ((LinkedList<TreeNode>) queue).push(root);

        while (!queue.isEmpty()) {

            TreeNode pop = queue.poll();

            if (null != pop.left) {
                queue.offer(pop.left);
            }

            if (null != pop.right) {
                queue.offer(pop.right);
            }

            if (null == pop.left && null == pop.right) {
                result.add(pop.val);
            }
        }

        return result;

    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(5);
        root.right = new TreeNode(1);

        root.left.left = new TreeNode(6);
        root.left.right = new TreeNode(2);
        root.left.right.left = new TreeNode(7);
        root.left.right.right = new TreeNode(4);

        root.right.left = new TreeNode(9);
        root.right.right = new TreeNode(8);

        solution.leafSimilar(root, new TreeNode(1));
    }
}
