package LeetCode.BinaryTree.SerializeNDeserializeBT;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SNDBinaryTree {


    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }


    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {

        if (null == root) {
            return "null,";
        }

        StringBuilder stringBuilder = new StringBuilder();

        rSerialize(root, stringBuilder);

        return stringBuilder.toString();
    }

    private void rSerialize(TreeNode root, StringBuilder stringBuilder) {
        if (null == root) {
            stringBuilder.append("null,");
        } else {
            stringBuilder.append(Integer.valueOf(root.val) + ",");
            rSerialize(root.left, stringBuilder);
            rSerialize(root.right, stringBuilder);
        }
    }

    // Decodes your encoded val to tree.
    public TreeNode deserialize(String data) {
        String[] arr = data.split(",");

        List<String> arrList = Arrays.stream(arr).collect(Collectors.toList());

        return rDeserialize(arrList);
    }

    private TreeNode rDeserialize(List<String> arrList) {

        if (arrList.get(0).equals("null")) {
            arrList.remove(0);
            return null;
        }

        TreeNode root = new TreeNode(Integer.valueOf(arrList.get(0)));
        arrList.remove(0);

        root.left = rDeserialize(arrList);
        root.right = rDeserialize(arrList);

        return root;
    }


    public static void main(String[] args) {
        SNDBinaryTree sndBinaryTree = new SNDBinaryTree();

        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(1);
        root.right = new TreeNode(4);
        root.left.right = new TreeNode(2);

        String s = sndBinaryTree.serialize(root);

        root = sndBinaryTree.deserialize(s);

        System.out.println(root);
    }
}
