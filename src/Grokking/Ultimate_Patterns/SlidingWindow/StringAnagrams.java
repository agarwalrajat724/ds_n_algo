package Grokking.Ultimate_Patterns.SlidingWindow;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * String Anagrams (hard)
 * Given a string and a pattern, find all anagrams of the pattern in the given string.
 *
 * Anagram is actually a Permutation of a string. For example, “abc” has the following six anagrams:
 *
 * abc
 * acb
 * bac
 * bca
 * cab
 * cba
 * Write a function to return a list of starting indices of the anagrams of the pattern in the given string.
 *
 *
 * Input: String="ppqp", Pattern="pq"
 * Output: [1, 2]
 * Explanation: The two anagrams of the pattern in the given string are "pq" and "qp".
 *
 * Input: String="abbcabc", Pattern="abc"
 * Output: [2, 3, 4]
 * Explanation: The three anagrams of the pattern in the given string are "bca", "cab", and "abc".
 *
 *
 */
public class StringAnagrams {

    public static List<Integer> findStringAnagrams(String str, String pattern) {
        List<Integer> resultIndices = new ArrayList<Integer>();
        int windowStart = 0, matchedCount = 0;
        Map<Character, Integer> characterFrequencyMap = new HashMap<>();

        char[] input = str.toCharArray();
        char[] patt = pattern.toCharArray();

        for (char ch : patt) {
            characterFrequencyMap.put(ch, characterFrequencyMap.getOrDefault(ch, 0) + 1);
        }

        for (int windowEnd = 0; windowEnd < input.length; windowEnd++) {
            char rightChar = input[windowEnd];
            if (characterFrequencyMap.containsKey(rightChar)) {
                characterFrequencyMap.put(rightChar, characterFrequencyMap.get(rightChar) - 1);
                if (characterFrequencyMap.get(rightChar) == 0) {
                    matchedCount++;
                }
            }
            if (matchedCount == characterFrequencyMap.size()) {
                resultIndices.add(windowStart);
            }

            if (windowEnd >= (patt.length - 1)) {
                char leftChar = input[windowStart];
                if (characterFrequencyMap.containsKey(leftChar)) {
                    if (characterFrequencyMap.get(leftChar) == 0) {
                        matchedCount--;
                    }
                    characterFrequencyMap.put(leftChar, characterFrequencyMap.get(leftChar) + 1);
                }
                windowStart++;
            }
        }
        return resultIndices;
    }

    public static void main(String[] args) {
        System.out.println(StringAnagrams.findStringAnagrams("ppqp", "pq"));
        System.out.println(StringAnagrams.findStringAnagrams("abbcabc", "abc"));
    }
}
