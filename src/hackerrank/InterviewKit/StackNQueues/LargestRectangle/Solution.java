package hackerrank.InterviewKit.StackNQueues.LargestRectangle;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class Solution {



    // Complete the largestRectangle function below.
    static long largestRectangle(int[] h) {

        if (null == h) {
            return 0l;
        }

        if (h.length == 1) {
            return h[0];
        }

        Stack<Integer> stack = new Stack<>();
        long maxArea = h[0];

        int i = 0;

        while (i < h.length) {

            if (stack.isEmpty() || (h[i] > h[stack.peek()]) ) {
                stack.push(i++);
            } else {
                int top = stack.pop();

                long area = h[top] * (stack.isEmpty() ? i : (i - stack.peek() - 1));

                maxArea = Math.max(maxArea, area);
            }
        }

        while (!stack.isEmpty()) {
            int top = stack.pop();

            long area = h[top] * (stack.isEmpty() ? i : (i - stack.peek() - 1));

            maxArea = Math.max(maxArea, area);
        }
        return maxArea;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] h = new int[n];

        String[] hItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int hItem = Integer.parseInt(hItems[i]);
            h[i] = hItem;
        }

        long result = largestRectangle(h);

        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }


}
