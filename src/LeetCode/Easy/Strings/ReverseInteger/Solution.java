package LeetCode.Easy.Strings.ReverseInteger;

public class Solution {

    /**
     *
     * @param num
     * @return
     */
    public int simpleReverse(int num) {
        int rev_num = 0;
        boolean isNeg = Boolean.FALSE;

        if (num < 0) {
            isNeg = Boolean.TRUE;
            num = -num;
        }
        while (num > 0) {
            rev_num = rev_num * 10 + (num % 10);
            num = num/10;
        }

        rev_num = isNeg? -rev_num : rev_num;

        return rev_num;
    }

    public int reverse(int x) {
        boolean isNeg = Boolean.FALSE;
        if (x < 0) {
            isNeg = Boolean.TRUE;
            x = -x;
        }

        int rev_num = 0, prev_num = 0;

        while (x != 0) {
            int curr_digit = x % 10;

            rev_num = (rev_num * 10) + curr_digit;

            if (((rev_num - curr_digit)/ 10) != prev_num) {
                return 0;
            }

            prev_num = rev_num;
            x = x/10;
        }
        return (isNeg ? -rev_num : rev_num);
    }

    public int simpleReverseRecursive(int num) {
        return 0;
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        System.out.println(solution.simpleReverse(-123));

        System.out.println(solution.reverse(123));
        System.out.println(solution.reverse(-123));
    }
}
