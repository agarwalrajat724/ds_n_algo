package LeetCode.Hard.BinaryTree.PostOrder;

import LeetCode.Hard.BinaryTree.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Given a binary tree, return the postorder traversal of its nodes' values.
 *
 * Example:
 *
 * Input: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * Output: [3,2,1]
 */
public class Solution {


    public List<Integer> postorderTraversal(TreeNode root) {

        if (null == root) {

            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {

            TreeNode pop = stack.pop();

            result.add(pop.val);

            if (null != pop.left) {
                stack.push(pop.left);
            }

            if (null != pop.right) {
                stack.push(pop.right);
            }
        }
        Collections.reverse(result);
        return result;
    }

    public List<Integer> postorder(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode current = root;
        boolean isDone = Boolean.FALSE;
        while (null != current || !stack.isEmpty()) {
            while (null != current) {
                if (null != current.right) {
                    stack.push(current.right);
                }
                stack.push(current);
                current = current.left;
            }

            if (null == current && !stack.isEmpty()) {
                TreeNode pop = stack.pop();
                if (!stack.isEmpty() && pop.right == stack.peek()) {
                    TreeNode popRight = stack.pop();
                    stack.push(pop);
                    current = popRight;
                } else {
                    result.add(pop.val);
                    current = null;
                }
            }
        }

        return result;
    }


    public static void main(String[] args) {

        Solution solution = new Solution();
        TreeNode node = new TreeNode(1);

        node.right = new TreeNode(2);
        node.right.left = new TreeNode(3);

//        TreeNode node = new TreeNode(1);
//        node.left = new TreeNode(2);
//        node.right = new TreeNode(3);
//
//        node.left.left = new TreeNode(4);
//        node.left.right = new TreeNode(5);
//
//        node.right.left = new TreeNode(6);
//        node.right.right = new TreeNode(7);

        List<Integer> result = solution.postorderTraversal(node);

        result.forEach(integer -> System.out.println(integer));

        solution.postorder(node);
    }
}
