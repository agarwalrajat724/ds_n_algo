package CondinGame.Temperature;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 *
 *
 * 5
 * 1 -2 -8 4 5
 */

public class Solution {


    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of temperatures to analyse

        int diff = Integer.MAX_VALUE;
        int res = 0;

        for (int i = 0; i < n; i++) {
            int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526

            int val = Math.abs(t - 0);



            if (diff > val) {
                diff = val;
                res = t;
            } else if (diff == val) {
                if (res < 0 && t > 0) {
                    res = t;
                }
            }
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(res);
    }
}
