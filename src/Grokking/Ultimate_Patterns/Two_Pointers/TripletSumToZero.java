package Grokking.Ultimate_Patterns.Two_Pointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Given an array of unsorted numbers, find all unique triplets in it that add up to zero.
 *
 * Input: [-3, 0, 1, 2, -1, 1, -2]
 * Output: [-3, 1, 2], [-2, 0, 2], [-2, 1, 1], [-1, 0, 1]
 * Explanation: There are four unique triplets whose sum is equal to zero.
 *
 * Input: [-5, 2, -1, -2, 3]
 * Output: [[-5, 2, 3], [-2, -1, 3]]
 * Explanation: There are two unique triplets whose sum is equal to zero.
 *
 */
public class TripletSumToZero {

    public static List<List<Integer>> searchTriplets(int[] arr) {
        List<List<Integer>> triplets = new ArrayList<>();
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 2; i++) {
            if (i > 0 && arr[i] == arr[i - 1]) {
                continue;
            }

            int low = i + 1, high = arr.length - 1;
            while (low < high) {

                int targetSum = -arr[i];
                int targetDiff = targetSum - arr[low];

                if ((arr[i] + arr[low] + arr[high]) == 0) {
                    triplets.add(Arrays.asList(-targetSum, arr[low], arr[high]));
                    low++;
                    high--;

                    while (low < high && arr[low] == arr[low - 1]) {
                        low++;
                    }
                    while (low < high && arr[high] == arr[high + 1]) {
                        high--;
                    }
                } else if ((arr[i] + arr[low] + arr[high]) < 0) {
                    low++;
                } else {
                    high--;
                }
            }
        }
        return triplets;
    }

    public static List<List<Integer>> searchTripletsTest(int[] arr) {
        List<List<Integer>> triplets = new ArrayList<>();
        Arrays.sort(arr);

        for (int i = 0; i < arr.length - 2; i++) {
            if (i > 0 && arr[i - 1] == arr[i]) {
                continue;
            }

            int low = i + 1, high = arr.length - 1;

            while (low < high) {
                int targetDiff = -arr[i] - arr[low];
                if (targetDiff == arr[high]) {
                    triplets.add(Arrays.asList(arr[i], arr[low], arr[high]));
                    low++;
                    high--;

                    while (low < high && arr[low - 1] == arr[low]) {
                        low++;
                    }

                    while (low < high && arr[high + 1] == arr[high]) {
                        high--;
                    }
                } else if (targetDiff > arr[high]) {
                    low++;
                } else {
                    high--;
                }
            }
        }

        return triplets;
    }

    public static void main(String[] args) {
        System.out.println(TripletSumToZero.searchTripletsTest(new int[] { -3, 0, 1, 2, -1, 1, -2 }));
        System.out.println(TripletSumToZero.searchTripletsTest(new int[] { -5, 2, -1, -2, 3 }));
    }

}
