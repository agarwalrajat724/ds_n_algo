package GeekForGeeks.DSA_Class.Trees.RightView;

import GeekForGeeks.DSA_Class.Trees.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class GfG {

    void rightView(TreeNode node) {

        if (null == node) {
            return;
        }

        Queue<TreeNode> queue = new LinkedList<>();

        queue.offer(node);

        while (!queue.isEmpty()) {

            int size = queue.size();

            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();
                if (i == 0) {
                    System.out.print(" " + pop.key);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }

                if (null != pop.left) {
                    queue.offer(pop.left);
                }
            }

        }
    }

    public static void main(String[] args) {
        GfG rightView = new GfG();

        TreeNode root = new TreeNode(10);

        root.left = new TreeNode(20);
        root.right = new TreeNode(30);

        root.left.left = new TreeNode(40);
        root.left.right = new TreeNode(60);


        rightView.rightView(root);
    }
}
