package GeekForGeeks.DSA_Class.Trees.LeftView;

import GeekForGeeks.DSA_Class.Trees.Node;

public class GfG {

    class MaxLevel {
        int MAX_LEVEL = 0;
    }

    void leftView(Node root) {
        MaxLevel maxLevel = new MaxLevel();
        leftViewUtil(root, 1, maxLevel);
    }

    private void leftViewUtil(Node root, int level, MaxLevel maxLevel) {
        if (null == root) {
            return;
        }

        if (maxLevel.MAX_LEVEL < level) {
            System.out.print(root.data + " ");
            maxLevel.MAX_LEVEL = level;
        }

        leftViewUtil(root.left, level + 1, maxLevel);
        leftViewUtil(root.right, level + 1, maxLevel);
    }

    public static void main(String[] args) {
        GfG leftView = new GfG();

        Node root = new Node(10);

        root.left = new Node(20);
        root.right = new Node(30);

        root.left.left = new Node(40);
        root.left.right = new Node(60);


        leftView.leftView(root);
    }
}
