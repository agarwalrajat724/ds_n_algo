package Grokking.Ultimate_Patterns.K_Way_Merge;

import java.util.PriorityQueue;
import java.util.Queue;

class MatrixNode {
    int row;
    int col;

    public MatrixNode(int row, int col) {
        this.row = row;
        this.col = col;
    }
}

public class KthSmallestInSortedMatrix {

    public static int findKthSmallest(int[][] matrix, int k) {

        Queue<MatrixNode> minHeap = new PriorityQueue<>(matrix.length,
                (node1, node2) -> matrix[node1.row][node1.col] - matrix[node2.row][node2.col]);

        for (int i = 0; i < matrix.length; i++) {
            minHeap.offer(new MatrixNode(i, 0));
        }
        int count = 0;

        while (!minHeap.isEmpty()) {
            MatrixNode node = minHeap.poll();
            int row = node.row;
            int col = node.col;
            count++;

            if (count == k) {
                return matrix[row][col];
            }
            col++;
            if (matrix[row].length > col) {
                minHeap.offer(new MatrixNode(row, col));
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int matrix[][] = { { 2, 6, 8 }, { 3, 7, 10 }, { 5, 8, 11 } };
        int result = KthSmallestInSortedMatrix.findKthSmallest(matrix, 5);
        System.out.print("Kth smallest number is: " + result);
    }
}
