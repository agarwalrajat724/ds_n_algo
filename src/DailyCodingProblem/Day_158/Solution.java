package DailyCodingProblem.Day_158;

import java.util.LinkedList;
import java.util.Queue;

class MatrixNode {
    int row, col, dist;

    public MatrixNode(int row, int col, int dist) {
        this.row = row;
        this.col = col;
        this.dist = dist;
    }
}

public class Solution {

    int noOfPaths(int[][] matrix) {
        int mRows = matrix.length;
        int nCols = matrix[0].length;

        int[][] dp = new int[mRows][nCols];
        dp[0][0] = 1;
        for (int i = 1 ; i < mRows; i++) {
            if (dp[i - 1][0] == 1 && matrix[i][0] == 0) {
                dp[i][0] = 1;
            }
        }

        for (int j = 1; j < nCols; j++) {
            if (dp[0][j - 1] == 1 && matrix[0][j] == 0) {
                dp[0][j] = 1;
            }
        }

        for (int i = 1; i < mRows; i++) {
            for (int j = 1; j < nCols; j++) {
                if (matrix[i][j] == 0) {
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
        }

        return dp[mRows - 1][nCols - 1];
    }

//    int noOfPaths(int[][] matrix) {
//        int n = matrix.length;
//        int m = matrix[0].length;
//        Queue<MatrixNode> queue = new LinkedList<>();
//        boolean[][] visited = new boolean[n][m];
//
//        ((LinkedList<MatrixNode>) queue).push(new MatrixNode(0, 0, 0));
//
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < m; j++) {
//                if (matrix[i][j] == 1) {
//                    visited[i][j] = true;
//                }
//            }
//        }
//
//        visited[0][0] = true;
//        int res = 0;
//        while (!queue.isEmpty()) {
//            MatrixNode node = queue.poll();
//
//            int pRow = node.row;
//            int pCol = node.col;
//            int pDist = node.dist;
//
//            if (pRow == n - 1 && pCol == m - 1) {
//                res++;
//            }
//
//            if ((pRow + 1) < n && matrix[pRow + 1][pCol] != 1 && !visited[pRow + 1][pCol]) {
//                queue.offer(new MatrixNode(pRow + 1, pCol, pDist + 1));
////                visited[pRow + 1][pCol] = true;
//            }
//
//            if ((pCol + 1) < m && matrix[pRow][pCol + 1] != 1 && !visited[pRow][pCol + 1]) {
//                queue.offer(new MatrixNode(pRow, pCol + 1, pDist + 1));
////                visited[pRow][pCol + 1] = true;
//            }
//        }
//        return res;
//    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.noOfPaths(new int[][]{
                {0, 0, 1}, {0, 0, 1}, {1, 0, 0}}));

        System.out.println(solution.noOfPaths(new int[][]{{0, 0, 1}, {1, 0, 1}, {1, 0, 0}}));

        System.out.println(solution.noOfPaths(new int[][]{{0, 0, 0}, {1, 1, 0}, {0, 0, 0}, {0, 1, 1}, {0, 0, 0}}));

        System.out.println(solution.noOfPaths(new int[][]{{0, 0, 0}, {1, 0, 0}, {0, 0, 0}, {0, 1, 1}, {0, 0, 0}}));
    }
}
