package Grokking.Ultimate_Patterns.Fast_N_Slow;

public class LinkedListCycleLength {

    public static int findCycleLength(ListNode head) {
        ListNode slow = head, fast = head, move = null;

        while (null != fast && null != fast.next) {
            fast = fast.next.next;
            slow = slow.next;

            if (slow == fast) {
                break;
            }
        }
        move = slow;
        int res = 0;
        while (null != move) {
            move = move.next;
            res++;
            if (move == slow) {
                return res;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        head.next.next.next.next.next = new ListNode(6);
        head.next.next.next.next.next.next = head.next.next;
        System.out.println("LinkedList cycle length: " + LinkedListCycleLength.findCycleLength(head));

        head.next.next.next.next.next.next = head.next.next.next;
        System.out.println("LinkedList cycle length: " + LinkedListCycleLength.findCycleLength(head));
    }
}
