package Graphs;

import java.util.LinkedList;

public class Graph {

    public int V;
    public LinkedList<Integer> adjListArray[];

    public Graph(int _V) {
        this.V = _V;
        this.adjListArray = new LinkedList[_V];

        for (int i = 0; i < _V; i++) {
            this.adjListArray[i] = new LinkedList<>();
        }
    }

    public void addEdge(int src, int dest) {
        adjListArray[src].add(dest);
    }

    public void printGraph() {
        for (int i = 0; i < V; i++) {
            System.out.println(" For vertes V : " + i);

            for (Integer integer : adjListArray[i]) {
                System.out.println(" --> " + integer);
            }
            System.out.println();
        }
    }
}
