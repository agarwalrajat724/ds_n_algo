package LeetCode.Medium.BinaryTree.PreOrderTraversal;

import LeetCode.Hard.BinaryTree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public List<Integer> preorderTraversal(TreeNode root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            TreeNode pop = stack.pop();
            result.add(pop.val);

            if (pop.right != null) {
                stack.push(pop.right);
            }

            if (pop.left != null) {
                stack.push(pop.left);
            }

        }

        return result;
    }

    public static void main(String[] args) {
        Solution solution =new Solution();


        TreeNode node = new TreeNode(1);

        node.right = new TreeNode(2);
        node.right.left = new TreeNode(3);

        List<Integer> result = solution.preorderTraversal(node);

        result.forEach(integer -> System.out.println(integer));
    }

}
