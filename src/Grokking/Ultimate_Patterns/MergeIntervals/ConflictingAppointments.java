package Grokking.Ultimate_Patterns.MergeIntervals;

import java.util.*;

class ConflictingAppointments {

    public static boolean canAttendAllAppointments(Interval[] intervals) {

        if (null == intervals || intervals.length < 2) {
            return true;
        }

        Arrays.sort(intervals, Comparator.comparing(Interval::getStart));

        for (int i = 1; i < intervals.length; i++) {

            // please note the comparison above, it is "<" and not "<="
            // while merging we needed "<=" comparison, as we will be merging the two
            // intervals having condition "intervals[i].start == intervals[i - 1].end" but
            // such intervals don't represent conflicting appointments as one starts right
            // after the other
            if (intervals[i].start < intervals[i - 1].end) {
                return false;
            }
        }
        return true;
    }

    public static void findAllConflictictingIntervals(Interval[] intervals) {

        Arrays.sort(intervals, Comparator.comparing(Interval::getStart));

        for (int i = 1; i < intervals.length; i++) {
            int index = i - 1;
            while (index >= 0) {
                if (intervals[i].start < intervals[index].end) {
                    System.out.println(intervals[i] + " and " + intervals[index] + " conflict.");
                    break;
                }
                index--;
            }
        }
    }

    public static void main(String[] args) {
        Interval[] intervals = { new Interval(1, 4), new Interval(2, 5), new Interval(7, 9) };
        boolean result = ConflictingAppointments.canAttendAllAppointments(intervals);
        System.out.println("Can attend all appointments: " + result);

        Interval[] intervals1 = { new Interval(6, 7), new Interval(2, 4), new Interval(8, 12) };
        result = ConflictingAppointments.canAttendAllAppointments(intervals1);
        System.out.println("Can attend all appointments: " + result);

        Interval[] intervals2 = { new Interval(4, 5), new Interval(2, 3), new Interval(3, 6) };
        result = ConflictingAppointments.canAttendAllAppointments(intervals2);
        System.out.println("Can attend all appointments: " + result);

        Interval[] intervals3 = { new Interval(4, 5), new Interval(2, 3), new Interval(3, 6), new Interval(5, 7), new Interval(7, 8)};
        findAllConflictictingIntervals(intervals3);

    }
}
