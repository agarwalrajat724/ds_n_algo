package LeetCode.Medium.Maths.DivideTwoIntegers;

public class Solution {

    public int divide(int dividend, int divisor) {

        if (dividend == 0) {
            return 0;
        }

        int sign = ((dividend < 0) ^ (divisor < 0)) ? -1 : 1;

        if (divisor == 0) {
            if (sign > 0) {
                return Integer.MAX_VALUE;
            }
            return Integer.MIN_VALUE;
        }

        if (divisor == 1) {
            return dividend;
        }

        if (divisor == -1) {
            return -dividend;
        }

        long dividendLong = Math.abs(dividend);
        long divisorLong = Math.abs(divisor);



        long quotient = 0;
        while (dividendLong >= divisorLong) {
            quotient++;
            dividendLong -= divisorLong;
        }

        if (sign > 0) {
            return Long.valueOf(quotient).intValue();
        }
        return -1 * Long.valueOf(quotient).intValue();
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
//        System.out.println(solution.divide(10, 3));
//        System.out.println(solution.divide(7, -3));
        System.out.println(solution.divide(-1,-1));
    }
}
