package LeetCode.Hash.TwoSum;

import java.util.HashMap;
import java.util.Map;

/**
 *  1. Two Sum
 *
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 *
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 *
 * Example:
 *
 * Given nums = [2, 7, 11, 15], target = 9,
 *
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */
public class Solution {

    public int[] twoSum(int[] nums, int target) throws Exception {
        if (null == nums || nums.length == 0) {
            return null;
        }

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int key = target - nums[i];
            if (map.containsKey(key)) {
                return new int[]{map.get(key), i};
            }

            map.put(nums[i], i);
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        Solution solution = new Solution();

        solution.twoSum(new int[]{2, 7, 11, 15}, 9);
    }

}
