package LeetCode.Stack.NextGreaterElem1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Solution {


    public int[] nextGreaterElement(int[] nums1, int[] nums2) {


        if (null == nums1 || null == nums2 || nums1.length < 1 && nums2.length < 1) {
            return new int[]{};
        }
        Map<Integer, Integer> map = new HashMap<>();

        Stack<Integer> stack = new Stack<>();
        stack.push(nums2[0]);
        for (int i = 1; i < nums2.length; i++) {
            int next = nums2[i];

            if (!stack.isEmpty()) {
                int elem = stack.pop();

                while (elem < next) {
                    map.put(elem, next);
                    if (stack.isEmpty()) {
                        break;
                    }
                    elem = stack.pop();
                }
                if (elem > next) {
                    stack.push(elem);
                }
            }
            stack.push(next);
        }

        while (!stack.isEmpty()) {
            map.put(stack.pop(), -1);
        }
        int[] result = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            result[i] = map.getOrDefault(nums1[i], -1);
        }

        return result;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        Arrays.stream(solution.nextGreaterElement(new int[]{4,1,2}, new int[]{1,3,4,2})).forEach(
                i -> System.out.println(i));
    }
}
