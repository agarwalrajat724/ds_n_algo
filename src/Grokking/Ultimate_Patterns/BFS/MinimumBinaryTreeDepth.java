package Grokking.Ultimate_Patterns.BFS;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 4
 * Find the minimum depth of a binary tree.
 */
public class MinimumBinaryTreeDepth {

    public static int findDepthDFS(TreeNode root) {
        // TODO: Write your code here
        if (null == root) {
            return 0;
        }

        if (null == root.left) {
            return findDepthDFS(root.right) + 1;
        }

        if (null == root.right) {
            return findDepthDFS(root.left) + 1;
        }

        return 1 + Math.min(findDepthDFS(root.left), findDepthDFS(root.right));
    }

    /**
     * BFS Approach : We will keep a track of depth of the Tree.
     * As soon as we find our first out first leaf node, that level will represent the Minimum
     * Depth of the Tree.
     * @param root
     * @return
     */
    public static int findDepth(TreeNode root) {
        // TODO: Write your code here
        if (null == root) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int minDepth = 0;

        while (!queue.isEmpty()) {
            int size = queue.size();
            minDepth++;
            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();
                if (null == pop.left && null == pop.right) {
                    return minDepth;
                }

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }
        }

        return minDepth;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        System.out.println("Tree Minimum Depth: " + MinimumBinaryTreeDepth.findDepth(root));
        System.out.println("Tree Minimum Depth: " + MinimumBinaryTreeDepth.findDepthDFS(root));
        root.left.left = new TreeNode(9);
        root.right.left.left = new TreeNode(11);
        System.out.println("Tree Minimum Depth: " + MinimumBinaryTreeDepth.findDepth(root));
        System.out.println("Tree Minimum Depth: " + MinimumBinaryTreeDepth.findDepthDFS(root));
    }
}
