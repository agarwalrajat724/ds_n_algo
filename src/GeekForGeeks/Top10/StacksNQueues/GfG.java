package GeekForGeeks.Top10.StacksNQueues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.StringTokenizer;

public class GfG {


    int minEle;
    Stack<Integer> s;

    /*returns min element from stack*/
    int getMin()
    {
        return minEle;
    }

    /*returns poped element from stack*/
    int pop()
    {
        if (!s.isEmpty()) {
            int y = s.pop();

            if (y < minEle) {
                minEle = 2 * minEle - y;
            }
            return y;
        } else {
            return -1;
        }
    }
    /*push element x into the stack*/
    void push(int x)
    {

        if (s.isEmpty()) {
            s.push(x);
            minEle = x;
        } else {
            if (x >= minEle) {
                s.push(x);
            } else {
                s.push(2*x - minEle);
                minEle = x;
            }
        }
    }


    public static void main(String[] args) {

        GfG solution = new GfG();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int n = bufferedReader.read();

            for (int i = 0; i < n; i++) {
                int total = bufferedReader.read();
                solution.s = new Stack<>();
                solution.minEle = -1;
                for (int j = 0; j < total; j++) {
                    StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine());
                    while (stringTokenizer.hasMoreTokens()) {
                        int op = Integer.parseInt(stringTokenizer.nextToken());

                        if (op == 1) {
                            solution.push(Integer.parseInt(stringTokenizer.nextToken()));
                        } else if (op == 2) {
                            System.out.println(solution.pop());
                        } else if (op == 3) {
                            System.out.println(solution.getMin());
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
