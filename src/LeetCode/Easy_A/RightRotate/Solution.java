package LeetCode.Easy_A.RightRotate;

public class Solution {

    public void rotate(int[] nums, int k) {
        if (null == nums || nums.length < 2) {
            return;
        }

        if (k > nums.length) {
            k = k % nums.length;
        }

        leftRotate(nums, k);
    }

    private void leftRotate(int[] nums, int k) {
        swap(nums, nums.length - k, nums.length - 1);
        swap(nums, 0, nums.length - k - 1);
        swap(nums, 0, nums.length - 1);
    }

    private void swap(int[] nums, int l, int h) {
        while (l < h) {
            int temp = nums[l];
            nums[l] = nums[h];
            nums[h] = temp;
            l++;
            h--;
        }
    }
}
