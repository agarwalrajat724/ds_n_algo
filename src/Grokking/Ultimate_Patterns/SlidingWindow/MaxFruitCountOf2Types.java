package Grokking.Ultimate_Patterns.SlidingWindow;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of characters where each character represents a fruit tree, you are
 * given two baskets and your goal is to put maximum number of fruits in each basket.
 * The only restriction is that each basket can have only one type of fruit.
 *
 * You can start with any tree, but once you have started you can’t skip a tree. You
 * will pick one fruit from each tree until you cannot, i.e., you will stop when you
 * have to pick from a third fruit type.
 *
 * Write a function to return the maximum number of fruits in both the baskets.
 *
 * Example 1:
 *
 * Input: Fruit=['A', 'B', 'C', 'A', 'C']
 * Output: 3
 * Explanation: We can put 2 'C' in one basket and one 'A' in the other from the subarray ['C', 'A', 'C']
 * Example 2:
 *
 * Input: Fruit=['A', 'B', 'C', 'B', 'B', 'C']
 * Output: 5
 * Explanation: We can put 3 'B' in one basket and two 'C' in the other basket.
 * This can be done if we start with the second letter: ['B', 'C', 'B', 'B', 'C']
 *
 */
public class MaxFruitCountOf2Types {

    public static int findLength(char[] arr) {
        if (null == arr || arr.length < 2) {
            return 0;
        }

        Map<Character, Integer> characterFrequencyMap = new HashMap<>();
        int maxFruits = Integer.MIN_VALUE;
        int windowStart = 0;

        for (int i = 0; i < arr.length ; i++) {
            Character rightChar = Character.valueOf(arr[i]);
            characterFrequencyMap.put(rightChar, characterFrequencyMap.getOrDefault(rightChar, 0) + 1);

            while (characterFrequencyMap.size() > 2) {
                Character leftChar = arr[windowStart];
                characterFrequencyMap.put(leftChar, characterFrequencyMap.get(leftChar) - 1);
                if (characterFrequencyMap.get(leftChar) == 0) {
                    characterFrequencyMap.remove(leftChar);
                }
                windowStart++;
            }
            maxFruits = Math.max(maxFruits, i - windowStart + 1);
        }
        return maxFruits;
    }

    public static void main(String[] args) {
        System.out.println("Maximum number of fruits: " +
                MaxFruitCountOf2Types.findLength(new char[] { 'A', 'B', 'C', 'A', 'C' }));
        System.out.println("Maximum number of fruits: " +
                MaxFruitCountOf2Types.findLength(new char[] { 'A', 'B', 'C', 'B', 'B', 'C' }));
    }
}
