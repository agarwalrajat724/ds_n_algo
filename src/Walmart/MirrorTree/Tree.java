package Walmart.MirrorTree;

import java.util.LinkedList;
import java.util.Queue;

class Node
{
    int data;
    Node left, right;
    Node(int item)
    {
        data = item;
        left = right = null;
    }
}

public class Tree {

//    void mirror(Node node)
//    {
//        if (null == node) {
//            return;
//        }
//
//        if (null == node.left && null == node.right) {
//            return;
//        }
//
//        Node temp = node.left;
//        node.left = node.right;
//        node.right = temp;
//        mirror(node.left);
//        mirror(node.right);
//    }

    void mirror(Node node) {
        if (null == node) {
            return;
        }

        if (null == node.left && null == node.right) {
            return;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);

        while (!queue.isEmpty()) {
            Node pop = queue.poll();
            if (null == pop.left && null == pop.right) {
                continue;
            }
            Node temp = pop.left;
            pop.left = pop.right;
            pop.right = temp;

            if (null != pop.left) {
                queue.offer(pop.left);
            }

            if (null != pop.right) {
                queue.offer(pop.right);
            }
        }
    }
}
