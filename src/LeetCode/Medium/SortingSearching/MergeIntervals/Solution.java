package LeetCode.Medium.SortingSearching.MergeIntervals;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Given a collection of intervals, merge all overlapping intervals.
 *
 * Example 1:
 *
 * Input: [[1,3],[2,6],[8,10],[15,18]]
 * Output: [[1,6],[8,10],[15,18]]
 * Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
 * Example 2:
 *
 * Input: [[1,4],[4,5]]
 * Output: [[1,5]]
 * Explanation: Intervals [1,4] and [4,5] are considered overlapping.
 * NOTE: input types have been changed on April 15, 2019. Please reset to
 * default code definition to get new method signature.
 */
public class Solution {

    static class Interval {
        int start;
        int end;

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public Interval[] merge(Interval[] intervals) {

        Arrays.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o2.start - o1.start;
            }
        });

        int index = 0;

        for (int i = 0; i < intervals.length; i++) {
            if (index != 0 && intervals[index - 1].start <= intervals[i].end) {
                while (index != 0 && intervals[index - 1].start <= intervals[i].end) {
                    intervals[index - 1].end = Math.max(intervals[index - 1].end, intervals[i].end);
                    intervals[index - 1].start = Math.min(intervals[index - 1].start, intervals[i].start);
                    index--;
                }
            } else {
                intervals[index] = intervals[i];
            }
            index++;
        }

//        return (Interval[]) IntStream.rangeClosed(1, intervals.length)
//                .mapToObj(i -> intervals[intervals.length - 1])
//                .toArray();
        System.out.println(intervals);
        return intervals;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        Interval[] intervals = new Interval[4];
        intervals[0] = new Interval(1, 3);
        intervals[1] = new Interval(2, 6);
        intervals[2] = new Interval(8, 10);
        intervals[3] = new Interval(15, 18);

        intervals = solution.merge(intervals);
        System.out.println(intervals);
    }
}
