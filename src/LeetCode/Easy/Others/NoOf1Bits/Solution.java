package LeetCode.Easy.Others.NoOf1Bits;

public class Solution {

    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int noOfBits = 0;
        while (n != 0) {
            noOfBits++;
            n = n & (n-1);
        }
        return noOfBits;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.hammingWeight(00000000000000000000000000001011));
    }
}
