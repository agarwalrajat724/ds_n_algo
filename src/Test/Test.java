package Test;

import java.util.ArrayList;
import java.util.List;

/**
 * The Fibonacci series is defined as F( N ) = F(N-1) + F(N-2)
 * // The series of N starts with 0, and F(0) == F(1) == 1
 * // Hence the Fibonacci series is: 1,1,2,3,5,8,13,....
 *
 * // Write a method fib( n ) which returns the nth Fibonacci Number where n is a natural number (non-negative integer)
 */
public class Test {

    List<Long> list = null;

    public Test() {
        list = new ArrayList<>();
        list.add(1l);
        list.add(1l);
    }

    public static void main(String[] args) {
        Test test = new Test();

        System.out.println(test.fib(0));
        System.out.println(test.fib(2));
        System.out.println(test.fib(3));
        System.out.println(test.fib(4));

    }

    public long fib(int n) {
        for (int i = list.size(); i <= n; i++) {
            list.add(list.get(i - 1) + list.get(i - 2));
        }
        return list.get(n);
    }


}
