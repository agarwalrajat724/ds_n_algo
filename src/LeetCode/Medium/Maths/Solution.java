package LeetCode.Medium.Maths;

public class Solution {

    public double myPow(double x, int n) {
        if (n == 0) {
            return 1;
        }
        double temp = myPow(x, n/2);

        boolean isEven = ((n % 2) == 0);

        if (isEven) {
                return temp * temp;
        } else {
            if (n > 0) {
                return temp * temp * x;
            } else {
                return ((temp * temp) / x);
            }
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.myPow(2.00000, -2));
    }

}
