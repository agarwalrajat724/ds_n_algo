package hackerrank.InterviewKit.StackNQueues.BalancedBrackets;

import java.io.*;
import java.util.*;

public class Solution {


    // Complete the isBalanced function below.
    static String isBalanced(String s) {


        if (null == s) {
            return "YES";
        }
        if (s.length() < 2) {
            return "NO";
        }

        char[] arr = s.toCharArray();

        Stack<Character> stack = new Stack<>();
        for (char ch : arr) {

            if (isOpeningBrace(ch)) {
                stack.push(ch);
            } else {
                if (!stack.isEmpty()) {
                    if ((ch == ']' && stack.peek() == '[') || (ch == '}' && stack.peek() == '{')
                            || (ch == ')' && stack.peek() == '(')) {
                        stack.pop();
                    } else {
                        return "NO";
                    }
                } else {
                    return "NO";
                }
            }
        }

        if (!stack.isEmpty()) {
            return "NO";
        }

        return "YES";
    }

    private static boolean isOpeningBrace(char ch) {
        if (ch == '(' || ch == '{' || ch == '[') {
            return true;
        }
        return false;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String s = scanner.nextLine();

            String result = isBalanced(s);
            System.out.println(result);
            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
