package LeetCode.BinaryTree;


import java.util.ArrayList;
import java.util.List;


/**
 * 1. Initialize current as root
 * 2. While current is not NULL
 *    If current does not have left child
 *       a) Print current’s data
 *       b) Go to the right, i.e., current = current->right
 *    Else
 *       a) Make current as right child of the rightmost
 *          node in current's left subtree
 *       b) Go to this left child, i.e., current = current->left
 */

public class Test {



    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add(null);

        System.out.println(list);

    }
}
