package LeetCode.Queue.NoOfRecentCalls;

import java.util.LinkedList;
import java.util.Queue;

public class RecentCounter {

    Queue<Integer> queue = null;

    public RecentCounter() {
        queue = new LinkedList<>();
    }

    public int ping(int t) {
        queue.offer(t);

        while (queue.peek() < (t - 3000)) {
            queue.poll();
        }

        return queue.size();
    }


    /**
     * Your RecentCounter object will be instantiated and called as such:
     * RecentCounter obj = new RecentCounter();
     * int param_1 = obj.ping(t);
     */


    public static void main(String[] args) {
        RecentCounter obj = new RecentCounter();

        System.out.println(obj.ping(1));
    }
}
