package hackerrank.InterviewKit.Sorting.CountingInversions;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {


//    static class Result {
//        long count;
//
//        public Result() {
//            this.count = 0l;
//        }
//    }

    // Complete the countInversions function below.
    static long countInversions(int[] arr) {
//        Result result = new Result();

        return mergeSort(arr, 0, arr.length - 1);

    }

    static long mergeSort(int[] arr, int low, int high) {

        long inv_count = 0l;

        if (low < high) {
            int mid = low + (high - low)/2;

            inv_count = mergeSort(arr, low, mid);
            inv_count += mergeSort(arr, mid + 1, high);
            inv_count += merge(arr, low, mid, high);
        }

        return inv_count;
    }

    private static long merge(int[] arr, int low, int mid, int high) {

        int n1 = mid - low + 1;
        int n2 = high - mid;
        long inv_count = 0l;
        int[] L = new int[n1];
        int[] R = new int[n2];

        int k = low;

        for (int i = 0; i < n1; i++) {
            L[i] = arr[low + i];
        }

        for (int i = 0; i < n2; i++) {
            R[i] = arr[mid + 1 + i];
        }

        int i = 0, j = 0;

        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
                inv_count = inv_count + (n1 - i);
            }
            k++;
        }

        while (i < n1) {
            arr[k++] = L[i];
            i++;
        }

        while (j < n2) {
            arr[k++] = R[j];
            j++;
        }

        return inv_count;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] arr = new int[n];

            String[] arrItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int arrItem = Integer.parseInt(arrItems[i]);
                arr[i] = arrItem;
            }

            long result = countInversions(arr);

            System.out.println(result);

            bufferedWriter.write(String.valueOf(result));
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
