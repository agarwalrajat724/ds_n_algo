package Grokking.Ultimate_Patterns.BFS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Second
 *
 * Given a binary tree, populate an array to represent its level-by-level traversal
 * in reverse order, i.e., the lowest level comes first. You should populate the values
 * of all nodes in each level from left to right in separate sub-arrays.
 */
public class ReverseLevelOrderTraversal {

    public static List<List<Integer>> traverse(TreeNode root) {
        List<List<Integer>> result = new LinkedList<List<Integer>>();

        if (null == root) {
            return result;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> elementsAtLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();
                elementsAtLevel.add(pop.val);
                if (null != pop.left) {
                    queue.offer(pop.left);
                }
                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }
            result.add(0, elementsAtLevel);
        }
        return result;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(9);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        List<List<Integer>> result = ReverseLevelOrderTraversal.traverse(root);
        System.out.println("Reverse level order traversal: " + result);
    }
}
