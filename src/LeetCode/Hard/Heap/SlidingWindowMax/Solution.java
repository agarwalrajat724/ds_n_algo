package LeetCode.Hard.Heap.SlidingWindowMax;

import java.util.*;

public class Solution {

    public int[] maxSlidingWindow(int[] nums, int k) {
        SortedSet<Integer> set = new TreeSet<>(Comparator.reverseOrder());
        int result[] = new int[nums.length-k+1];

        if (null != nums || nums.length > 0) {

            for (int i = 0; i < k; i++) {
                set.add(nums[i]);
            }

            for (int i = 0; i <= (nums.length - k); i++) {
                result[i] = set.first();

                set.remove(nums[i]);
                if ((i + k) < nums.length) {
                    set.add(nums[i + k]);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] result = solution.maxSlidingWindow(new int[]{1, 2, 3, 1, 4, 5, 2, 3, 6}, 3);

        Arrays.stream(result).forEach(i -> System.out.println(i));
    }
}
