package LeetCode.Easy.Sample;


/**
 * Count Primes
 *
 * Count the number of prime numbers less than a non-negative number, n.
 *
 * Example:
 *
 * Input: 10
 * Output: 4
 * Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
 *
 */
public class CountPrime {

    public int countPrimes(int n) {
        return 0;
    }

    public static void main(String[] args) {
        CountPrime countPrime = new CountPrime();

        System.out.println(countPrime.countPrimes(10));
    }

}

