package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/**
 * Design a class that simulates a Stack data structure, implementing the following two operations:
 * push(int num): Pushes the number ‘num’ on the stack.
 * pop(): Returns the most frequent number in the stack. If there is a tie, return the number which was pushed later.
 *
 * After following push operations: push(1), push(2), push(3), push(2), push(1), push(2), push(5)
 *
 * 1. pop() should return 2, as it is the most frequent number
 * 2. Next pop() should return 1
 * 3. Next pop() should return 2
 */
public class FrequencyStack {

    Map<Integer, Integer> freqMap = null;
    Map<Integer, Stack<Integer>> group = null;
    int maxFreq = 0;

    public FrequencyStack() {
        freqMap = new HashMap<>();
        group = new HashMap<>();
        maxFreq = 0;
    }

    public void push(int num) {
        freqMap.put(num, freqMap.getOrDefault(num, 0) + 1);
        int freq = freqMap.get(num);
        group.computeIfAbsent(freq, integer -> new Stack<>()).push(num);
        maxFreq = Math.max(maxFreq, freq);
    }

    public int pop() {
        int pop = group.get(maxFreq).pop();

        if (freqMap.get(pop) > 1) {
            freqMap.put(pop, freqMap.get(pop) - 1);
        } else {
            freqMap.remove(pop);
        }

        if (group.get(maxFreq).size() == 0) {
            maxFreq--;
        }

        return pop;
    }

    public static void main(String[] args) {
        FrequencyStack frequencyStack = new FrequencyStack();
        frequencyStack.push(1);
        frequencyStack.push(2);
        frequencyStack.push(3);
        frequencyStack.push(2);
        frequencyStack.push(1);
        frequencyStack.push(2);
        frequencyStack.push(5);
        System.out.println(frequencyStack.pop());
        System.out.println(frequencyStack.pop());
        System.out.println(frequencyStack.pop());
    }
}
