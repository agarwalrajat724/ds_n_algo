package Grokking.Ultimate_Patterns.BinarySearch;

/**
 * Given an array of lowercase letters sorted in ascending order, find the smallest letter
 * in the given array greater than a given ‘key’.
 *
 * Assume the given array is a circular list, which means that the last letter is assumed to
 * be connected with the first letter. This also means that the smallest letter in the given array is greater than the last letter of the array and is also the first letter of the array.
 *
 * Write a function to return the next letter of the given ‘key’.
 *
 * Input: ['a', 'c', 'f', 'h'], key = 'f'
 * Output: 'h'
 * Explanation: The smallest letter greater than 'f' is 'h' in the given array.
 *
 * Input: ['a', 'c', 'f', 'h'], key = 'b'
 * Output: 'c'
 * Explanation: The smallest letter greater than 'b' is 'c'.
 *
 * Input: ['a', 'c', 'f', 'h'], key = 'm'
 * Output: 'a'
 * Explanation: As the array is assumed to be circular, the smallest letter greater than 'm' is 'a'
 *
 * Input: ['a', 'c', 'f', 'h'], key = 'h'
 * Output: 'a'
 * Explanation: As the array is assumed to be circular, the smallest letter greater than 'h' is 'a'.
 */
public class NextLetter {

    public static char searchNextLetter(char[] letters, char key) {
        if (key < letters[0] || key > letters[letters.length - 1]) {
            return letters[0];
        }

        int low = 0, high = letters.length - 1;

        while (low <= high) {
            int mid = low + (high - low)/2;

            if (key < letters[mid]) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        /**
         * In the end, instead of returning the element pointed out by start, we have to return the letter pointed
         * out by start % array_length. This is needed because of point 2 discussed above. Imagine that the last
         * letter of the array is equal to the ‘key’. In that case, we have to return the first letter of the input array.
         */
        return letters[low % letters.length];
    }

    public static void main(String[] args) {
        System.out.println(NextLetter.searchNextLetter(new char[] { 'a', 'c', 'f', 'h' }, 'f'));
        System.out.println(NextLetter.searchNextLetter(new char[] { 'a', 'c', 'f', 'h' }, 'b'));
        System.out.println(NextLetter.searchNextLetter(new char[] { 'a', 'c', 'f', 'h' }, 'm'));
        System.out.println(NextLetter.searchNextLetter(new char[] { 'a', 'c', 'f', 'h' }, 'h'));
    }
}
