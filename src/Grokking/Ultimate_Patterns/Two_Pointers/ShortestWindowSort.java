package Grokking.Ultimate_Patterns.Two_Pointers;

/**
 * Minimum Window Sort (medium)
 * Given an array, find the length of the smallest subarray in it which when sorted will sort the whole array.
 *
 * Input: [1, 2, 5, 3, 7, 10, 9, 12]
 * Output: 5
 * Explanation: We need to sort only the subarray [5, 3, 7, 10, 9] to make the whole array sorted
 *
 * Input: [1, 3, 2, 0, -1, 7, 10]
 * Output: 5
 * Explanation: We need to sort only the subarray [1, 3, 2, 0, -1] to make the whole array sorted
 *
 * Input: [1, 2, 3]
 * Output: 0
 * Explanation: The array is already sorted
 *
 * Input: [3, 2, 1]
 * Output: 3
 * Explanation: The whole array needs to be sorted.
 *
 *
 */
public class ShortestWindowSort {

    public static int sort(int[] arr) {

        int low = 0, high = arr.length - 1;

        while (low < arr.length - 1 && arr[low] <= arr[low + 1]) {
            low++;
        }

        while (high > 0 && arr[high] >= arr[high - 1]) {
            high--;
        }

        if (low == arr.length - 1) {
            return 0;
        }

        int subArrayMin = Integer.MAX_VALUE, subArrayMax = Integer.MIN_VALUE;

        for (int i = low ; i <= high; i++) {
            subArrayMax = Math.max(subArrayMax, arr[i]);
            subArrayMin = Math.min(subArrayMin, arr[i]);
        }

        while (low > 0 && arr[low - 1] > subArrayMin) {
            low--;
        }

        while (high < arr.length - 1 && arr[high + 1] < subArrayMax) {
            high++;
        }

        return high - low + 1;
    }

    public static void main(String[] args) {
        System.out.println(ShortestWindowSort.sort(new int[] { 1, 2, 5, 3, 7, 10, 9, 12 }));
        System.out.println(ShortestWindowSort.sort(new int[] { 1, 3, 2, 0, -1, 7, 10 }));
        System.out.println(ShortestWindowSort.sort(new int[] { 1, 2, 3 }));
        System.out.println(ShortestWindowSort.sort(new int[] { 3, 2, 1 }));
    }
}
