package LeetCode.Medium.SortingSearching.SearchInRotatedSortedArray;

public class Solution {

    public int search(int[] nums, int target) {
        if (null == nums) {
            return -1;
        }

        return searchUtil(nums, 0, nums.length - 1, target, nums.length);
    }

    private int searchUtil(int[] nums, int low, int high, int target, int n) {
        if (low > high) {
            return -1;
        }

        int mid = low + (high - low)/2;

        if (nums[mid] == target) {
            return mid;
        } else if (nums[low] <= nums[mid]) {

            if (nums[low] <= target && target <= nums[mid]) {
                return searchUtil(nums, low, mid - 1, target, n);
            }
            return searchUtil(nums, mid + 1, high, target, n);
        }

        if (nums[mid] <= target && target <= nums[high]) {
            return searchUtil(nums, mid + 1, high, target, n);
        }
        return searchUtil(nums, low, mid - 1, target, n);
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.search(new int[]{4,5,6,7,0,1,2}, 0));

        System.out.println(solution.search(new int[]{4,5,6,7,0,1,2}, 3));
    }
}
