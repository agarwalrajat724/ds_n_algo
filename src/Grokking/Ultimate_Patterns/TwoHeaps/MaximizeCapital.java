package Grokking.Ultimate_Patterns.TwoHeaps;

import java.util.PriorityQueue;
import java.util.Queue;

public class MaximizeCapital {

    public static int findMaximumCapital(int[] capital, int[] profits, int numberOfProjects, int initialCapital) {
        int n = profits.length;
        Queue<Integer> minCapitalHeap = new PriorityQueue<>(n, (i1, i2) -> capital[i1] - capital[i2]);
        Queue<Integer> maxProfitHeap = new PriorityQueue<>(n, (i1, i2) -> profits[i2] - profits[i1]);

        for (int i = 0; i < capital.length; i++) {
            minCapitalHeap.offer(i);
        }

        for (int i = 0; i < numberOfProjects; i++) {

            while (!minCapitalHeap.isEmpty() && capital[minCapitalHeap.peek()] <= initialCapital) {
                maxProfitHeap.offer(minCapitalHeap.poll());
            }

            if (maxProfitHeap.isEmpty()) {
                break;
            }

            initialCapital += profits[maxProfitHeap.poll()];
        }
        return initialCapital;
    }

    public static void main(String[] args) {
        int result = MaximizeCapital.findMaximumCapital(new int[] { 0, 1, 2 }, new int[] { 1, 2, 3 }, 2, 1);
        System.out.println("Maximum capital: " + result);
        result = MaximizeCapital.findMaximumCapital(new int[] { 0, 1, 2, 3 }, new int[] { 1, 2, 3, 5 }, 3, 0);
        System.out.println("Maximum capital: " + result);
    }
}
