package CollectionDesign.ThreadException;


final class MyTask implements Runnable {
    @Override
    public void run() {
        System.out.println("My task is started running...");
        // ...
        throw new ArithmeticException();
        // ...
    }
}
