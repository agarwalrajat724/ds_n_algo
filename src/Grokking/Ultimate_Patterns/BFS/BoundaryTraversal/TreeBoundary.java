package Grokking.Ultimate_Patterns.BFS.BoundaryTraversal;

import java.util.*;

/**
 * To find the leaves in the left to right order we can’t use BFS because any leaf
 * node coming in a higher level will not necessarily come before a leaf on the lower level.
 *
 * Tree Left View - contains the first node of each level
 * Leaves - contains all leaf nodes.
 * Tree Right View - contains the last node of each level
 */

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }
}

public class TreeBoundary {

    private static List<TreeNode> findBoundary(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<TreeNode> leftView = new ArrayList<>();
        List<TreeNode> rightView = new LinkedList<>();

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode current = queue.poll();
                if (null == current.left && null == current.right) {
                    continue;
                }

                if (i == 0) {
                    leftView.add(current);
                } else if (i == (size - 1)) {
                    rightView.add(0, current);
                }

                if (null != current.left) {
                    queue.offer(current.left);
                }

                if (null != current.right) {
                    queue.offer(current.right);
                }
            }
        }

        return new ArrayList<TreeNode>() {
            {
                addAll(leftView);
                addAll(findAllLeavesDFS(root));
                addAll(rightView);
            }
        };
    }

    private static List<TreeNode> findAllLeavesDFS(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<TreeNode> allLeaves = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            TreeNode pop = stack.pop();
            if (null == pop.left && null == pop.right) {
                allLeaves.add(pop);
            }

            if (null != pop.right) {
                stack.push(pop.right);
            }

            if (null != pop.left) {
                stack.push(pop.left);
            }
        }
        return allLeaves;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(4);
        root.left.left.left = new TreeNode(9);
        root.left.right = new TreeNode(3);
        root.left.right.left = new TreeNode(15);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        root.right.right.left = new TreeNode(6);
        List<TreeNode> result = TreeBoundary.findBoundary(root);
        for (TreeNode node : result) {
            System.out.print(node.val + " ");
        }
    }


}
