package LeetCode.Medium.Maths.Sqrt;

public class Solution {

    public int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }

        long start = 0, end = x/2, ans = 0;

        while (start <= end) {
            long mid = start + (end - start)/2;
            long res = mid * mid;

            if (res == x) {
                return Long.valueOf(mid).intValue();
            } else if (res < x) {
                ans = mid;
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }

        return Long.valueOf(ans).intValue();
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.mySqrt(2147395599));
//        System.out.println(solution.mySqrt(8));
//        System.out.println(solution.mySqrt(5));
    }
}
