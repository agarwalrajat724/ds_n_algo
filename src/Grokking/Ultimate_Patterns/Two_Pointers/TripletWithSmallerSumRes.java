package Grokking.Ultimate_Patterns.Two_Pointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a function to return the list of all such triplets instead of the count.
 * How will the time complexity change in this case?
 *
 * Time : Order of N cube
 */
public class TripletWithSmallerSumRes {


    public static void main(String[] args) {
        System.out.println(TripletWithSmallerSumRes.searchTriplets(new int[] { -1, 0, 2, 3 }, 3));
        System.out.println(TripletWithSmallerSumRes.searchTriplets(new int[] { -1, 4, 2, 1, 3 }, 5));
    }

    private static List<List<Integer>> searchTriplets(int[] arr, int target) {
        Arrays.sort(arr);
        List<List<Integer>> triplets = new ArrayList<>();

        for (int i = 0; i < arr.length - 2; i++) {
            int low = i + 1, high = arr.length - 1;
            while (low < high) {
                int sum = arr[i] + arr[low] + arr[high];

                if (sum < target) {
                    for (int k = low + 1; k <= high; k++) {
                        triplets.add(Arrays.asList(arr[i], arr[low], arr[k]));
                    }
                    low++;
                } else {
                    high--;
                }
            }
        }
        return triplets;
    }
}
