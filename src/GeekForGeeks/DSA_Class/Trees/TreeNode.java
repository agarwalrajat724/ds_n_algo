package GeekForGeeks.DSA_Class.Trees;

public class TreeNode {

    public int key;
    public TreeNode left, right;

    public TreeNode(int key) {
        this.key = key;
    }
}
