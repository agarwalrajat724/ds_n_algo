package LeetCode.Easy.Arrays.TwoSumMultiple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {


    public int[] twoSum(int[] nums, int target) {
        if (null == nums || nums.length < 2) {
            return new int[]{};
        }

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {

            if (map.containsKey(target - nums[i])) {
                return new int[]{map.get(target - nums[i]), i};
            }
            map.put(nums[i], i);
        }

        return new int[]{};
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        Arrays.stream(solution.twoSum(new int[]{2, 7, 11, 15}, 9)).boxed()
                .forEach(integer -> System.out.println(integer));
    }
}
