package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.*;

class Entry {
    int key;
    int value;

    public Entry(int key, int value) {
        this.key = key;
        this.value = value;
    }

    public int closenessWithX(int X) {
        return Math.abs(value - X);
    }
}

/**
 * Given a sorted number array and two integers ‘K’ and ‘X’, find ‘K’ closest numbers to ‘X’
 * in the array. Return the numbers in the sorted order.
 *
 * Input: [5, 6, 7, 8, 9], K = 3, X = 7
 * Output: [6, 7, 8]
 *
 * Input: [2, 4, 5, 6, 9], K = 3, X = 6
 * Output: [4, 5, 6]
 *
 * Input: [2, 4, 5, 6, 9], K = 3, X = 10
 * Output: [5, 6, 9]
 */
class KClosestElements {

    public static List<Integer> findClosestElements(int[] arr, int K, Integer X) {
        List<Integer> result = new ArrayList<>();
        Queue<Entry> maxHeap = new PriorityQueue<>(K, (o1, o2) -> o2.closenessWithX(X) - o1.closenessWithX(X));

        for (int i = 0; i < K; i++) {
            maxHeap.offer(new Entry(i, arr[i]));
        }

        for (int i = K; i < arr.length ; i++) {
            Entry entry = new Entry(i, arr[i]);
            if (maxHeap.peek().closenessWithX(X) > entry.closenessWithX(X)) {
                maxHeap.poll();
                maxHeap.offer(entry);
            }
        }
        while (!maxHeap.isEmpty()) {
            result.add(maxHeap.poll().value);
        }
        Collections.sort(result);
        return result;
    }

    public static void main(String[] args) {
        List<Integer> result = KClosestElements.findClosestElements(new int[] { 5, 6, 7, 8, 9 }, 3, 7);
        System.out.println("'K' closest numbers to 'X' are: " + result);

        result = KClosestElements.findClosestElements(new int[] { 2, 4, 5, 6, 9 }, 3, 6);
        System.out.println("'K' closest numbers to 'X' are: " + result);

        result = KClosestElements.findClosestElements(new int[] { 2, 4, 5, 6, 9 }, 3, 10);
        System.out.println("'K' closest numbers to 'X' are: " + result);
    }
}

