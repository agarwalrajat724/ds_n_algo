package Grokking.Ultimate_Patterns.K_Way_Merge;

import java.util.PriorityQueue;
import java.util.Queue;

class ListNode {
    int value;
    ListNode next;

    public ListNode(int value) {
        this.value = value;
    }
}

public class MergeKSortedLists {

    public static ListNode merge(ListNode[] lists) {
        ListNode result = new ListNode(-1);
        // TODO: Write your code here
        ListNode resultHead = null, resultTrail = null;
        Queue<ListNode> minHeap = new PriorityQueue<>(lists.length, (node1, node2) -> node1.value - node2.value);

        for (int i = 0; i < lists.length; i++) {
            minHeap.offer(lists[i]);
        }

        while (!minHeap.isEmpty()) {
            ListNode node = minHeap.poll();

            if (null == resultHead) {
                resultHead = node;
                resultTrail = node;
            } else {
                resultTrail.next = node;
                resultTrail = resultTrail.next;
            }

            if (null != node.next) {
                minHeap.offer(node.next);
            }
        }
        result.next = resultHead;
        return result;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(6);
        l1.next.next = new ListNode(8);

        ListNode l2 = new ListNode(3);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(7);

        ListNode l3 = new ListNode(1);
        l3.next = new ListNode(3);
        l3.next.next = new ListNode(4);

        ListNode result = MergeKSortedLists.merge(new ListNode[] { l1, l2, l3 });
        System.out.print("Here are the elements form the merged list: ");
        while (result != null) {
            System.out.print(result.value + " ");
            result = result.next;
        }
    }
}
