package LeetCode.Easy.Others.ValidParantheses;

import java.util.Stack;

public class Solution {

    public boolean isValid(String s) {
        if (null == s || s.isEmpty() || s.trim().isEmpty()) {
            return true;
        }
        char[] arr = s.toCharArray();
        Stack<Character> stack = new Stack<>();

        for (char c : arr) {
            if (stack.isEmpty()) {
                if (!startParantheses(c)) {
                    return false;
                }
                stack.push(c);
            } else {
                if (startParantheses(c)) {
                    stack.push(c);
                } else {
                    Character peek = stack.peek();
                    if (')' == c && Character.valueOf('(').equals(peek)) {
                        stack.pop();
                    } else if ('}' == c && Character.valueOf('{').equals(peek)) {
                        stack.pop();
                    } else if (']' == c && Character.valueOf('[').equals(peek)) {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
            }
        }

        if (!stack.isEmpty()) {
            return false;
        }

        return true;
    }

    public boolean startParantheses(char c) {
        if ('(' == c || '{' == c || '[' == c) {
            return true;
        }
        return false;
    }

    public boolean closingParantheses(char c) {
        if (')' == c || '}' == c || ']' == c) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.isValid("()"));
        System.out.println(solution.isValid("()[]{}"));
        System.out.println(solution.isValid("(]"));
        System.out.println(solution.isValid("([)]"));
        System.out.println(solution.isValid("{[]}"));
    }
}
