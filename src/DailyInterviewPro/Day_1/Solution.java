package DailyInterviewPro.Day_1;

/**
 * You are given two linked-lists representing two non-negative integers. The digits are stored
 * in reverse order and each of their nodes contain a single digit.
 * Add the two numbers and return it as a linked list.
 *
 * Example:
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * Explanation: 342 + 465 = 807.
 */
public class Solution {

    static class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public int addTwoNumbers(Node n1, Node n2) {
        int carry = 0;
        int sum = 0;
        Node head1 = n1;
        Node head2 = n2;
        while (null != head1 || null != head2) {
            int temp = carry;
            if (null != head1) {
                temp += head1.data;
                head1 = head1.next;
            }

            if (null != head2) {
                temp += head2.data;
                head2 = head2.next;
            }

            carry = temp/10;
            temp = temp % 10;
            sum = (sum * 10) + temp;
        }

        if (carry > 0) {
            sum = (sum * 10) + carry;
        }
        return sum;
    }

    public static void main(String[] args) {

        Solution solution = new Solution();
        Node node1 = new Node(2);
        node1.next = new Node(4);
        node1.next.next = new Node(3);

        Node node2 = new Node(5);
        node2.next = new Node(6);
        node2.next.next = new Node(4);

        System.out.println(solution.addTwoNumbers(node1, node2));

        Node node3 = new Node(2);
        node3.next = new Node(4);
        node3.next.next = new Node(3);

        Node node4 = new Node(5);
        node4.next = new Node(6);
        node4.next.next = new Node(8);

        System.out.println(solution.addTwoNumbers(node3, node4));

    }
}
