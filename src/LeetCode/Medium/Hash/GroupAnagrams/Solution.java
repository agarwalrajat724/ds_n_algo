package LeetCode.Medium.Hash.GroupAnagrams;

import java.util.*;

/**
 * Given an array of strings, group anagrams together.
 *
 * Example:
 *
 * Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
 * Output:
 * [
 *   ["ate","eat","tea"],
 *   ["nat","tan"],
 *   ["bat"]
 * ]
 * Note:
 *
 * All inputs will be in lowercase.
 * The order of your output does not matter.
 */
public class Solution {


    public List<List<String>> groupAnagrams(String[] strs) {

        Map<String, List<String>> map = new HashMap<>();

        for (String s : strs) {
            char[] chars = s.toCharArray();
            Arrays.sort(chars);
            String key = String.valueOf(chars);
            if (!map.containsKey(key)) {
                List<String> list = new ArrayList<>();
                list.add(s);
                map.put(key, list);
            } else {
                map.get(key).add(s);
            }
        }

        return new ArrayList<>(map.values());

    }

    private List<List<String>> groupAnagramsCountBased(String[] strings) {
        if (null == strings || strings.length == 0) {
            return new ArrayList<>();
        }

        Map<String, List<String>> map = new HashMap<>();
        int count[] = new int[26];
        for (String s : strings) {
            Arrays.fill(count, 0);

            for (char ch : s.toCharArray()) {
                count[ch - 'a']++;
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int i =0; i < 26; i++) {
                stringBuilder.append("#");
                stringBuilder.append(count[i]);
            }

            String key = stringBuilder.toString();

            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }

            map.get(key).add(s);

        }

        return new ArrayList<>(map.values());

    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        List<List<String>> result = solution.groupAnagramsCountBased(new String[]{"eat", "tea", "tan", "ate", "nat", "bat"});

        result.forEach(strings -> result.forEach(strings1 -> System.out.println(strings)));
    }
}
