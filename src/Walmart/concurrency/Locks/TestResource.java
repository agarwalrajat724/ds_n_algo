package Walmart.concurrency.Locks;

import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestResource {

    private final Lock displayQueueLock = new ReentrantLock();
    private final Lock readQueueLock = new ReentrantLock();

    public void displayRecord() {
        final Lock displayLock = this.displayQueueLock;
        displayLock.lock();
        try
        {
            Long duration =
                    (long) (Math.random() * 10000);
            System.out.println(Thread.currentThread().
                    getName() + ": TestResource: display a Job"+
                    " during " + (duration / 1000) + " seconds ::"+
                    " Time - " + new Date());
            Thread.sleep(duration);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        finally
        {
            System.out.printf("%s: The document has been"+
                    " dispalyed\n", Thread.currentThread().getName());
            displayLock.unlock();
        }
    }

    public void readRecord()
    {
        final Lock readQueueLock = this.readQueueLock;
        readQueueLock.lock();
        try
        {
            Long duration =
                    (long) (Math.random() * 10000);
            System.out.println
                    (Thread.currentThread().getName()
                            + ": TestResource: reading a Job during " +
                            (duration / 1000) + " seconds :: Time - " +
                            new Date());
            Thread.sleep(duration);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        finally
        {
            System.out.printf("%s: The document has"+
                    " been read\n", Thread.currentThread().getName());
            readQueueLock.unlock();
        }
    }
}
