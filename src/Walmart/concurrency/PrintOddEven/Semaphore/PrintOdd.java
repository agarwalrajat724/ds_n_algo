package Walmart.concurrency.PrintOddEven.Semaphore;

public class PrintOdd implements Runnable {

    private final int max;

    private final SharedPrinter sharedPrinter;

    public PrintOdd(int max, SharedPrinter sharedPrinter) {
        this.max = max;
        this.sharedPrinter = sharedPrinter;
    }

    @Override
    public void run() {
        for (int i = 1; i <= max; i = i + 2) {
            sharedPrinter.printOdd(i);
        }
    }
}
