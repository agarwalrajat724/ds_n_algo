package GeekForGeeks.DSA_Class.Trees.Inorder;

import GeekForGeeks.DSA_Class.Trees.Node;

import java.util.Stack;

public class GfG {

    void inOrder(Node root)
    {
        // Your code here

        if (null == root) {
            return;
        }
        Stack<Node> stack = new Stack<>();
        Node current = root;

        while (null != current || !stack.isEmpty()) {
            while (null != current) {
                stack.push(current);
                current = current.left;
            }

            Node pop = stack.pop();
            System.out.print(pop.data + " ");
            current = pop.right;
        }
    }

    public static void main(String[] args) {
        GfG inorder = new GfG();
        Node root = new Node(1);
        root.left = new Node(10);
        root.right = new Node(39);
        root.left.left = new Node(5);
        inorder.inOrder(root);
    }
}
