package hackerrank.InterviewKit.Strings.SpecialPalindrome;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

/**
 * https://www.hackerrank.com/challenges/special-palindrome-again/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=strings
 */
public class Solution {

    // Complete the substrCount function below.
    static long substrCount(int n, String s) {

        Map<Character, List<Integer>> map = new HashMap<>();
        char[] array = s.toCharArray();
        int result = 0;
        int[] sameChar = new int[n];
        int i = 0;
        while (i < n) {
            int sameCharCount = 1;
            int j = i + 1;
            while ((j < n) && array[j] == array[i]) {
                sameCharCount++;
                j++;
            }
            result += (sameCharCount * (sameCharCount + 1)) / 2;
            sameChar[i] = sameCharCount;
            i = j;
        }

        for (int j = 1; j < n; j++) {
            if (array[j] == array[j - 1]) {
                sameChar[j] = sameChar[j - 1];
            }
            if ((j > 0 && j < n - 1) && (array[j - 1] == array[j + 1]) && array[j] != array[j - 1]) {
                result += Math.min(sameChar[j - 1], sameChar[j + 1]);
            }
        }

        return result;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        long result = substrCount(n, s);

        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

}
