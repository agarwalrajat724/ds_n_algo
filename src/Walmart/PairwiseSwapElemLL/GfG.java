package Walmart.PairwiseSwapElemLL;

class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
    }
}

public class GfG {

    public static void pairwiseSwap(Node node)
    {
        if (null == node) {
            return;
        }

        Node head = node;

        while (null != head && null != head.next) {
            int temp = head.data;
            head.data = head.next.data;
            head.next.data = temp;
            head = head.next.next;
        }
    }

    public static void printLL(Node node) {
        Node head = node;
        while (null != head) {
            System.out.println(head.data + " -> ");
            head = head.next;
        }
    }

    public static void main(String[] args) {
        Node node = new Node(1);
        node.next = new Node(2);
        node.next.next = new Node(2);
        node.next.next.next = new Node(4);
        node.next.next.next.next = new Node(5);
        node.next.next.next.next.next = new Node(6);
        node.next.next.next.next.next.next = new Node(7);
        node.next.next.next.next.next.next.next = new Node(8);

        pairwiseSwap(node);
        printLL(node);
    }
}
