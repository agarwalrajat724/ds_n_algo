package Walmart.concurrency.PrintOddEven.Semaphore;

public class PrintEven implements Runnable {

    private int max;
    private SharedPrinter sharedPrinter;

    public PrintEven(int max, SharedPrinter sharedPrinter) {
        this.max = max;
        this.sharedPrinter = sharedPrinter;
    }


    @Override
    public void run() {
        for (int i = 2; i <= max; i = i + 2) {
            sharedPrinter.printEven(i);
        }
    }
}
