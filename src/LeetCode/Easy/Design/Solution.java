package LeetCode.Easy.Design;

import java.util.Arrays;
import java.util.Random;

/**
 * Shuffle a set of numbers without duplicates.
 *
 * Example:
 *
 * // Init an array with set 1, 2, and 3.
 * int[] nums = {1,2,3};
 * KthSmallestElem solution = new KthSmallestElem(nums);
 *
 * // Shuffle the array [1,2,3] and return its result. Any permutation of [1,2,3] must equally likely to be returned.
 * solution.shuffle();
 *
 * // Resets the array back to its original configuration [1,2,3].
 * solution.reset();
 *
 * // Returns the random shuffling of array [1,2,3].
 * solution.shuffle();
 */
public class Solution {

    int[] original = null;
    int[] randomNums = null;
    Random random = null;

    public Solution(int[] nums) {
        this.original = nums.clone();
        this.randomNums = nums.clone();
        this.random = new Random();
    }

    /** Resets the array to its original configuration and return it. */
    public int[] reset() {
        return original;
    }

    /** Returns a random shuffling of the array. */
    public int[] shuffle() {
        for (int i = randomNums.length - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            int temp = randomNums[i];
            randomNums[i] = randomNums[index];
            randomNums[index] = temp;
        }
        return randomNums;
    }

    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 0; i <100; i++) {
            int j = random.nextInt(10);
            System.out.println("10 , i : " + j);
        }
    }
}

/**
 * Your KthSmallestElem object will be instantiated and called as such:
 * KthSmallestElem obj = new KthSmallestElem(nums);
 * int[] param_1 = obj.reset();
 * int[] param_2 = obj.shuffle();
 */
