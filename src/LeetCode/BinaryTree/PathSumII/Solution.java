package LeetCode.BinaryTree.PathSumII;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<List<Integer>> result = new ArrayList<>();
        int path[] = new int[1000];

        pathSumUtil(root, result, path, sum, 0);

        return result;
    }

    private void pathSumUtil(TreeNode root, List<List<Integer>> result, int[] path, int sum, int depth) {
        if (null == root) {
            return;
        }

        path[depth] = root.val;
        if (null == root.left && null == root.right) {

            int total = 0;
            for (int index = 0; index <= depth; index++) {
                total += path[index];
            }
            if (total == sum) {
                List<Integer> list = new ArrayList<>(depth + 1);
                for (int i = 0; i <= depth; i++) {
                    list.add(path[i]);
                }
                result.add(list);
                //result.add(Arrays.stream(path).boxed().collect(Collectors.toList()));
            }
        } else {
            pathSumUtil(root.left, result, path, sum, depth + 1);
            pathSumUtil(root.right, result, path, sum, depth + 1);
        }
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

//        TreeNode root = new TreeNode(5);
//        root.left = new TreeNode(4);
//        root.right = new TreeNode(8);
//
//        root.left.left = new TreeNode(11);
//
//        root.left.left.left = new TreeNode(7);
//        root.left.left.right = new TreeNode(2);
//
//        root.right.left = new TreeNode(13);
//        root.right.right = new TreeNode(4);
//        root.right.right.right = new TreeNode(1);
//        root.right.right.left = new TreeNode(5);

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(-2);
        root.right = new TreeNode(-3);

        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);

        root.right.left = new TreeNode(-2);
        root.left.left.left = new TreeNode(-1);

        solution.pathSum(root, 2);
    }
}
