package LeetCode.BinaryTree.UniqBST;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *  96. Unique Binary Search Trees
 *
 * Given n, how many structurally unique BST's (binary search trees) that store values 1 ... n?
 *
 * Example:
 *
 * Input: 3
 * Output: 5
 * Explanation:
 * Given n = 3, there are a total of 5 unique BST's:
 *
 *    1         3     3      2      1
 *     \       /     /      / \      \
 *      3     2     1      1   3      2
 *     /     /       \                 \
 *    2     1         2                 3
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }


    public int numTrees(int n) {

        if (n <= 2) {
            return n;
        }

        int catalan[] = new int[n + 1];

        catalan[0] = 1;
        catalan[1] = 1;

        for (int i = 2; i <= n; i++) {
            catalan[i] = 0;
            for (int j = 0; j < i; j++) {
                catalan[i] = catalan[i] + (catalan[j] * catalan[i-j-1]);
            }
        }
        return catalan[n];
    }

    private List<TreeNode> generateUniqBST(int start, int end) {

        List<TreeNode> list = new ArrayList<>();

        if (start > end) {
            list.add(null);
            return list;
        }

        for (int i = start; i <= end; i++) {


            List<TreeNode> leftTree = generateUniqBST(start, i - 1);
            List<TreeNode> rightTree = generateUniqBST(i + 1, end);

            for (int j = 0; j < leftTree.size(); j++) {
                TreeNode left = leftTree.get(j);

                for (int k = 0; k < rightTree.size(); k++) {
                    TreeNode right = rightTree.get(k);
                    TreeNode node = new TreeNode(i);

                    node.left = left;
                    node.right = right;

                    list.add(node);
                }
            }
        }

        //list = getOutputResult(list);

        return list;
    }

//    private List<TreeNode> getOutputResult(List<TreeNode> list) {
//
//        for (TreeNode treeNode : list) {
//            Queue<TreeNode> queue = new LinkedList<>();
//            List<>
//            queue.offer(treeNode)
//        }
//    }

    public void preOrderTraversal(TreeNode root) {
        if (null != root) {
            System.out.println(root.val);
            preOrderTraversal(root.left);
            preOrderTraversal(root.right);
        }
    }



    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.numTrees(3));
        System.out.println(solution.numTrees(5));

        List<TreeNode> result = solution.generateUniqBST(1,3);

        result.forEach(treeNode -> {
            System.out.println("Preorder");
            solution.preOrderTraversal(treeNode);
        });



    }
}

// Initialize list of BST as empty
// for every i = 1 to N
//     create a node with value i
//     recursively create leftSubTree and rightSubtree

// Iterate for all leftSubtree
//  for current leftSubtree, iterate for all right subtrees
//      add current left and right to node
//      add node to the list
