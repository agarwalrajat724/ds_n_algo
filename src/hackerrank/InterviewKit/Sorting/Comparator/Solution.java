package hackerrank.InterviewKit.Sorting.Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {


    public static void main(String[] args) {
        List<Player> players = new ArrayList<>();

        players.add(new Player("Smith", 20));
        players.add(new Player("Jones", 15));
        players.add(new Player("Jones", 20));

        Checker checker = new Checker();

        Collections.sort(players, checker);


        players.forEach(player -> System.out.println(player));
    }
}
