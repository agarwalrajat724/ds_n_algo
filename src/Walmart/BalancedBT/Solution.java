package Walmart.BalancedBT;

import GeekForGeeks.DSA_Class.Trees.TreeNode;



public class Solution {

    static class Height {
        int h;
    }

    public boolean isBalanced(TreeNode root) {
        if (null == root || (null == root.left && null == root.right)) {
            return true;
        }

        return isBalancedUtil(root, new Height());
    }

    private boolean isBalancedUtil(TreeNode root, Height height) {
        if (null == root) {
            height.h = 0;
            return true;
        }

        Height lHeight = new Height();
        Height rHeight = new Height();

        boolean l = isBalancedUtil(root.left, lHeight);
        boolean r = isBalancedUtil(root.right, rHeight);

        height.h = 1 + Math.max(lHeight.h, rHeight.h);

        if (Math.abs(lHeight.h - rHeight.h) > 1) {
            return false;
        }

        return l && r;
    }
}
