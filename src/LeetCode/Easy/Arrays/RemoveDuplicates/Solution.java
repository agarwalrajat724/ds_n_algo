package LeetCode.Easy.Arrays.RemoveDuplicates;

/**
 * Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
 * <p>
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 * <p>
 * Example 1:
 * <p>
 * Given nums = [1,1,2],
 * <p>
 * Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
 * <p>
 * It doesn't matter what you leave beyond the returned length.
 * Example 2:
 * <p>
 * Given nums = [0,0,1,1,1,2,2,3,3,4],
 * <p>
 * Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.
 * <p>
 * It doesn't matter what values are set beyond the returned length.
 */
class Solution {

    public int removeDuplicates(int[] nums) {

        int n = nums.length;
        int len = 0;
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            int last = nums[0];
            for (int i = 1; i < n; i++) {
                if (last == nums[i]) {
                    len++;
                }
                last = nums[i];
            }
        }
        return (nums.length - len);
    }

    public int removeDuplicates1(int[] nums) {

        if (nums.length < 2) {
            return nums.length;
        }

        int i = 0;
        int j = 1;

        while (j < nums.length) {
            if (nums[i] == nums[j]) {
                j++;
            } else {
                i++;
                nums[i] = nums[j];
                j++;
            }
        }

        return (i + 1);
    }

    public static void main(String[] args) {

        Solution solution = new Solution();

        int nums[] = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};

        int len = solution.removeDuplicates1(nums);

        System.out.println(len);

    }

}
