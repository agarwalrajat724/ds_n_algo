package GeekForGeeks.MustDo.Arrays.StockBuyNSell;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class GFG {


    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer stringTokenizer = null;
        try {
            int T = Integer.parseInt(bufferedReader.readLine());

            for (int i = 0; i < T; i++) {
                int n = Integer.parseInt(bufferedReader.readLine());
                int[] arr = new int[n];
                stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int j = 0;
                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }

                GFG.printDays(arr);
            }


        } catch (Exception ex) {

        }

    }

    /**
     * 100 180 260 310 40 535 695
     *
     * 23 13 25 29 33 19 34 45 65 67
     * @param arr
     */
    private static void printDays(int[] arr) {

        int start = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i - 1]) {
                if (((i-1) - start) >= 1) {
                    System.out.println("(" + start + "," + (i - 1) + ")");
                }
                start = i;
            }
        }

        if (arr[start] < arr[arr.length - 1] && ((arr.length - (1 + start)) >= 1)) {
            System.out.println("(" + start + "," + (arr.length - 1) + ")");
        }

    }
}
