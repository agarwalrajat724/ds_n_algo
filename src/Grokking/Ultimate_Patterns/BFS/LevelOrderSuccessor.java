package Grokking.Ultimate_Patterns.BFS;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Sixth
 *
 * Given a binary tree and a node, find the level order successor of the given
 * node in the tree. The level order successor is the node that appears right after
 * the given node in the level order traversal.
 */
public class LevelOrderSuccessor {

    public static TreeNode findSuccessor(TreeNode root, int key) {
        if (null == root) {
            return null;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            TreeNode pop = queue.poll();

            if (null != pop.left) {
                queue.offer(pop.left);
            }

            if (null != pop.right) {
                queue.offer(pop.right);
            }
            if (key == pop.val) {
                return queue.peek();
            }
        }
        return null;
    }

    public static TreeNode findSuccessor_1(TreeNode root, int key) {

        if (null == root) {
            return null;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        Boolean isNextResult = Boolean.FALSE;

        while (!queue.isEmpty()) {
            TreeNode pop = queue.poll();
            if (isNextResult) {
                return pop;
            }
            if (key == pop.val) {
                isNextResult = Boolean.TRUE;
            }

            if (null != pop.left) {
                queue.offer(pop.left);
            }

            if (null != pop.right) {
                queue.offer(pop.right);
            }
        }
        return null;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(9);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        TreeNode result = LevelOrderSuccessor.findSuccessor(root, 12);
        if (result != null) {
            System.out.println(result.val + " ");
        }
        result = LevelOrderSuccessor.findSuccessor(root, 9);
        if (result != null) {
            System.out.println(result.val + " ");
        }
    }
}
