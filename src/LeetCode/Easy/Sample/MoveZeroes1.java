package LeetCode.Easy.Sample;


import java.util.Arrays;

public class MoveZeroes1 {


    public void moveZeroes(int[] nums) {

        if (null == nums || nums.length < 2) {
            return;
        }

        int lastZeroAt = 0;

        for (int curr = 0; curr < nums.length; curr++) {
            if (nums[curr] != 0) {
                int temp = nums[lastZeroAt];
                nums[lastZeroAt] = nums[curr];
                nums[curr] = temp;
                lastZeroAt++;
            }
        }

        Arrays.stream(nums).boxed().forEach(i -> System.out.println(i));
    }

    public static void main(String[] args) {
        MoveZeroes1 solution = new MoveZeroes1();

        solution.moveZeroes(new int[]{0,1,0,3,12});
    }
}
