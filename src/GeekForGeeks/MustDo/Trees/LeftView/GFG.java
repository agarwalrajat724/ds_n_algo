package GeekForGeeks.MustDo.Trees.LeftView;

import java.util.LinkedList;
import java.util.Queue;

public class GFG {


    static class Node {
        int data;
        Node left, right;

        public Node(int data) {
            this.data = data;
        }
    }


    void leftView(Node root)
    {
        if (null == root) {
            return;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                Node pop = queue.poll();
                if (i == 0) {
                    System.out.println(pop.data + " ");
                }

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }

        }
    }

    public static void main(String[] args) {
        GFG gfg = new GFG();

        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.left.left.right = new Node(8);

        root.right.left = new Node(6);
        root.right.right = new Node(7);

        gfg.leftView(root);
    }
}
