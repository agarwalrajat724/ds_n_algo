package Grokking.Ultimate_Patterns.Two_Pointers;


/**
 * Maximum Trapping Water (medium)
 *
 * Suppose you are given an array containing non-negative numbers representing heights of a set
 * of buildings. Now, because of differences in heights of buildings water can be trapped between them.
 * Find the two buildings that will trap the most amount of water. Write a function that will return
 * the maximum volume of water that will be trapped between these two buildings.
 *
 * Input: [1, 3, 5, 4, 1]
 * Output: 6
 * Explanation: The maximum water will be trapped between buildings of height 3 and 4.
 *
 * Input: [3, 2, 5, 4, 2]
 * Output: 9
 * Explanation: The maximum water will be trapped between buildings of height 3 and 4.
 *
 * Input: [1, 4, 3, 2, 5, 8, 4]
 * Output: 20
 * Explanation: The maximum water will be trapped between buildings of height 4 and 4.
 *
 */
public class WaterContainer {
    public static int findMaxWater(int[] buildingHeights) {
        int area = Integer.MIN_VALUE;
        int low = 0, high = buildingHeights.length - 1;

        while (low < high) {
            int currArea = 0;
            if (buildingHeights[low] < buildingHeights[high]) {
                currArea = (high - low) * buildingHeights[low];
                low++;
            } else {
                currArea = (high - low) * buildingHeights[high];
                high--;
            }
            area = Math.max(area, currArea);
        }
        return area;
    }

    public static void main(String[] args) {
        System.out.println(WaterContainer.findMaxWater(new int[] { 1, 3, 5, 4, 1 }));
        System.out.println(WaterContainer.findMaxWater(new int[] { 3, 2, 5, 4, 2 }));
        System.out.println(WaterContainer.findMaxWater(new int[] { 1, 4, 3, 2, 5, 8, 4 }));
    }
}
