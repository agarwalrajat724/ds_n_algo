package GeekForGeeks.MustDo.Arrays.Sort0s1s2s;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class GFG {

    public static void sort(int[] arr) {
        int lo = 0, mid = 0, hi = arr.length - 1, temp = 0;

        while (mid <= hi) {
            switch (arr[mid]) {
                case 0: {
                    temp = arr[lo];
                    arr[lo] = arr[mid];
                    arr[mid] = temp;
                    lo++;
                    mid++;
                    break;
                }
                case 1: {
                    mid++;
                    break;
                }

                case 2: {
                    temp = arr[mid];
                    arr[mid] = arr[hi];
                    arr[hi] = temp;
                    hi--;
                    break;
                }

            }
        }

    }


    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int T = Integer.parseInt(bufferedReader.readLine());

            for (int i = 0; i < T; i++) {
                int n = Integer.parseInt(bufferedReader.readLine());

                int arr[] = new int[n];
                StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int j = 0;
                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }
                GFG.sort(arr);
                Arrays.stream(arr).forEach(i1 -> System.out.println(i1));

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
