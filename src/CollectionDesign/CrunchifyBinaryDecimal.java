package CollectionDesign;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Crunchify.com
 * Program: Java program to convert number from Binary to Decimal and Decimal to Binary
 * Version: 1.0.0
 *
 */
public class CrunchifyBinaryDecimal {

    // Main() method.
    // We will create 10 Binary value from Decimal.
    // Then we will convert those Binary value to Decimal
    public static void main(String a[]) {

        CrunchifyBinaryDecimal object = new CrunchifyBinaryDecimal();

        object.crunchifyGenerateBinaryNumbers(10);
    }

    // This method will convert Decimal to Binary
    public void crunchifyGenerateBinaryNumbers(int n) {

        // Queue is a collection designed for holding elements prior to processing.
        Queue<String> crunchifyQueue = new LinkedList<String>();

        // Enqueue the first binary number
        crunchifyQueue.add("1");

        // This loops is Binary First Search of a tree with 1 as root
        // We will make sure left child always be 0 and right child 1
        for (int i = 1; i <= n; i++) {

            String crunchifyString = crunchifyQueue.peek();
            crunchifyQueue.remove();
            println("We have generated Binary element for number " + i + " which is: " + crunchifyString);

            // calling convertBinaryToDecimal() method to convert it back to decima :)
            println("Converting back to Decimal: " + convertBinaryToDecimal(Integer.parseInt(crunchifyString)) + "\n");

            // Make sure we are storing previous value
            String previousFront = crunchifyString;

            // Append "0" to crunchifyQueue and enqueue it
            crunchifyQueue.add(crunchifyString + "0");

            // Append "1" to previousFront and enqueue it.
            crunchifyQueue.add(previousFront + "1");

        }

    }

    // This method will convert Binary Value to Decimal
    public static int convertBinaryToDecimal(int crunchifyBinaryValue) {

        int crunchifyDecimalValue = 0;
        int crunchifyPower = 0;
        while (true) {
            if (crunchifyBinaryValue == 0) {
                break;
            } else {
                int crunchifyTemp = crunchifyBinaryValue % 10;
                crunchifyDecimalValue += crunchifyTemp * Math.pow(2, crunchifyPower);
                crunchifyBinaryValue = crunchifyBinaryValue / 10;
                crunchifyPower++;
            }
        }
        return crunchifyDecimalValue;
    }

    private static void println(String string) {
        System.out.println(string);

    }

}

