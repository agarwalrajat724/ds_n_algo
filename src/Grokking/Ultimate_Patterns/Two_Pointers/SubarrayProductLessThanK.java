package Grokking.Ultimate_Patterns.Two_Pointers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Subarrays with Product Less than a Target (medium)
 *
 * Given an array with positive numbers and a target number, find all of its subarrays
 * (contiguous set of elements) whose product is less than the target number.
 *
 * Input: [2, 5, 3, 10], target=30
 * Output: [2], [5], [2, 5], [3], [5, 3], [10]
 * Explanation: There are six subarrays whose product is less than the target.
 *
 * Input: [8, 2, 6, 5], target=50
 * Output: [8], [2], [8, 2], [6], [2, 6], [5], [6, 5]
 * Explanation: There are seven subarrays whose product is less than the target.
 *
 */
public class SubarrayProductLessThanK {

    public static List<List<Integer>> findSubarrays(int[] arr, int target) {
        List<List<Integer>> subarrays = new ArrayList<>();

        int left = 0, product = 1;

        for (int right = 0; right < arr.length; right++) {
            product = product * arr[right];

            while (product >= target && left < arr.length) {
                product = product/arr[left];
                left++;
            }

            List<Integer> tempLinkedList = new LinkedList<>();
            for (int i = right; i >= left; i--) {
                tempLinkedList.add(0, arr[i]);
                subarrays.add(new ArrayList<>(tempLinkedList));
            }
        }
        return subarrays;
    }

    public static void main(String[] args) {
        System.out.println(SubarrayProductLessThanK.findSubarrays(new int[] { 2, 5, 3, 10 }, 30));
        System.out.println(SubarrayProductLessThanK.findSubarrays(new int[] { 8, 2, 6, 5 }, 50));
    }
}
