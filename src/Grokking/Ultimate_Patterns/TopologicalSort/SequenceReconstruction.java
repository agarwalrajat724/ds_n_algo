package Grokking.Ultimate_Patterns.TopologicalSort;

import java.util.*;

/**
 * Given a sequence originalSeq and an array of sequences, write a method to find if originalSeq can be
 * uniquely reconstructed from the array of sequences.
 *
 * Unique reconstruction means that we need to find if originalSeq is the only sequence such that all
 * sequences in the array are subsequences of it.
 *
 * Input: originalSeq: [1, 2, 3, 4], seqs: [[1, 2], [2, 3], [3, 4]]
 * Output: true
 * Explanation: The sequences [1, 2], [2, 3], and [3, 4] can uniquely reconstruct [1, 2, 3, 4], in other
 * words, all the given sequences uniquely define the order of numbers in the 'originalSeq'.
 *
 * Input: originalSeq: [1, 2, 3, 4], seqs: [[1, 2], [2, 3], [2, 4]]
 * Output: false
 * Explanation: The sequences [1, 2], [2, 3], and [2, 4] cannot uniquely reconstruct [1, 2, 3, 4]. There
 * are two possible sequences we can construct from the given sequences:
 * 1) [1, 2, 3, 4]
 * 2) [1, 2, 4, 3]
 *
 * Input: originalSeq: [3, 1, 4, 2, 5], seqs: [[3, 1, 5], [1, 4, 2, 5]]
 * Output: true
 * Explanation: The sequences [3, 1, 5] and [1, 4, 2, 5] can uniquely reconstruct [3, 1, 4, 2, 5].
 *
 */
public class SequenceReconstruction {

    public static boolean canConstruct(int[] originalSeq, int[][] sequences) {
        List<Integer> sortedOrder = new ArrayList<>();
        if (originalSeq.length <= 0)
            return false;

        // a. Initialize the graph
        HashMap<Integer, Integer> inDegree = new HashMap<>(); // count of incoming edges for every vertex
        HashMap<Integer, List<Integer>> graph = new HashMap<>(); // adjacency list graph
        Queue<Integer> sources = new LinkedList<>();
        for (int[] sequence : sequences) {
            for (int i = 0; i < sequence.length; i++) {
                inDegree.putIfAbsent(sequence[i], 0);
                graph.putIfAbsent(sequence[i], new ArrayList<>());
            }
        }

        for (int[] sequence : sequences) {
            for (int i = 1; i < sequence.length; i++) {
                int parent = sequence[i - 1], child = sequence[i];
                graph.get(parent).add(child);
                inDegree.put(child, inDegree.get(child) + 1);
            }
        }

        // if we don't have ordering rules for all the numbers we'll not able to uniquely construct the sequence
        if (inDegree.size() != originalSeq.length)
            return false;

        for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
            if (entry.getValue() == 0) {
                sources.offer(entry.getKey());
            }
        }

        if (sources.size() > 1) {
            return false;
        }

        while (!sources.isEmpty()) {

            if (sources.size() > 1) {
                return false;
            }

            int source = sources.poll();
            sortedOrder.add(source);
            if (originalSeq[sortedOrder.size() - 1] != source) {
                return false;
            }


            List<Integer> children = graph.get(source);
            for (int child : children) {
                if (inDegree.containsKey(child)) {
                    inDegree.put(child, inDegree.get(child) - 1);

                    if (inDegree.get(child) == 0) {
                        sources.add(child);
                    }
                }
            }
        }
        return sortedOrder.size() == originalSeq.length;
    }

    public static void main(String[] args) {
        boolean result = SequenceReconstruction.canConstruct(new int[] { 1, 2, 3, 4 },
                new int[][] { new int[] { 1, 2 }, new int[] { 2, 3 }, new int[] { 3, 4 } });
        System.out.println("Can we uniquely construct the sequence: " + result);

        result = SequenceReconstruction.canConstruct(new int[] { 1, 2, 3, 4 },
                new int[][] { new int[] { 1, 2 }, new int[] { 2, 3 }, new int[] { 2, 4 } });
        System.out.println("Can we uniquely construct the sequence: " + result);

        result = SequenceReconstruction.canConstruct(new int[] { 3, 1, 4, 2, 5 },
                new int[][] { new int[] { 3, 1, 5 }, new int[] { 1, 4, 2, 5 } });
        System.out.println("Can we uniquely construct the sequence: " + result);
    }

}
