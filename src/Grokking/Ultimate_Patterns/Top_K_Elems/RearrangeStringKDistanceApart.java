package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.*;

/**
 * Rearrange String K Distance Apart (hard)
 *
 * Given a string and a number ‘K’, find if the string can be rearranged such that
 * the same characters are at least ‘K’ distance apart from each other.
 *
 * Input: "mmpp", K=2
 * Output: "mpmp" or "pmpm"
 * Explanation: All same characters are 2 distance apart.
 *
 * Input: "Programming", K=3
 * Output: "rgmPrgmiano" or "gmringmrPoa"
 * Explanation: All same characters are 3 distance apart.
 *
 * Input: "aab", K=2
 * Output: "aba"
 * Explanation: All same characters are 2 distance apart.
 *
 * Input: "aappa", K=3
 * Output: ""
 * Explanation: We cannot find an arrangement of the string where any two 'a' are 3 distance apart.
 *
 */
public class RearrangeStringKDistanceApart {

    public static String reorganizeString(String str, int k) {
        char[] arr = str.toCharArray();
        Map<Character, Integer> frequencyMap = new HashMap<>();
        Queue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>((o1, o2) -> o2.getValue() - o1.getValue());
        Queue<Map.Entry<Character, Integer>> queue = new LinkedList<>();

        for (char ch : arr) {
            frequencyMap.put(ch, frequencyMap.getOrDefault(ch, 0) + 1);
        }
        maxHeap.addAll(frequencyMap.entrySet());
        StringBuilder result = new StringBuilder();

        while (!maxHeap.isEmpty()) {
            Map.Entry<Character, Integer> currentEntry = maxHeap.poll();
            result.append(currentEntry.getKey());
            currentEntry.setValue(currentEntry.getValue() - 1);
            queue.offer(currentEntry);
            if (queue.size() == k) {
                Map.Entry<Character, Integer> previousEntry = queue.poll();
                if (previousEntry.getValue() > 0) {
                    maxHeap.offer(previousEntry);
                }
            }
        }
        return result.toString().length() == str.length() ? result.toString() : "";
    }

    public static void main(String[] args) {
        System.out.println("Reorganized string: " +
                RearrangeStringKDistanceApart.reorganizeString("mmpp", 2));
        System.out.println("Reorganized string: " +
                RearrangeStringKDistanceApart.reorganizeString("Programming", 3));
        System.out.println("Reorganized string: " +
                RearrangeStringKDistanceApart.reorganizeString("aab", 2));
        System.out.println("Reorganized string: " +
                RearrangeStringKDistanceApart.reorganizeString("aappa", 3));
    }
}
