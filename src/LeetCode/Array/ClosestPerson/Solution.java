package LeetCode.Array.ClosestPerson;

import java.util.Arrays;

/**
 * In a row of seats, 1 represents a person sitting in that seat, and 0 represents that the seat is empty.
 *
 * There is at least one empty seat, and at least one person sitting.
 *
 * Alex wants to sit in the seat such that the distance between him and the closest person to him is maximized.
 *
 * Return that maximum distance to closest person.
 *
 * Example 1:
 *
 * Input: [1,0,0,0,1,0,1]
 * Output: 2
 * Explanation:
 * If Alex sits in the second open seat (seats[2]), then the closest person has distance 2.
 * If Alex sits in any other open seat, the closest person has distance 1.
 * Thus, the maximum distance to the closest person is 2.
 * Example 2:
 *
 * Input: [1,0,0,0]
 * Output: 3
 * Explanation:
 * If Alex sits in the last seat, the closest person is 3 seats away.
 * This is the maximum distance possible, so the answer is 3.
 * Note:
 *
 * 1 <= seats.length <= 20000
 * seats contains only 0s or 1s, at least one 0, and at least one 1
 */
public class Solution {

    public int maxDistToClosestSpace(int[] seats) {
        if (null == seats) {
            return 0;
        }
        int ans = 0;
        int N = seats.length;
        int[] left = new int[N];
        int[] right = new int[N];

        Arrays.fill(left, N);
        Arrays.fill(right, N);

        for (int i = 0; i < N; i++) {
            if (seats[i] == 1) {
                left[i] = 0;
            } else if (i > 0) {
                left[i] = left[i - 1] + 1;
            }
        }

        for (int i = N - 1; i >= 0; i--) {
            if (seats[i] == 1) {
                right[i] = 0;
            } else if (i < N - 1) {
                right[i] = right[i+1] + 1;
            }
        }

        for (int i = 0; i < N; i++) {
            ans = Math.max(ans, Math.min(left[i], right[i]));
        }

        return ans;
    }

    public int maxDistToClosest(int[] seats) {
        if (null == seats) {
            return 0;
        }
        int ans = 0;
        int N = seats.length;
        int K = 0;

        for (int i = 0; i < N; i++) {
            if (seats[i] == 1) {
                K = 0;
            } else {
                K++;
                ans = Math.max(ans, (K+1)/2);
            }
        }

        for (int i = 0; i < N; i++) {
            if (seats[i] == 1) {
                ans = Math.max(ans, i);
                break;
            }
        }

        for (int i = N-1; i >= 0; i--) {
            if (seats[i] == 1) {
                ans = Math.max(ans, N - i - 1);
                break;
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.maxDistToClosest(new int[]{1,0,0,0,1,0,1}));
        System.out.println(solution.maxDistToClosest(new int[]{1,0,0,0}));
    }
}
