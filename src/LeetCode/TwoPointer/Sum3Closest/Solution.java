package LeetCode.TwoPointer.Sum3Closest;

import java.util.Arrays;

public class Solution {


    public int threeSumClosest(int[] nums, int target) {
        if (null == nums && nums.length < 3) {
            return -1;
        }

        Arrays.sort(nums);
        int n = nums.length - 1;
        int maxDiff = Integer.MAX_VALUE;
        int result = -1;

        for (int i = 0; i < n - 1; i++) {

            int l = i + 1;
            int r = n;

            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];

                int curDiff = Math.abs(sum - target);

                if (curDiff == 0) {
                    return sum;
                }

                if (curDiff < maxDiff) {
                    maxDiff = curDiff;
                    result = sum;
                }

                if (sum <= target) {
                    l++;
                } else {
                    r--;
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.threeSumClosest(new int[]{-1, 2, 1, -4}, 1));
    }
}
