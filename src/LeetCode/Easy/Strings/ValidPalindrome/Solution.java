package LeetCode.Easy.Strings.ValidPalindrome;

public class Solution {

    public boolean isPalindrome(String s) {

        if (null == s || s.length() < 2) {
            return true;
        }

        char arr[] = s.toCharArray();

        int l = 0;
        int h = arr.length - 1;
        int n = arr.length;

        while (l < h) {
            while (l < n && !Character.isLetterOrDigit(arr[l])) {
                l++;
            }

            while (h >= 0 && !Character.isLetterOrDigit(arr[h])) {
                h--;
            }

            if (l < n && h >= 0) {
                if (String.valueOf(arr[l]).equalsIgnoreCase(String.valueOf(arr[h]))) {
                    l++;
                    h--;
                } else {
                    return false;
                }
            }
        }

        return true;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isPalindrome("race a car"));

        System.out.println(solution.isPalindrome("A man, a plan, a canal: Panama"));

        System.out.println(solution.isPalindrome("0P"));
    }
}
