package LeetCode.Design.DesignHashSet;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * It allows at most one null element.
 * It internally uses HashMap as backing data structure and stores private static final Object class instance PRESENT
 * as a value of all the keys. As we are storing same static field as value to all the keys, we can neglect additional
 * space being used by this value field.
 * HashSet's add() method uses HashMap's put() method internally.
 * As it is backed by HashMap, we must override hashCode()  and  equals() method while creating HashSet of custom class
 * objects. Refer our article How does HashMap work internally in Java? to know more about HashMap internal implementation.
 * HashSet is not thread-safe. Here are some of the thread-safe implementation of Set:
 * –CopyOnWriteArraySet
 * –Collections.synchronizedSet(Set set)
 * –ConcurrentSkipListSet
 * –​Collections.newSetFromMap(new ConcurrentHashMap())
 *
 * HashSet doesn't maintain insertion order. To maintain insertion or access order, we need to use LinkedHashSet.
 * HashSet also doesn't store elements in sorted order. Instead we should use TreeSet to store elements in sorted order.
 * Similar to HashSet, LinkedHashSet has also been implemented using LinkedHashMap and TreeSet is implemented using TreeMap.
 * And all these data structure uses private static final Object class instance PRESENT as a value of all their keys.
 *
 *
 */

public class MyHashSet {

    private Map<Integer, Object> map;

    private static final Object PRESENT = new Object();


    /** Initialize your data structure here. */
    public MyHashSet() {
        map = new HashMap<>();
    }

    public void add(int key) {
        boolean isAdded = (map.put(key, PRESENT) == null);
    }

    public void remove(int key) {
        boolean isRemoved = (map.remove(key) == PRESENT);
    }

    /** Returns true if this set contains the specified element */
    public boolean contains(int key) {
        return map.containsKey(key);
    }


    public static void main(String[] args) {

        MyHashSet hashSet = new MyHashSet();
        hashSet.add(1);
        hashSet.add(2);
        System.out.println(hashSet.contains(1));    // returns true
        System.out.println(hashSet.contains(3));    // returns false (not found)
        hashSet.add(2);
        System.out.println(hashSet.contains(2));    // returns true
        hashSet.remove(2);
        System.out.println(hashSet.contains(2));    // returns false (already removed)

    }
}
