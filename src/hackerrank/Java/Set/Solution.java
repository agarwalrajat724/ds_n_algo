package hackerrank.Java.Set;

import java.util.HashSet;
import java.util.Observable;
import java.util.Scanner;
import java.util.Set;

public class Solution {


//    static class Pair {
//        String s1;
//        String s2;
//
//        public Pair(String s1, String s2) {
//            this.s1 = s1;
//            this.s2 = s2;
//        }
//
//        @Override
//        public int hashCode() {
//            return this.s1.hashCode() * this.s2.hashCode();
//        }
//
//        @Override
//        public boolean equals(Object object) {
//            Pair pair = (Pair) object;
//
//            return s1.equals(pair.s1) && s2.equals(pair.s2);
//        }
//    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int t = s.nextInt();
        String[] pair_left = new String[t];
        String[] pair_right = new String[t];

        for (int i = 0; i < t; i++) {
            pair_left[i] = s.next();
            pair_right[i] = s.next();
        }

        class Pair {
            String s1;
            String s2;

            public Pair(String s1, String s2) {
                this.s1 = s1;
                this.s2 = s2;
            }

            @Override
            public int hashCode() {
                return this.s1.hashCode() * this.s2.hashCode();
            }

            @Override
            public boolean equals(Object object) {
                Pair pair = (Pair) object;

                return s1.equals(pair.s1) && s2.equals(pair.s2);
            }
        }
        Set<Pair> set = new HashSet<>();

        for (int i = 0; i < pair_left.length; i++) {
            set.add(new Pair(pair_left[i], pair_right[i]));

            System.out.println(set.size());
        }

    }
}
