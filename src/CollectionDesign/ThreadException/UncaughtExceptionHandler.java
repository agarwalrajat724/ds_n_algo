package CollectionDesign.ThreadException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class UncaughtExceptionHandler {
    public static void main(String[] args) {

        ThreadFactory factory = new MyThreadFactory(new MyExceptionHandler());
        ExecutorService threadPool = Executors.newFixedThreadPool(10, factory);
        threadPool.execute(new MyTask());
        // ...
    }
}
