package Grokking.Ultimate_Patterns.test;


import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int val) {
        this.val = val;
        this.next = null;
    }
};

public class Solution {

    public ListNode mergeKLists(ArrayList<ListNode> a) {
        if (null == a || a.isEmpty()) {
            return null;
        }

        Queue<ListNode> minHeap = new PriorityQueue<>(a.size(), (node1, node2) -> node1.val - node2.val);

        for (int i = 0; i < a.size(); i++) {
            if (null != a.get(i)) {
                minHeap.offer(a.get(i));
            }
        }
        // TODO: Write your code here
        ListNode resultHead = null, resultTrail = null;

        while (!minHeap.isEmpty()) {
            ListNode node = minHeap.poll();

            if (null == resultHead) {
                resultHead = node;
                resultTrail = node;
            } else {
                resultTrail.next = node;
                resultTrail = resultTrail.next;
            }

            if (null != node.next) {
                minHeap.offer(node.next);
            }
        }
        return resultHead;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        ListNode listNode1 = new ListNode(1);
        listNode1.next = new ListNode(10);
        listNode1.next.next = new ListNode(20);

        ListNode listNode2 = new ListNode(4);
        listNode2.next = new ListNode(11);
        listNode2.next.next = new ListNode(13);

        ListNode listNode3 = new ListNode(3);
        listNode3.next = new ListNode(8);
        listNode3.next.next = new ListNode(9);

        ArrayList<ListNode> a = new ArrayList<>();
        a.add(listNode1);
        a.add(listNode2);
        a.add(listNode3);

        ListNode res = solution.mergeKLists(a);

        while (null != res) {
            System.out.println(res.val);
            res = res.next;
        }

    }
}
