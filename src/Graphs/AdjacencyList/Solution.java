package Graphs.AdjacencyList;

import java.util.LinkedList;
import java.util.List;

/**
 * undirectedgraph
 */

public class Solution {


    static class Graph {
        int V;
        LinkedList<Integer> adjacencyListArray[];

        Graph(int _V) {
            this.V = _V;
            adjacencyListArray = new LinkedList[V];

            for (int i = 0; i < V; i++) {
                adjacencyListArray[i] = new LinkedList<>();
            }
        }

        private void addEdge(int src, int dest) {
            int i = adjacencyListArray[src].indexOf(dest);
            int j = adjacencyListArray[dest].indexOf(src);

            if (i == -1) {
                adjacencyListArray[src].add(dest);
            }

            if (j == -1) {
                adjacencyListArray[dest].add(src);
            }

        }

        private void printGraph() {
            for (int i = 0; i < V; i++) {
                System.out.println("Adjacency List representation for node : " + i);
                for (Integer integer : adjacencyListArray[i]) {
                    System.out.println(" --> " + integer);
                }
                System.out.println();
            }
        }
    }


    public static void main(String[] args) {
        Graph graph = new Graph(5);

        graph.addEdge( 0, 1);
        graph.addEdge( 0, 4);
        graph.addEdge( 1, 2);
        graph.addEdge( 1, 3);
        graph.addEdge( 1, 4);
        graph.addEdge( 2, 3);
        graph.addEdge( 3, 4);


        graph.printGraph();
    }
}
