package CollectionDesign.Basic;

public class BinarySearch {

    private static int search(int[] arr, int key) {

        int low = 0;
        int high = arr.length - 1;

        while (low <= high) {
            int mid = low + (high - low)/2;

            if (key < arr[mid]) {
                high = mid - 1;
            } else if (key == arr[mid]) {
                return (mid + 1);
            } else {
                low = mid + 1;
            }
        }

        if (low > high) {
            return -1;
        }

        return -1;
    }


    public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,4,5,6,7,7,8,8,8,9,9,9};

        System.out.println(search(arr, 7));
    }
}
