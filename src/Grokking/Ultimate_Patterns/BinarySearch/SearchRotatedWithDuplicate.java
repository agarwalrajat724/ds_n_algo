package Grokking.Ultimate_Patterns.BinarySearch;


/**
 * How do we search in a sorted and rotated array that also has duplicates?
 *
 * Input: [3, 3, 7, 3], key = 7
 * Output: 2
 * Explanation: '7' is present in the array at index '2'.
 */
public class SearchRotatedWithDuplicate {

    private static int search(int[] arr, int key) {

        int low = 0, high = arr.length - 1;

        while (low <= high) {
            int mid = low + (high - low)/2;

            if (arr[mid] == key) {
                return mid;
            }

            if (arr[low] == arr[mid] && arr[mid] == arr[high]) {
                low ++;
                high--;
            } else {
                if (arr[low] <= arr[mid]) {
                    if (arr[low] <= key && key < arr[mid]) {
                        high = mid - 1;
                    } else {
                        low = mid + 1;
                    }
                } else {
                    if (arr[mid] < key && key <= arr[high]) {
                        low = mid + 1;
                    } else {
                        high = mid - 1;
                    }
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(SearchRotatedWithDuplicate.search(new int[] { 3, 3, 7, 3 }, 7));
    }
}
