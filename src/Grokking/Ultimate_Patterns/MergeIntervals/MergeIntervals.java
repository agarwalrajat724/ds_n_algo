package Grokking.Ultimate_Patterns.MergeIntervals;

import java.util.*;

/**
 * 2) When Not already sorted
 */


public class MergeIntervals {

    public static List<Interval> merge(List<Interval> intervals) {
        List<Interval> mergedIntervals = new LinkedList<Interval>();

        if (null == intervals || intervals.isEmpty()) {
            return new ArrayList<>();
        }
        Collections.sort(intervals, Comparator.comparing(Interval::getStart, Collections.reverseOrder()));

        int index = 0;
        for (int i = 0; i < intervals.size(); i++) {
            if (index != 0 && intervals.get(index - 1).start <= intervals.get(i).end) {
                while (index != 0 && intervals.get(index - 1).start <= intervals.get(i).end) {
                    intervals.get(index - 1).start = Math.min(intervals.get(index - 1).start, intervals.get(i).start);
                    intervals.get(index - 1).end = Math.max(intervals.get(index - 1).end, intervals.get(i).end);
                    index--;
                }
            } else {
                intervals.add(index, intervals.get(i));
            }
            index++;
        }

        for (int i = 0; i < index; i++) {
            mergedIntervals.add(0, intervals.get(i));
        }

        // TODO: Write your code here
        return mergedIntervals;
    }

    public static void main(String[] args) {
        List<Interval> input = new ArrayList<Interval>();
        input.add(new Interval(1, 4));
        input.add(new Interval(2, 5));
        input.add(new Interval(7, 9));
        System.out.print("Merged intervals: ");
        for (Interval interval : MergeIntervals.merge(input))
            System.out.print("[" + interval.start + "," + interval.end + "] ");
        System.out.println();

        input = new ArrayList<Interval>();
        input.add(new Interval(6, 7));
        input.add(new Interval(2, 4));
        input.add(new Interval(5, 9));
        System.out.print("Merged intervals: ");
        for (Interval interval : MergeIntervals.merge(input))
            System.out.print("[" + interval.start + "," + interval.end + "] ");
        System.out.println();

        input = new ArrayList<Interval>();
        input.add(new Interval(1, 4));
        input.add(new Interval(2, 6));
        input.add(new Interval(3, 5));
        System.out.print("Merged intervals: ");
        for (Interval interval : MergeIntervals.merge(input))
            System.out.print("[" + interval.start + "," + interval.end + "] ");
        System.out.println();
    }
}
