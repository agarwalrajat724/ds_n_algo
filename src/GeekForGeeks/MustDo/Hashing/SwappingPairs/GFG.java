package GeekForGeeks.MustDo.Hashing.SwappingPairs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class GFG {


    private static int swap(int[] arr1, int[] arr2) {
        if (arr1.length > arr2.length) {
            int[] temp = arr1;
            arr1 = arr2;
            arr2 = temp;
        }
        int sum1 = Arrays.stream(arr1).sum();
        int sum2 = Arrays.stream(arr2).sum();
        Set<Integer> set = Arrays.stream(arr1).boxed().collect(Collectors.toSet());
        int target = (sum1 - sum2)/2;
        for (int i = 0; i < arr2.length; i++) {
            if (set.contains(arr2[i] + target)) {
                return 1;
            }
        }
        return 0;
    }


    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int T = Integer.parseInt(bufferedReader.readLine());

            StringTokenizer stringTokenizer = null;
            for (int i = 0; i < T; i++) {
                stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int M = Integer.parseInt(stringTokenizer.nextToken());
                int N = Integer.parseInt(stringTokenizer.nextToken());

                int arr1[] = new int[M];
                int arr2[] = new int[N];

                stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int j = 0;

                while (stringTokenizer.hasMoreTokens()) {
                    arr1[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }

                j = 0;
                stringTokenizer = new StringTokenizer(bufferedReader.readLine());
                while (stringTokenizer.hasMoreTokens()) {
                    arr2[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }

                System.out.println(GFG.swap(arr1, arr2));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
