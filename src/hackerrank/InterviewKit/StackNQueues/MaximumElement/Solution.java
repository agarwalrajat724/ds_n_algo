package hackerrank.InterviewKit.StackNQueues.MaximumElement;


import java.util.Scanner;
import java.util.Stack;

public class Solution {

    public static void maxElement() {

    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> maxStack = new Stack<>();
        while(t-- > 0) {
            int op = scan.nextInt();
//            StringTokenizer stringTokenizer = new StringTokenizer(s, " ");

            //int op = Integer.parseInt(stringTokenizer.nextToken());

            if (op == 1) {
                int data = scan.nextInt();

                stack.push(data);

                if (maxStack.isEmpty() || maxStack.peek() <= data) {
                    maxStack.push(data);
                }
            } else if (op == 2) {

                int pop = Integer.MIN_VALUE;

                if (!stack.isEmpty()) {
                    pop = stack.pop();
                }

                if (!maxStack.isEmpty() && pop == maxStack.peek()) {
                    maxStack.pop();
                }
            } else {
                if (!maxStack.isEmpty()) {
                    System.out.println(maxStack.peek());
                }
            }
        }
        scan.close();
    }
}
