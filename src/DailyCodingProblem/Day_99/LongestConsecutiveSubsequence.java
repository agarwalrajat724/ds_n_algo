package DailyCodingProblem.Day_99;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Given an array of integers, find the length of the longest sub-sequence such that
 * elements in the subsequence are consecutive integers, the consecutive numbers can be in any order.
 * Examples:
 *
 * Input: arr[] = {1, 9, 3, 10, 4, 20, 2}
 * Output: 4
 * The subsequence 1, 3, 4, 2 is the longest subsequence
 * of consecutive elements
 *
 * Input: arr[] = {36, 41, 56, 35, 44, 33, 34, 92, 43, 32, 42}
 * Output: 5
 * The subsequence 36, 35, 33, 34, 32 is the longest subsequence
 * of consecutive elements.
 */
public class LongestConsecutiveSubsequence {

    private int findLongesetConsecutiveSubsequence(int[] arr) {

        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < arr.length; i++) {
            set.add(arr[i]);
        }

        int maxLength = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (set.contains(arr[i])) {
                int start = arr[i];
                int end = start;
                while (set.contains(end)) {
                    end++;
                }
                maxLength = Math.max(maxLength, end - start);
            }
        }
        return maxLength;
    }

    public int factorial(int n) {
        int res = 1;

        for (int i = 1 ; i <= n; i++) {
            res = res * i;
        }
        return res;
    }

//    int g() {
//        return 2147483648;
//    }

    public static void main(String[] args) {
        LongestConsecutiveSubsequence longestConsecutiveSubsequence = new LongestConsecutiveSubsequence();

        System.out.println(longestConsecutiveSubsequence.findLongesetConsecutiveSubsequence(new int[]{100, 4, 200, 1, 3, 2}));
        System.out.println(longestConsecutiveSubsequence.findLongesetConsecutiveSubsequence(new int[]{1, 9, 3, 10, 4, 20, 2}));
        System.out.println(longestConsecutiveSubsequence.findLongesetConsecutiveSubsequence(new int[]{36, 41, 56, 35, 44, 33, 34, 92, 43, 32, 42}));

        System.out.println(longestConsecutiveSubsequence.factorial(10));
    }
}
