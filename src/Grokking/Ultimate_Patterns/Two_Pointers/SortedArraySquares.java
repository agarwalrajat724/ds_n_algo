package Grokking.Ultimate_Patterns.Two_Pointers;

/**
 * Given a sorted array, create a new array containing squares of all the number of the input array in the sorted order.
 *
 * Input: [-2, -1, 0, 2, 3]
 * Output: [0, 1, 4, 4, 9]
 *
 * Input: [-3, -1, 0, 1, 2]
 * Output: [0 1 1 4 9]
 */
public class SortedArraySquares {

    public static int[] makeSquares(int[] arr) {
//        int[] squares = new int[arr.length];
//
//        int low = 0, high = arr.length - 1;
//        int index = squares.length - 1;
//
//        while (low <= high) {
//            if (Math.pow(arr[low], 2) <= Math.pow(arr[high], 2)) {
//                squares[index--] = arr[high] * arr[high];
//                high--;
//            } else {
//                squares[index--] = arr[low] * arr[low];
//                low++;
//            }
//        }
//        return squares;
        int[] squares = new int[arr.length];
        // TODO: Write your code here
        int left = 0, right = arr.length - 1;
        int index = squares.length - 1;
        while(left <= right) {
            double leftSquare = arr[left] * arr[left];
            double rightSquare = arr[right] * arr[right];

            if(rightSquare >= leftSquare) {
                squares[index--] = arr[right] * arr[right];
                right--;
            } else {
                squares[index--] = arr[left] * arr[left];
                left++;
            }
        }

        return squares;
    }

    public static void main(String[] args) {
        SortedArraySquares sortedArraySquares = new SortedArraySquares();
        System.out.println(sortedArraySquares.makeSquares(new int[]{-2, -1, 0, 2, 3}));
        System.out.println(sortedArraySquares.makeSquares(new int[]{-3, -1, 0, 1, 2}));
    }
}
