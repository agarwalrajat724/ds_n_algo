package LeetCode.BackTracking.Subsets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        subsetsUtil(nums, result, 0, new ArrayList<>());
        return result;
    }

    private void subsetsUtil(int[] nums, List<List<Integer>> result, int l, List<Integer> cur) {
        result.add(new ArrayList<>(cur));
        for (int i = l; i < nums.length; i++) {
            cur.add(nums[i]);
            subsetsUtil(nums, result, i + 1, cur);
            cur.remove(cur.size() - 1);
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.subsets(new int[]{1,2,3}).forEach(integers -> {
            System.out.println("jsdjsd");
            integers.forEach(integer -> System.out.println(integer));
        });
    }

}

