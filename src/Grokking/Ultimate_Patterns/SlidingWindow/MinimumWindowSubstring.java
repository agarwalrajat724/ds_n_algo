package Grokking.Ultimate_Patterns.SlidingWindow;

import java.util.HashMap;
import java.util.Map;

public class MinimumWindowSubstring {

    public static String findSubstring(String str, String pattern) {
        int windowStart = 0, subStrStart = 0, matchedCount = 0, minLegth = str.length() + 1;
        Map<Character, Integer> characterFrequencyMap = new HashMap<>();

        char[] input = str.toCharArray();
        char[] patternArray = pattern.toCharArray();

        for (char ch : patternArray) {
            characterFrequencyMap.put(ch, characterFrequencyMap.getOrDefault(ch, 0) + 1);
        }

        for (int windowEnd = 0; windowEnd < input.length; windowEnd++) {
            char rightChar = input[windowEnd];

            if (characterFrequencyMap.containsKey(rightChar)) {
                characterFrequencyMap.put(rightChar, characterFrequencyMap.get(rightChar) - 1);
                if (characterFrequencyMap.get(rightChar) >= 0) {
                    matchedCount++;
                }
            }

            while (matchedCount == pattern.length()) {
                if ((windowEnd - windowStart + 1) < minLegth) {
                    minLegth = windowEnd - windowStart + 1;
                    subStrStart = windowStart;
                }

                char leftChar = input[windowStart];
                if (characterFrequencyMap.containsKey(leftChar)) {
                    // note that we could have redundant matching characters, therefore we'll decrement the
                    // matched count only when the last occurrence of a matched character is going out of the window
                    if (characterFrequencyMap.get(leftChar) == 0) {
                        matchedCount--;
                    }
                    characterFrequencyMap.put(leftChar, characterFrequencyMap.get(leftChar) + 1);
                }
                windowStart++;
            }
        }
        return minLegth > str.length() ? "" : str.substring(subStrStart, subStrStart + minLegth);
    }

    public static void main(String[] args) {
        System.out.println(MinimumWindowSubstring.findSubstring("aabdec", "abc"));
        System.out.println(MinimumWindowSubstring.findSubstring("abdabca", "abc"));
        System.out.println(MinimumWindowSubstring.findSubstring("adcad", "abc"));
    }

}
