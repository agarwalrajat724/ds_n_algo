package Grokking.Ultimate_Patterns.TopologicalSort;

import java.util.*;

/**
 * There are ‘N’ tasks, labeled from ‘0’ to ‘N-1’. Each task can have some prerequisite
 * tasks which need to be completed before it can be scheduled. Given the number of tasks
 * and a list of prerequisite pairs, write a method to find the ordering of tasks we should pick to finish all tasks.
 *
 * Input: Tasks=3, Prerequisites=[0, 1], [1, 2]
 * Output: [0, 1, 2]
 * Explanation: To execute task '1', task '0' needs to finish first. Similarly, task '1' needs to finish
 * before '2' can be scheduled. A possible scheduling of tasks is: [0, 1, 2]
 *
 * Input: Tasks=3, Prerequisites=[0, 1], [1, 2], [2, 0]
 * Output: []
 * Explanation: The tasks have cyclic dependency, therefore they cannot be scheduled.
 *
 * Input: Tasks=6, Prerequisites=[2, 5], [0, 5], [0, 4], [1, 4], [3, 2], [1, 3]
 * Output: [0 1 4 3 2 5]
 * Explanation: A possible scheduling of tasks is: [0 1 4 3 2 5]
 */
public class TaskSchedulingOrder {

    public static List<Integer> findOrder(int tasks, int[][] prerequisites) {
        List<Integer> sortedOrder = new ArrayList<>();

        Map<Integer, List<Integer>> graph = new HashMap<>();
        Map<Integer, Integer> indegrees = new HashMap<>();
        Queue<Integer> sources = new LinkedList<>();

        for (int i = 0; i < tasks; i++) {
            graph.put(i, new ArrayList<>());
            indegrees.put(i, 0);
        }

        for (int i = 0; i < prerequisites.length; i++) {
            int parent = prerequisites[i][0];
            int child = prerequisites[i][1];

            graph.get(parent).add(child);
            indegrees.put(child, indegrees.get(child) + 1);
        }

        for (Map.Entry<Integer, Integer> entry : indegrees.entrySet()) {
            if (entry.getValue() == 0) {
                sources.offer(entry.getKey());
            }
        }

        while (!sources.isEmpty()) {
            int source = sources.poll();
            sortedOrder.add(source);
            List<Integer> children = graph.get(source);

            for (int child : children) {
                indegrees.put(child, indegrees.get(child) - 1);
                if (indegrees.get(child) == 0) {
                    sources.offer(child);
                }
            }
        }

        if (sortedOrder.size() != tasks) {
            return new ArrayList<>();
        }
        return sortedOrder;
    }

    public static void main(String[] args) {
        List<Integer> result = TaskSchedulingOrder.findOrder(3, new int[][] { new int[] { 0, 1 }, new int[] { 1, 2 } });
        System.out.println(result);

        result = TaskSchedulingOrder.findOrder(3,
                new int[][] { new int[] { 0, 1 }, new int[] { 1, 2 }, new int[] { 2, 0 } });
        System.out.println(result);

        result = TaskSchedulingOrder.findOrder(6, new int[][] { new int[] { 2, 5 }, new int[] { 0, 5 }, new int[] { 0, 4 },
                new int[] { 1, 4 }, new int[] { 3, 2 }, new int[] { 1, 3 } });
        System.out.println(result);
    }
}
