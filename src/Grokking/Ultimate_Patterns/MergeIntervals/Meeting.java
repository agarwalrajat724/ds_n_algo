package Grokking.Ultimate_Patterns.MergeIntervals;

public class Meeting {

    public int start;
    public int end;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public Meeting(int start, int end) {
        this.start = start;
        this.end = end;
    }


}
