package LeetCode.Easy.Arrays.CountTripletsSumLessThanGiven;

import java.util.Arrays;

public class Solution {

    public int count(int[] arr, int sum) {
        Arrays.sort(arr);
        int ans = 0;
        int n = arr.length - 1;
        for (int i = 0; i < arr.length; i++) {
            int low = i + 1;
            int high = n;

            while (low < high) {
                if (arr[i] + arr[low] + arr[high] <= sum) {
                    high--;
                } else {
                    ans += (high-low);
                    low++;
                }
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.count(new int[]{5, 1, 3, 4, 7}, 12));
    }
}
