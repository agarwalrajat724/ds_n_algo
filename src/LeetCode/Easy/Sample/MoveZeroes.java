package LeetCode.Easy.Sample;

/**
 *
 * 283. Move Zeroes
 *
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the
 * relative order of the non-zero elements.
 *
 * Example:
 *
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 * Note:
 *
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 */
public class MoveZeroes {

    public void moveZeroes(int[] nums) {
        if (null == nums || nums.length < 2) {
            return;
        }
        for (int lastNonZeroAt = 0,curr = 0; curr < nums.length; curr++) {
            if (nums[curr] != 0) {
                swap(nums, lastNonZeroAt, curr);
                lastNonZeroAt++;
            }
        }
    }


    public void moveZeroesOther(int[] nums) {

        if (null == nums || nums.length == 0) {
            return;
        }

        int i = 0;
        int j = nums.length - 1;

        while (i < j) {
            while (nums[j] == 0 && i < j) {
                j--;
            }

            if (nums[i] == 0) {
                swap(nums, i, j);
                j--;
            }
            i++;
        }
    }

    public void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        MoveZeroes moveZeroes = new MoveZeroes();

        moveZeroes.moveZeroes(new int[]{0,1,0,3,12});
    }
}
