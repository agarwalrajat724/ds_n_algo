package LeetCode.Hard.Design.PeekingIterator;

import java.util.Iterator;

public class PeekingIterator1 implements Iterator<Integer> {

    private Iterator<Integer> it;

    private int peakElement;

    private boolean isPeaked;

    public PeekingIterator1(Iterator<Integer> it) {
        this.it = it;
        peakElement = -1;
        isPeaked = false;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Integer next() {
        return null;
    }
}
