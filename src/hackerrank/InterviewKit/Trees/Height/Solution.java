package hackerrank.InterviewKit.Trees.Height;


import java.util.*;

class Node {
    Node left;
    Node right;
    int data;

    Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
}

public class Solution {


    /*
    class Node
    	int data;
    	Node left;
    	Node right;
	*/
    public static int height(Node root) {

        int lHeight = 0;
        int rHeight = 0;

        if (null != root) {
            if (null != root.left) {
                lHeight = height(root.left);
            }

            if (null != root.right) {
                rHeight = height(root.right);
            }
        }

        return (lHeight > rHeight)? (lHeight + 1) : (rHeight + 1);
    }


    public static Node insert(Node root, int data) {
        if(root == null) {
            return new Node(data);
        } else {
            Node cur;
            if(data <= root.data) {
                cur = insert(root.left, data);
                root.left = cur;
            } else {
                cur = insert(root.right, data);
                root.right = cur;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Node root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
        scan.close();
        int height = height(root);
        System.out.println(height);
    }
}
