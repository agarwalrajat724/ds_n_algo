package LeetCode.Easy.BinaryTree.TreePaths;

import java.util.ArrayList;
import java.util.List;

/**
 * 257. Binary Tree Paths
 *
 *
 * Given a binary tree, return all root-to-leaf paths.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Input:
 *
 *    1
 *  /   \
 * 2     3
 *  \
 *   5
 *
 * Output: ["1->2->5", "1->3"]
 *
 * Explanation: All root-to-leaf paths are: 1->2->5, 1->3
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }


    private int path[] = new int[1000];

    public List<String> binaryTreePaths(TreeNode root) {

        List<String> result = new ArrayList<>();

        if (null == root) {
            return result;
        }

        binaryTreePathsUtil(root, result, 0);

        return result;
    }

    private void binaryTreePathsUtil(TreeNode root, List<String> result, int depth) {

        if (null == root) {
            return;
        }

        path[depth] = root.val;

        if (null == root.left && null == root.right) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(path[0]);
            for (int i = 1; i <= depth; i++) {
                stringBuffer.append("->" + path[i]);
            }
            result.add(stringBuffer.toString());
        } else {

            binaryTreePathsUtil(root.left, result, depth + 1);
            binaryTreePathsUtil(root.right, result, depth + 1);
        }
    }


    public static void main(String[] args) {

        Solution solution = new Solution();

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);

        root.left.right = new TreeNode(5);

        List<String> result = solution.binaryTreePaths(root);

        result.forEach(s -> System.out.println(s));

    }
}
