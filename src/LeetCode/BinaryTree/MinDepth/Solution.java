package LeetCode.BinaryTree.MinDepth;

import java.util.LinkedList;
import java.util.Queue;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public int minDepth(TreeNode root) {
        if (null == root) {
            return 0;
        }

        if (null == root.left && null == root.right) {
            return 1;
        }

        return minDepthUtil(root);
    }

    private int minDepthUtil(TreeNode root) {
        if (null == root) {
            return 0;
        }

        if (null == root.left && null == root.right) {
            return 1;
        }

        if (null == root.left) {
            return minDepthUtil(root.right) + 1;
        }

        if (null == root.right) {
            return minDepthUtil(root.left) + 1;
        }

        return Math.min(minDepthUtil(root.left), minDepthUtil(root.right)) + 1;
    }

    static class Node {
        TreeNode node;
        int depth;

        public Node(TreeNode node, int depth) {
            this.node = node;
            this.depth = depth;
        }
    }

    private int minDepthLevelOrder(TreeNode root) {
        if (null == root) {
            return 0;
        } else if (null == root.left && null == root.right) {
            return 1;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.offer(new Node(root, 1));

        while (!queue.isEmpty()) {
            Node pop = queue.poll();

            TreeNode node = pop.node;
            int depth = pop.depth;

            if (null == node.left && null == node.right) {
                return depth;
            }

            if (null != node.left) {
                queue.offer(new Node(node.left, depth + 1));
            }

            if (null != node.right) {
                queue.offer(new Node(node.right, depth + 1));
            }

        }

        return -1;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);

        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);

        System.out.println(solution.minDepth(root));

        System.out.println(solution.minDepthLevelOrder(root));
    }
}
