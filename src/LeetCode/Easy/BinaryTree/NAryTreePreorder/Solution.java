package LeetCode.Easy.BinaryTree.NAryTreePreorder;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Given an n-ary tree, return the preorder traversal of its nodes' values.
 *
 *
 * For example, given a 3-ary tree:
 *
 *
 *
 *
 *
 * Return its preorder traversal as: [1,3,5,6,2,4].
 */
public class Solution {

    static class Node {

        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val, List<Node> _children) {
            this.val = _val;
            this.children = _children;
        }
    }

    public List<Integer> preorder(Node root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        Stack<Node> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node pop = stack.pop();

            result.add(pop.val);

            List<Node> children = pop.children;

            if (null != children && !children.isEmpty()) {
                for (int i = children.size()-1; i >= 0; i--) {
                    Node child = children.get(i);
                    stack.push(child);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        List<Node> chilLeft = new ArrayList<>();
        chilLeft.add(new Node(5, null));
        chilLeft.add(new Node(6, null));
        Node node = new Node(3, chilLeft);

        List<Node> allChild = new ArrayList<>();
        allChild.add(node);
        allChild.add(new Node(2, null));
        allChild.add(new Node(4, null));

        Node root = new Node(1, allChild);

        List<Integer> result = solution.preorder(root);

        result.forEach(integer -> System.out.println(integer));
    }
}
