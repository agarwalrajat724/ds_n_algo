package Walmart;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithKUniq {

    public static void main(String[] args) {
        System.out.println(longestSubstring("aabbcc", 1));
        System.out.println(longestSubstring("aabbcc", 2));
        System.out.println(longestSubstring("aabbcc", 3));
        System.out.println(longestSubstring("aaabbb", 3));
    }

    private static boolean isValid(int[] count, int k) {
        int noOfDistinctChars = 0;
        for (int i = 0; i < count.length; i++) {
            if (count[i] > 0) {
                noOfDistinctChars++;
            }
        }

        return noOfDistinctChars <= k;
    }

    private static String longestSubstring(String string, int k) {
        int[] count = new int[26];
        char[] arr = string.toCharArray();

        int curr_start = 0, curr_end = 0, max_window_size = 0, max_window_start = 0;

        for (int i = 0; i < arr.length; i++) {
            count[arr[i] - 'a']++;
            curr_end++;

            while (!isValid(count, k)) {
                count[arr[curr_start] - 'a']--;
                curr_start++;
            }

            if ((curr_end - curr_start + 1) >  max_window_size) {
                max_window_size = curr_end - curr_start + 1;
                max_window_start = curr_start;
            }
        }
        return string.substring(max_window_start, max_window_size);
    }
}
