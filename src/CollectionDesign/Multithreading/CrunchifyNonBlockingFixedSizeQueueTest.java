package CollectionDesign.Multithreading;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Crunchify.com
 *
 */

public class CrunchifyNonBlockingFixedSizeQueueTest {

    public static void main(String[] args) {

        // Test ArrayBlockingQueue with size 10
        CrunchifyOwnNonBlockingFixedSizeQueue();
    }

    private static void CrunchifyOwnNonBlockingFixedSizeQueue() {

        // crunchifyQueue with type CrunchifyNonBlockingFixedSizeQueue
        ArrayBlockingQueue<String> crunchifyQueue = new CrunchifyNonBlockingFixedSizeQueue<String>(10);

        String crunchifyMsg = "This is CrunchifyNonBlockingFixedSizeQueueTest - ";
        try {
            // We are looping for 15 times - No error even after queue is full
            for (int i = 1; i <= 15; i++) {
                crunchifyQueue.add(crunchifyMsg + i);
                log("CrunchifyNonBlockingFixedSizeQueueTest size: " + crunchifyQueue.size());
            }
        } catch (Exception e) {
            log("\nException Occurred: ");
            e.printStackTrace();
        }

    }

    private static void log(String crunchifyText) {
        System.out.println(crunchifyText);

    }
}
