package hackerrank.InterviewKit.Trees.TopView;

import java.util.*;


/**
 *
 *
 *116
 * 37 23 108 59 86 64 94 14 105 17 111 65 55 31 79 97 78 25 50 22 66 46 104 98 81 90 68
 * 40 103 77 74 18 69 82 41 4 48 83 67 6 2 95 54 100 99 84 34 88 27 72 32 62 9 56 109 115
 * 33 15 91 29 85 114 112 20 26 30 93 96 87 42 38 60 7 73 35 12 10 57 80 13 52 44 16 70 8
 * 39 107 106 63 24 92 45 75 116 5 61 49 101 71 11 53 43 102 110 1 58 36 28 76 47 113 21 89
 * 51 19 3
 *
 *
 * 1 2 4 14 23 37 108 111 115 116 83 84 85
 */


class Node {
    Node left;
    Node right;
    int data;

    Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
}

public class Solution {

    static class NodeObj {
        Node node;
        int horizontalDistance;

        public NodeObj(Node node, int horizontalDistance) {
            this.node = node;
            this.horizontalDistance = horizontalDistance;
        }
    }


    public static void topView(Node root) {

        if (null == root) {
            return;
        }

        Queue<NodeObj> queue = new LinkedList<>();
        Map<Integer, Integer> map = new TreeMap<>();

        NodeObj nodeObj = new NodeObj(root, 0);

        queue.add(nodeObj);

        while (!queue.isEmpty()) {
            NodeObj pop = queue.poll();

            int hd = pop.horizontalDistance;
            Node node = pop.node;

            if (!map.containsKey(hd)) {
                map.put(hd, node.data);
            }

            if (node.left != null) {
                queue.add(new NodeObj(node.left, hd - 1));
            }

            if (null != node.right) {
                queue.add(new NodeObj(node.right, hd + 1));
            }
        }

        for (Map.Entry<Integer, Integer> entry: map.entrySet()) {
            System.out.print(entry.getValue() + " ");
        }
    }

    public static Node insert(Node root, int data) {
        if (null == root) {
            return new Node(data);
        } else {
            Node curr = null;
            if (data <= root.data) {
                curr = insert(root.left, data);
                root.left = curr;
            } else {
                curr = insert(root.right, data);
                root.right = curr;
            }
            return root;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int t = scan.nextInt();
        Node root = null;
        while(t-- > 0) {
            int data = scan.nextInt();
            root = insert(root, data);
        }
        scan.close();
        topView(root);
    }
}
