package Walmart.concurrency.PrintOddEven.Basic;

public class PrintOddEven implements Runnable {

    private int max;
    private boolean isEven;
    private Printer printer;

    public PrintOddEven(int max, boolean isEven, Printer printer) {
        this.max = max;
        this.isEven = isEven;
        this.printer = printer;
    }

    @Override
    public void run() {
        int number = isEven ? 2 : 1;
        while (number <= max) {
            if (isEven) {
                printer.printEven(number);
            } else {
                printer.printOdd(number);
            }
            number += 2;
        }
    }

    public static void main(String[] args) {
        Printer printer = new Printer();
        Thread printOdd = new Thread(new PrintOddEven(10, false, printer));
        Thread printEven = new Thread(new PrintOddEven(10, true, printer));
        printEven.start();
        printOdd.start();

    }
}
