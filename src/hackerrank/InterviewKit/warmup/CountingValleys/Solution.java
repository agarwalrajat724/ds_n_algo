package hackerrank.InterviewKit.warmup.CountingValleys;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

/**
 * https://www.hackerrank.com/challenges/counting-valleys/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=warmup
 */
public class Solution {

    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {

        int level = 0;
        boolean belowSea = false;
        int valleyCount = 0;
        for (int i = 0; i < n; i++) {
            char ch = s.charAt(i);

            if (ch == 'U') {
                level++;
            } else {
                level--;
            }

            if (!belowSea && level < 0) {
                valleyCount++;
                belowSea = true;
            }

            if (level == 0) {
                belowSea = false;
            }
        }

        return valleyCount;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        System.out.println(result);
        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
