package LeetCode.DynamicProgramming.HouseRobber;

public class Solution {

    public int rob(int[] nums) {
        if (null == nums) {
            return 0;
        }

        if (nums.length < 2) {
            return nums[1];
        }

        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }

        int dp[] = new int[nums.length];

        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);

        for (int i = 2; i < nums.length; i++) {
            dp[i] = Math.max(nums[i] + dp[i-2], dp[i - 1]);
        }

        return dp[nums.length - 1];
    }

    public int robBetter(int[] nums) {
        if (null == nums || nums.length == 0) {
            return 0;
        }

        if (nums.length < 2) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        int value1 = nums[0];
        int value2 = Math.max(nums[0], nums[1]);
        int max_value = 0;

        for (int i = 2; i < nums.length; i++) {

            max_value = Math.max(value1 + nums[i], value2);

            value1 = value2;
            value2 = max_value;
        }
        return max_value;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.rob(new int[]{2,1,1,2}));

        System.out.println(solution.robBetter(new int[]{2,1,1,2}));

        System.out.println(solution.robBetter(new int[]{0}));
    }
}
