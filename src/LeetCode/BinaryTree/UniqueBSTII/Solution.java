package LeetCode.BinaryTree.UniqueBSTII;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 95. Unique Binary Search Trees II
 *
 * Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1 ... n.
 *
 * Example:
 *
 * Input: 3
 * Output:
 * [
 *   [1,null,3,2],
 *   [3,2,null,1],
 *   [3,1,null,null,2],
 *   [2,1,3],
 *   [1,null,2,null,3]
 * ]
 * Explanation:
 * The above output corresponds to the 5 unique BST's shown below:
 *
 *    1         3     3      2      1
 *     \       /     /      / \      \
 *      3     2     1      1   3      2
 *     /     /       \                 \
 *    2     1         2                 3
 */
public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    public List<TreeNode> generateTrees(int n) {

        if (n < 1) {
            return new ArrayList<>();
        }
        return generateTreesUtils(1, n);
    }

    private List<TreeNode> generateTreesUtils(int start, int end) {
        List<TreeNode> list = new ArrayList<>();

        if (start > end) {
            list.add(null);
            return list;
        }

        for (int i = start; i <= end; i++) {
            List<TreeNode> leftTree = generateTreesUtils(start, i - 1);
            List<TreeNode> rightTree = generateTreesUtils(i + 1, end);

            for (int j = 0; j < leftTree.size(); j++) {
                TreeNode left = leftTree.get(j);

                for (int k= 0; k < rightTree.size(); k++) {
                    TreeNode right = rightTree.get(k);

                    TreeNode node = new TreeNode(i);

                    node.left = left;
                    node.right = right;

                    list.add(node);
                }
            }
        }

        return list;
    }


    private void preOrderTraversal(TreeNode root) {
        if (null != root) {
            System.out.println(root.val);
            preOrderTraversal(root.left);
            preOrderTraversal(root.right);
        }
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        List<TreeNode> results = solution.generateTrees(3);

        results.forEach(treeNode -> {
            System.out.println("Preorder Traversal");
            solution.preOrderTraversal(treeNode);
        });
    }
}
