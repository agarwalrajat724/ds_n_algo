package Walmart.CloneBinaryTree;

import java.util.HashMap;
import java.util.Map;

class BinaryTree {
    int data;
    BinaryTree left, right, random;

    public BinaryTree(int data) {
        this.data = data;
    }
}

public class CloneBT {

    private static BinaryTree copyLeftToRight(BinaryTree root, Map<BinaryTree, BinaryTree> map) {
        if (null == root) {
            return null;
        }
        BinaryTree cloneBinaryTree = new BinaryTree(root.data);
        map.put(root, cloneBinaryTree);

        cloneBinaryTree.left = copyLeftToRight(root.left, map);
        cloneBinaryTree.right = copyLeftToRight(root.right, map);
        return cloneBinaryTree;
    }

    private static void copyRandom(BinaryTree cloneTree, BinaryTree givenTree, Map<BinaryTree, BinaryTree> map) {
        if (null == cloneTree) {
            return;
        }

        cloneTree.random = map.get(givenTree.random);
        copyRandom(cloneTree.left, givenTree.left, map);
        copyRandom(cloneTree.right, givenTree.right, map);
    }

    private static void inOrder(BinaryTree root) {
        if (null == root) {
            return;
        }
        inOrder(root.left);

        if (null == root.random) {
            System.out.println(root.data + " -> NULL ");
        } else {
            System.out.println(root.data + " -> " + root.random.data);
        }
        inOrder(root.right);
    }

    public static void main(String[] args) {

        BinaryTree root = new BinaryTree(10);
        BinaryTree n2 = new BinaryTree(6);
        BinaryTree n3 = new BinaryTree(12);
        BinaryTree n4 = new BinaryTree(5);
        BinaryTree n5 = new BinaryTree(8);
        BinaryTree n6 = new BinaryTree(11);
        BinaryTree n7 = new BinaryTree(13);
        BinaryTree n8 = new BinaryTree(7);
        BinaryTree n9 = new BinaryTree(9);

        root.left = n2;
        root.right = n3;
        root.random = n2;

        n2.left = n4;
        n2.right = n5;
        n2.random = n8;

        n3.left = n6;
        n3.right = n7;
        n3.random = n5;

        n4.random = n9;

        n5.left = n8;
        n5.right = n9;
        n5.random = root;

        n6.random = n9;
        n9.random = n8;

        inOrder(root);
        Map<BinaryTree, BinaryTree> map = new HashMap<>();

        BinaryTree cloneTree = copyLeftToRight(root, map);
        copyRandom(cloneTree, root, map);

        System.out.println("------------------------------->");
        inOrder(cloneTree);
    }
}
