package LeetCode.BinaryTree.NAryPostOrder;

import java.util.*;

public class NAryPostOrder {


    static class Node {
        int val;
        List<Node> children;

        public Node() {
        }

        public Node(int _val, List<Node> _children) {
            this.val = _val;
            this.children = _children;
        }
    }

    public List<Integer> postOrderRecursive(Node root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        postOrderRecursiveUtil(root, result);
        return result;
    }

    private void postOrderRecursiveUtil(Node root, List<Integer> result) {

        if (null == root) {
            return;
        }

        if (null != root.children && !root.children.isEmpty()) {
            for (Node node : root.children) {
                postOrderRecursiveUtil(node, result);
            }
        }

        result.add(root.val);
    }


    public List<Integer> postorder(Node root) {

        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        Stack<Node> stack = new Stack<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node pop = stack.pop();

            result.add(pop.val);

            if (null != pop.children && !pop.children.isEmpty()) {
                for (Node node : pop.children) {
                    stack.push(node);
                }
            }
        }

        Collections.reverse(result);
        return result;
    }


    public static void main(String[] args) {
        NAryPostOrder nAryPostOrder = new NAryPostOrder();

        Node lNode = new Node(5, null);
        Node rNode = new Node(6, null);

        List<Node> lChildren = new ArrayList<>(Arrays.asList(lNode, rNode));

        Node lNode1 = new Node(3, lChildren);

        Node mid = new Node(2, null);
        Node right = new Node(4, null);

        List<Node> children = new ArrayList<>();
        children.add(lNode1);
        children.add(mid);
        children.add(right);


        Node root = new Node(1, children);

        nAryPostOrder.postorder(root).forEach(integer -> System.out.println(integer));

        nAryPostOrder.postOrderRecursive(root);
    }
}
