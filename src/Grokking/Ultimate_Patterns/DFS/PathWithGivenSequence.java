package Grokking.Ultimate_Patterns.DFS;

class PathWithGivenSequence {
    public static boolean findPath(TreeNode root, int[] sequence) {
        // TODO: Write your code here
        if (null == root && (null == sequence || sequence.length == 0)) {
            return true;
        }

        if (null == root || null == sequence) {
            return false;
        }

        return findPathUtil(root, 0, sequence);
    }

    private static boolean findPathUtil(TreeNode root, int index, int[] sequence) {

        if (null == root || (index >= sequence.length)) {
            return false;
        }

        if (index >= sequence.length || sequence[index] != root.val) {
            return false;
        }

        if (null == root.left && null == root.right && index == sequence.length - 1) {
            return true;
        }

        return findPathUtil(root.left, index + 1, sequence) || findPathUtil(root.right, index + 1, sequence);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(7);
        root.right = new TreeNode(9);
//        root.left.left = new TreeNode(1);
        root.right.left = new TreeNode(2);
        root.right.right = new TreeNode(9);

        System.out.println("Tree has path sequence: " + PathWithGivenSequence.findPath(root, new int[] { 1, 0, 7 }));
        System.out.println("Tree has path sequence: " + PathWithGivenSequence.findPath(root, new int[] { 1, 1, 6 }));
        System.out.println("Tree has path sequence: " + PathWithGivenSequence.findPath(root, new int[] { 1, 9, 9 }));
        System.out.println("Tree has path sequence: " + PathWithGivenSequence.findPath(root, new int[] { 1, 7 }));
    }
}
