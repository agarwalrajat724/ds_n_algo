package LeetCode.DynamicProgramming.BeautifulNumber;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        Solution solution = new Solution();
        int T = Integer.parseInt(bufferedReader.readLine().trim());

        for (int i = 0; i < T; i++) {
            int n = Integer.parseInt(bufferedReader.readLine().trim());
            String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
            int arr[] = new int[n];
            for (int k = 0; k < n; k++) {
                arr[k] = Integer.parseInt(nr[k]);
            }
            solution.beautifulNumber(arr, n);
        }
    }

    private void beautifulNumber(int[] arr, int n) {
        if (arr.length < 2) {
            System.out.println(arr[0] + 1);
        }

        int prevNo = arr[0];

        for (int i = 1; i < arr.length - 1; i++) {
            if (arr[i] - prevNo > 1) {
                arr[i] = prevNo + 1;
            }
        }

        if ((arr[arr.length - 1] - arr[arr.length - 2]) > 1) {
            System.out.println(arr[arr.length - 2] + 2);
        } else {
            System.out.println(arr[arr.length - 1]);
        }

    }
}
