package LeetCode.BinaryTree.ConvertSortArrToBBST;

public class Solution {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }


    public TreeNode sortedArrayToBST(int[] nums) {
        if (nums == null || nums.length < 1) {
            return null;
        }

        int n = nums.length - 1;

        return sortedArrayToBSTUtil(nums, 0, n);
    }

    private TreeNode sortedArrayToBSTUtil(int[] nums, int low, int high) {
        TreeNode root = null;
        if (low <= high) {
            int mid = low + (high - low)/2;

            root = new TreeNode(nums[mid]);
            root.left = sortedArrayToBSTUtil(nums, low, mid - 1);
            root.right = sortedArrayToBSTUtil(nums, mid + 1, high);
        }

        return root;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        TreeNode result = solution.sortedArrayToBST(new int[]{-10,-3,0,5,9});

        System.out.println(result);
    }
}
