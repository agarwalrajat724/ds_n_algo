package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Design a class to efficiently find the Kth largest element in a stream of numbers.
 *
 * The class should have the following two things:
 *
 * The constructor of the class should accept an integer array containing initial numbers
 * from the stream and an integer ‘K’.
 * The class should expose a function add(int num) which will store the given number and
 * return the Kth largest number.
 */
public class KthLargestNumberInStream {

    Queue<Integer> minHeap = null;

    public KthLargestNumberInStream(int[] nums, int k) {
        minHeap = new PriorityQueue<>(k);
        for (int i = 0; i < k; i++) {
            minHeap.offer(nums[i]);
        }

        for (int i = k; i < nums.length; i++) {
            if (minHeap.peek() < nums[i]) {
                minHeap.poll();
                minHeap.offer(nums[i]);
            }
        }
    }

    public int add(int num) {
        if (minHeap.peek() < num) {
            minHeap.poll();
            minHeap.offer(num);
        }
        return minHeap.peek();
    }

    public static void main(String[] args) {
        int[] input = new int[] { 3, 1, 5, 12, 2, 11 };
        KthLargestNumberInStream kthLargestNumber = new KthLargestNumberInStream(input, 4);
        System.out.println("4th largest number is: " + kthLargestNumber.add(6));
        System.out.println("4th largest number is: " + kthLargestNumber.add(13));
        System.out.println("4th largest number is: " + kthLargestNumber.add(4));
    }
}
