package LeetCode.Medium.DynamicProgramming.JumpGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class CandidateCode {


    static int findMinInsertions(char str[], int l,
                                 int h)
    {
        if (l > h) return Integer.MAX_VALUE;
        if (l == h) return 0;
        if (l == h - 1) return (str[l] == str[h])? 0 : 1;

        return (str[l] == str[h])?
                findMinInsertions(str, l + 1, h - 1):
                (Integer.min(findMinInsertions(str, l, h - 1),
                        findMinInsertions(str, l + 1, h)) + 1);
    }

    static int findMinInsertionsDP(char str[], int n)
    {
        int table[][] = new int[n][n];
        int l, h;

        // Fill the table
        for (int gap = 1; gap < n; ++gap)
            for (l = 0, h = gap; h < n; ++l, ++h)
                table[l][h] = (str[l] == str[h])?
                        table[l+1][h-1] :
                        (Integer.min(table[l][h-1],
                                table[l+1][h]) + 1);

        // Return minimum number of insertions
        // for str[0..n-1]
        return table[0][n-1];
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bufferedReader.readLine());
//        String s = bufferedReader.readLine().replaceAll("\\s+$", "");
        String s = bufferedReader.readLine();

        char[] arr = s.toCharArray();
        System.out.println(findMinInsertionsDP(arr, n));
//        scanner.close();
        bufferedReader.close();
    }
}
