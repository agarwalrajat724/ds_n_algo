package CollectionDesign;

public class PrintOddEvenMain {


    volatile static int number = 1;
    static int MAX = 10;

    static Object lock = new Object();

    volatile static int remainder;


    public static void main(String[] args) {

        OddEvenRunnable oddRunnable=new OddEvenRunnable(1);
        OddEvenRunnable evenRunnable=new OddEvenRunnable(0);

        Thread t1=new Thread(oddRunnable,"Odd");
        Thread t2=new Thread(evenRunnable,"Even");

        t1.start();
        t2.start();


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (number < MAX) {
                    synchronized (lock) {
                        while (number % 2 != remainder) {
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                System.out.println("Number : " + number);
                number += 1;
                lock.notifyAll();;
            }
        };

    }
}
