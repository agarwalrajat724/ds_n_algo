package Walmart.concurrency.PrintOddEven.Basic;

public class Printer {

    boolean isOddPrinted = false;

    public synchronized void printEven(int number) {
        while (!isOddPrinted) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Number : " + number);
        isOddPrinted = false;
        notify();
    }

    public synchronized void printOdd(int number) {
        while (isOddPrinted) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Number : " + number);
        isOddPrinted = true;
        notify();
    }
}
