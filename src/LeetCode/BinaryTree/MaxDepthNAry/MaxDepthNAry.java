package LeetCode.BinaryTree.MaxDepthNAry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * 559. Maximum Depth of N-ary Tree
 *
 * Given a n-ary tree, find its maximum depth.
 *
 * The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 *
 * For example, given a 3-ary tree:
 *
 *
 *
 *
 *
 * We should return its max depth, which is 3.
 *
 * Note:
 *
 * The depth of the tree is at most 1000.
 * The total number of nodes is at most 5000.
 */
public class MaxDepthNAry {

    static class Node {
        int val;
        List<Node> children;

        public Node(int _val, List<Node> _children) {
            this.val = _val;
            this.children = _children;
        }


    }


    public int maxDepth(Node root) {

        if (null == root) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        if (null != root.children && !root.children.isEmpty()) {
            for (Node node : root.children) {
                int val = maxDepth(node) + 1;
                if (val > max) {
                    max = val;
                }
            }
        } else {
            return 1;
        }

        return max;
    }


    public static void main(String[] args) {

        MaxDepthNAry maxDepthNAry = new MaxDepthNAry();

        Node lChild = new Node(5, null);
        Node rChild = new Node(6, null);

        Node lNode = new Node(3, new ArrayList<>(Arrays.asList(lChild, rChild)));

        Node mNode = new Node(2, null);
        Node rNode = new Node(4, null);

        Node root = new Node(1, new ArrayList<>(Arrays.asList(lNode, mNode, rNode)));

        System.out.println(maxDepthNAry.maxDepth(root));
    }
}
