package hackerrank.InterviewKit.Greedy.MaxMin;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the maxMin function below.
    static int maxMin(int k, int[] arr) {

        Arrays.sort(arr);

        int low = 0;
        int high = arr.length - 1;

        int minDiff = Integer.MAX_VALUE;

        while (high - low >= k) {

            int diff1 = Integer.MAX_VALUE, diff2 = Integer.MAX_VALUE;

            if ((low + k - 1) >= 0 && (low + k - 1) <= arr.length - 1) {
                diff1 = arr[low + k - 1] - arr[low];
            }

            if ((high - k + 1) >= 0 && (high - k + 1) <= arr.length - 1) {
                diff2 = arr[high] - arr[high - k + 1];
            }

            int diff = Math.min(diff1, diff2);

            minDiff = Math.min(minDiff, diff);

            low++;
            high--;
        }

        return minDiff;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int k = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            int arrItem = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            arr[i] = arrItem;
        }

        int result = maxMin(k, arr);


        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}