package LeetCode.Medium.DynamicProgramming;

import java.io.*;
import java.util.*;

class Node {
    int label;
    List<Edge> edges;

    public Node(int label) {
        this.label = label;
        this.edges = new LinkedList<>();
    }
};

class Edge {
    Node destination;
    long weight;
};

class MaxDiff {
    int visited;
    long diff;

    public MaxDiff(int visited, long diff) {
        this.visited = visited;
        this.diff = diff;
    }

    public MaxDiff() {
    }
}

public class CandidateCode {

    private final int V;
    private final List<Node> adj;

    public CandidateCode(int V)
    {
        this.V = V;
        adj = new ArrayList<>(V + 1);
        adj.add(new Node(0));
        for (int i = 1; i <= V; i++) {
            Node node = new Node(i);
            adj.add(node);
        }
    }

    private void addEdge(int source, int dest, int weight) {
        Edge edge = new Edge();
        edge.destination = new Node(dest);
        edge.weight = weight;
        adj.get(source).edges.add(edge);
    }

    private void test() {
        System.out.println(adj);
    }

    private MaxDiff maxDiff() {
        MaxDiff maxDiff = new MaxDiff(Integer.MAX_VALUE, 0);
        for (int i = 1; i <= V; i++) {
            boolean[] visited = new boolean[V + 1];
            boolean[] recStack = new boolean[V + 1];
            MaxDiff maxDiff1 = new MaxDiff(0, 0);
            if (isCyclic(i, visited, recStack, maxDiff1)) {
                if (maxDiff1.visited < maxDiff.visited) {
                    maxDiff.visited = maxDiff1.visited;
                    maxDiff.diff = maxDiff1.diff;
                } else if (maxDiff1.visited == maxDiff.visited) {
                    maxDiff.diff = Math.max(maxDiff1.diff, maxDiff.diff);
                }
            }
        }
        return maxDiff;
    }

    private boolean isCyclic(int i, boolean[] visited, boolean[] recStack, MaxDiff maxDiff1) {

        if (recStack[i]) {
            return true;
        }

        if (visited[i]) {
            return false;
        }

        visited[i] = true;
        recStack[i] = true;
        maxDiff1.visited = maxDiff1.visited + 1;
        List<Edge> children = adj.get(i).edges;

        for (Edge edge : children) {
            maxDiff1.diff = maxDiff1.diff + edge.weight;
            if (isCyclic(edge.destination.label, visited, recStack, maxDiff1)) {
                return true;
            }
            maxDiff1.diff = maxDiff1.diff - edge.weight;
        }
        recStack[i] = false;
        maxDiff1.visited = maxDiff1.visited - 1;
        return false;
    }

    public static void main(String args[] ) throws Exception {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String nm[] = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
        int N = Integer.parseInt(nm[0]);
        int M = Integer.parseInt(nm[1]);
        CandidateCode candidateCode = new CandidateCode(N);
        for (int i = 0; i < M; i++) {
            String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
            int u = Integer.parseInt(nr[0]);
            int v = Integer.parseInt(nr[1]);
            int t = Integer.parseInt(nr[2]);
            int h = Integer.parseInt(nr[3]);
            candidateCode.addEdge(u, v, h - t);
        }
        MaxDiff maxDiff = candidateCode.maxDiff();

        System.out.print(maxDiff.visited + " " + new Long(maxDiff.diff).intValue());
    }
}
