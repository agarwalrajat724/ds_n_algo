package DynammicProg.RodCutting;

public class RodCutting {

    public static void main(String[] args) {
        int n = 5, a = 1, b = 2, c =3;
        System.out.println(maxc(n, a, b, c));
        System.out.println(maxDP(n, a, b, c));
        System.out.println(maxc(25, 11, 12, 13));
        System.out.println(maxDP(25, 11, 12, 13));
    }

    private static int maxc(int n, int a, int b, int c) {
        if (n < 0) {
            return -1;
        }

        if (n == 0) {
            return 0;
        }

        int res = maxOfThree(maxc(n - a, a, b, c), maxc(n - b, a, b, c), maxc(n - c, a, b, c));

        if (res == -1) {
            return res;
        }

        return res + 1;
    }

    private static int maxDP(int n, int a, int b, int c) {
        int dp[] = new int[n + 1];
        dp[0] = 0;
        for (int i = 1; i <= n; i++) {
            dp[i] = -1;
            int a1 = -1, b1 = -1, c1 = -1;
            if ((i - a) >= 0) {
                a1 = Math.max(dp[i], dp[i - a]);
            }

            if ((i - b) >= 0) {
                b1 = Math.max(dp[i], dp[i - b]);
            }

            if ((i - c) >= 0) {
                c1 = Math.max(dp[i], dp[i - c]);
            }

            int res = maxOfThree(a1, b1, c1);
            if (res != -1) {
                dp[i] = 1 + res;
            }
        }

        return dp[n];
    }

    private static int maxOfThree(int a, int b, int c) {

        if (a >= b && a >= c) {
            return a;
        }

        if (b >= a && b >= c) {
            return b;
        }

        return c;
    }
}
