package GeekForGeeks.MustDo.Arrays.TrappingRainWater;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class GFG {


    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer stringTokenizer = null;
        try {

            int T = Integer.parseInt(bufferedReader.readLine());

            for (int i = 0; i < T; i++) {
                int n = Integer.parseInt(bufferedReader.readLine());
                int[] arr = new int[n];

                int j = 0;

                stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");

                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }

                System.out.println(GFG.trappedWater(arr));
            }
        } catch (Exception e) {

        }
    }

    private static int trappedWater(int[] arr) {
        System.out.println("sdgshdgshds");
        int n = arr.length;
        int leftMax[] = new int[n];
        int rightMax[] = new int[n];

        leftMax[0] = arr[0];
        rightMax[n - 1] = arr[n - 1];

        for (int i = 0; i < n; i++) {
            leftMax[i] = Math.max(leftMax[i - 1], arr[i]);
        }

        for (int i = n - 2; i >= 0; i--) {
            rightMax[i] = Math.max(rightMax[i + 1], arr[i]);
        }

        int total = 0;

        for (int i = 0; i < arr.length; i++) {
            total += (Math.min(leftMax[i], rightMax[i]) - arr[i]);
        }

        return total;
    }
}
