package Walmart.concurrency.CyclicBarrier;

import Walmart.concurrency.CyclicBarrier.Task;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Test {

    static Semaphore semaphore = new Semaphore(10);

    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> {
            // ...
            System.out.println("All previous tasks are completed");
        });

        Thread t1 = new Thread(new Task(cyclicBarrier), "T1");
        Thread t2 = new Thread(new Task(cyclicBarrier), "T2");
        Thread t3 = new Thread(new Task(cyclicBarrier), "T3");

        if (!cyclicBarrier.isBroken()) {
            t1.start();
            t2.start();
            t3.start();
        }
        execute();
    }

    public static void execute() throws InterruptedException {

        System.out.println("Available permit : " + semaphore.availablePermits());
        System.out.println("Number of threads waiting to acquire: " +
                semaphore.getQueueLength());

        if (semaphore.tryAcquire()) {
            semaphore.acquire();
            // ...
            semaphore.release();
        }

    }
}
