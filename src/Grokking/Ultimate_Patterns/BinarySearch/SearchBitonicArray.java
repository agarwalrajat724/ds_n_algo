package Grokking.Ultimate_Patterns.BinarySearch;

import static java.util.Arrays.binarySearch;

/**
 * Given a Bitonic array, find if a given ‘key’ is present in it. An array is considered bitonic
 * if it is monotonically increasing and then monotonically decreasing. Monotonically increasing
 * or decreasing means that for any index i in the array arr[i] != arr[i+1].
 *
 * Write a function to return the index of the ‘key’. If the ‘key’ is not present, return -1.
 *
 * Input: [1, 3, 8, 4, 3], key=4
 * Output: 3
 *
 * Input: [3, 8, 3, 1], key=8
 * Output: 1
 *
 * Input: [1, 3, 8, 12], key=12
 * Output: 3
 *
 * Input: [10, 9, 8], key=10
 * Output: 0
 *
 */
public class SearchBitonicArray {

    public static int search(int[] arr, int key) {

        int findMaxElemIndex = findMaxElem(arr);
        int result = binarySearchForKey(arr, 0, findMaxElemIndex, key);
        if (result == -1) {
            result = binarySearchForKey(arr, findMaxElemIndex + 1, arr.length - 1, key);
        }
        return result;
    }

    private static int binarySearchForKey(int[] arr, int low, int high, int key) {
        while (low <= high) {
            int mid = low + (high - low)/2;

            if (arr[mid] == key) {
                return mid;
            } else if (arr[mid] < key) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }

    private static int findMaxElem(int[] arr) {
        int low = 0, high = arr.length - 1;
        while (low < high) {
            int mid = low + (high - low)/2;

            if ((mid + 1 < arr.length) && arr[mid] > arr[mid + 1]) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }
        return low;
    }

    public static void main(String[] args) {
        System.out.println(SearchBitonicArray.search(new int[] { 1, 3, 8, 4, 3 }, 4));
        System.out.println(SearchBitonicArray.search(new int[] { 3, 8, 3, 1 }, 8));
        System.out.println(SearchBitonicArray.search(new int[] { 1, 3, 8, 12 }, 12));
        System.out.println(SearchBitonicArray.search(new int[] { 10, 9, 8 }, 10));
    }
}
