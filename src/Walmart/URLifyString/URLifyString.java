package Walmart.URLifyString;

public class URLifyString {


    private String URLify(String string) {
        int underscore_count = 0;

        char[] arr = string.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '_') {
                underscore_count++;
            }
        }
        int index = arr.length - 1;
        while (arr[index] == '_') {
            underscore_count--;
            index--;
        }

        int new_length = index + 2 * underscore_count;

        if (new_length > arr.length) {
            return null;
        }

        for (int j = index; j >= 0; j --) {
            if (arr[j] == '_') {
                arr[new_length--] = '0';
                arr[new_length--] = '2';
                arr[new_length--] = '%';
            } else {
                arr[new_length--] = arr[j];
            }
        }

        return new String(arr);
    }


    public static void main(String[] args) {
        URLifyString urLifyString = new URLifyString();
        System.out.println(urLifyString.URLify("Today_is_a_good_day________"));
    }
}
