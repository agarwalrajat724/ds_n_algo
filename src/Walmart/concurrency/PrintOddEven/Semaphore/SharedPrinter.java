package Walmart.concurrency.PrintOddEven.Semaphore;

import java.util.concurrent.Semaphore;

public class SharedPrinter {

    private Semaphore semaphoreOdd = new Semaphore(1);
    private Semaphore semaphoreEven = new Semaphore(0);

    public void printOdd(int num) {
        try {
            semaphoreOdd.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " : " + num);
        semaphoreEven.release();
    }

    public void printEven(int num) {
        try {
            semaphoreEven.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " : " + num);
        semaphoreOdd.release();
    }

    public static void main(String[] args) {
        SharedPrinter sharedPrinter = new SharedPrinter();
        Thread printEven = new Thread(new PrintEven(10, sharedPrinter), "EVEN");
        Thread printOdd = new Thread(new PrintOdd(10, sharedPrinter), "ODD");

        printEven.start();
        printOdd.start();
    }
}
