package Grokking.Ultimate_Patterns.Top_K_Elems;

import java.util.PriorityQueue;
import java.util.Queue;

public class ConnectRopes {

    public static int minimumCostToConnectRopes(int[] ropeLengths) {
        Queue<Integer> minHeap = new PriorityQueue<>();
        for (int i = 0; i < ropeLengths.length; i++) {
            minHeap.offer(ropeLengths[i]);
        }
        int res = 0;
        while (minHeap.size() > 1) {
            int val1 = minHeap.poll();
            int val2 = minHeap.poll();

            res += (val1 + val2);
            minHeap.offer(val1 + val2);
        }

        return res;
    }

    public static void main(String[] args) {
        int result = ConnectRopes.minimumCostToConnectRopes(new int[] { 1, 3, 11, 5 });
        System.out.println("Minimum cost to connect ropes: " + result);
        result = ConnectRopes.minimumCostToConnectRopes(new int[] { 1, 3, 11, 5, 2 });
        System.out.println("Minimum cost to connect ropes: " + result);
    }
}
