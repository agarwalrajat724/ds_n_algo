package GeekForGeeks.MustDo.Hashing.LargestSubArrayZeroSum;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
public class GfG {
    private static int largestSubarray(int arr[]) {

        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        int max_len = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if (arr[i] == 0 && max_len == 0) {
                max_len = 1;
            }
            if (sum == 0) {
                max_len = i + 1;
            }
            Integer prev_i = map.get(sum);
            if (null != prev_i) {
                max_len = Math.max(max_len, i - prev_i);
            } else {
                map.put(sum, i);
            }
        }
        return max_len;
    }
    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int T = Integer.parseInt(bufferedReader.readLine());
            for (int i = 0; i < T; i++) {
                int n = Integer.parseInt(bufferedReader.readLine());
                int arr[] = new int[n];
                StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine(), " ");
                int j = 0;
                while (stringTokenizer.hasMoreTokens()) {
                    arr[j++] = Integer.parseInt(stringTokenizer.nextToken());
                }
                System.out.println(GfG.largestSubarray(arr));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
