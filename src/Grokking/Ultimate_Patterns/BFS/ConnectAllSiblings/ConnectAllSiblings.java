package Grokking.Ultimate_Patterns.BFS.ConnectAllSiblings;

import java.util.LinkedList;
import java.util.Queue;

public class ConnectAllSiblings {


    public static void connect(TreeNode root) {
        if (null == root) {
            return;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        TreeNode prevNode = null;

        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                TreeNode pop = queue.poll();

                if (null != prevNode) {
                    prevNode.next = pop;
                }
                prevNode = pop;

                if (null != pop.left) {
                    queue.offer(pop.left);
                }

                if (null != pop.right) {
                    queue.offer(pop.right);
                }
            }
        }
        if (null != prevNode) {
            prevNode.next = null;
        }
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(9);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        ConnectAllSiblings.connect(root);

        // level order traversal using 'next' pointer
        TreeNode current = root;
        System.out.println("Traversal using 'next' pointer: ");
        while (current != null) {
            System.out.print(current.val + " ");
            current = current.next;
        }
    }
}
