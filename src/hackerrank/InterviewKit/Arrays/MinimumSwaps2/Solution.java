package hackerrank.InterviewKit.Arrays.MinimumSwaps2;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.stream.Stream;

public class Solution {

    // Complete the minimumSwaps function below.
    static int minimumSwaps(int[] arr) {

        int n = arr.length;

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < arr.length; i++) {
            map.put(arr[i], i);
        }

        Map<Integer, Integer> sortedMap = new HashMap<>();

        Stream<Map.Entry<Integer, Integer>> entryStream = map.entrySet().stream();

        entryStream.sorted(Map.Entry.comparingByKey()).forEachOrdered(i -> {
            sortedMap.put(i.getKey(), i.getValue());
        });


        int ans = 0;

        boolean[] visited = new boolean[n];

        for (int i = 0; i < arr.length; i++) {

            if (visited[i] || sortedMap.get(i + 1) == i) {
                continue;
            }

            int cycle = 0;
            int j = i;

            while (!visited[j]) {
                visited[j] = Boolean.TRUE;
                j = sortedMap.get(j + 1);
                cycle++;
            }

            if (cycle > 0) {
                ans += (cycle - 1);
            }
        }

        return ans;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int res = minimumSwaps(arr);

        System.out.println(res);

        bufferedWriter.write(String.valueOf(res));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
