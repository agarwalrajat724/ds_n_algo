package LeetCode.Medium.SortingSearching.KthLargestElem;


import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 *    Kth Largest Element in an Array
 *
 * Find the kth largest element in an unsorted array. Note that it is the kth largest element in
 * the sorted order, not the kth distinct element.
 *
 * Example 1:
 *
 * Input: [3,2,1,5,6,4] and k = 2
 * Output: 5
 * Example 2:
 *
 * Input: [3,2,3,1,2,4,5,5,6] and k = 4
 * Output: 4
 * Note:
 * You may assume k is always valid, 1 ≤ k ≤ array's length.
 */
public class Solution {

    public int findKthLargest(int[] nums, int k) {
        if (null == nums || nums.length < k) {
            return -1;
        }

        Queue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o2, o1);
            }
        });
        Arrays.stream(nums).forEach(value -> queue.add(value));
        int res = -1;
        while (k > 0) {
            res = queue.poll();
            k--;
        }
        return res;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = new int[]{3,2,1,5,6,4};
        System.out.println(solution.findKthLargest(nums, 2));
    }
}
