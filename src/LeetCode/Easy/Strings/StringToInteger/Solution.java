package LeetCode.Easy.Strings.StringToInteger;

import com.sun.xml.internal.ws.util.StringUtils;

public class Solution {

    public int myAtoi(String str) {

        if (null == str) {
            return 0;
        }

        str = str.trim();

        if (str.equals("")) {
            return 0;
        }

        boolean isPositive = Boolean.TRUE;

        int j = 0;
        if (str.charAt(0) == '+') {
            isPositive = Boolean.TRUE;
            j++;
        } else if (str.charAt(0) == '-') {
            isPositive = Boolean.FALSE;
            j++;
        } else if (Character.isLetter(str.charAt(0))) {
            return 0;
        }


        long result = 0l;
        for (int i = j; i < str.length(); i++) {
            if (str.charAt(i) == '.') {
                break;
            }

            if (!Character.isLetterOrDigit(str.charAt(i))) {
                return 0;
            }

            if (Character.isDigit(str.charAt(i))) {
                result = (result * 10) + (str.charAt(i) - '0');
            }
        }

        if (!isPositive) {
            result = -result;
        }

        if (result > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        } else if (result < Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }

        return (int) result;
    }

    public static void main(String[] args) {

        Solution solution = new Solution();

       // System.out.println(solution.myAtoi("42"));

        System.out.println(solution.myAtoi("   -42"));


        System.out.println(solution.myAtoi("4193 with words"));

        System.out.println(solution.myAtoi("words and 987"));

        System.out.println(solution.myAtoi("-91283472332"));

        System.out.println(solution.myAtoi("3.75757"));

        System.out.println(solution.myAtoi("+-2"));
    }
}
