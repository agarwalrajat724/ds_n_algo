package LeetCode.Medium.SortingSearching.TopKFrequentElem;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Given a non-empty array of integers, return the k most frequent elements.
 *
 * Example 1:
 *
 * Input: nums = [1,1,1,2,2,3], k = 2
 * Output: [1,2]
 * Example 2:
 *
 * Input: nums = [1], k = 1
 * Output: [1]
 * Note:
 *
 * You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
 * Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
 *
 */
public class Solution {

    static class CustomObj {
        int val;
        int freq;

        public CustomObj(int val, int freq) {
            this.val = val;
            this.freq = freq;
        }
    }

    public List<Integer> topKFrequent(int[] nums, int k) {

        if (null == nums || nums.length < k) {
            return new ArrayList<>();
        }

        Queue<CustomObj> priorityQueue = new PriorityQueue<>(new Comparator<CustomObj>() {
            @Override
            public int compare(CustomObj o1, CustomObj o2) {
                return o2.freq - o1.freq;
            }
        });

        Map<Integer, Integer> freqMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int val = nums[i];
            if (freqMap.containsKey(val)) {
                freqMap.put(val, freqMap.get(val) + 1);
            } else {
                freqMap.put(val, 1);
            }
        }
        List<CustomObj> customObjs = freqMap.entrySet().stream().map(integerIntegerEntry -> new CustomObj(integerIntegerEntry.getKey(), integerIntegerEntry.getValue())).collect(Collectors.toList());
        priorityQueue.addAll(customObjs);

        List<Integer> res = new ArrayList<>();

        for (int i = 0; i < k; i++) {
            res.add(priorityQueue.poll().val);
        }
        return res;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = new int[]{1,1,1,2,2,3};
        List<Integer> res = solution.topKFrequent(nums, 2);
        System.out.println(res);
    }
}
