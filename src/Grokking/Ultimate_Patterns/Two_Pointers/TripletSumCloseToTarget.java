package Grokking.Ultimate_Patterns.Two_Pointers;


import java.util.Arrays;

/**
 * Given an array of unsorted numbers and a target number, find a triplet in the array whose
 * sum is as close to the target number as possible, return the sum of the triplet. If there
 * are more than one such triplet, return the sum of the triplet with the smallest sum.
 *
 * Input: [-2, 0, 1, 2], target=2
 * Output: 1
 * Explanation: The triplet [-2, 1, 2] has the closest sum to the target.
 *
 * Input: [-3, -1, 1, 2], target=1
 * Output: 0
 * Explanation: The triplet [-3, 1, 2] has the closest sum to the target.
 *
 * Input: [1, 0, 1, 1], target=100
 * Output: 3
 * Explanation: The triplet [1, 1, 1] has the closest sum to the target.
 *
 */
public class TripletSumCloseToTarget {

    public static int searchTriplet(int[] arr, int targetSum) {
        int res = 0;
        int minDiff = Integer.MAX_VALUE;
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 2; i++) {
            if (i > 0 && arr[i] == arr[i - 1]) {
                continue;
            }

            int low = i + 1, high = arr.length - 1;
            while (low < high) {
                int sum = arr[i] + arr[low] + arr[high];
                if (Math.abs(targetSum - sum) < minDiff) {
                    minDiff = targetSum - sum;
                    res = sum;
                }
                if (sum == targetSum) {
                    break;
                } else if (sum > targetSum) {
                    high--;
                } else {
                    low++;
                }
            }
        }
        return res;
    }


    public static void main(String[] args) {
        System.out.println(TripletSumCloseToTarget.searchTriplet(new int[] { -2, 0, 1, 2 }, 2));
        System.out.println(TripletSumCloseToTarget.searchTriplet(new int[] { -3, -1, 1, 2 }, 1));
        System.out.println(TripletSumCloseToTarget.searchTriplet(new int[] { 1, 0, 1, 1 }, 100));
    }
}
