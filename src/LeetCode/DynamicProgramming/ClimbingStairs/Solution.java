package LeetCode.DynamicProgramming.ClimbingStairs;

public class Solution {




    public int climbStairs(int n) {

        int fib[] = new int[n + 1];

        return climbStairsUtil(n, fib);
    }

    private int climbStairsUtil(int n, int[] fib) {
        if (n == 0 || n == 1 || n == 2) {
            return n;
        }

        if (fib[n] != 0) {
            return fib[n];
        }

        fib[n] = climbStairsUtil(n - 1, fib) + climbStairsUtil(n - 2, fib);

        return fib[n];
    }


    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.climbStairs(4));
    }
}
