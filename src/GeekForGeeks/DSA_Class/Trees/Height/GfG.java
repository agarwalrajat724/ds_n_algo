package GeekForGeeks.DSA_Class.Trees.Height;

import GeekForGeeks.DSA_Class.Trees.Node;

public class GfG {

    int height(Node node)
    {
        if (null == node) {
            return 0;
        }

        return 1 + Math.max(height(node.left), height(node.right));
    }

    public static void main(String[] args) {
        GfG height = new GfG();
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);

        root.left.left = new Node(4);
        root.left.right = new Node(5);

        root.right.left = new Node(6);
        root.right.right = new Node(7);

        System.out.println(height.height(root));
    }
}
