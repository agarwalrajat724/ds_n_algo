package GeekForGeeks.DSA_Class.Stacks.GetMinAtPop;

import java.util.Stack;

public class GetMin {

    /* inserts elements of the array into
            stack and return the stackn*/
    public static Stack<Integer> _push(int arr[], int n)
    {
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> auxStack = new Stack<>();
        // your code here
        for (int i = 0; i < n; i++) {
            stack.push(arr[i]);
            if (auxStack.isEmpty() || arr[i] <= auxStack.peek()) {
                auxStack.push(arr[i]);
            } else {
                auxStack.push(auxStack.peek());
            }
        }
        return auxStack;
    }

    /* print minimum element of the stack each time
       after popping*/
    static void _getMinAtPop(Stack<Integer>s)
    {
        // your code here
        while (!s.isEmpty()) {
            System.out.print(s.pop() + " ");
        }
    }
}
