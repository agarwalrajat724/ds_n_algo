package GeekForGeeks.DSA_Class.Queues.Print_First_K_Elems;

import java.util.Queue;
import java.util.Stack;

public class GfG {

    public Queue<Integer> modifyQueue(Queue<Integer> q, int k)
    {
        if (null == q || q.isEmpty() || q.size() < k || k < 0) {
            return q;
        }

        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < k ; i++) {
            stack.push(q.poll());
        }

        while (!stack.isEmpty()) {
            q.offer(stack.pop());
        }

        for (int i = 0; i < (q.size() - k); i++) {
            q.offer(q.poll());
        }

        return q;
    }
}
