package Grokking.Ultimate_Patterns.SlidingWindow;

/**
 * Given an array of positive numbers and a positive number ‘S’, find the length of
 * the smallest subarray whose sum is greater than or equal to ‘S’. Return 0,
 * if no such subarray exists.
 *
 * Example 1:
 *
 * Input: [2, 1, 5, 2, 3, 2], S=7
 * Output: 2
 * Explanation: The smallest subarray with a sum great than or equal to '7' is [5, 2].
 * Example 2:
 *
 * Input: [2, 1, 5, 2, 8], S=7
 * Output: 1
 * Explanation: The smallest subarray with a sum greater than or equal to '7' is [8].
 * Example 3:
 *
 * Input: [3, 4, 1, 1, 6], S=8
 * Output: 3
 * Explanation: Smallest subarrays with a sum greater than or equal to '8' are [3, 4, 1]
 * or [1, 1, 6].
 *
 * Time Complexity : O(N + N) => O(N)
 * Space Complexity : O(1)
 */
public class MinSizeSubArraySum {

    public static int findMinSubArray(int S, int[] arr) {
        // TODO: Write your code here
        int windowStart = 0;
        int sum = 0;
        int minLength = Integer.MAX_VALUE;

        for (int windowEnd = 0; windowEnd < arr.length; windowEnd++) {
            sum += arr[windowEnd];

            while (sum >= S && windowStart <= windowEnd) {
                minLength = Math.min(minLength, windowEnd - windowStart);
                sum -= arr[windowStart];
                windowStart++;
            }
        }

        if (minLength != Integer.MAX_VALUE) {
            return minLength + 1;
        }

        return 0;
    }

    public static void main(String[] args) {
        int result = MinSizeSubArraySum.findMinSubArray(7, new int[] { 2, 1, 5, 2, 3, 2 });
        System.out.println("Smallest subarray length: " + result);
        result = MinSizeSubArraySum.findMinSubArray(7, new int[] { 2, 1, 5, 2, 8 });
        System.out.println("Smallest subarray length: " + result);
        result = MinSizeSubArraySum.findMinSubArray(8, new int[] { 3, 4, 1, 1, 6 });
        System.out.println("Smallest subarray length: " + result);
    }
}
