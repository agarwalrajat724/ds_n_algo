package hackerrank.InterviewKit.RecursionNBacktracking.Fibonacci;

import java.util.*;

public class Solution {


    public static int fibonacci(int n) {

        if (n == 0 || n == 1) {
            return n;
        }

        int[] fib = new int[n + 1];

        return fibonacciUtil(n, fib);
    }

    private static int fibonacciUtil(int n, int[] fib) {
        if (n == 0 || n == 1) {
            return n;
        }

        if (fib[n] != 0) {
            return fib[n];
        } else {
            fib[n] = fibonacciUtil(n - 1, fib) + fibonacciUtil(n - 2, fib);
        }

        return fib[n];
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.close();
        System.out.println(fibonacci(n));
    }
}
