package Grokking.Ultimate_Patterns.BFS;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 3
 * Given a binary tree, populate an array to represent its zigzag level order traversal.
 * You should populate the values of all nodes of the first level from left to right,
 * then right to left for the next level and keep alternating in the same manner for the following levels.
 */
public class ZigzagTraversal {

    public static List<List<Integer>> traverse(TreeNode root) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        // TODO: Write your code here

        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        stack1.push(root);

        while (!stack1.isEmpty() || !stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                int size = stack1.size();
                List<Integer> res = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    TreeNode pop = stack1.pop();
                    res.add(pop.val);
                    if (null != pop.left) {
                        stack2.push(pop.left);
                    }
                    if (null != pop.right) {
                        stack2.push(pop.right);
                    }
                }
                result.add(res);
            }

            while (!stack2.isEmpty()) {
                int size = stack2.size();
                List<Integer> res = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    TreeNode pop = stack2.pop();
                    res.add(pop.val);
                    if (null != pop.right) {
                        stack1.push(pop.right);
                    }
                    if (null != pop.left) {
                        stack1.push(pop.left);
                    }
                }
                result.add(res);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(12);
        root.left = new TreeNode(7);
        root.right = new TreeNode(1);
        root.left.left = new TreeNode(9);
        root.right.left = new TreeNode(10);
        root.right.right = new TreeNode(5);
        root.right.left.left = new TreeNode(20);
        root.right.left.right = new TreeNode(17);
        List<List<Integer>> result = ZigzagTraversal.traverse(root);
        System.out.println("Zigzag traversal: " + result);
    }
}
