package LeetCode.Design.MinStack;

import java.util.Calendar;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicReference;

public class MinStack {


    Stack<Integer> stack = null;
    Stack<Integer> auxStack = null;

    /** initialize your data structure here. */
    public MinStack() {
        stack = new Stack<>();
        auxStack = new Stack<>();
    }

    public void push(int x) {
        stack.push(x);

        if (auxStack.isEmpty() || auxStack.peek() >= x) {
            auxStack.push(x);
        }
    }

    public void pop() {

        if (stack.isEmpty()) {
            System.out.println("ERROR ...................");
        }

        int pop = stack.pop();

        if (auxStack.peek() == pop) {
            auxStack.pop();
        }
    }

    public int top() {
        if (stack.isEmpty()) {
            System.out.println("ERROR.............");
        }

        return stack.peek();
    }

    public int getMin() {
        return auxStack.peek();
    }


    public static void main(String[] args) {
        MinStack minStack = new MinStack();

        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        System.out.println(minStack.getMin());   //--> Returns -3.
        minStack.pop();
        System.out.println(minStack.top());      //--> Returns 0.
        System.out.println(minStack.getMin());   // --> Returns -2.
    }
}
