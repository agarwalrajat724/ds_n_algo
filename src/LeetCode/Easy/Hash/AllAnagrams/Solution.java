package LeetCode.Easy.Hash.AllAnagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * 438. Find All Anagrams in a String
 *
 * Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
 *
 * Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.
 *
 * The order of output does not matter.
 *
 * Example 1:
 *
 * Input:
 * s: "cbaebabacd" p: "abc"
 *
 * Output:
 * [0, 6]
 *
 * Explanation:
 * The substring with start index = 0 is "cba", which is an anagram of "abc".
 * The substring with start index = 6 is "bac", which is an anagram of "abc".
 * Example 2:
 *
 * Input:
 * s: "abab" p: "ab"
 *
 * Output:
 * [0, 1, 2]
 *
 * Explanation:
 * The substring with start index = 0 is "ab", which is an anagram of "ab".
 * The substring with start index = 1 is "ba", which is an anagram of "ab".
 * The substring with start index = 2 is "ab", which is an anagram of "ab".
 */
public class Solution {

    public List<Integer> findAnagrams(String s, String p) {

        if (null == s || s.length() == 0 || p == null || p.length() == 0) {
            return new ArrayList<>();
        }
        List<Integer> result = new ArrayList<>();
        int hashArray[] = new int[26];

        for (int i = 0; i < p.length() ; i++) {
            hashArray[p.charAt(i) - 'a']++;
        }

        for (int i = 0; i < s.length() - (p.length() - 1); i++  ) {
            if (Arrays.equals(hashArray, calculateHash(s.substring(i, i + p.length())))) {
                result.add(i);
            }
        }

        return result;
    }

    private int[] calculateHash(String str) {
        int hashArray[] = new int[26];

        for (int i = 0; i < str.length() ; i++) {
            hashArray[str.charAt(i) - 'a']++;
        }

        return hashArray;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();


        solution.findAnagrams("abab", "ab").forEach(integer -> System.out.println(integer));
    }
}
