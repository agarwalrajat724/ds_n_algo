package hackerrank.InterviewKit.warmup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Sol1 {

    private static final Scanner scanner = new Scanner(System.in);


    private void permute(String str, int l, int r, List<String> yList)
    {
        if (l == r)
            System.out.println(str);
        else
        {
            for (int i = l; i <= r; i++)
            {
                str = swap(str,l,i);
                permute(str, l+1, r, yList);
                str = swap(str,l,i);
            }
        }
    }


    public String swap(String a, int i, int j)
    {
        char temp;
        char[] charArray = a.toCharArray();
        temp = charArray[i] ;
        charArray[i] = charArray[j];
        charArray[j] = temp;
        return String.valueOf(charArray);
    }


    public static void main(String[] args) throws Exception {


        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/home/raagar/Documents/CodingInterview/src/hackerrank/FileWriter.txt"));

        int n = scanner.nextInt();

        String x = scanner.nextLine();

        String y = scanner.nextLine();


        Stack<Integer> stack = new Stack<>();

        Stack<Integer> auxStack = new Stack<>();

        for (int i = 0; i <= n; i++) {
            String s = scanner.nextLine();
            StringTokenizer stringTokenizer = new StringTokenizer(s);
            while (stringTokenizer.hasMoreElements()) {


                String op = stringTokenizer.nextToken();

                if (op.equals("push")) {
                    int data = Integer.parseInt(stringTokenizer.nextToken());

                    stack.push(data);

                    System.out.println(stack.peek());
                } else if (op.equals("pop")) {

                    if (stack.isEmpty()) {
                        System.out.println("EMPTY");
                    } else {
                        stack.pop();
                    }

                    System.out.println(stack.peek());

                } else {
                    int count = Integer.parseInt(stringTokenizer.nextToken());

                    int inc = Integer.parseInt(stringTokenizer.nextToken());

                    auxStack = new Stack<>();

                    while (!stack.isEmpty()) {
                        int size = stack.size();
                        if (count == size) {
                            int pop = stack.pop();
                            auxStack.push(pop + inc);
                            count--;
                        } else {
                            auxStack.push(stack.pop());
                        }
                    }

                    while (!auxStack.isEmpty()) {
                        stack.push(auxStack.pop());
                    }

                    System.out.println(stack.peek());
                }
            }
        }

    }
}
