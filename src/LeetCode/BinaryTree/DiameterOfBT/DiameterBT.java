package LeetCode.BinaryTree.DiameterOfBT;

public class DiameterBT {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    class Result {
        int ans;

        public Result() {
            this.ans = Integer.MIN_VALUE;
        }
    }

    public int diameterOfBinaryTree(TreeNode root) {
        if (null == root) {
            return 0;
        }

        Result result = new Result();
        diameterOfBinaryTreeUtil(root, result);

        return result.ans;
    }

    private int diameterOfBinaryTreeUtil(TreeNode root, Result result) {
        if (null == root) {
            return 0;
        }

        int l = diameterOfBinaryTreeUtil(root.left, result);
        int r = diameterOfBinaryTreeUtil(root.right, result);

        result.ans = Math.max(result.ans, 1 + l + r);

        return Math.max(l, r) + 1;
    }

    public static void main(String[] args) {
        DiameterBT diameterBT = new DiameterBT();

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);

        System.out.println(diameterBT.diameterOfBinaryTree(root));
    }
}
