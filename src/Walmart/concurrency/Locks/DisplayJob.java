package Walmart.concurrency.Locks;

public class DisplayJob implements Runnable {

    private final TestResource testResource;

    public DisplayJob(TestResource testResource) {
        this.testResource = testResource;
    }

    @Override
    public void run() {
        System.out.println("Display Job");
        testResource.displayRecord();
    }
}
