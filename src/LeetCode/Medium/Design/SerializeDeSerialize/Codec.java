package LeetCode.Medium.Design.SerializeDeSerialize;

import java.util.Stack;

public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (null == root) {
            return null;
        }
        Stack<TreeNode> stack = new Stack<>();

        stack.push(root);
        StringBuilder stringBuilder = new StringBuilder();

        while (!stack.isEmpty()) {
            TreeNode pop = stack.pop();
            if (null != pop) {
                stringBuilder.append(pop.val + ",");
                stack.push(pop.left);
                stack.push(pop.right);
            } else {
                stringBuilder.append("#,");
            }
        }

        return stringBuilder.toString().substring(0, stringBuilder.length() - 1);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (null == data) {
            return null;
        }
        return null;
    }

    public static void main(String[] args) {

    }
}
