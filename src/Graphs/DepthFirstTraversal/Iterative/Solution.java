package Graphs.DepthFirstTraversal.Iterative;

import Graphs.Graph;

import java.util.Iterator;
import java.util.Stack;

public class Solution {

    private void dfs(int n, Graph graph) {
        boolean visited[] = new boolean[n];

        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                DFSUtil(i, graph, visited);
            }
        }
    }

    private void DFSUtil(int vertex, Graph graph, boolean[] visited) {
        Stack<Integer> stack = new Stack<>();
        stack.push(vertex);

        while (!stack.isEmpty()) {

            int s = stack.peek();
            stack.pop();

            if (!visited[s]) {
                System.out.println(s + " --> ");
                visited[s] = true;
            }

            Iterator<Integer> iterator = graph.adjListArray[s].listIterator();

            while (iterator.hasNext()) {
                int toVisit = iterator.next();
                if (!visited[toVisit]) {
                    stack.push(toVisit);
                }
            }
        }
    }

    public static void main(String[] args) {

        int n = 5;

        Graph graph = new Graph(5);
        graph.addEdge(0, 2);
        graph.addEdge(0, 3);
        graph.addEdge(1, 0);
        graph.addEdge(2, 1);
        graph.addEdge(3, 4);
        graph.addEdge(4, 0);

        graph.printGraph();

        Solution solution = new Solution();
        solution.dfs(n, graph);
    }
}
