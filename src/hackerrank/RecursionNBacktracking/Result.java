package hackerrank.RecursionNBacktracking;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Result {


    /*
     * Complete the 'getMinimumTravelTime' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING weightsOfPeople as parameter.
     */

    public static int getMinimumTravelTime(String weightsOfPeople) {

        if (null == weightsOfPeople || weightsOfPeople.trim().equals("")) {
            return 0;
        }

        String[] str = weightsOfPeople.split(",");

        int[] arr = new int[str.length];

        for (int i = 0; i < str.length; i++) {
            arr[i] = Integer.parseInt(str[i]);
        }

        Arrays.sort(arr);

        if (arr.length == 1) {
            return arr[0];
        } else if (arr.length == 2) {
            return arr[1];
        }

//        if (arr.length < 3) {
//            return arr[arr.length - 1];
//        }

        int minTime = 0;

        for (int i = 1; i < arr.length - 1; i++) {
            minTime += arr[i];
            minTime += arr[0];
        }

//        minTime += ((arr.length - 2) * arr[0]);

        minTime += arr[arr.length - 1];

        return minTime;

    }


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/rajatagarwal/Learning/IdeaProjects/ds_n_algo/src/hackerrank/FileWriter.txt"));

        String weightsOfPeople = bufferedReader.readLine();

        int result = Result.getMinimumTravelTime(weightsOfPeople);

        System.out.println(result);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
