package LeetCode.Easy.BitManipulation;

public class SumTwoIntegers {

    public int getSum(int a, int b) {

        if (b == 0) {
            return a;
        }

        int sum = a ^ b;
        int carry = (a & b) << 1;

        return getSum(sum, carry);
    }


    public static void main(String[] args) {
        SumTwoIntegers sumTwoIntegers = new SumTwoIntegers();

        System.out.println(sumTwoIntegers.getSum(3, 5));
    }
}
