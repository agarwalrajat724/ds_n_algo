package LeetCode.Medium.BinaryTree;

public class TreeNode {

    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int _val) {
        this.val = _val;
    }
}
