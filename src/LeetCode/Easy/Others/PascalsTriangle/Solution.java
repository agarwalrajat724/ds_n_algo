package LeetCode.Easy.Others.PascalsTriangle;

import java.util.ArrayList;
import java.util.List;

/**
 * Pascal's Triangle
 *   Go to Discuss
 * Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.
 *
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 *
 * Example:
 *
 * Input: 5
 * Output:
 * [
 *      [1],
 *     [1,1],
 *    [1,2,1],
 *   [1,3,3,1],
 *  [1,4,6,4,1]
 * ]
 */
public class Solution {

    public List<List<Integer>> generate(int numRows) {
        if (numRows == 0) {
            return new ArrayList<>();
        }
        List<List<Integer>> res = new ArrayList<>();
        int[][] arr = new int[numRows][numRows];

        for (int line = 0; line < numRows; line++) {
            List<Integer> arrayAsList = new ArrayList<>();
            for (int i = 0; i <= line; i++) {
                if (i == 0 || i == line) {
                    arr[line][i] = 1;
                } else {
                    arr[line][i] = arr[line - 1][i] + arr[line - 1][i - 1];
                }
                arrayAsList.add(arr[line][i]);
            }
            res.add(arrayAsList);
        }
        return res;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        List<List<Integer>> res = solution.generate(5);

        System.out.println(res);
    }
}
