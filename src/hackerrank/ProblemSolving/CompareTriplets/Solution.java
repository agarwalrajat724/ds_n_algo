package hackerrank.ProblemSolving.CompareTriplets;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * https://www.hackerrank.com/challenges/compare-the-triplets/problem
 *
 *
 * Sample Input 0
 *
 * 5 6 7
 * 3 6 10
 * Sample Output 0
 *
 * 1 1
 */
public class Solution {

    // Complete the compareTriplets function below.
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {

        int aScore = 0, bScore = 0;
        List<Integer> result = new ArrayList<>();
        int i = 0, j = 0;
        while (i < a.size() && j < b.size()) {

            if (a.get(i) > b.get(j)) {
                aScore++;
            } else if (a.get(i) < b.get(j)) {
                bScore++;
            }
            i++;
            j++;
        }
        result.add(aScore);
        result.add(bScore);

        return result;
    }

    // Complete the aVeryBigSum function below.
    static long aVeryBigSum(long[] ar) {

        long res = 0l;

        for (int i = 0; i < ar.length; i++) {
            res += ar[i];
        }

        return res;
    }


//    public static int diagonalDifference(List<List<Integer>> arr) {
//        // Write your code here
//
//    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/rajatagarwal/Learning/IdeaProjects/ds_n_algo/src/hackerrank/ProblemSolving/Output.txt"));

        List<Integer> a = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> b = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> result = compareTriplets(a, b);

        bufferedWriter.write(
                result.stream()
                        .map(Object::toString)
                        .collect(joining(" "))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }

}
