package LeetCode.BinaryTree.SumRootToLeaf;

public class SumRootToLeaf {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int _val) {
            this.val = _val;
        }
    }

    class Result {
        int totalSum;

        public Result() {
            this.totalSum = 0;
        }
    }


    public int sumNumbers(TreeNode root) {
        if (null == root) {
            return 0;
        }

        int path[] = new int[1000];

        Result result = new Result();

        sumNumbersUtil(root, path, 0, result);

        return result.totalSum;
    }

    private void sumNumbersUtil(TreeNode root, int[] path, int depth, Result result) {

        if (null == root) {
            return;
        }

        path[depth] = root.val;

        if (null == root.left && null == root.right) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i <= depth; i++) {
                stringBuffer.append(path[i]);
            }
            result.totalSum += Integer.valueOf(stringBuffer.toString());
        } else {
            sumNumbersUtil(root.left, path, depth + 1, result);
            sumNumbersUtil(root.right, path, depth + 1, result);
        }
    }


    public static void main(String[] args) {

        SumRootToLeaf sumRootToLeaf = new SumRootToLeaf();

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);

        System.out.println(sumRootToLeaf.sumNumbers(root));
    }
}
